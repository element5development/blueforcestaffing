<?php
// if (!is_page('selecting-page')) {
//  if(!isset($_COOKIE["cat_selection"])) {
//      wp_redirect(get_bloginfo('url').'/selecting-page/');
//      exit();
//  }
// }
if (is_page('skills-checklist') && $_COOKIE['cat_selection'] == 'providers') {
    wp_redirect('/');
    exit();
}
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WorkScout
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta name="p:domain_verify" content="2416aa20e429f4ba7c04dcdd2f7dcb95"/>
<link rel="icon" type="image/png" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/08/favicon.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/08/favicon.png" sizes="16x16" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link href="https://cdn.jsdelivr.net/gh/noelboss/featherlight@master/src/featherlight.css" type="text/css" rel="stylesheet" />
<?php wp_head(); ?>
<!-- Hotjar Tracking Code for http://blueforcestaffing.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:231620,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<script>
function count_my_fun()
{
    var txt_val = $("#display_post_no").val();
    var adding_pag = 15;
    var totla_val = +txt_val + +adding_pag;
    $(".display_post").html(totla_val);
    $("#display_post_no").val(totla_val);
}
</script>
<!--Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1034725683306429'); // Insert your pixel ID here.
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1034725683306429&ev=ViewContent&noscript=1"
/></noscript>
<noscript><img height="1" width="1" style="display:none"
 src="https://www.facebook.com/tr?id=1034725683306429&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
</head>
<?php $layout = Kirki::get_option( 'workscout','pp_body_style','fullwidth' ); ?>
<body <?php body_class($layout); ?>>
<?php if(!isset($_COOKIE["cat_selection"]) && !is_404() && (is_page('travel-nurse-jobs') || is_front_page() || is_page('pay-benefits') || is_page('skills-checklist'))) { ?>
<header id="main-header" class="<?php $style = Kirki::get_option( 'workscout','pp_header_style', 'default' ); echo $style; ?> selectingHeader">
    <div class="container" style="padding: 0; border: 0px;">
        <div class="sixteen columns">

            <!-- Logo -->
            <div id="logo" style="position: relative;">
                 <?php

                    $logo = Kirki::get_option( 'workscout', 'pp_logo_upload', '' );
                    if($logo) {
                        if(is_front_page()){ ?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
                        <?php } else { ?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
                        <?php }
                    } else {
                        if(is_front_page()) { ?>
                        <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                        <?php } else { ?>
                        <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
                        <?php }
                    }
                    ?>
                    <?php if(get_theme_mod('workscout_tagline_switch','hide') == 'show') { ?><div id="blogdesc"><?php bloginfo( 'description' ); ?></div><?php } ?>

            </div>
        <div class="social-i">
            <div class="footer-bottom landingfooterbottom">
                <style type="text/css">.hideDesktop {display: none !important;} @media (max-width: 1024px) {.hideDesktop {display: inline-block !important;}} @media (max-width: 550px) {/*.mobileChange {position: absolute;}*/ .page #titlebar {margin-top: -190px !important;} .page .social-i .footer-bottom .social-icons li a {padding: 0px !important;} #banner {margin-top: -182px;} .footer-bottom .social-icons li {float: none; text-align: center;} .twitter i, .twitter:before, .pinterest i, .pinterest:before, .instagram i, .instagram:before, .facebook i, .facebook:before, .linkedin i, .linkedin:before {margin: 13px 0 0 -5px;} .social-icons li a i {margin-left: -5px;} .social-icons li.hideDesktop {width: 100%;} .social-icons li.hideDesktopBottom {margin-bottom: 15px; margin-top: -10px;}}</style>
                <?php /* get the slider array */
                $footericons = ot_get_option( 'pp_footericons', array() );
                if ( !empty( $footericons ) ) {
                    echo '<ul class="social-icons mobileChange">';
                    foreach( $footericons as $icon ) {
                        echo '<li><a target="_blank" class="' . $icon['icons_service'] . '" title="' . esc_attr($icon['title']) . '" href="' . esc_url($icon['icons_url']) . '"><i class="icon-' . $icon['icons_service'] . '"></i></a></li>';
                    }
                    // echo '<li class="hideDesktop"><a href="/apply-now" target="_blank" style="width: auto; border: 0px; padding: 10px; height: auto;" class="scriptTriggerApply"><img style="max-width:100px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt="" /></a></li>';
                    echo '<li class="hideDesktop hideDesktopBottom"><div style="display: inline-block; line-height: 20px; padding-top: 2px; color: #fff;" class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none" style="font-family: inherit;"></i></div></li>';
                    echo '</ul>';
                }
                ?>
            </div>
        </div>
        </div>
    </div>
</header>

<section class="selectingSection clearfix sp">
    <div id="nursingButton" class="column-4 nursing">
        <img src="/wp-content/uploads/2018/08/Nursing1.jpg" alt="nurse">
        <div></div>
        <a href="#" id="nursingButton">I’m a nursing professional</a>
    </div>
    <div id="alliedButton" class="column-4 allied">
        <img src="/wp-content/uploads/2018/08/Allied.jpg" alt="nurse">
        <div></div>
        <a href="#" id="alliedButton">I’m an allied professional</a>
    </div>
    <div id="providersButton" class="column-4 provider">   
        <img src="/wp-content/uploads/2018/08/Bitmap6.jpg" alt="nurse">
        <div></div>
        <a href="#" id="providersButton">I’m a MD/DO/NP/PA</a>
    </div>
		<div class="column-4 home-video" data-featherlight="#homevideo">   
        <img src="/wp-content/themes/workscout-child/images/video-bg.png" alt="Blueforce Staffing Video">
        <div></div>
				<button class="video-button" data-featherlight="#homevideo">How Blueforce helps you</button>
    </div>
		<div id="homevideo" class="featherlight">
			<div class="vid-contain">
				<iframe src="https://player.vimeo.com/video/362348439" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>
    <div style="clear:both;"></div>
    <div id="clientButton" class="column-12 client">
        <span>For Clients</span>
        <div class="sp"><a href="#" id="clientButton">Looking for staffing solutions?</a><img src="/wp-content/themes/workscout-child/images/arrow.png" alt=""></div>
        <div class="social-i">
            <div class="footer-bottom footerBottom2 landingfooterbottom" style="width: auto; color: #fff; position: relative; text-align: right;">
                <img src="/wp-content/uploads/2016/05/phone.png" alt="" class="ndimg" style="float: left; margin-top: 6px;display: inline-block;
                float: left;     position: absolute;
                left: 15px;
                top: 4px;border-radius:7px;" />
                <div style="display: inline-block; line-height: 20px; padding-top: 12px;padding-bottom:5px;" class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none"></i></div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .phone-no0 {
        color: #1D3465;
    }
    #clientButton {
        cursor: pointer;
    }
    header#main-header .container {
        padding: 0 40px !important;
        width: 100% !important;
    }
    .page .social-i .footer-bottom {
        padding: 0 0 0 45px;
    }
    .selectingHeader {
        position: absolute !important;
        top: 0px;
        left: 0px;
        right: 0px;
        margin: 0 auto;
    }
    .selectingHeader #logo {
        border: 0px !important;
        padding: 10px !important;
        background: transparent !important;
        margin-top: 5px;
    }
    .selectingHeader #logo img {
        position: relative;
        z-index: 1;
    }
    .selectingHeader #logo:before {
        content: '';
        background: url('/wp-content/uploads/2018/10/logo-white-bg-2x.png');
        display: block;
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        /*filter: blur(30px);*/
        top: 0;
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100%;
    }
    .selectingSection * {
        box-sizing: border-box;
    }
    .selectingSection a {
        color: #FFFFFF;
        font-family: "Lato";
        font-size: 18px;
        line-height: 17px;
        text-align: right;
    }
    .selectingSection {
        height: 100vh;
        /*margin-top: -24px;*/
        position: relative;
				display: flex;
				flex-wrap: wrap;
				align-items: center;
    }
		.vid-contain {
				position: absolute;
				width: 80vw;
				height: 0;
				padding-bottom: 50%;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
    }
		.vid-contain iframe {
				position: absolute;
				top: 50%;
				left: 0;
				width: 100%;
				height: 100%;
				transform: translateY(-50%);
		}
		.featherlight .featherlight-content {
			background: none;
		}
		.featherlight .featherlight-close-icon {
			background: none;
		}
    .selectingSection .column-4:hover:before {
        top: 25%;
        transition: 1s;
    }
    .selectingSection .column-4:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        transition: 1s;
        width: 100%;
        visibility: inherit;
        z-index: 10;
        pointer-events: none;
        height: 100%;
        background-image: linear-gradient(to top, #05132D 0%, rgba(5,19,45,0.4) 40%, rgba(5,19,45,0) 54%);
    }
    .footer-bottom .facebook {
    background-color: #4a6d9d!important;
    border-color: #4a6d9d!important;
    text-decoration: none;
    }
    .footer-bottom .linkedin {
    background-color: #0b7bb5!important;
    border-color: #0b7bb5!important;
    text-decoration: none;
    }
    .footerBottom2 {
        border:none;
        margin-top: 6px;
        padding: 5px;
        background: none;
    }
    .footerBottom2 .phone-no0 {
        color: #fff;
        padding-left: 40px;
    }
    .social-icons a,.social-icons a:hover {
    -webkit-transition: 0s!important;
    -moz-transition: 0s!important;
    -o-transition: 0s!important;
    -ms-transition: 0s!important;
    transition: 0s!important;
    }
    .footer-bottom .instagram {
    background-color: #3f6f94!important;
    border-color: #3f6f94!important;text-decoration: none;
    }
    .footer-bottom .pinterest {
    background-color: #cb1f25!important;
    border-color: #cb1f25!important;text-decoration: none;
    }
    .footer-bottom .twitter:hover, .twitter {
    background-color: #3bc1ed!important;
    border-color: #3bc1ed!important;text-decoration: none;
    }
    .selectingSection .column-4 {
        width: 25%;
        float: left;
        padding: 0;
        height: 80%;
        cursor: pointer;
        overflow: hidden;
        text-align: center;
        position: relative;
        background-size: cover !important;
        background-position-x:center!important;
        transform: scale(1);
        transition: .5s;
    }
    .selectingSection .column-4:hover img {
        transform: scale(1.1);
        transition: 1s;
    }
    .selectingSection .column-4 img {
        position: absolute;
        top: 0;
        cursor: pointer;
        left: 0;
        transition: 1s;
        height: 100%;
        width: auto;
        opacity: 0;
        max-width: inherit;
    }
    .selectingSection .column-4 > div {
        position: absolute;
        top: 0;
        bottom: 0;
        display: none;
        left: 0;
        right: 0;
        width: 100%;
        height: 80%;
        //background: rgba(0,0,0,0.5);
        z-index: 0;
    }
    .selectingSection .column-4 a,
		.selectingSection .column-4 button {
        display: inline-block;
        border: 2px solid #fff;
        padding: 14px;
        transition: .5s;
        position: absolute;
        z-index: 100;
        text-decoration: none;
				bottom: 4rem;
				left: 50%;
				transform: translatex(-50%);
				width: 82%;
				text-align: center;
    }
		.selectingSection .column-4 button {
        border: none;
				border: 2px solid #0275D8;
        background-color: #0275D8;
				font-family: "Lato";
				font-size: 18px;
				font-weight: 400;
				line-height: 17px;
				padding-left: 3em;
    }
		.selectingSection .column-4 button:hover,
		.selectingSection .column-4 button:focus {
				border: 2px solid #fff;
        background-color: #1D3465;
    }
		.selectingSection .column-4 button:before {
        content: '';
				position: absolute;
				top: 50%;
				left: 1em;
				background-image: url(../wp-content/themes/workscout-child/images/PlayButton_Icon.svg);
				background-position: center;
				background-repeat: no-repeat;
				background-size: 25px;
				z-index: 2;
				width: 25px;
				height: 25px;
				transform: translateY(-50%);
    }
    .selectingSection .column-4 a:hover {
        background: #fff;
        color: #1D3465;
    }
    .selectingSection .column-4 a:focus, .selectingSection .column-4 a:active, .selectingSection .column-4 a:visited {
        color: #fff;
    }
    body .selectingSection .column-4 a:hover {
        transition: .5s;
        color: #fff;
        background: #1D3465;
        border: 1px solid #1D3465
    }
    .landingfooterbottom {
        padding-right: 0;
    }
    body .social-i .footer-bottom.landingfooterbottom {
        padding-right: 40px!important;
        padding-left: 5px!Important;
        border-radius: 7px;
    }
    body .social-i .footer-bottom.landingfooterbottom+.footer-bottom {
        margin-right: 20px!important;
    }
    .selectingSection .column-12 {
        text-align: center;
        width: 100%;
        float: left;
        height: 20%;
    }

    .selectingSection .column-12.client {
        background: #1D3465;
        transition: .5s;
        padding: 0;
        float: none;
        position: relative;
    }
    .selectingSection .column-12.client:hover {
        background: #4271be;
        transition: .5s;
    }
    .selectingSection .column-12.client>.sp {
        float: left;
        width: calc(100% / 3);
        position: relative;
        top: calc(50% - 8.5px);
        text-align: center;
    }
    .selectingSection .column-12.client>.sp a,.selectingSection .column-12.client>.sp img {
        display: inline-block;
    }
    .social-i {
        float: left;
        width: calc(100% / 3);
        height: 100%;
    }
    #main-header .social-i {
        float: right;
        margin-top: 15px;
    }
    body #main-header .social-i .footer-bottom.landingfooterbottom{
        padding-right: 0!important;
    }
    .footerBottom2 .phone-no0 {
        position: relative;
        top: calc(50% - 48px);
    }
    .page .social-i .footer-bottom .social-icons {
        margin-bottom: 0;
    }
    .selectingSection .column-12.client span {
        width: calc(100% / 3);
        float: left;
        color: #FFFFFF;
        font-family: "Montserrat";
        font-size: 36px;
        font-weight: 600;
        line-height: 30px;
        text-align: left;
        position: relative;
        top: calc(50% - 15px);
        padding-left: 40px;
    }
    body .social-i .footer-bottom.landingfooterbottom {
        position: relative;
        top: calc(50% - 25px);
    }
    footer-bottom landingfooterbottom {
        position: re;
        top: calc(50% - 32px);
    }
    #clientButton:hover,#clientButton:focus,#clientButton:active,#clientButton:visited {
        color: #fff;
        text-decoration: none;
    }
    #clientButton img {
        margin-left: 5px;
    }
    #wrapper {
        display: none !important;
    }
    @media only screen and (max-width: 1024px) {
        .selectingSection .column-4 img {
            opacity: 1;
        }
        .selectingSection .column-12.client>.sp {
            top: auto;
            float: none;
        }
        .selectingSection .column-12.client span,body .selectingSection .column-12.client>a,#clientButton .social-i {
            width: 100%;
            float: none;
            padding-left: 40px;
            padding-right: 40px;
            text-align: center;
        }
        #clientButton .social-i {
            padding-bottom: 60px;
            margin-left: 0;
        }
        #clientButton>span {
            padding-top: 40px;
            display: block;
            top: 0;
        }
        .selectingSection .column-12.client>a,body .social-i .footer-bottom.landingfooterbottom {
            top: 0;
        }
        body .selectingSection .column-12.client .sp {
            margin-bottom: 60px!important;
            margin-top: 60px!important;
        }
        body .selectingSection .column-12.client>a {
            width: 350px;
            margin: auto;
        }
        body .social-i .footer-bottom.landingfooterbottom {
            float: none!important;
        }
    }
    @media only screen and (max-width:768px) {
         #clientButton .social-i {
            margin: auto!important;
            display: block;
            width: 300px!important;
        }
        #clientButton .social-i {
            margin-bottom: 0;
        }
        #clientButton .social-i img {
            top: -5px!important;
        }
        div#clientButton {
            width: 100%!important;
        }
        body .selectingSection .column-12.client {
            height: auto!important;
        }
        .page .social-i .footer-bottom .social-icons {
            float: right!important;
        }
        .page .social-i .footer-bottom .social-icons i {
            line-height: 15px!important;
        }
        header#main-header .container {
            padding: 0 10px!important;
        }
        #logo img {
            float: left;
        }
        body .social-i .footer-bottom.landingfooterbottom+.footer-bottom {
            padding-right: 0!important;
        }
    }
    .header-newsletter .gform_footer {
        margin-top: 0px !important;
    }
    @media only screen and (max-width:420px) {
        .selectingSection a {
            text-align: center;
        }
        #clientButton img {
            margin-bottom: 30px;
        }
    }
</style>
<?php } ?>
<div id="wrapper">
    <?php if (!is_page('selecting-page')) { ?>
        <?php if ($_COOKIE["cat_selection"] == 'nursing') { ?>
        <div class="header-newsletter">
            <div class="container">
							<div>
								<h2>Everything you need to know about travel Nursing in one complete guide</h2>
								<a href="/complete-guide-to-travel-nursing/" class="button">View Guide</a>
								<span id="header-newsletter-close" href="#"><i class="fa fa-times-circle"></i></span>
							</div>
            </div>
        </div>
        <?php } ?>  


        <header id="main-header" class="<?php if($_COOKIE["cat_selection"] == 'clients') { echo 'clients'; }; ?> <?php if($_COOKIE["cat_selection"] == 'providers') { echo 'providers'; }; ?>  <?php $style = Kirki::get_option( 'workscout','pp_header_style', 'default' ); echo $style; ?>">
            <div class="container">
                <div class="sixteen columns">

                    <!-- Logo -->
                    <div id="logo" style="position: relative;">
                        <div style="position: absolute; left: 15px; right: 0; width: 80px; height: 20px; margin: 0 auto; background: #fff; border-radius: 50%; top: 10%; z-index: -9; box-shadow: 0px 1px 90px 70px #fff;"></div>
                         <?php

                            $logo = Kirki::get_option( 'workscout', 'pp_logo_upload', '' );
                            if($logo) {
                                if(is_front_page()){ ?>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
                                <?php } else { ?>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
                                <?php }
                            } else {
                                if(is_front_page()) { ?>
                                <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                <?php } else { ?>
                                <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
                                <?php }
                            }
                            ?>
                            <?php if(get_theme_mod('workscout_tagline_switch','hide') == 'show') { ?><div id="blogdesc"><?php bloginfo( 'description' ); ?></div><?php } ?>

                    </div>
                    <div class="huffmaster0">
                        <?php if ($_COOKIE['cat_selection'] != 'clients'): ?>
                        <a class="small-dialog popup-with-zoom-anim apply_link" href="#apply-dialog2">Apply now <i class="fa fa-edit"></i></a>
                        <?php endif ?>
                    </div>
                    <style>.phone-no0 a {color: #fff !important; text-decoration: none !important; border: 0px !important; width: auto !important; height: auto !important; padding: 0px !important; margin: 0px !important;}</style>
                    <!-- Menu -->

                    <nav id="navigation" class="menu">

                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'responsive','container' => false ) ); ?>

                        <?php
                        $minicart_status = Kirki::get_option( 'workscout', 'pp_minicart_in_header', false );
                        if(Kirki::get_option( 'workscout', 'pp_login_form_status', true ) ) { ?>
                        <ul class="float-right">
                        <?php if($minicart_status == 'on') {  get_template_part( 'inc/mini_cart'); } ?>
                        <?php
                            if ( is_user_logged_in() ) { ?>
                                <?php $loginpage = ot_get_option('pp_login_page');
                                if( ! empty( $loginpage )) { $loginlink = get_permalink($loginpage); ?>
                                <li><a href="<?php echo esc_url($loginlink); ?>"><i class="fa fa-sign-out"></i> <?php esc_html_e('User Page','workscout') ?></a></li>
                                <?php } ?>
                                <li><a href="<?php echo wp_logout_url( home_url() );  ?>"><i class="fa fa-sign-out"></i> <?php esc_html_e('Log Out','workscout') ?></a></li>
                            </ul>
                            <?php
                            } else {
                                if(ot_get_option('pp_login_form','on') == 'off') {
                                    $loginpage = ot_get_option('pp_login_page');
                                    if( ! empty( $loginpage )) {
                                        $loginlink = get_permalink($loginpage);
                                    } else {
                                        $loginlink = wp_login_url( get_permalink() );
                                    } ?>
                                        <li><a href="<?php echo esc_url($loginlink); ?>#tab-register"><i class="fa fa-user"></i> <?php esc_html_e('Sign Up','workscout') ?></a></li>
                                        <li><a href="<?php echo esc_url($loginlink); ?>"><i class="fa fa-lock"></i> <?php esc_html_e('Log In','workscout') ?></a></li>
                                    <?php

                                } else { ?>
                                        <li><a href="#singup-dialog" class="small-dialog popup-with-zoom-anim"><i class="fa fa-user"></i> <?php esc_html_e('Sign Up','workscout') ?></a></li>
                                        <li><a href="#login-dialog" class="small-dialog popup-with-zoom-anim"><i class="fa fa-lock"></i> <?php esc_html_e('Log In','workscout') ?></a></li>
                                <?php } ?>


                                    </ul>

                        <?php if(ot_get_option('pp_login_form','on') == 'on') { ?>
                            <div id="singup-dialog" class="small-dialog zoom-anim-dialog mfp-hide apply-popup">
                                <div class="small-dialog-headline">
                                    <h2><?php esc_html_e('Sign Up','workscout'); ?></h2>
                                </div>
                                <div class="small-dialog-content">
                                    <?php echo do_shortcode('[register_form]'); ?>
                                </div>
                            </div>
                            <div id="login-dialog" class="small-dialog zoom-anim-dialog mfp-hide apply-popup">
                                <div class="small-dialog-headline">
                                    <h2><?php esc_html_e('Login','workscout'); ?></h2>
                                </div>
                                <div class="small-dialog-content">
                                    <?php echo do_shortcode('[login_form]');  ?>
                                </div>
                            </div>
                            <?php }
                            }
                        }?>
                    </nav>

                    <!-- Navigation -->
                    <div id="mobile-navigation">
                        <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i><?php esc_html_e('Menu','workscout'); ?></a>
                    </div>

                </div>
            </div>
            <div class="social-i container">
                <div class="footer-bottom socialSection">
                    <style type="text/css">.hideDesktop {display: none !important;}  @media (max-width: 550px) {body .selectingSection .column-12.client .sp a{    font-size: 20px; line-height:26px;} .phone-no0{padding-top: 5px!important; }body .social-i .footer-bottom.landingfooterbottom+.footer-bottom img{margin-top: 0!important;} .page #titlebar {margin-top: -190px !important;} .page .social-i .footer-bottom .social-icons li a {padding: 0px !important;} #banner {margin-top: -182px;} .footer-bottom .social-icons li {float: none; text-align: center;} .twitter i, .twitter:before, .pinterest i, .pinterest:before, .instagram i, .instagram:before, .facebook i, .facebook:before, .linkedin i, .linkedin:before {margin: 13px 0 0 -5px;} .social-icons li a i {margin-left: -5px;} .social-icons li.hideDesktop {width: 100%;} .social-icons li.hideDesktopBottom {margin-bottom: 15px; margin-top: -10px;}}</style>
                                <?php /* get the slider array */
                                $footericons = ot_get_option( 'pp_footericons', array() );
                                if ( !empty( $footericons ) ) {
                                    echo '<ul class="social-icons mobileChange">';
                                    foreach( $footericons as $icon ) {
                                        echo '<li><a target="_blank" class="' . $icon['icons_service'] . '" title="' . esc_attr($icon['title']) . '" href="' . esc_url($icon['icons_url']) . '"><i class="icon-' . $icon['icons_service'] . '"></i></a></li>';
                                    }
                                    // echo '<li class="hideDesktop"><a href="/apply-now" target="_blank" style="width: auto; border: 0px; padding: 10px; height: auto;" class="scriptTriggerApply"><img style="max-width:100px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt="" /></a></li>';
                                    echo '<li class="hideDesktop hideDesktopBottom"><div style="display: inline-block; line-height: 20px; padding-top: 2px; color: #fff;" class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none" style="font-family: inherit;"></i></div></li>';
                                    echo '</ul>';
                                }
                                ?>
                </div>
                <div class="huffmaster1">
                    <a style="color:#fff;" href="https://www.jointcommission.org/">
                    <img src="/wp-content/uploads/2016/05/huff.png" alt="" />
                    <span class="top-text0">Blueforce has earned<br>The Joint Commission's Gold<br>Seal of Approval</span>
                    <!-- <a href="/apply-now" target="_blank" class="scriptTriggerApply"><img style="max-width:160px; margin: 8px 0 0 10px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt="" /></a> -->
                    <!-- <img src="/wp-content/uploads/2016/05/phone.png" alt="" class="ndimg" />
                    <div class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none">1 (248) 588-1600 (Outside US)</i></div> -->
                    </a>
                </div>
                <div class="footer-bottom mobileHide numberSection" style="width: 160px; color: #fff; padding: 4px 0 0 0; position: relative; text-align: right;">
                    <img src="/wp-content/uploads/2016/05/phone.png" alt="" class="ndimg" style="float: left; margin-top: 12px;display: inline-block;
                    float: left;     position: absolute;
                    left: 0px;
                    top: 4px;" />
                    <div style="display: inline-block; line-height: 20px; padding-top: 16px;padding-bottom:10px;" class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none"></i></div>
                </div>
                <div class="footer-bottom menu" style="float: right; padding: 0;">
                    <?php
                   if(strpos(implode(' ',get_body_class()), 'page-template-default') !== FALSE) {
                       $allow_1 = 1;
                       $allow_2 = 1;
                       $allow_3 = 1;
                       $allow_4 = 1;
                   } else if(strpos(implode(' ',get_body_class()), 'page-template-template-home') !== FALSE) {
                       $allow_1 = 1;
                       $allow_2 = 1;
                       $allow_3 = 1;
                       $allow_4 = 1;
                   } else if(strpos(implode(' ',get_body_class()), 'page-template-template-skills') !== FALSE) {
                        $args = array(
                            'taxonomy' => 'skills_checklists',
                            'hide_empty' => false,
                            'order' => 'DESC'
                        );
                        $terms = get_terms( $args );
                        foreach($terms as $term) {
                            $pods = pods( 'skills_checklists', $term->slug ); 
                            $categories_sp = $pods->field('choose_category');            
                             if(gettype($categories_sp) == 'array') {
                                foreach($categories_sp as $item) {
                                    if(strtolower($item) == strtolower('nursing')) {
                                        $allow_1 = 1;
                                    };
                                   if(strtolower($item) == strtolower('allied')) {
                                        $allow_2 = 1;
                                    };
                                    if(strtolower($item) == strtolower('providers')) {
                                        $allow_3 = 1;
                                    };
                                    if(strtolower($item) == strtolower('clients')) {
                                        $allow_4 = 1;
                                    };
                                };
                            } else if(strtolower($categories_sp) == strtolower('nursing')) {
                                 $allow_1 = 1;
                            } else if(strtolower($categories_sp) == strtolower('allied')) {
                                 $allow_2 = 1;
                            } else if(strtolower($categories_sp) == strtolower('providers')) {
                                 $allow_3 = 1;
                            } else if(strtolower($categories_sp) == strtolower('for clients')) {
                                 $allow_4 = 1;
                            }        
                        }
                   } else {
                       $allow_1 = 1;
                       $allow_2 = 1;
                       $allow_3 = 1;
                       $allow_4 = 1;
                   }  
                   if(strpos($_SERVER['REQUEST_URI'], 'pay-benefits') !== FALSE) {
                       $allow_4 = 0;
                   }
                   ?>
                    <ul class="sp" style="margin: 8px 0 0 0;">
                        <?php if($allow_1 == 1) { ?>
                        <li class="nursingButton <?php echo($_COOKIE["cat_selection"] == 'nursing')?'act':'' ?>"><a href="#">Nursing</a></li>
                        <?php }; if($allow_2 == 1) { ?>
                        <li class="alliedButton <?php echo($_COOKIE["cat_selection"] == 'allied')?'act':'' ?>"><a href="#">Allied</a></li>
                        <?php }; if($allow_3 == 1) { ?>
                        <li class="providersButton <?php echo($_COOKIE["cat_selection"] == 'providers')?'act':'' ?>"><a href="#">Providers</a></li>
                        <?php }; if($allow_4 == 1) { ?>
                        <li class="clientButton <?php echo($_COOKIE["cat_selection"] == 'clients')?'act':'' ?>"><a href="/prospective-clients/">FOR CLIENTS</a></li>
                        <?php }; ?>
                    </ul>
                </div>
            </div>
            <style type="text/css">
                .menu ul.sp>li>a {
                    border: none;
                    border-radius: 0px;
                    background: rgba(255,255,255,.2);
                    border: 1px solid rgba(255,255,255,.5);
                    padding-left: 15px;
                    padding-right: 15px;
                }
                .menu ul.sp>li:last-child>a {
                    border-top-right-radius: 7px;
                    border-bottom-right-radius: 7px;
                }
                .menu ul.sp>li:first-child>a {
                    border-top-left-radius: 7px;
                    border-bottom-left-radius: 7px;
                }
                @media (max-width: 1024px) {.mobileHide {display: none !important;}}
            </style>
            <div class="headerCatButtons container">
<!--                 <ul class="sp cleafix" style="margin: 8px 0 0 0;">
                    <?php if($allow_1 == 1) { ?><li class="nursingButton <?php echo($_COOKIE["cat_selection"] == 'nursing')?'act':'' ?>"><a href="#">Nursing</a></li><?php }; if($allow_2 == 1) { ?><li class="alliedButton <?php echo($_COOKIE["cat_selection"] == 'allied')?'act':'' ?>"><a href="#">Allied</a></li><?php }; if($allow_3 == 1) { ?><li class="providersButton <?php echo($_COOKIE["cat_selection"] == 'providers')?'act':'' ?>"><a href="#">Providers</a></li><?php }; if($allow_4 == 1) { ?><li class="clientButton <?php echo($_COOKIE["cat_selection"] == 'clients')?'act':'' ?>"><a href="/prospective-clients/">FOR CLIENTS</a></li><?php }; ?>
                </ul> -->
                <ul class="notinew cleafix" style="margin: 8px 0 0 0;">
                    <li class="nursingButton <?php echo($_COOKIE["cat_selection"] == 'nursing')?'act':'' ?>"><a href="#" style="text-decoration: :none;">Nursing</a></li><li class="alliedButton <?php echo($_COOKIE["cat_selection"] == 'allied')?'act':'' ?>"><a href="#" style="text-decoration: :none;">Allied</a></li><li class="providersButton <?php echo($_COOKIE["cat_selection"] == 'providers')?'act':'' ?>"><a href="#" style="text-decoration: :none;">Providers</a></li><li class="clientButton <?php echo($_COOKIE["cat_selection"] == 'clients')?'act':'' ?>"><a href="/prospective-clients/" style="text-decoration: none;">FOR CLIENTS</a></li>
                </ul>
            </div>
        </header>
        <div class="clearfix"></div>
        <?php if(isset($_GET['success']) && $_GET['success'] == 1 )  { ?>
             <script type="text/javascript">
                jQuery(document).ready(function ($) {

                        $.magnificPopup.open({
                          items: {
                            src: '<div id="singup-dialog" class="small-dialog zoom-anim-dialog apply-popup">'+
                                    '<div class="small-dialog-headline"><h2><?php esc_html_e("Success!","workscout"); ?></h2></div>'+
                                    '<div class="small-dialog-content"><p class="margin-reset"><?php esc_html_e("You have successfully registered and logged in. Thank you!","workscout"); ?></p></div>'+
                                '</div>', // can be a HTML string, jQuery object, or CSS selector
                            type: 'inline'
                          }
                        });
                    });
            </script>
        <?php } ?>
        <?php if (is_page('travel-nurse-jobs')): ?>
            <style type="text/css">
                #search_keywords, #search_gross {
                    height: 57px;
                }
            </style>
        <?php endif ?>
        <script>

            

						jQuery('#header-newsletter-close').on('click', function(){
                // alert();
                jQuery('.header-newsletter').remove();
            });

            jQuery('.huffmaster0 a:eq(1)').on('click',function(e) {
              // e.preventDefault();
              ga('send', 'event', 'Lead', 'Click', 'Apply Now Lead', 1);
              fbq('track', 'Lead',{content_name: 'Apply Now'});
              fbq('track', 'ViewContent',{button:'Apply Now'});
              // window.open('/apply-now/', '_blank');
            });
            
            jQuery('#mobile-navigation .menu-trigger').on('click',function() {
               function s(){
                    // jQuery('#jPanelMenu-menu').append('<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://chk.tbe.taleo.net/chk05/ats/careers/v2/applyRequisition?org=HUFFMASTERS&amp;cws=45&amp;rid=741" target="_blank" class="scriptTriggerApply"><img style="max-width:160px; margin: 2px 0 0 10px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt=""></a></li>');
                    jQuery('#jPanelMenu-menu').append('<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="/apply-now/"><img style="max-width:160px; margin: 2px 0 0 10px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt=""></a></li>');
               }
                setTimeout(s,500);
            });
        </script>
    <?php } ?>
    <?php if ($_COOKIE['cat_selection'] == 'clients'): ?>
        
<style type="text/css">
    .hideClient {
        display: none !important;
    }
</style>
    <?php endif ?>

<?php if (is_404() && !isset($_COOKIE['cat_selection'])): ?>
<?php 
$url404 = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                    global $wpdb;
                    $getResults = $wpdb->get_row( "SELECT * FROM wp_expired_jobs WHERE url = '".$url404."'" );
 ?>
    <?php if ($getResults): ?>
        <?php 
        $catt = json_decode($getResults->category);
        $spee = json_decode($getResults->specialty);
        // die;
        $nurrr = array('registered-nurse-rn','licensed-practical-vocational-nurse-lpn-lvn','certified-nursing-assistant-cna');
        $provv = array('provider');
        $alli = array('allied-health-professional','service-health-professional');
        if (in_array($catt[0]->slug, $nurrr)) { ?>

    <script type="text/javascript">
        jQuery(document).ready(function($){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "nursing", { expires: 7, path: '/' });
            $.cookie("specialty_cookie", "<?php echo $spee[0]->term_id ?>", { expires: 7, path: '/' });
            location.reload();
        });
    </script>
            <?php
        }
        if (in_array($catt[0]->slug, $provv)) { ?>

    <script type="text/javascript">
        jQuery(document).ready(function($){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "providers", { expires: 7, path: '/' });
            $.cookie("specialty_cookie", "<?php echo $spee[0]->term_id ?>", { expires: 7, path: '/' });
            location.reload();
        });
    </script>
            <?php
        }
        if (in_array($catt[0]->slug, $alli)) { ?>

    <script type="text/javascript">
        jQuery(document).ready(function($){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "allied", { expires: 7, path: '/' });
            $.cookie("specialty_cookie", "<?php echo $spee[0]->term_id ?>", { expires: 7, path: '/' });
            location.reload();
        });
    </script>
            <?php
        }

         ?>
    <?php endif ?>
<?php endif ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        if ($('body').width() < 800) {
            // $('select[name="search_categories[]"]').prepend('<option value="" selected="selected">Specialty</option>');
        }
        // setTimeout(function(){
        //     $('#search_categories').prepend('<option value="" selected="selected">Specialty</option>');
        // }, 2000);
        
        $('.job_types.checkboxes').css('opacity', 0);
        function remove_nur() {
          if($('#job_type_allied').attr('checked') || $('#job_type_providers').attr('checked')) {
              $('#job_type_nursing').prop('checked', false);
          }  
          if($('#job_type_allied').attr('checked') || $('#job_type_nursing').attr('checked')) {
              $('#job_type_providers').prop('checked', false);
          }
          if($('#job_type_providers').attr('checked') || $('#job_type_nursing').attr('checked')) {
              $('#job_type_allied').prop('checked', false);
          }
           $('.job_types.checkboxes').css('opacity', 1); 
        };
        if(location.pathname == '/travel-nurse-jobs/') {
            setTimeout(remove_nur,500);
        }
        
        $(document).on("click", "#nursingButton, .nursingButton a", function(){
            $.ajax({
                type: "POST",
                url: window.wp_data.ajax_url,
                data: {
                    action : 'ajax_request',
                    post_name : 'nursing'
                },
                success: function(data) {
                    console.log(data);
                }
            });
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "nursing", { expires: 7, path: '/' });
            if($(this).parent().parent().hasClass('sp')) {
               // window.location.href = "<?php echo get_bloginfo('url'); ?>";
               location.reload();
            } else if($(this).parent().parent().hasClass('notinew')) { window.location.href = "<?php echo get_bloginfo('url'); ?>"; 
            } else window.open("<?php echo get_bloginfo('url'); ?>"); 
            return false;
        });
        $(document).on("click", "#alliedButton, .alliedButton a", function(){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "allied", { expires: 7, path: '/' });
            if($(this).parent().parent().hasClass('sp')) {
               // window.location.href = "<?php echo get_bloginfo('url'); ?>";
               location.reload();
            } else if($(this).parent().parent().hasClass('notinew')) { window.location.href = "<?php echo get_bloginfo('url'); ?>"; 
            } else window.open("<?php echo get_bloginfo('url'); ?>"); 
            return false;
        });
        $(document).on("click", "#providersButton, .providersButton a", function(){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "providers", { expires: 7, path: '/' });
            if($(this).parent().parent().hasClass('sp')) {
               // window.location.href = "<?php echo get_bloginfo('url'); ?>";
               location.reload();
            } else if($(this).parent().parent().hasClass('notinew')) { window.location.href = "<?php echo get_bloginfo('url'); ?>"; 
            } else window.open("<?php echo get_bloginfo('url'); ?>"); 
            return false;
        });
        $(document).on("click", "#clientButton, .clientButton a", function(){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "clients", { expires: 7, path: '/' });
            if($(this).parent().parent().hasClass('sp')) {
               // window.location.href = "<?php echo get_bloginfo('url'); ?>";
               location.reload();
            } else if($(this).parent().parent().hasClass('notinew')) { window.location.href = "<?php echo get_bloginfo('url'); ?>"; 
            } else window.open("<?php echo get_bloginfo('url'); ?>"); 
            return false;
        });
        $(document).on("click", "#clientButton2", function(){
            $.removeCookie("cat_selection");
            $.cookie("cat_selection", "clients", { expires: 7, path: '/' });
            window.open("<?php echo get_bloginfo('url'); ?>"); 
            return false;
        });
        $(document).on("change", 'select[name="search_categories[]"]', function(){
            $.removeCookie("specialty_cookie");
            $.cookie("specialty_cookie", $('select[name="search_categories[]"]').val(), { expires: 7, path: '/' });
            // return false;
        });
        // $("#job_type_nursing").click(function(){
        //  $.removeCookie("cat_selection");
        //  $.cookie("cat_selection", "nursing", { expires: 7, path: '/' });
        //  $('#selectionTitle').html('Nursing');
        // });
        // $("#job_type_allied").click(function(){
        //  $.removeCookie("cat_selection");
        //  $.cookie("cat_selection", "allied", { expires: 7, path: '/' });
        //  $('#selectionTitle').html('Allied');
        // });
        // $("#job_type_providers").click(function(){
        //  $.removeCookie("cat_selection");
        //  $.cookie("cat_selection", "providers", { expires: 7, path: '/' });
        //  $('#selectionTitle').html('Providers');
        // });
        // $('.filter_bullhorn_categories_wid input[type="checkbox"]').click(function(){
        //  $(this).parent().parent().find('input[type="checkbox"]:checked').click();
        // });
        $('body').on('click', 'img[alt="BlueForce"]', function(e) {
           e.preventDefault(); 
           $.removeCookie('cat_selection', { path: '/' });
           location.href = '/';
        });
        if ($.cookie("cat_selection") == "nursing") {
            $('.providersSection').remove();
            $('.clientSection').remove();
            $('.alliedSection').remove();
            // setTimeout(function(){
            //  $("#job_type_nursing").click();
            // }, 500);
        }
        if ($.cookie("cat_selection") == "providers") {
            $('.nursingSection').remove();
            $('.clientSection').remove();
            $('.alliedSection').remove();
            // setTimeout(function(){
            //  $("#job_type_providers").click();
            // }, 500);
        }
        if ($.cookie("cat_selection") == "allied") {
            $('.providersSection').remove();
            $('.clientSection').remove();
            $('.nursingSection').remove();
            // setTimeout(function(){
            //  $("#job_type_allied").click();
            // }, 500);
        }
        if ($.cookie("cat_selection") == "client") {
            $('.nursingSection').remove();
            $('.providersSection').remove();
            $('.alliedSection').remove();
            // setTimeout(function(){
            //  $("#job_type_h").click();
            // }, 500);
        }
        if($('body').height() < $(window).height()) {
            var h = $(window).height() - $('body').height();
            var h_bl = $('.selectingSection .column-12').height();
            $('.selectingSection .column-12').css('height', h+h_bl + 'px');
        }
        $(window).scroll(function() {
            if($('body').height() < $(window).height()) {
                var h = $(window).height() - $('body').height();
                var h_bl = $('.selectingSection .column-12').height();
                $('.selectingSection .column-12').css('height', h+h_bl + 'px');
            }
        });
        $('body').on('click', 'div[data-callMe="true"]', function(e) {
           location.href = 'tel:+18667952583';
        });
        
        
            function si() {
                var len = $('.selectingSection .column-4 img').length;
                for(var i = 0; i < len; i++) {
                    var zn = ($('.selectingSection .column-4 img:eq('+i+')').width() - $('.selectingSection .column-4:eq('+i+')').width()) / 2;
                    if(zn > 0) {
                        $('.selectingSection .column-4 img:eq('+i+')').css('left', -zn + 'px');
                    } else {
                        var zn = Math.abs(zn);
                        var h = $('.selectingSection .column-4 img:eq('+i+')').height();
                        var w = $('.selectingSection .column-4 img:eq('+i+')').width();
                        var w_need = w + (zn * 2);
                        var h_need = h * w_need / w;
                        $('.selectingSection .column-4 img:eq('+i+')').css({
                            width: w_need + 'px',
                            height: h_need + 'px'
                        });
                    }
                }
            }
            setTimeout(si,500);
            function sI() {
                $('.selectingSection .column-4 img').css('opacity',1);
            }
            setTimeout(sI,1000);

        $(window).resize(function() {
            var len = $('.selectingSection .column-4 img').length;
            for(var i = 0; i < len; i++) {
                var zn = ($('.selectingSection .column-4 img:eq('+i+')').width() - $('.selectingSection .column-4:eq('+i+')').width()) / 2;
                if(zn > 0) {
                    $('.selectingSection .column-4 img:eq('+i+')').css('left', -zn + 'px');
                } else {
                    $('.selectingSection .column-4 img:eq('+i+')').css('left', '0px');
                    var zn = Math.abs(zn);
                    var h = $('.selectingSection .column-4 img:eq('+i+')').height();
                    var w = $('.selectingSection .column-4 img:eq('+i+')').width();
                    var w_need = w + (zn * 2);
                    var h_need = h * w_need / w;
                    $('.selectingSection .column-4 img:eq('+i+')').css({
                        width: w_need + 'px',
                        height: h_need + 'px'
                    });
                }
            }
            function sI() {
                $('.selectingSection .column-4 img').css('opacity',1);
            }
            setTimeout(sI,1000);
        });
        
        
    });
</script>
<style type="text/css">
    
    #main-header #logo > div {
        display: none;
    }
    #main-header #logo img {
        z-index: 1;
        position: relative;
    }
    #main-header #logo:before {
        content: '';
        background: url('/wp-content/uploads/2018/10/logo-white-bg-2x.png');
        display: block;
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        /*filter: blur(30px);*/
        top: 0;
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100%;
    }
    #wrapper {
        /*margin-top: -22px;*/
    }
</style>

<?php if (is_page('skills-checklist')): ?>
    <style type="text/css">
        #titlebar .container .sixteen.columns {
            margin: 0;
        }
    </style>
<?php endif ?>
<?php if (strpos($_SERVER['HTTP_REFERER'], get_bloginfo('url')) === false): ?>
    <?php if (isset($_SERVER['HTTP_REFERER'])): 
            $parse = parse_url($_SERVER['HTTP_REFERER']);
            $sourceHost = $parse['host'];
            if (isset($_GET['li_source'])) {
                $sourceHost = $_GET['li_source'];
            } ?>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                $.cookie("main_source", "<?php echo $sourceHost; ?>", { expires: 7, path: '/' });
            });
        </script>
    <?php endif ?>
<?php endif ?>