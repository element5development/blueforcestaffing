<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WorkScout
 */

?>
    <?php if (!is_page('selecting-page')) { ?>
        <!-- Footer
        ================================================== -->
        <div class="margin-top-45"></div>

        <div id="footer">
            <!-- Main -->
            <div class="container">
                <div class="columns">
                    <aside id="text-2" class="footer-widget widget_text">
                        <div class="textwidget">
                            <img src="/wp-content/uploads/2016/05/logo.png">
                        </div>
                                                <div class="seals"> <!--ADDED 10/31 REQUTED ZENDESK TRICKET #38588 & #38774 -->
                                                    <img src="https://blueforcestaffing.com/wp-content/uploads/2018/12/NAPR_2019.png" alt="Blueforce is an offical NAPR Member"/>
                                                    <img src="https://blueforcestaffing.com/wp-content/uploads/2018/10/NALTO-Member-logo.png" alt="Blueforce is an offical NALTO Member"/>
                                                    <img src="https://blueforcestaffing.com/wp-content/uploads/2018/11/gold-seal.png" alt="Blueforce has earned The joint Commission's Gold Seal of Approval"/>
                                                </div>
                    </aside>
                </div>
                <div class="columns" style="float: right;">
                    <nav id="navigation" class="menu">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_id' => 'responsive','container' => false ) ); ?>
                    </nav>
                </div>
                    <?php
                    // $footer_layout = Kirki::get_option( 'workscout', 'pp_footer_widgets' );
                    // $footer_layout_array = explode(',', $footer_layout);
                    // $x = 0;
                    // foreach ($footer_layout_array as $value) {
                        // $x++;
                         ?>
                         <!-- <div class="<?php echo esc_attr(workscout_number_to_width($value)); ?> columns"> -->
                            <?php //if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer'.$x)) : endif; ?>
                        <!-- </div> -->
                    <?php //} ?>
            </div>
            <!-- 2017 Notice -->
            <!--<div class="container">
                <div>
                    <p class="under-footer" style="margin-bottom: 0;font-size: 12px;">Huffmaster is scheduled to undergo a review by The Joint Commission on Wednesday, December 20, 2017 to assess compliance with standards of the Healthcare Staffing Services Certification program. Anyone with concerns about operations of the company or the quality of clinical staff placed in assignments by Huffmaster is encouraged to contact The Joint Commission before the scheduled review. For more information: <a href="https://facilities.huffmaster.com/on-site-review/">click here</a>.</p>
                </div>
            </div>-->

            <!-- Bottom -->
            <div class="container">
                <div class="footer-bottom" style="width:100%;">
                    <div class="">
                        <div class="copyrights"><?php $copyrights = Kirki::get_option( 'workscout', 'pp_copyrights' );
                        if (function_exists('icl_register_string')) {
                            icl_register_string('Copyrights in footer','copyfooter', $copyrights);
                            // echo icl_t('Copyrights in footer','copyfooter', $copyrights);
                        } else {
                            // echo wp_kses($copyrights,array('br' => array(),'em' => array(),'strong' => array(),));
                        } ?>
                            Copyright &copy; <?php echo date('Y'); ?> - Design by <a href="http://www.element5digital.com" target="_blank" style="color: #000;">Element5 Digital</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Back To Top Button -->
        <div id="backtotop"><a href="#"></a></div>
    <?php } ?>
</div>
<!-- Wrapper / End -->


<?php if ( is_page_template( 'template-contact.php' ) ) { ?>
<script type="text/javascript">
(function($){
    $(document).ready(function(){
        $('#googlemaps').gMap({
            maptype: '<?php echo ot_get_option('pp_contact_maptype','ROADMAP') ?>',
            scrollwheel: false,
            zoom: <?php echo ot_get_option('pp_contact_zoom',13) ?>,
            markers: [
                <?php $markers = ot_get_option('pp_contact_map');
                if(!empty($markers)) {
                    $allowed_tags = wp_kses_allowed_html( 'post' );
                    foreach ($markers as $marker) {
                        $str = str_replace(array("\n", "\r"), '', $marker['content']);?>
                    {
                        address: '<?php echo esc_js($marker['address']); ?>', // Your Adress Here
                        html: '<strong style="font-size: 14px;"><?php echo esc_js($marker['title']); ?></strong></br><?php echo wp_kses($str,$allowed_tags); ?>',
                        popup: true,
                    },
                    <?php }
                } ?>
                    ],
                });
    });
})(this.jQuery);
</script>
<?php } //eof is_page_template ?>
<?php //if ( get_post_type( get_the_ID() ) != 'job_listing' ) { ?>
    <div id="apply-dialog" class="small-dialog zoom-anim-dialog mfp-hide apply-popup" style="margin-top: 0px !important;">
        <div class="small-dialog-headline">
            <h2><?php esc_html_e('Apply Now','workscout') ?></h2>
        </div>
        <div class="small-dialog-content">
            <?php
            echo do_shortcode('[gravityform id="11" title="false" description="false" ajax="true"]');
                /**
                 * job_manager_application_details_email or job_manager_application_details_url hook
                 */
                // do_action( 'job_manager_application_details_' . $apply->type, $apply );
            ?>
        </div>
    </div>
    <div id="apply-dialog2" class="small-dialog zoom-anim-dialog mfp-hide apply-popup" style="margin-top: 0px !important;">
        <div class="small-dialog-headline">
            <h2><?php esc_html_e('Apply Now','workscout') ?></h2>
        </div>
        <div class="small-dialog-content">
            <?php
            echo do_shortcode('[gravityform id="13" title="false" description="false" ajax="true"]');
                /**
                 * job_manager_application_details_email or job_manager_application_details_url hook
                 */
                // do_action( 'job_manager_application_details_' . $apply->type, $apply );
            ?>
        </div>
    </div>
<?php //} ?>
<style type="text/css">
    #gform_wrapper_11,
    #gform_wrapper_13 {
        display: block !important;
    }
    #field_11_27,
    #field_11_26,
    #field_11_15,
    #field_11_2,
    #field_11_16,
    #field_11_17,
    #field_11_18,
    #field_11_19,
    #field_11_20,
    #field_11_21,
    #field_11_24,
    #field_11_23,
    #field_11_8,
    #field_11_22,
    #field_11_4,
    #field_11_9,
    #field_11_10,
    #gform_wrapper_11 .gform_footer
    {
        display: none;
    }
    #field_13_27,
    #field_13_2,
    #field_13_16,
    #field_13_17,
    #field_13_18,
    #field_13_19,
    #field_13_20,
    #field_13_21,
    #field_13_24,
    #field_13_23,
    #field_13_8,
    #field_13_22,
    #field_13_4,
    #field_13_9,
    #field_13_10,
    #field_13_26,
    #field_13_15,
    #gform_wrapper_13 .gform_footer
    {
        display: none;
    }
    #field_14_27,
    #field_14_2,
    #field_14_16,
    #field_14_17,
    #field_14_18,
    #field_14_19,
    #field_14_20,
    #field_14_21,
    #field_14_24,
    #field_14_23,
    #field_14_8,
    #field_14_22,
    #field_14_4,
    #field_14_9,
    #field_14_10,
    #field_14_26,
    #field_14_15,
    #gform_wrapper_14 .gform_footer
    {
        display: none;
    }
    div.gform_validation_error #field_11_27,
    div.gform_validation_error #field_11_26,
    div.gform_validation_error #field_11_15,
    div.gform_validation_error #field_11_2,
    div.gform_validation_error #field_11_16,
    div.gform_validation_error #field_11_27,
    div.gform_validation_error #field_11_17,
    div.gform_validation_error #field_11_18,
    div.gform_validation_error #field_11_19,
    div.gform_validation_error #field_11_20,
    div.gform_validation_error #field_11_21,
    div.gform_validation_error #field_11_24,
    div.gform_validation_error #field_11_23,
    div.gform_validation_error #field_11_8,
    div.gform_validation_error #field_11_22,
    div.gform_validation_error #field_11_4,
    div.gform_validation_error #field_11_9,
    div.gform_validation_error #field_11_10,
    div.gform_validation_error#gform_wrapper_11 .gform_footer
    {
        display: block;
    }
    div.gform_validation_error #partialSubmit, div.gform_validation_error #TextSubmit {
        display: none;
    }
    div.gform_validation_error #field_13_27,
    div.gform_validation_error #field_13_2,
    div.gform_validation_error #field_13_16,
    div.gform_validation_error #field_13_27,
    div.gform_validation_error #field_13_17,
    div.gform_validation_error #field_13_18,
    div.gform_validation_error #field_13_19,
    div.gform_validation_error #field_13_20,
    div.gform_validation_error #field_13_21,
    div.gform_validation_error #field_13_24,
    div.gform_validation_error #field_13_23,
    div.gform_validation_error #field_13_8,
    div.gform_validation_error #field_13_22,
    div.gform_validation_error #field_13_4,
    div.gform_validation_error #field_13_9,
    div.gform_validation_error #field_13_10,
    div.gform_validation_error #field_13_26,
    div.gform_validation_error #field_13_15,
    div.gform_validation_error#gform_wrapper_13 .gform_footer
    {
        display: block;
    }
    div.gform_validation_error #partialSubmit2, div.gform_validation_error #TextSubmit2 {
        display: none;
    }
    div.gform_validation_error #field_14_27,
    div.gform_validation_error #field_14_2,
    div.gform_validation_error #field_14_16,
    div.gform_validation_error #field_14_27,
    div.gform_validation_error #field_14_17,
    div.gform_validation_error #field_14_18,
    div.gform_validation_error #field_14_19,
    div.gform_validation_error #field_14_20,
    div.gform_validation_error #field_14_21,
    div.gform_validation_error #field_14_24,
    div.gform_validation_error #field_14_23,
    div.gform_validation_error #field_14_8,
    div.gform_validation_error #field_14_22,
    div.gform_validation_error #field_14_4,
    div.gform_validation_error #field_14_9,
    div.gform_validation_error #field_14_10,
    div.gform_validation_error #field_14_26,
    /*div.gform_validation_error #field_14_15,*/
    div.gform_validation_error#gform_wrapper_14 .gform_footer
    {
        display: block;
    }
    div.gform_validation_error #partialSubmit3, div.gform_validation_error #TextSubmit3 {
        display: none;
    }
    .mfp-bg {
        z-index: 10001;
    }
</style>
<?php if (is_home()): ?>
    <script type="text/javascript">
    jQuery(document).ready(function($){
        $('#postSorting').change(function(){
            // alert($(this).val());
            var linkk = $('#postSorting option[value="'+$(this).val()+'"]').attr('data-linkk');
            window.location = linkk;
        });
    });
    </script>
<?php endif ?>
<?php if (is_page('travel-nurse-jobs')): ?>
    <style type="text/css">#selectionTitle + span {display: none;}</style>
    <script type="text/javascript">
        setTimeout(function(){
            jQuery('#selectionTitle + span').show();
        }, 3000);
    </script>
<?php endif ?>
    <style type="text/css">.chosen-results li.group-result {cursor: pointer;}</style>
    <script type="text/javascript">
        // jQuery(document).on('click', '.chosen-results li', function() {
        //     if($(this).html() == 'West') {
        //         window.location = '<?php echo get_bloginfo("url"); ?>/travel-nurse-jobs-opportunities/west/';
        //     }
        //     if($(this).html() == 'Midwest') {
        //         window.location = '<?php echo get_bloginfo("url"); ?>/travel-nurse-jobs-opportunities/midwest/';
        //     }
        //     if($(this).html() == 'Southwest') {
        //         window.location = '<?php echo get_bloginfo("url"); ?>/travel-nurse-jobs-opportunities/southwest/';
        //     }
        //     if($(this).html() == 'Northeast') {
        //         window.location = '<?php echo get_bloginfo("url"); ?>/travel-nurse-jobs-opportunities/northeast/';
        //     }
        //     if($(this).html() == 'Southeast') {
        //         window.location = '<?php echo get_bloginfo("url"); ?>/travel-nurse-jobs-opportunities/southeast/';
        //     }
        // });
    </script>
    <script type="text/javascript">

    jQuery(document).ready(function($){
        
        $(window).scroll(function() {
            //for fixed form
            if($('#gform_wrapper_6').length > 0 && $(window).width() > 1024) {

                if($('#gform_wrapper_6 form').offset().top < $(window).scrollTop()) {
                    $('#gform_wrapper_6 form').css({
                      position: 'fixed',
                      top: '0px',
                      'max-width':'370px'
                    });                      
                }
                
                if($('#gform_wrapper_6 form').offset().top - $('#gform_wrapper_6').offset().top > 50 && $('#gform_wrapper_6 form').css('position') == 'fixed') {
                    $('#gform_wrapper_6 form').css({
                      position: 'fixed',
                      top: '0px',
                      'max-width':'370px'
                    });  
                } else {
                    $('#gform_wrapper_6 form').removeAttr('style');
                }
            };
        });
        
        
        //mobile menu
        $('body').on('click', '#jPanelMenu-menu a', function(e) {
            if($(this).parent().hasClass('menu-item-has-children')) {
                e.preventDefault();
                $(this).parent().find('ul').css('display','block').find('li').css('float','none');
            }
        });

        $(document).on('click', '#partialSubmit', function(){
            if (
                $('#input_11_1_3').val() && 
                $('#input_11_1_4').val() && 
                $('#input_11_1_6').val() && 
                $('#input_11_3').val()
            ) {
            var data = new FormData();
            data.append("email", $('#input_11_3').val());  
            data.append("action", "candidate_check_form");  
            var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
            $('#partialSubmit').after('<p id="TextSubmit">Submitting...</p>');
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                // url: "",
                data: data,
                contentType: false,
                processData: false,
                success: function(response){
                    if (response.trim() == 'notfound') {
                        $('#field_11_26, #field_11_27, #field_11_2, #field_11_16, #field_11_17, #field_11_18, #field_11_19, #field_11_21, #field_11_24, #field_11_23, #field_11_8, #field_11_22, #field_11_4, #field_11_9, #field_11_10, #gform_11 .gform_footer').css('display', 'block');
                        $('#partialSubmit, #TextSubmit').remove();
                    } else {
                        $('#input_11_2').val('(111) 111-1111');
                        $('#input_11_21 option[value="Alabama"]').attr('selected', 'selected');
                        $('#input_11_21').val("Alabama");
                        $('#input_11_17, #input_11_19,#input_11_24').val('(111) 111-1111');
                        jQuery('#gform_11').submit();
                    }
                }
            });

                return false;
            } else {
                alert('Please fill out all fields.');
                return false;
            }
        });
        
        $(document).on('click', '#partialSubmit2', function(){
            if (
                $('#input_13_1_3').val() && 
                $('#input_13_1_6').val() && 
                $('#input_13_3').val()
            ) {
            var data = new FormData();
            data.append("email", $('#input_13_3').val());  
            data.append("action", "candidate_check_form");  
            var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
            $('#partialSubmit2').after('<p id="TextSubmit2">Submitting...</p>');
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                // url: "",
                data: data,
                contentType: false,
                processData: false,
                success: function(response){
                    if (response.trim() == 'notfound') {
                        $('#field_13_27, #field_13_2, #field_13_16, #field_13_27, #field_13_17, #field_13_18, #field_13_19, #field_13_21, #field_13_24, #field_13_23, #field_13_8, #field_13_22, #field_13_4, #field_13_9, #field_13_10, #field_13_26, #gform_13 .gform_footer').css('display', 'block');
                        $('#partialSubmit2, #TextSubmit2').remove();
                    } else {
                        $('#input_13_2').val('(111) 111-1111');
                        $('#input_13_21 option[value="Alabama"]').attr('selected', 'selected');
                        $('#input_13_21').val("Alabama");
                        $('#input_13_17, #input_13_19,#input_13_24').val('(111) 111-1111');
                        jQuery('#gform_13').submit();
                    }
                }
            });

                return false;
            } else {
                alert('Please fill out all fields.');
                return false;
            }
        });
        
        $(document).on('click', '#partialSubmit3', function(){
            if (
                $('#input_14_1_3').val() && 
                $('#input_14_1_4').val() && 
                $('#input_14_1_6').val() && 
                $('#input_14_3').val()
            ) {
            var data = new FormData();
            data.append("email", $('#input_14_3').val());  
            data.append("action", "candidate_check_form");  
            var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
            $('#partialSubmit3').after('<p id="TextSubmit3">Submitting...</p>');
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                // url: "",
                data: data,
                contentType: false,
                processData: false,
                success: function(response){
                    if (response.trim() == 'notfound') {
                        $('#field_14_27, #field_14_2, #field_14_16, #field_14_27, #field_14_17, #field_14_18, #field_14_19, #field_14_21, #field_14_24, #field_14_23, #field_14_8, #field_14_22, #field_14_4, #field_14_9, #field_14_10, #field_14_26, #gform_14 .gform_footer').css('display', 'block');
                        $('#partialSubmit3, #TextSubmit3').remove();
                    } else {
                        $('#input_14_2').val('(111) 111-1111');
                        $('#input_14_21 option[value="Alabama"]').attr('selected', 'selected');
                        $('#input_14_21').val("Alabama");
                        $('#input_14_17, #input_14_19,#input_14_24').val('(111) 111-1111');
                        jQuery('#gform_14').submit();
                    }
                }
            });

                return false;
            } else {
                alert('Please fill out all fields.');
                return false;
            }
        });


        // $('.tooltip').tooltipster();
        <?php if($_COOKIE['cat_selection'] != 'clients') { ?>
      setTimeout(function(){
        var wh = $('#wrapper').height();
        // var hh = $('#page > header').height();
        // var fh = $('#page > footer').height();
        var bh = $('html').height()-100;
        var cp = $('#footer').height();
        var fp = parseInt(bh) - parseInt(wh);
        var pp = parseInt(fp) + parseInt(cp);
        // var fpt = parseInt(bh) - parseInt(fp);
        // var fptr = parseInt(fpt) - parseInt(fh) - 68;
        // alert(pp);
        if (pp > 1) {
            $('#footer').css('margin-top', pp+'px');
        }
      }, 1000);
        <?php } ?>
    });

        jQuery(document).ready(function ($) {
            $('#banner.workscout-search-banner').append('<a href="javascript:;" class="scrollBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/scroll.png"></a>');

            setTimeout(function(){
                $('.scrollBtn').click(function(){
                    $("html, body").animate({ scrollTop: $('.location-section').offset().top }, 1000);
                });
            }, 1000);

            setTimeout(function(){
                $('#gform_widget-3 #input_5_3_3').attr('tabindex', '8');
                $('#gform_widget-3 #input_5_2').attr('tabindex', '9');
            }, 1500);

            setTimeout(function(){
                $('.search-job-location > *').css('visibility', 'visible');
                $('.search-job-location').addClass('displaynoneafter');
            }, 1500);

            var loaderTimer = setInterval(function(){
                if ($('ul.job_listings.job-list.full > li').length > 2) {
                    $('.container.page-container.home-page-container .five.columns.omega > *').css('opacity', '1');
                    $('.container.page-container.home-page-container .eleven.columns.alpha > *').css('opacity', '1');
                    $('.container.page-container.home-page-container .eleven.columns.alpha').addClass('displaynonebefore');
                    $('.container.page-container.home-page-container .five.columns.omega').addClass('displaynonebefore');
                    clearInterval(loaderTimer)
                }
            }, 1000)
        });
    </script>

    <script type="text/javascript">
        jQuery(function($){
            setTimeout(function(){
                updateSpecialtyDropdown();
            }, 2000);

            $('input[name="filter_bullhorn_categories[]"]').click(function(){
                updateSpecialtyDropdown();
            });

            function updateSpecialtyDropdown() {
                $('#search_categories option').hide();

                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-nursing="1"]').show();
                }
                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-provider="1"]').show();
                }
                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-allied="1"]').show();
                }

                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-nursing="1"]').show();
                    $('#search_categories option[data-provider="1"]').show();
                }
                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-nursing="1"]').show();
                    $('#search_categories option[data-allied="1"]').show();
                }

                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-provider="1"]').show();
                    $('#search_categories option[data-nursing="1"]').show();
                }
                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-provider="1"]').show();
                    $('#search_categories option[data-allied="1"]').show();
                }

                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-allied="1"]').show();
                    $('#search_categories option[data-nursing="1"]').show();
                }
                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-allied="1"]').show();
                    $('#search_categories option[data-provider="1"]').show();
                }

                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-allied="1"]').show();
                    $('#search_categories option[data-provider="1"]').show();
                    $('#search_categories option[data-nursing="1"]').show();
                }

                if($('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_allied:checked').length == 0 && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_providers:checked').length == 0 && $('.job_types.checkboxes.filter_bullhorn_categories_wid #job_type_nursing:checked').length == 0) {
                    $('#search_categories option').hide();
                    $('#search_categories option[data-<?php echo ($_COOKIE['cat_selection'] == 'providers') ? 'provider' : $_COOKIE['cat_selection']; ?>="1"]').show();
                }

                $('#search_categories').trigger("chosen:updated");
            }

            $(document).on('div#search_categories_chosen', 'click', function(){
                $('.chosen-results').scrollTop(0);
            });

            if ($('body').width() < 800) {
	            var last_valid_selection = null;
	            $('#search_categories, select[name="search_categories[]"]').change(function(event) {
	                if ($(this).val().length > 5) {
	                    alert('You can only select upto 5 specialties.')
	                    $(this).val(last_valid_selection);
	                } else {
	                    last_valid_selection = $(this).val();
	                }
	            });
                $('select[name="search_categories[]"]').prepend('<option value="" selected="selected">Specialty</option>');
                if ($('select[name="search_categories[]"]').val().length > 1) {
                    $('select[name="search_categories[]"] option[value=""]').remove();
                }
            }

        });
    </script>

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<script type="text/javascript">
    
        jQuery(document).ready(function($){
            $("#search_categories").chosen().change(function(){
                $.removeCookie("specialty_cookie");
                $.cookie("specialty_cookie", $('select[name="search_categories[]"]').val(), { expires: 7, path: '/' });
            });
        });
</script>
<script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="https://cdn.jsdelivr.net/gh/noelboss/featherlight@master/src/featherlight.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
<?php //echo 'Source: '.$_SERVER['HTTP_REFERER']; ?>