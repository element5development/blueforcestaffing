<?php
/**
 * Template Name: eBook
 */

get_header('ebook'); ?>

<section id="overview" class="guide-overview">
	<h2><?php the_field('section_title'); ?></h2>
	<div>
		<?php if( have_rows('parts') ): ?>
			<?php while ( have_rows('parts') ) : the_row(); ?>

				<article class="part-preview">
					<?php $icon = get_sub_field('part_icon'); ?>
					<img src="<?php echo $icon['sizes']['thumbnail']; ?>" alt="<?php echo $icon['alt']; ?>" />
					<div>
						<h6>Part <?php the_sub_field('part_number'); ?></h6>
						<h4><?php the_sub_field('part_name'); ?></h4>
						<?php $anchor = get_sub_field('part_name'); ?>
						<a class="button" href="#<?php echo (str_replace(' ', '-', strtolower($anchor))); ?>">Read This</a>
					</div>
				</article>
				
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>
<section class="narrow">
	<?php the_content(); ?>
</section>

<?php if( have_rows('parts') ): ?>
	<?php while ( have_rows('parts') ) : the_row(); ?>
	<div class="part">

		<?php $background = get_sub_field('part_header_background'); ?>
		<?php $anchor = get_sub_field('part_name'); ?>
		<section id="<?php echo (str_replace(' ', '-', strtolower($anchor))); ?>" class="part-header" style="background-image: url('<?php echo $background['url']; ?>');">
			<div>
				<h6>Complete Guide to Travel Nursing - Part <?php the_sub_field('part_number'); ?></h6>
				<h4><?php the_sub_field('part_name'); ?></h4>
			</div>
		</section>
		<section class="narrow">
			<?php the_sub_field('part_intro'); ?>
		</section>
		<section class="anchor-links narrow">
			<?php if( have_rows('part_content') ):  ?>
			<div>
				<?php while ( have_rows('part_content') ) : the_row(); ?>
					<?php if( get_row_layout() == 'blog_post' ): ?>
						<?php $postlink = get_sub_field('post_link'); ?>
						<a href="#<?php echo (str_replace(' ', '-', strtolower($postlink['title']))); ?>"><?php echo $postlink['title'] ?></a>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</section>

		<?php if( have_rows('part_content') ):  ?>
			<?php while ( have_rows('part_content') ) : the_row(); ?>
				<?php if( get_row_layout() == 'editor' ): ?>
				<section class="narrow">
					<div>
						<?php the_sub_field('content'); ?>
					</div>
				</section>
				<?php elseif( get_row_layout() == 'blog_post' ): ?>
					<?php $postlink = get_sub_field('post_link'); ?>
				<section class="post-preview narrow">
					<div id="<?php echo (str_replace(' ', '-', strtolower($postlink['title']))); ?>">
						<a href="<?php echo $postlink['url'] ?>"><?php echo $postlink['title'] ?></a>
						<?php the_sub_field('post_excerpt'); ?>
					</div>
				</section>
				<?php elseif( get_row_layout() == 'two_image' ): ?>
				<section class="two-images wide">
					<div>
						<?php $image1 = get_sub_field('image_1'); ?>
						<?php $image2 = get_sub_field('image_2'); ?>
						<img src="<?php echo $image1['sizes']['large']; ?>" alt="<?php echo $image1['alt']; ?>" />
						<img src="<?php echo $image2['sizes']['large']; ?>" alt="<?php echo $image2['alt']; ?>" />
					</div>
				</section>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>

		<?php $imageright = get_sub_field('right_image'); ?>
		<?php $imageleft = get_sub_field('left_image'); ?>
		<img src="<?php echo $imageright['sizes']['large']; ?>" alt="<?php echo $imageright['alt']; ?>" />
		<img src="<?php echo $imageleft['sizes']['large']; ?>" alt="<?php echo $imageleft['alt']; ?>" />
		<a class="back-to-top" href="#overview">Back to Top</a>
	</div>
	<?php endwhile; ?>
<?php endif; ?>

<div class="sticky-download">
	<h3>Want a copy of the Complete Guide to Travel Nursing?</h3>
	<a class="button" href="#gform_wrapper_15">Download Now</a>
</div>
<section class="conclusion">
	<div>
		<h2><?php the_field('conclusion'); ?></h2>
		<?php the_field('conclusion_content'); ?>
		<?php $formid = get_field('conclusion_form_id'); ?>
		<?php echo do_shortcode( '[gravityform id="'. $formid .'" title="true" description="false"]' ); ?>
	</div>
	<div class="conclusion-bg"></div>
	<div class="conclusion-apply">
		<h3>Apply now to become a travel nurse with BlueForce</h3>
		<a class="button is-white popup-with-zoom-anim apply_link" href="#apply-dialog2">Apply Now</a>
	</div>
</section>

<?php get_footer('ebook'); ?>