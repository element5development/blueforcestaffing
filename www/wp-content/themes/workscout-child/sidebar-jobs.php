<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WorkScout
 */

if (isset($_GET['search_location_region'])) {
  $region = $_GET['search_location_region'];
}

 if(strpos($_SERVER['REQUEST_URI'],'travel-nurse-jobs-opportunities')!==false && !isset($_GET['search_location_region']))
 {

   $array_region = explode('travel-nurse-jobs-opportunities/',$_SERVER['REQUEST_URI']);
   $region = rtrim($array_region[1],'/');
  /* $region_term = get_term_by('slug',$region,'job_listing_region');
   $_GET['search_location_region'] =$region_term->name;*/
  $_REQUEST['search_location_region'] =  $_GET['search_location_region'] = $region;
   //print_r($array_region);

}

 if(strpos($_SERVER['REQUEST_URI'],'travel-nurse-jobs')!==false && !isset($_GET['search_categories']))
 {

   $array_cat = explode('travel-nurse-jobs/',$_SERVER['REQUEST_URI']);
   $category = rtrim($array_cat[1],'/');
  $job_cat = get_term_by('slug',$category,'job_listing_category');
   $_GET['search_categories'] =$job_cat->term_id;



}

?>
<div class="five columns sidebar jobs-listing-content-align-right"  role="complementary">
<form class="list-search showJustMobile" method="GET" action="<?php bloginfo('url'); ?>/travel-nurse-jobs/">
        <div class="search_keywords">
          <button><i class="fa fa-search"></i></button>
          <input type="text" name="search_keywords" id="search_keywords" placeholder="job title, keywords or company name" value="">
          <div class="clearfix"></div>
        </div>
      </form>
<!-- Widgets -->
<style type="text/css">
.showJustMobile {
  display: none; 
}
@media (max-width: 767px) {
.list-search .search_keywords {
  display: none;
}
.list-search.showJustMobile,
.list-search.showJustMobile .search_keywords {
  display: block;
}
}
@media (max-width: 400px) {
  #titlebar.single {
    height: auto;
  }
}
.daterangepicker .ranges {
  float: none !important;
}
.daterangepicker.dropdown-menu {
  /*width: 250px;*/
}

.dropdown-menu {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  font-size: 14px;
  text-align: left;
  list-style: none;
  background-color: #fff;
  -webkit-background-clip: padding-box;
          background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, .15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
          box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
}
.dropdown-menu.pull-right {
  right: 0;
  left: auto;
}
.dropdown-menu .divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.dropdown-menu > li > a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: normal;
  line-height: 1.42857143;
  color: #333;
  white-space: nowrap;
}
.dropdown-menu > li > a:hover,
.dropdown-menu > li > a:focus {
  color: #262626;
  text-decoration: none;
  background-color: #f5f5f5;
}
.dropdown-menu > .active > a,
.dropdown-menu > .active > a:hover,
.dropdown-menu > .active > a:focus {
  color: #fff;
  text-decoration: none;
  background-color: #337ab7;
  outline: 0;
}
.dropdown-menu > .disabled > a,
.dropdown-menu > .disabled > a:hover,
.dropdown-menu > .disabled > a:focus {
  color: #777;
}
.dropdown-menu > .disabled > a:hover,
.dropdown-menu > .disabled > a:focus {
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.open > .dropdown-menu {
  display: block;
}
.open > a {
  outline: 0;
}
.dropdown-menu-right {
  right: 0;
  left: auto;
}
.dropdown-menu-left {
  right: auto;
  left: 0;
}
.dropdown-header {
  display: block;
  padding: 3px 20px;
  font-size: 12px;
  line-height: 1.42857143;
  color: #777;
  white-space: nowrap;
}
.dropdown-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 990;
}
.pull-right > .dropdown-menu {
  right: 0;
  left: auto;
}
.dropup .caret,
.navbar-fixed-bottom .dropdown .caret {
  content: "";
  border-top: 0;
  border-bottom: 4px dashed;
  border-bottom: 4px solid \9;
}
.dropup .dropdown-menu,
.navbar-fixed-bottom .dropdown .dropdown-menu {
  top: auto;
  bottom: 100%;
  margin-bottom: 2px;
}
@media (min-width: 768px) {
  .navbar-right .dropdown-menu {
    right: 0;
    left: auto;
  }
  .navbar-right .dropdown-menu-left {
    right: auto;
    left: 0;
  }
}
.btn-group,
.btn-group-vertical {
  position: relative;
  display: inline-block;
  vertical-align: middle;
}
.btn-group > .btn,
.btn-group-vertical > .btn {
  position: relative;
  float: left;
}
.btn-group > .btn:hover,
.btn-group-vertical > .btn:hover,
.btn-group > .btn:focus,
.btn-group-vertical > .btn:focus,
.btn-group > .btn:active,
.btn-group-vertical > .btn:active,
.btn-group > .btn.active,
.btn-group-vertical > .btn.active {
  z-index: 2;
}
.btn-group .btn + .btn,
.btn-group .btn + .btn-group,
.btn-group .btn-group + .btn,
.btn-group .btn-group + .btn-group {
  margin-left: -1px;
}
.btn-toolbar {
  margin-left: -5px;
}
.btn-toolbar .btn,
.btn-toolbar .btn-group,
.btn-toolbar .input-group {
  float: left;
}
.btn-toolbar > .btn,
.btn-toolbar > .btn-group,
.btn-toolbar > .input-group {
  margin-left: 5px;
}
.btn-group > .btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
  border-radius: 0;
}
.btn-group > .btn:first-child {
  margin-left: 0;
}
.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.btn-group > .btn:last-child:not(:first-child),
.btn-group > .dropdown-toggle:not(:first-child) {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.btn-group > .btn-group {
  float: left;
}
.btn-group > .btn-group:not(:first-child):not(:last-child) > .btn {
  border-radius: 0;
}
.btn-group > .btn-group:first-child:not(:last-child) > .btn:last-child,
.btn-group > .btn-group:first-child:not(:last-child) > .dropdown-toggle {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.btn-group > .btn-group:last-child:not(:first-child) > .btn:first-child {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.btn-group .dropdown-toggle:active,
.btn-group.open .dropdown-toggle {
  outline: 0;
}
.btn-group > .btn + .dropdown-toggle {
  padding-right: 8px;
  padding-left: 8px;
}
.btn-group > .btn-lg + .dropdown-toggle {
  padding-right: 12px;
  padding-left: 12px;
}
.btn-group.open .dropdown-toggle {
  -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
          box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
}
.btn-group.open .dropdown-toggle.btn-link {
  -webkit-box-shadow: none;
          box-shadow: none;
}
.btn .caret {
  margin-left: 0;
}
.btn-lg .caret {
  border-width: 5px 5px 0;
  border-bottom-width: 0;
}
.dropup .btn-lg .caret {
  border-width: 0 5px 5px;
}
.btn-group-vertical > .btn,
.btn-group-vertical > .btn-group,
.btn-group-vertical > .btn-group > .btn {
  display: block;
  float: none;
  width: 100%;
  max-width: 100%;
}
.btn-group-vertical > .btn-group > .btn {
  float: none;
}
.btn-group-vertical > .btn + .btn,
.btn-group-vertical > .btn + .btn-group,
.btn-group-vertical > .btn-group + .btn,
.btn-group-vertical > .btn-group + .btn-group {
  margin-top: -1px;
  margin-left: 0;
}
.btn-group-vertical > .btn:not(:first-child):not(:last-child) {
  border-radius: 0;
}
.btn-group-vertical > .btn:first-child:not(:last-child) {
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.btn-group-vertical > .btn:last-child:not(:first-child) {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
}
.btn-group-vertical > .btn-group:not(:first-child):not(:last-child) > .btn {
  border-radius: 0;
}
.btn-group-vertical > .btn-group:first-child:not(:last-child) > .btn:last-child,
.btn-group-vertical > .btn-group:first-child:not(:last-child) > .dropdown-toggle {
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.btn-group-vertical > .btn-group:last-child:not(:first-child) > .btn:first-child {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.btn-group-justified {
  display: table;
  width: 100%;
  table-layout: fixed;
  border-collapse: separate;
}
.btn-group-justified > .btn,
.btn-group-justified > .btn-group {
  display: table-cell;
  float: none;
  width: 1%;
}
.btn-group-justified > .btn-group .btn {
  width: 100%;
}
.btn-group-justified > .btn-group .dropdown-menu {
  left: auto;
}
[data-toggle="buttons"] > .btn input[type="radio"],
[data-toggle="buttons"] > .btn-group > .btn input[type="radio"],
[data-toggle="buttons"] > .btn input[type="checkbox"],
[data-toggle="buttons"] > .btn-group > .btn input[type="checkbox"] {
  position: absolute;
  clip: rect(0, 0, 0, 0);
  pointer-events: none;
}
.input-group {
  position: relative;
  display: table;
  border-collapse: separate;
}
.input-group[class*="col-"] {
  float: none;
  padding-right: 0;
  padding-left: 0;
}
.input-group .form-control {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
}
.input-group .form-control:focus {
  z-index: 3;
}
.input-group-lg > .form-control,
.input-group-lg > .input-group-addon,
.input-group-lg > .input-group-btn > .btn {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
select.input-group-lg > .form-control,
select.input-group-lg > .input-group-addon,
select.input-group-lg > .input-group-btn > .btn {
  height: 46px;
  line-height: 46px;
}
textarea.input-group-lg > .form-control,
textarea.input-group-lg > .input-group-addon,
textarea.input-group-lg > .input-group-btn > .btn,
select[multiple].input-group-lg > .form-control,
select[multiple].input-group-lg > .input-group-addon,
select[multiple].input-group-lg > .input-group-btn > .btn {
  height: auto;
}
.input-group-sm > .form-control,
.input-group-sm > .input-group-addon,
.input-group-sm > .input-group-btn > .btn {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
select.input-group-sm > .form-control,
select.input-group-sm > .input-group-addon,
select.input-group-sm > .input-group-btn > .btn {
  height: 30px;
  line-height: 30px;
}
textarea.input-group-sm > .form-control,
textarea.input-group-sm > .input-group-addon,
textarea.input-group-sm > .input-group-btn > .btn,
select[multiple].input-group-sm > .form-control,
select[multiple].input-group-sm > .input-group-addon,
select[multiple].input-group-sm > .input-group-btn > .btn {
  height: auto;
}
.input-group-addon,
.input-group-btn,
.input-group .form-control {
  display: table-cell;
}
.input-group-addon:not(:first-child):not(:last-child),
.input-group-btn:not(:first-child):not(:last-child),
.input-group .form-control:not(:first-child):not(:last-child) {
  border-radius: 0;
}
.input-group-addon,
.input-group-btn {
  width: 1%;
  white-space: nowrap;
  vertical-align: middle;
}
.input-group-addon {
  padding: 6px 12px;
  font-size: 14px;
  font-weight: normal;
  line-height: 1;
  color: #555;
  text-align: center;
  background-color: #eee;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.input-group-addon.input-sm {
  padding: 5px 10px;
  font-size: 12px;
  border-radius: 3px;
}
.input-group-addon.input-lg {
  padding: 10px 16px;
  font-size: 18px;
  border-radius: 6px;
}
.input-group-addon input[type="radio"],
.input-group-addon input[type="checkbox"] {
  margin-top: 0;
}
.input-group .form-control:first-child,
.input-group-addon:first-child,
.input-group-btn:first-child > .btn,
.input-group-btn:first-child > .btn-group > .btn,
.input-group-btn:first-child > .dropdown-toggle,
.input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle),
.input-group-btn:last-child > .btn-group:not(:last-child) > .btn {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.input-group-addon:first-child {
  border-right: 0;
}
.input-group .form-control:last-child,
.input-group-addon:last-child,
.input-group-btn:last-child > .btn,
.input-group-btn:last-child > .btn-group > .btn,
.input-group-btn:last-child > .dropdown-toggle,
.input-group-btn:first-child > .btn:not(:first-child),
.input-group-btn:first-child > .btn-group:not(:first-child) > .btn {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.input-group-addon:last-child {
  border-left: 0;
}
.input-group-btn {
  position: relative;
  font-size: 0;
  white-space: nowrap;
}
.input-group-btn > .btn {
  position: relative;
}
.input-group-btn > .btn + .btn {
  margin-left: -1px;
}
.input-group-btn > .btn:hover,
.input-group-btn > .btn:focus,
.input-group-btn > .btn:active {
  z-index: 2;
}
.input-group-btn:first-child > .btn,
.input-group-btn:first-child > .btn-group {
  margin-right: -1px;
}
.input-group-btn:last-child > .btn,
.input-group-btn:last-child > .btn-group {
  z-index: 2;
  margin-left: -1px;
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}
.nav > li {
  position: relative;
  display: block;
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 10px 15px;
}
.nav > li > a:hover,
.nav > li > a:focus {
  text-decoration: none;
  background-color: #eee;
}
.nav > li.disabled > a {
  color: #777;
}
.nav > li.disabled > a:hover,
.nav > li.disabled > a:focus {
  color: #777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {
  background-color: #eee;
  border-color: #337ab7;
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.nav > li > a > img {
  max-width: none;
}
.nav-tabs {
  border-bottom: 1px solid #ddd;
}
.nav-tabs > li {
  float: left;
  margin-bottom: -1px;
}
.nav-tabs > li > a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 1px solid transparent;
  border-radius: 4px 4px 0 0;
}
.nav-tabs > li > a:hover {
  border-color: #eee #eee #ddd;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:hover,
.nav-tabs > li.active > a:focus {
  color: #555;
  cursor: default;
  background-color: #fff;
  border: 1px solid #ddd;
  border-bottom-color: transparent;
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0;
}
.nav-tabs.nav-justified > li {
  float: none;
}
.nav-tabs.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-tabs.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-tabs.nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs.nav-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .nav-tabs.nav-justified > .active > a,
  .nav-tabs.nav-justified > .active > a:hover,
  .nav-tabs.nav-justified > .active > a:focus {
    border-bottom-color: #fff;
  }
}
.nav-pills > li {
  float: left;
}
.nav-pills > li > a {
  border-radius: 4px;
}
.nav-pills > li + li {
  margin-left: 2px;
}
.nav-pills > li.active > a,
.nav-pills > li.active > a:hover,
.nav-pills > li.active > a:focus {
  color: #fff;
  background-color: #337ab7;
}
.nav-stacked > li {
  float: none;
}
.nav-stacked > li + li {
  margin-top: 2px;
  margin-left: 0;
}
.nav-justified {
  width: 100%;
}
.nav-justified > li {
  float: none;
}
.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs-justified {
  border-bottom: 0;
}
.nav-tabs-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs-justified > .active > a,
.nav-tabs-justified > .active > a:hover,
.nav-tabs-justified > .active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .nav-tabs-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .nav-tabs-justified > .active > a,
  .nav-tabs-justified > .active > a:hover,
  .nav-tabs-justified > .active > a:focus {
    border-bottom-color: #fff;
  }
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}
.nav-tabs .dropdown-menu {
  margin-top: -1px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 20px;
  border: 1px solid transparent;
}
@media (min-width: 768px) {
  .navbar {
    border-radius: 4px;
  }
}
@media (min-width: 768px) {
  .navbar-header {
    float: left;
  }
}
.navbar-collapse {
  padding-right: 15px;
  padding-left: 15px;
  overflow-x: visible;
  -webkit-overflow-scrolling: touch;
  border-top: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
}
.navbar-collapse.in {
  overflow-y: auto;
}
@media (min-width: 768px) {
  .navbar-collapse {
    width: auto;
    border-top: 0;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  .navbar-collapse.collapse {
    display: block !important;
    height: auto !important;
    padding-bottom: 0;
    overflow: visible !important;
  }
  .navbar-collapse.in {
    overflow-y: visible;
  }
  .navbar-fixed-top .navbar-collapse,
  .navbar-static-top .navbar-collapse,
  .navbar-fixed-bottom .navbar-collapse {
    padding-right: 0;
    padding-left: 0;
  }
}
.navbar-fixed-top .navbar-collapse,
.navbar-fixed-bottom .navbar-collapse {
  max-height: 340px;
}
@media (max-device-width: 480px) and (orientation: landscape) {
  .navbar-fixed-top .navbar-collapse,
  .navbar-fixed-bottom .navbar-collapse {
    max-height: 200px;
  }
}
.container > .navbar-header,
.container-fluid > .navbar-header,
.container > .navbar-collapse,
.container-fluid > .navbar-collapse {
  margin-right: -15px;
  margin-left: -15px;
}
@media (min-width: 768px) {
  .container > .navbar-header,
  .container-fluid > .navbar-header,
  .container > .navbar-collapse,
  .container-fluid > .navbar-collapse {
    margin-right: 0;
    margin-left: 0;
  }
}
.navbar-static-top {
  z-index: 1000;
  border-width: 0 0 1px;
}
@media (min-width: 768px) {
  .navbar-static-top {
    border-radius: 0;
  }
}
.navbar-fixed-top,
.navbar-fixed-bottom {
  position: fixed;
  right: 0;
  left: 0;
  z-index: 1030;
}
@media (min-width: 768px) {
  .navbar-fixed-top,
  .navbar-fixed-bottom {
    border-radius: 0;
  }
}
.navbar-fixed-top {
  top: 0;
  border-width: 0 0 1px;
}
.navbar-fixed-bottom {
  bottom: 0;
  margin-bottom: 0;
  border-width: 1px 0 0;
}
.navbar-brand {
  float: left;
  height: 50px;
  padding: 15px 15px;
  font-size: 18px;
  line-height: 20px;
}
.navbar-brand:hover,
.navbar-brand:focus {
  text-decoration: none;
}
.navbar-brand > img {
  display: block;
}
@media (min-width: 768px) {
  .navbar > .container .navbar-brand,
  .navbar > .container-fluid .navbar-brand {
    margin-left: -15px;
  }
}
.navbar-toggle {
  position: relative;
  float: right;
  padding: 9px 10px;
  margin-top: 8px;
  margin-right: 15px;
  margin-bottom: 8px;
  background-color: transparent;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.navbar-toggle:focus {
  outline: 0;
}
.navbar-toggle .icon-bar {
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px;
}
.navbar-toggle .icon-bar + .icon-bar {
  margin-top: 4px;
}
@media (min-width: 768px) {
  .navbar-toggle {
    display: none;
  }
}
.navbar-nav {
  margin: 7.5px -15px;
}
.navbar-nav > li > a {
  padding-top: 10px;
  padding-bottom: 10px;
  line-height: 20px;
}
@media (max-width: 767px) {
  .navbar-nav .open .dropdown-menu {
    position: static;
    float: none;
    width: auto;
    margin-top: 0;
    background-color: transparent;
    border: 0;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  .navbar-nav .open .dropdown-menu > li > a,
  .navbar-nav .open .dropdown-menu .dropdown-header {
    padding: 5px 15px 5px 25px;
  }
  .navbar-nav .open .dropdown-menu > li > a {
    line-height: 20px;
  }
  .navbar-nav .open .dropdown-menu > li > a:hover,
  .navbar-nav .open .dropdown-menu > li > a:focus {
    background-image: none;
  }
}
@media (min-width: 768px) {
  .navbar-nav {
    float: left;
    margin: 0;
  }
  .navbar-nav > li {
    float: left;
  }
  .navbar-nav > li > a {
    padding-top: 15px;
    padding-bottom: 15px;
  }
}
.navbar-form {
  padding: 10px 15px;
  margin-top: 8px;
  margin-right: -15px;
  margin-bottom: 8px;
  margin-left: -15px;
  border-top: 1px solid transparent;
  border-bottom: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1), 0 1px 0 rgba(255, 255, 255, .1);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1), 0 1px 0 rgba(255, 255, 255, .1);
}
@media (min-width: 768px) {
  .navbar-form .form-group {
    display: inline-block;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .navbar-form .form-control {
    display: inline-block;
    width: auto;
    vertical-align: middle;
  }
  .navbar-form .form-control-static {
    display: inline-block;
  }
  .navbar-form .input-group {
    display: inline-table;
    vertical-align: middle;
  }
  .navbar-form .input-group .input-group-addon,
  .navbar-form .input-group .input-group-btn,
  .navbar-form .input-group .form-control {
    width: auto;
  }
  .navbar-form .input-group > .form-control {
    width: 100%;
  }
  .navbar-form .control-label {
    margin-bottom: 0;
    vertical-align: middle;
  }
  .navbar-form .radio,
  .navbar-form .checkbox {
    display: inline-block;
    margin-top: 0;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .navbar-form .radio label,
  .navbar-form .checkbox label {
    padding-left: 0;
  }
  .navbar-form .radio input[type="radio"],
  .navbar-form .checkbox input[type="checkbox"] {
    position: relative;
    margin-left: 0;
  }
  .navbar-form .has-feedback .form-control-feedback {
    top: 0;
  }
}
@media (max-width: 767px) {
  .navbar-form .form-group {
    margin-bottom: 5px;
  }
  .navbar-form .form-group:last-child {
    margin-bottom: 0;
  }
}
@media (min-width: 768px) {
  .navbar-form {
    width: auto;
    padding-top: 0;
    padding-bottom: 0;
    margin-right: 0;
    margin-left: 0;
    border: 0;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
}
.navbar-nav > li > .dropdown-menu {
  margin-top: 0;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.navbar-fixed-bottom .navbar-nav > li > .dropdown-menu {
  margin-bottom: 0;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.navbar-btn {
  margin-top: 8px;
  margin-bottom: 8px;
}
.navbar-btn.btn-sm {
  margin-top: 10px;
  margin-bottom: 10px;
}
.navbar-btn.btn-xs {
  margin-top: 14px;
  margin-bottom: 14px;
}
.navbar-text {
  margin-top: 15px;
  margin-bottom: 15px;
}
@media (min-width: 768px) {
  .navbar-text {
    float: left;
    margin-right: 15px;
    margin-left: 15px;
  }
}
@media (min-width: 768px) {
  .navbar-left {
    float: left !important;
  }
  .navbar-right {
    float: right !important;
    margin-right: -15px;
  }
  .navbar-right ~ .navbar-right {
    margin-right: 0;
  }
}
.navbar-default {
  background-color: #f8f8f8;
  border-color: #e7e7e7;
}
.navbar-default .navbar-brand {
  color: #777;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus {
  color: #5e5e5e;
  background-color: transparent;
}
.navbar-default .navbar-text {
  color: #777;
}
.navbar-default .navbar-nav > li > a {
  color: #777;
}
.navbar-default .navbar-nav > li > a:hover,
.navbar-default .navbar-nav > li > a:focus {
  color: #333;
  background-color: transparent;
}
.navbar-default .navbar-nav > .active > a,
.navbar-default .navbar-nav > .active > a:hover,
.navbar-default .navbar-nav > .active > a:focus {
  color: #555;
  background-color: #e7e7e7;
}
.navbar-default .navbar-nav > .disabled > a,
.navbar-default .navbar-nav > .disabled > a:hover,
.navbar-default .navbar-nav > .disabled > a:focus {
  color: #ccc;
  background-color: transparent;
}
.navbar-default .navbar-toggle {
  border-color: #ddd;
}
.navbar-default .navbar-toggle:hover,
.navbar-default .navbar-toggle:focus {
  background-color: #ddd;
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #888;
}
.navbar-default .navbar-collapse,
.navbar-default .navbar-form {
  border-color: #e7e7e7;
}
.navbar-default .navbar-nav > .open > a,
.navbar-default .navbar-nav > .open > a:hover,
.navbar-default .navbar-nav > .open > a:focus {
  color: #555;
  background-color: #e7e7e7;
}
@media (max-width: 767px) {
  .navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #777;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    color: #333;
    background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
    color: #555;
    background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > .disabled > a,
  .navbar-default .navbar-nav .open .dropdown-menu > .disabled > a:hover,
  .navbar-default .navbar-nav .open .dropdown-menu > .disabled > a:focus {
    color: #ccc;
    background-color: transparent;
  }
}
.navbar-default .navbar-link {
  color: #777;
}
.navbar-default .navbar-link:hover {
  color: #333;
}
.navbar-default .btn-link {
  color: #777;
}
.navbar-default .btn-link:hover,
.navbar-default .btn-link:focus {
  color: #333;
}
.navbar-default .btn-link[disabled]:hover,
fieldset[disabled] .navbar-default .btn-link:hover,
.navbar-default .btn-link[disabled]:focus,
fieldset[disabled] .navbar-default .btn-link:focus {
  color: #ccc;
}
.navbar-inverse {
  background-color: #222;
  border-color: #080808;
}
.navbar-inverse .navbar-brand {
  color: #9d9d9d;
}
.navbar-inverse .navbar-brand:hover,
.navbar-inverse .navbar-brand:focus {
  color: #fff;
  background-color: transparent;
}
.navbar-inverse .navbar-text {
  color: #9d9d9d;
}
.navbar-inverse .navbar-nav > li > a {
  color: #9d9d9d;
}
.navbar-inverse .navbar-nav > li > a:hover,
.navbar-inverse .navbar-nav > li > a:focus {
  color: #fff;
  background-color: transparent;
}
.navbar-inverse .navbar-nav > .active > a,
.navbar-inverse .navbar-nav > .active > a:hover,
.navbar-inverse .navbar-nav > .active > a:focus {
  color: #fff;
  background-color: #080808;
}
.navbar-inverse .navbar-nav > .disabled > a,
.navbar-inverse .navbar-nav > .disabled > a:hover,
.navbar-inverse .navbar-nav > .disabled > a:focus {
  color: #444;
  background-color: transparent;
}
.navbar-inverse .navbar-toggle {
  border-color: #333;
}
.navbar-inverse .navbar-toggle:hover,
.navbar-inverse .navbar-toggle:focus {
  background-color: #333;
}
.navbar-inverse .navbar-toggle .icon-bar {
  background-color: #fff;
}
.navbar-inverse .navbar-collapse,
.navbar-inverse .navbar-form {
  border-color: #101010;
}
.navbar-inverse .navbar-nav > .open > a,
.navbar-inverse .navbar-nav > .open > a:hover,
.navbar-inverse .navbar-nav > .open > a:focus {
  color: #fff;
  background-color: #080808;
}
@media (max-width: 767px) {
  .navbar-inverse .navbar-nav .open .dropdown-menu > .dropdown-header {
    border-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu .divider {
    background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu > li > a {
    color: #9d9d9d;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:hover,
  .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:focus {
    color: #fff;
    background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a,
  .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:hover,
  .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a,
  .navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a:hover,
  .navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a:focus {
    color: #444;
    background-color: transparent;
  }
}
.btn {
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.btn:focus,
.btn:active:focus,
.btn.active:focus,
.btn.focus,
.btn:active.focus,
.btn.active.focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}
.btn:hover,
.btn:focus,
.btn.focus {
  color: #333;
  text-decoration: none;
}
.btn:active,
.btn.active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
          box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
}
.btn-success {
  color: #fff;
  background-color: #5cb85c;
  border-color: #4cae4c;
}
.btn-success:focus,
.btn-success.focus {
  color: #fff;
  background-color: #449d44;
  border-color: #255625;
}
.btn-success:hover {
  color: #fff;
  background-color: #449d44;
  border-color: #398439;
}
.btn-success:active,
.btn-success.active,
.open > .dropdown-toggle.btn-success {
  color: #fff;
  background-color: #449d44;
  border-color: #398439;
}
.btn-success:active:hover,
.btn-success.active:hover,
.open > .dropdown-toggle.btn-success:hover,
.btn-success:active:focus,
.btn-success.active:focus,
.open > .dropdown-toggle.btn-success:focus,
.btn-success:active.focus,
.btn-success.active.focus,
.open > .dropdown-toggle.btn-success.focus {
  color: #fff;
  background-color: #398439;
  border-color: #255625;
}
.btn-success:active,
.btn-success.active,
.open > .dropdown-toggle.btn-success {
  background-image: none;
}
.btn-success.disabled:hover,
.btn-success[disabled]:hover,
fieldset[disabled] .btn-success:hover,
.btn-success.disabled:focus,
.btn-success[disabled]:focus,
fieldset[disabled] .btn-success:focus,
.btn-success.disabled.focus,
.btn-success[disabled].focus,
fieldset[disabled] .btn-success.focus {
  background-color: #5cb85c;
  border-color: #4cae4c;
}
.btn-success .badge {
  color: #5cb85c;
  background-color: #fff;
}
.btn-default {
  color: #333;
  background-color: #fff;
  border-color: #ccc;
}
.btn-default:focus,
.btn-default.focus {
  color: #333;
  background-color: #e6e6e6;
  border-color: #8c8c8c;
}
.btn-default:hover {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad;
}
.btn-default:active,
.btn-default.active,
.open > .dropdown-toggle.btn-default {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad;
}
.btn-default:active:hover,
.btn-default.active:hover,
.open > .dropdown-toggle.btn-default:hover,
.btn-default:active:focus,
.btn-default.active:focus,
.open > .dropdown-toggle.btn-default:focus,
.btn-default:active.focus,
.btn-default.active.focus,
.open > .dropdown-toggle.btn-default.focus {
  color: #333;
  background-color: #d4d4d4;
  border-color: #8c8c8c;
}
.btn-default:active,
.btn-default.active,
.open > .dropdown-toggle.btn-default {
  background-image: none;
}
.btn-default.disabled:hover,
.btn-default[disabled]:hover,
fieldset[disabled] .btn-default:hover,
.btn-default.disabled:focus,
.btn-default[disabled]:focus,
fieldset[disabled] .btn-default:focus,
.btn-default.disabled.focus,
.btn-default[disabled].focus,
fieldset[disabled] .btn-default.focus {
  background-color: #fff;
  border-color: #ccc;
}
.btn-default .badge {
  color: #fff;
  background-color: #333;
}
.btn-sm,
.btn-group-sm > .btn {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
</style>
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" /> -->


<!-- Include Date Range Picker -->

<script type="text/javascript">
$(function() {

  // $('.list-search .search_keywords button').click(function(){
  //   return false;
  // });
  $('.applyBtn').click(function(){
    var target   = $('div.job_listings' );

    target.triggerHandler( 'update_results', [ ] );
    job_manager_store_state( target, 1 );
  });


  // $('.search_keywords.mainSearch input#search_keywords').attr('tabindex', 10);
  // $('.filter_bullhorn_categories_wid #job_type_nursing').attr('tabindex', 11);
  // $('.filter_bullhorn_categories_wid #job_type_allied').attr('tabindex', 12);
  // $('.filter_bullhorn_categories_wid #job_type_providers').attr('tabindex', 13);
  // $('ul.job_new.checkboxes label').attr('tabindex', 14);
  // $('div#search_location_region_chosen .chosen-search input').attr('tabindex', 15);
  // $('div#search_categories_chosen .chosen-search input').attr('tabindex', 16);
  // $('.widget input#search_gross').attr('tabindex', 17);
  // $('ul.job_shifts.checkboxes label.days').attr('tabindex', 18);
  // $('ul.job_shifts.checkboxes label.swing').attr('tabindex', 19);
  // $('ul.job_shifts.checkboxes label.nights').attr('tabindex', 20);
  // $('ul.job_shifts.checkboxes label.rotating').attr('tabindex', 21);
  // $('ul.job_shifts.checkboxes label.flexible').attr('tabindex', 22);
  // $('ul.job_shifts.checkboxes label.to-be-determined').attr('tabindex', 23);
  // $('ul.job_shifts.checkboxes label.8-hours').attr('tabindex', 24);
  // $('ul.job_shifts.checkboxes label.10-hours').attr('tabindex', 25);
  // $('ul.job_shifts.checkboxes label.12-hours').attr('tabindex', 26);
  // $('ul.job_types.checkboxes label.housing-stipend').attr('tabindex', 27);
  // $('ul.job_types.checkboxes label.meals-incidentals').attr('tabindex', 28);
  // $('ul.job_types.checkboxes label.travel').attr('tabindex', 29);


});
</script>
<style type="text/css">ul.job_types.checkboxes {opacity: 1 !important;}</style>
<form class="job_filters in_sidebar">
  <?php
    if ( ! empty( $_GET['search_keywords'] ) ) {
      $keywords = sanitize_text_field( $_GET['search_keywords'] );
    } else {
      $keywords = '';
    }
  ?>



  <div class="job_filters_links"></div>
  <?php if(get_query_var( 'company')) {?>
    <input type="hidden" name="company_field" value="<?php echo urldecode( get_query_var( 'company') ) ?>">
  <?php } ?>
    <input type="hidden" id="search_keywords" name="search_keywords" placeholder="" value="<?php echo ($_GET['search_keywords']) ? $_GET['search_keywords'] : ''; ?>"/>

    <div class="widget ">
        <h4><?php esc_html_e('Category','workscout'); ?></h4>
        <style type="text/css">.job_types.checkboxes.filter_bullhorn_categories_wid.sp {opacity: 1 !important;}</style>
        <ul class="job_types checkboxes filter_bullhorn_categories_wid sp" style="">
            <li class="<?php echo ($_COOKIE['cat_selection'] == 'nursing') ? 'cookieHasIt' : ''; ?>"><input type="checkbox" name="filter_bullhorn_categories[]" <?php echo ($_COOKIE['cat_selection'] == 'nursing') ? 'checked="checked"' : ''; ?> value="<?php echo "nursing" ?>" id="job_type_<?php echo "nursing"; ?>" /><label for="job_type_<?php echo "nursing"; ?>" class="<?php echo sanitize_title( "Nursing" ); ?>"><?php echo "Nursing"; ?></label> </li>
            <li class="<?php echo ($_COOKIE['cat_selection'] == 'allied') ? 'cookieHasIt' : ''; ?>"><input type="checkbox" name="filter_bullhorn_categories[]" <?php echo ($_COOKIE['cat_selection'] == 'allied') ? 'checked="checked"' : ''; ?> value="<?php echo "allied" ?>" id="job_type_<?php echo "allied"; ?>" /><label for="job_type_<?php echo "allied"; ?>" class="<?php echo sanitize_title( "Allied" ); ?>"><?php echo "Allied"; ?></label> </li>
            <li class="<?php echo ($_COOKIE['cat_selection'] == 'providers') ? 'cookieHasIt' : ''; ?>"><input type="checkbox" name="filter_bullhorn_categories[]" <?php echo ($_COOKIE['cat_selection'] == 'providers') ? 'checked="checked"' : ''; ?> value="<?php echo "providers" ?>" id="job_type_<?php echo "providers"; ?>" /><label for="job_type_<?php echo "providers"; ?>" class="<?php echo sanitize_title( "Providers" ); ?>"><?php echo "Providers"; ?></label> </li>
        </ul>
        <input type="hidden" name="filter_bullhorn_categories[]" value="" />
    </div>
  <div class="widget">
    <h4><?php esc_html_e('New Listings','workscout'); ?></h4>
<ul class="job_new checkboxes">
    <li style="list-style: none;">
  <input type="checkbox" name="filter_job_new[]" value="new" id="job_type_1" autocomplete="on"
  <?php if ( isset( $_GET['new_job'] ) ) {
    echo 'checked="checked"';
  } ?>>
  <label for="job_type_1" class="1"> New (7 Days)</label>
</li>
</ul>
  </div>
    <div class="widget">
      <h4><?php esc_html_e('Location','workscout'); ?></h4>
      <?php /*?><input type="text" id="search_location_region" name="search_location_region" placeholder="Location" value="<?php echo ($_GET['search_location_region']) ? $_GET['search_location_region'] : ''; ?>"/><?php */
do_action('job_region_custom_location_dropdown_action');
          ?>
          <?php if ($region) { ?>
            <?php if ($region == 'northeast' || strpos($region, 'northeast') !== false) { ?>
<input type="hidden" name="search_location_region_all[]" value="Connecticut (CT)">
<input type="hidden" name="search_location_region_all[]" value="Maine (ME)">
<input type="hidden" name="search_location_region_all[]" value="Massachusetts (MA)">
<input type="hidden" name="search_location_region_all[]" value="New Hampshire (NH">
<input type="hidden" name="search_location_region_all[]" value="Rhode Island (RI">
<input type="hidden" name="search_location_region_all[]" value="Vermont (VT)">
<input type="hidden" name="search_location_region_all[]" value="New Jersey (NJ)">
<input type="hidden" name="search_location_region_all[]" value="New York (NY)">
<input type="hidden" name="search_location_region_all[]" value="Pennsylvania (PA)">
<input type="hidden" name="search_location_region_all[]" value="Delaware (DE)">
<input type="hidden" name="search_location_region_all[]" value="Maryland (MD)">
            <?php } else if ($region == 'midwest' || strpos($region, 'midwest') !== false) { ?>
<input type="hidden" name="search_location_region_all[]" value="Illinois (IL)">
<input type="hidden" name="search_location_region_all[]" value="Indiana (IN)">
<input type="hidden" name="search_location_region_all[]" value="Michigan (MI)">
<input type="hidden" name="search_location_region_all[]" value="Ohio (OH)">
<input type="hidden" name="search_location_region_all[]" value="South Dakota (SD)">
<input type="hidden" name="search_location_region_all[]" value="Wisconsin (WI)">
<input type="hidden" name="search_location_region_all[]" value="Iowa (IA)">
<input type="hidden" name="search_location_region_all[]" value="Kansas (KS)">
<input type="hidden" name="search_location_region_all[]" value="Minnesota (MN)">
<input type="hidden" name="search_location_region_all[]" value="Missouri (MO)">
<input type="hidden" name="search_location_region_all[]" value="Nebraska (NE)">
<input type="hidden" name="search_location_region_all[]" value="North Dakota (ND)">
            <?php } else if ($region == 'southwest' || strpos($region, 'southwest') !== false) { ?>
<input type="hidden" name="search_location_region_all[]" value="Texas (TX)">
<input type="hidden" name="search_location_region_all[]" value="Oklahoma (OK)">
<input type="hidden" name="search_location_region_all[]" value="New Mexico (NM)">
<input type="hidden" name="search_location_region_all[]" value="Arizona (AZ)">
            <?php } else if ($region == 'southeast' || strpos($region, 'southeast') !== false) { ?>
<input type="hidden" name="search_location_region_all[]" value="West Virginia (WV)">
<input type="hidden" name="search_location_region_all[]" value="Virginia (VA)">
<input type="hidden" name="search_location_region_all[]" value="Kentucky (KY)">
<input type="hidden" name="search_location_region_all[]" value="Tennessee (TN)">
<input type="hidden" name="search_location_region_all[]" value="North Carolina (NC)">
<input type="hidden" name="search_location_region_all[]" value="South Carolina (SC)">
<input type="hidden" name="search_location_region_all[]" value="Georgia (GA)">
<input type="hidden" name="search_location_region_all[]" value="Alabama (AL)">
<input type="hidden" name="search_location_region_all[]" value="Mississippi (MS)">
<input type="hidden" name="search_location_region_all[]" value="Arkansas (AR)">
<input type="hidden" name="search_location_region_all[]" value="Louisiana (LA)">
<input type="hidden" name="search_location_region_all[]" value="Florida (FL)">
            <?php } else if ($region == 'west' || strpos($region, 'west') !== false) { ?>
<input type="hidden" name="search_location_region_all[]" value="Colorado (CO)">
<input type="hidden" name="search_location_region_all[]" value="Idaho (ID)">
<input type="hidden" name="search_location_region_all[]" value="Montana (MT)">
<input type="hidden" name="search_location_region_all[]" value="Nevada (NV)">
<input type="hidden" name="search_location_region_all[]" value="New Mexico (NM)">
<input type="hidden" name="search_location_region_all[]" value="Utah (UT)">
<input type="hidden" name="search_location_region_all[]" value="Wyoming (WY)">
<input type="hidden" name="search_location_region_all[]" value="Alaska (AK)">
<input type="hidden" name="search_location_region_all[]" value="California (CA)">
<input type="hidden" name="search_location_region_all[]" value="Hawaii (HI)">
<input type="hidden" name="search_location_region_all[]" value="Oregon (OR)">
<input type="hidden" name="search_location_region_all[]" value="Washington (WA)">
            <?php } else if ($region == 'compact-state-multi-state-license' || strpos($region, 'compact-state-multi-state-license') !== false) { ?>
<input type="hidden" name="search_location_region_all[]" value="Arizona (AZ)">
<input type="hidden" name="search_location_region_all[]" value="Arkansas (AR)">
<input type="hidden" name="search_location_region_all[]" value="Colorado (CO)">
<input type="hidden" name="search_location_region_all[]" value="Delaware (DE)">
<input type="hidden" name="search_location_region_all[]" value="Florida (FL)">
<input type="hidden" name="search_location_region_all[]" value="Georgia (GA)">
<input type="hidden" name="search_location_region_all[]" value="Idaho (ID)">
<input type="hidden" name="search_location_region_all[]" value="Iowa (IA)">
<input type="hidden" name="search_location_region_all[]" value="Kansas (KS)">
<input type="hidden" name="search_location_region_all[]" value="Kentucky (KY)">
<input type="hidden" name="search_location_region_all[]" value="Louisiana (LA)">
<input type="hidden" name="search_location_region_all[]" value="Maine (ME)">
<input type="hidden" name="search_location_region_all[]" value="Maryland (MD)">
<input type="hidden" name="search_location_region_all[]" value="Mississippi (MS)">
<input type="hidden" name="search_location_region_all[]" value="Missouri (MO)">
<input type="hidden" name="search_location_region_all[]" value="Montana (MT)">
<input type="hidden" name="search_location_region_all[]" value="Nebraska (NE)">
<input type="hidden" name="search_location_region_all[]" value="New Hampshire (NH">
<input type="hidden" name="search_location_region_all[]" value="New Mexico (NM)">
<input type="hidden" name="search_location_region_all[]" value="North Carolina (NC)">
<input type="hidden" name="search_location_region_all[]" value="North Dakota (ND)">
<input type="hidden" name="search_location_region_all[]" value="Oklahoma (OK)">
<input type="hidden" name="search_location_region_all[]" value="South Carolina (SC)">
<input type="hidden" name="search_location_region_all[]" value="South Dakota (SD)">
<input type="hidden" name="search_location_region_all[]" value="Tennessee (TN)">
<input type="hidden" name="search_location_region_all[]" value="Texas (TX)">
<input type="hidden" name="search_location_region_all[]" value="Utah (UT)">
<input type="hidden" name="search_location_region_all[]" value="Virginia (VA)">
<input type="hidden" name="search_location_region_all[]" value="West Virginia (WV)">
<input type="hidden" name="search_location_region_all[]" value="Wisconsin (WI)">
<input type="hidden" name="search_location_region_all[]" value="Wyoming (WY)">
            <?php } ?>
          <?php } ?>
    </div>


  <?php
  if (get_terms( 'job_listing_category' ) ) :
    $show_category_multiselect = get_option( 'job_manager_enable_default_category_multiselect', false );

    if ( ! empty( $_GET['search_category'] ) ) {
      $selected_category = sanitize_text_field( $_GET['search_category'] );
    } else {
      $selected_category = "";
    }
     if(is_array($_GET['search_categories']))
      {
         $selected_category =  $_GET['search_categories'];
      }else{
      if ( isset( $_GET['search_categories'] ) ) {
      if (explode(',', $_GET['search_categories'])) {
        $selected_category = array_values(explode(',', $_GET['search_categories']));
      } else {
        $selected_category = array_values($_GET['search_categories']);
      }
       }
    }

    ?>
    <div class="widget">
      <h4><?php esc_html_e('Specialty','workscout'); ?></h4>
      <div class="search_categories">

        <?php /*if ( $show_category_multiselect ) : ?>
          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
        <?php else : ?>
          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => esc_html__( 'Any category', 'workscout' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
        <?php endif;*/ ?>
          <?php /*job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => esc_html__( 'Specialty', 'workscout' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); */ ?>

                   <?php  do_action('job_specialty_custom_job_dropdown_action'); ?>
      </div>
    </div>
  <?php else: ?>
    <input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( get_query_var('job_listing_category') ); ?>" />
  <?php endif; ?>

      <div class="widget">
        <h4><?php esc_html_e('Min Weekly Gross','workscout'); ?></h4>
          <input type="text" id="search_gross" name="search_gross" placeholder="Min Weekly Gross" value="<?php echo ($_GET['search_gross']) ? $_GET['search_gross'] : ''; ?>"/>
        </div>
        <?php if ($_GET['search_region'] == 162): ?>
          <input type="hidden" id="search_region" name="search_region" value="162" />
        <?php endif ?>

        <?php if (is_tax('job_listing_region')): ?>
          <?php $queried_object = get_queried_object(); ?>
      <div class="widget" style="visibility: hidden; height: 0px; overflow: hidden;">
        <h4><?php esc_html_e('Region','workscout'); ?></h4>
        <div class="search_location">
          <?php
          wp_dropdown_categories( apply_filters( 'job_manager_regions_dropdown_args', array(
            'show_option_all' => __( 'All Regions', 'wp-job-manager-locations' ),
            'hierarchical' => true,
            'orderby' => 'name',
            'taxonomy' => 'job_listing_region',
            'name' => 'search_region',
            'id' => 'search_location',
            'class' => 'search_region job-manager-category-dropdown chosen-select ' . ( is_rtl() ? 'chosen-rtl' : '' ),
            'hide_empty' => 0,
            'selected' => $queried_object->term_id
          ) ) );
          ?>
        </div>
      </div>

        <?php endif ?>

  <?php if(get_option('workscout_enable_location_sidebar') == 1) { ?>
    <?php if ( get_option( 'job_manager_regions_filter' ) || is_tax( 'job_listing_region' ) ) {  ?>
      <?php } else { ?>
      <div class="widget">
        <h4><?php esc_html_e('Location','workscout'); ?></h4>
        <div class="search_location">
          <?php
          if ( ! empty( $_GET['search_location'] ) ) {
            $location = sanitize_text_field( $_GET['search_location'] );
          } else {
            $location = '';
          } ?>
          <input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Location', 'workscout' ); ?>" value="<?php echo esc_attr( $location ); ?>" />

        </div>
      </div>
    <?php } ?>
  <?php } ?>


    <?php get_job_manager_template( 'job-filter-job-shifts.php', array( 'job_shfits' => '', 'atts' => '', 'selected_job_shfits' => '' ) ); ?>

  <div class="widget">
    <h4><?php esc_html_e('Shift','workscout'); ?></h4>
    <?php get_job_manager_template( 'job-filter-job-shift-type.php', array( 'job_shfits' => '', 'atts' => '', 'selected_job_shfits' => '' ) ); ?>
  </div>

  <div class="widget">
    <h4><?php esc_html_e('Shift Length','workscout'); ?></h4>
    <?php get_job_manager_template( 'job-filter-job-shift-length.php', array( 'job_shfits' => '', 'atts' => '', 'selected_job_shfits' => '' ) ); ?>
  </div>


  <div class="widget">
    <h4><?php esc_html_e('Benefits and Incidentals','workscout'); ?></h4>
    <?php get_job_manager_template( 'job-filter-job-types.php', array( 'job_types' => '', 'atts' => '', 'selected_job_types' => '' ) ); ?>
  </div>




  <?php if(get_option('workscout_enable_filter_salary')) : ?>
  <div class="widget widget_range_filter" style="margin-bottom: 10px">

    <h4 class="checkboxes" style="margin-bottom: 0;">
      <input type="checkbox"  name="filter_by_salary_check" id="salary_check" class="filter_by_check">
      <label for="salary_check"><?php esc_html_e('Filter by Salary','workscout'); ?></label>
    </h4>

    <div class="widget_range_filter-inside">
      <div class="salary_amount range-indicator">
        <span class="from"></span> &mdash; <span class="to"></span>
      </div>
        <input type="hidden" name="filter_by_salary" id="salary_amount" type="checkbox"   >
      <div id="salary-range"></div>
      <div class="margin-bottom-50"></div>
    </div>

  </div>
  <?php endif; ?>

  <?php if(get_option('workscout_enable_filter_rate')) : ?>
  <div class="widget widget_range_filter">
    <h4 class="checkboxes" style="margin-bottom: 0;">
      <input type="checkbox" name="filter_by_rate_check" id="filter_by_rate" class="filter_by_check">
      <label for="filter_by_rate"><?php esc_html_e('Filter by Rate','workscout'); ?></label>
    </h4>
    <div class="widget_range_filter-inside">
      <div class="rate_amount range-indicator">
        <span class="from"></span> &mdash; <span class="to"></span>
      </div>
        <input type="hidden" name="filter_by_rate" id="rate_amount" type="checkbox"  >
      <div id="rate-range"></div>
    </div>
  </div>
  <?php endif; ?>

</form>
  <?php dynamic_sidebar( 'sidebar-jobs' ); ?>
</div><!-- #secondary -->
