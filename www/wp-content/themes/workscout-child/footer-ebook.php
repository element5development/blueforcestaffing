<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WorkScout
 */

?>
</div>
<!-- Wrapper / End -->

<div id="apply-dialog" class="small-dialog zoom-anim-dialog mfp-hide apply-popup" style="margin-top: 0px !important;">
	<div class="small-dialog-headline">
		<h2><?php esc_html_e('Apply Now','workscout') ?></h2>
	</div>
	<div class="small-dialog-content">
		<?php
		echo do_shortcode('[gravityform id="11" title="false" description="false" ajax="true"]');
			/**
			 * job_manager_application_details_email or job_manager_application_details_url hook
			 */
			// do_action( 'job_manager_application_details_' . $apply->type, $apply );
		?>
	</div>
</div>
<div id="apply-dialog2" class="small-dialog zoom-anim-dialog mfp-hide apply-popup" style="margin-top: 0px !important;">
	<div class="small-dialog-headline">
		<h2><?php esc_html_e('Apply Now','workscout') ?></h2>
	</div>
	<div class="small-dialog-content">
		<?php
		echo do_shortcode('[gravityform id="13" title="false" description="false" ajax="true"]');
			/**
			 * job_manager_application_details_email or job_manager_application_details_url hook
			 */
			// do_action( 'job_manager_application_details_' . $apply->type, $apply );
		?>
	</div>
</div>
<style type="text/css">
	#field_11_27,
	#field_11_26,
	#field_11_15,
	#field_11_2,
	#field_11_16,
	#field_11_17,
	#field_11_18,
	#field_11_19,
	#field_11_20,
	#field_11_21,
	#field_11_24,
	#field_11_23,
	#field_11_8,
	#field_11_22,
	#field_11_4,
	#field_11_9,
	#field_11_10,
	#gform_wrapper_11 .gform_footer
	{
		display: none;
	}
	#field_13_27,
	#field_13_2,
	#field_13_16,
	#field_13_17,
	#field_13_18,
	#field_13_19,
	#field_13_20,
	#field_13_21,
	#field_13_24,
	#field_13_23,
	#field_13_8,
	#field_13_22,
	#field_13_4,
	#field_13_9,
	#field_13_10,
	#field_13_26,
	#field_13_15,
	#gform_wrapper_13 .gform_footer
	{
		display: none;
	}
	#field_14_27,
	#field_14_2,
	#field_14_16,
	#field_14_17,
	#field_14_18,
	#field_14_19,
	#field_14_20,
	#field_14_21,
	#field_14_24,
	#field_14_23,
	#field_14_8,
	#field_14_22,
	#field_14_4,
	#field_14_9,
	#field_14_10,
	#field_14_26,
	#field_14_15,
	#gform_wrapper_14 .gform_footer
	{
		display: none;
	}
	div.gform_validation_error #field_11_27,
	div.gform_validation_error #field_11_26,
	div.gform_validation_error #field_11_15,
	div.gform_validation_error #field_11_2,
	div.gform_validation_error #field_11_16,
	div.gform_validation_error #field_11_27,
	div.gform_validation_error #field_11_17,
	div.gform_validation_error #field_11_18,
	div.gform_validation_error #field_11_19,
	div.gform_validation_error #field_11_20,
	div.gform_validation_error #field_11_21,
	div.gform_validation_error #field_11_24,
	div.gform_validation_error #field_11_23,
	div.gform_validation_error #field_11_8,
	div.gform_validation_error #field_11_22,
	div.gform_validation_error #field_11_4,
	div.gform_validation_error #field_11_9,
	div.gform_validation_error #field_11_10,
	div.gform_validation_error#gform_wrapper_11 .gform_footer
	{
		display: block;
	}
	div.gform_validation_error #partialSubmit, div.gform_validation_error #TextSubmit {
		display: none;
	}
	div.gform_validation_error #field_13_27,
	div.gform_validation_error #field_13_2,
	div.gform_validation_error #field_13_16,
	div.gform_validation_error #field_13_27,
	div.gform_validation_error #field_13_17,
	div.gform_validation_error #field_13_18,
	div.gform_validation_error #field_13_19,
	div.gform_validation_error #field_13_20,
	div.gform_validation_error #field_13_21,
	div.gform_validation_error #field_13_24,
	div.gform_validation_error #field_13_23,
	div.gform_validation_error #field_13_8,
	div.gform_validation_error #field_13_22,
	div.gform_validation_error #field_13_4,
	div.gform_validation_error #field_13_9,
	div.gform_validation_error #field_13_10,
	div.gform_validation_error #field_13_26,
	div.gform_validation_error #field_13_15,
	div.gform_validation_error#gform_wrapper_13 .gform_footer
	{
		display: block;
	}
	div.gform_validation_error #partialSubmit2, div.gform_validation_error #TextSubmit2 {
		display: none;
	}
	div.gform_validation_error #field_14_27,
	div.gform_validation_error #field_14_2,
	div.gform_validation_error #field_14_16,
	div.gform_validation_error #field_14_27,
	div.gform_validation_error #field_14_17,
	div.gform_validation_error #field_14_18,
	div.gform_validation_error #field_14_19,
	div.gform_validation_error #field_14_20,
	div.gform_validation_error #field_14_21,
	div.gform_validation_error #field_14_24,
	div.gform_validation_error #field_14_23,
	div.gform_validation_error #field_14_8,
	div.gform_validation_error #field_14_22,
	div.gform_validation_error #field_14_4,
	div.gform_validation_error #field_14_9,
	div.gform_validation_error #field_14_10,
	div.gform_validation_error #field_14_26,
	/*div.gform_validation_error #field_14_15,*/
	div.gform_validation_error#gform_wrapper_14 .gform_footer
	{
		display: block;
	}
	div.gform_validation_error #partialSubmit3, div.gform_validation_error #TextSubmit3 {
		display: none;
	}
	.mfp-bg {
		z-index: 10001;
	}
</style>
<script type="text/javascript">

jQuery(document).ready(function($){
		
		$(window).scroll(function() {
				//for fixed form
				if($('#gform_wrapper_6').length > 0 && $(window).width() > 1024) {

						if($('#gform_wrapper_6 form').offset().top < $(window).scrollTop()) {
								$('#gform_wrapper_6 form').css({
									position: 'fixed',
									top: '0px',
									'max-width':'370px'
								});                      
						}
						
						if($('#gform_wrapper_6 form').offset().top - $('#gform_wrapper_6').offset().top > 50 && $('#gform_wrapper_6 form').css('position') == 'fixed') {
								$('#gform_wrapper_6 form').css({
									position: 'fixed',
									top: '0px',
									'max-width':'370px'
								});  
						} else {
								$('#gform_wrapper_6 form').removeAttr('style');
						}
				};
		});
		
		
		//mobile menu
		$('body').on('click', '#jPanelMenu-menu a', function(e) {
				if($(this).parent().hasClass('menu-item-has-children')) {
						e.preventDefault();
						$(this).parent().find('ul').css('display','block').find('li').css('float','none');
				}
		});

		$(document).on('click', '#partialSubmit', function(){
				if (
						$('#input_11_1_3').val() && 
						$('#input_11_1_4').val() && 
						$('#input_11_1_6').val() && 
						$('#input_11_3').val()
				) {
				var data = new FormData();
				data.append("email", $('#input_11_3').val());  
				data.append("action", "candidate_check_form");  
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				$('#partialSubmit').after('<p id="TextSubmit">Submitting...</p>');
				$.ajax({
						type: 'POST',
						url: ajaxurl,
						// url: "",
						data: data,
						contentType: false,
						processData: false,
						success: function(response){
								if (response.trim() == 'notfound') {
										$('#field_11_26, #field_11_27, #field_11_2, #field_11_16, #field_11_17, #field_11_18, #field_11_19, #field_11_21, #field_11_24, #field_11_23, #field_11_8, #field_11_22, #field_11_4, #field_11_9, #field_11_10, #gform_11 .gform_footer').css('display', 'block');
										$('#partialSubmit, #TextSubmit').remove();
								} else {
										$('#input_11_2').val('(111) 111-1111');
										$('#input_11_21 option[value="Alabama"]').attr('selected', 'selected');
										$('#input_11_21').val("Alabama");
										$('#input_11_17, #input_11_19,#input_11_24').val('(111) 111-1111');
										jQuery('#gform_11').submit();
								}
						}
				});

						return false;
				} else {
						alert('Please fill out all fields.');
						return false;
				}
		});
		
		$(document).on('click', '#partialSubmit2', function(){
				if (
						$('#input_13_1_3').val() && 
						$('#input_13_1_4').val() && 
						$('#input_13_1_6').val() && 
						$('#input_13_3').val()
				) {
				var data = new FormData();
				data.append("email", $('#input_13_3').val());  
				data.append("action", "candidate_check_form");  
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				$('#partialSubmit2').after('<p id="TextSubmit2">Submitting...</p>');
				$.ajax({
						type: 'POST',
						url: ajaxurl,
						// url: "",
						data: data,
						contentType: false,
						processData: false,
						success: function(response){
								if (response.trim() == 'notfound') {
										$('#field_13_27, #field_13_2, #field_13_16, #field_13_27, #field_13_17, #field_13_18, #field_13_19, #field_13_21, #field_13_24, #field_13_23, #field_13_8, #field_13_22, #field_13_4, #field_13_9, #field_13_10, #field_13_26, #gform_13 .gform_footer').css('display', 'block');
										$('#partialSubmit2, #TextSubmit2').remove();
								} else {
										$('#input_13_2').val('(111) 111-1111');
										$('#input_13_21 option[value="Alabama"]').attr('selected', 'selected');
										$('#input_13_21').val("Alabama");
										$('#input_13_17, #input_13_19,#input_13_24').val('(111) 111-1111');
										jQuery('#gform_13').submit();
								}
						}
				});

						return false;
				} else {
						alert('Please fill out all fields.');
						return false;
				}
		});
		
		$(document).on('click', '#partialSubmit3', function(){
				if (
						$('#input_14_1_3').val() && 
						$('#input_14_1_4').val() && 
						$('#input_14_1_6').val() && 
						$('#input_14_3').val()
				) {
				var data = new FormData();
				data.append("email", $('#input_14_3').val());  
				data.append("action", "candidate_check_form");  
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				$('#partialSubmit3').after('<p id="TextSubmit3">Submitting...</p>');
				$.ajax({
						type: 'POST',
						url: ajaxurl,
						// url: "",
						data: data,
						contentType: false,
						processData: false,
						success: function(response){
								if (response.trim() == 'notfound') {
										$('#field_14_27, #field_14_2, #field_14_16, #field_14_27, #field_14_17, #field_14_18, #field_14_19, #field_14_21, #field_14_24, #field_14_23, #field_14_8, #field_14_22, #field_14_4, #field_14_9, #field_14_10, #field_14_26, #gform_14 .gform_footer').css('display', 'block');
										$('#partialSubmit3, #TextSubmit3').remove();
								} else {
										$('#input_14_2').val('(111) 111-1111');
										$('#input_14_21 option[value="Alabama"]').attr('selected', 'selected');
										$('#input_14_21').val("Alabama");
										$('#input_14_17, #input_14_19,#input_14_24').val('(111) 111-1111');
										jQuery('#gform_14').submit();
								}
						}
				});

						return false;
				} else {
						alert('Please fill out all fields.');
						return false;
				}
		});


		$('.tooltip').tooltipster();
		<?php if($_COOKIE['cat_selection'] != 'clients') { ?>
	setTimeout(function(){
		var wh = $('#wrapper').height();
		// var hh = $('#page > header').height();
		// var fh = $('#page > footer').height();
		var bh = $('html').height()-100;
		var cp = $('#footer').height();
		var fp = parseInt(bh) - parseInt(wh);
		var pp = parseInt(fp) + parseInt(cp);
		// var fpt = parseInt(bh) - parseInt(fp);
		// var fptr = parseInt(fpt) - parseInt(fh) - 68;
		// alert(pp);
		if (pp > 1) {
				$('#footer').css('margin-top', pp+'px');
		}
	}, 1000);
		<?php } ?>
});

		jQuery(document).ready(function ($) {
				$('#banner.workscout-search-banner').append('<a href="javascript:;" class="scrollBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/scroll.png"></a>');

				setTimeout(function(){
						$('.scrollBtn').click(function(){
								$("html, body").animate({ scrollTop: $('.location-section').offset().top }, 1000);
						});
				}, 1000);

				setTimeout(function(){
						$('#gform_widget-3 #input_5_3_3').attr('tabindex', '8');
						$('#gform_widget-3 #input_5_2').attr('tabindex', '9');
				}, 1500);
		});
</script>

<?php wp_footer(); ?>

</body>
</html>