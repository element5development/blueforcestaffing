<?php
/**
 * Template Name: Browse Locations Page Template
 *
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage resumes
 * @since resumes 1.0
 */

get_header();


while ( have_posts() ) : the_post(); ?>
<?php $header_image = get_post_meta($post->ID, 'pp_job_header_bg', TRUE); 
if(!empty($header_image)) { ?>
<div id="titlebar" class="photo-bg" style="background: url('<?php echo esc_url($header_image); ?>')">
<?php } else { ?>
<div id="titlebar" class="photo-bg">
<?php } ?>

  <div class="container">
    <div class="sixteen columns">
      <div class="ten columns">
        <h2><?php the_title();?></h2>
      </div>
      <?php if(get_option('workscout_enable_add_job_button')) { ?>
            <div class="six columns">
          <a href="<?php echo get_permalink(get_option('job_manager_submit_job_form_page_id')); ?>" class="button"><?php esc_html_e('Apply For Your Dream Job','workscout'); ?></a>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<!-- 960 Container -->
<div class="container page-container home-page-container">
    <article  <?php post_class("sixteen columns"); ?>>
        <?php the_content(); ?>
<div class="categories-group">
  <?php
  $option_tree = get_option('option_tree');
  $array_exclude_region_list = $option_tree['pp_exclude_region_ids'];
  if(!is_array($array_exclude_region_list)){
	  $array_exclude_region_list = array(0);
  }else
  {
	  if(0== count($array_exclude_region_list))
	  {
		  $array_exclude_region_list = array(0);
	  }
  }
   $terms = get_terms( array(
    'taxonomy' => 'job_listing_region',
    'hide_empty' => false,
	'orderby'=>'name',
	'exclude'=>$array_exclude_region_list,
	
) );
$abc = round(count($terms)/4);
?>
  <div class="container">
      <div class="sixteen columns">
        <h4 class="parent-jobs-category">Browse Locations</h4>
      </div>
      <div class="four columns">
        <ul>
        <?php $i=0; foreach ($terms as $value): ?>
          <?php if ($i == $abc): ?>
              </ul>
            </div>
            <div class="four columns">
              <ul>
          <?php $i=0; ?>
          <?php endif ?>
          <li><a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/<?php echo $value->slug; ?>"><?php echo $value->name; ?></a></li>
        <?php $i++; endforeach; ?>
        </ul>
      </div>
    </div>
</div>
    </article>
</div>
<div  style="margin-bottom:-45px;"></div>
<?php endwhile; // end of the loop.

get_footer(); ?>