<article id="post-<?php the_ID(); ?>" <?php post_class('post-container'); ?> >

  <div class="post-img">
    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();  ?><div class="hover-icon"></div></a>
  </div>
  <section class="post-content">

    <a href="<?php the_permalink(); ?>"><h3 style="margin-bottom: 0"><?php the_title(); ?></h3></a>
    <small>Published on: <?php the_time(get_option('date_format')); ?></small>   
    <div class="meta-tags">
      <?php foreach((get_the_category()) as $category) {
                echo '<a href="' . get_category_link( $category->term_id ) . '" class="' . sprintf( $category->slug ) . '" ' . '>' . $category->name.'</a> ';
          } ?>
    </div>
    <div class="clearfix"></div>
    <div class="margin-bottom-25"></div>
    
    <?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" class="button"><?php esc_html_e('Read More','workscout') ?></a>

  </section>

</article>