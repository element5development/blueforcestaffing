function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '))
}
jQuery(document).ready(function($) {
    var xhr = [];
    $('.job_listings').on('update_results', function(event, page, append, loading_previous) {

        var data = '';
        var target = $(this);
        var form = $('.job_filters');
        var showing = ('h2.showing_jobs');
        var results = target.find('.job_listings');
        var per_page = target.data('per_page');
        var orderby = target.data('orderby');
        var order = target.data('order');
        var featured = target.data('featured');
        var filled = target.data('filled');
        var index = $('div.job_listings').index(this);
        var spinner = $('.listings-loader');
        if (index < 0) {
            return
        }
        if (xhr[index]) {
            xhr[index].abort()
        }
        if (!append) {
            $(results).addClass('loading');
            $(spinner).fadeIn();
            $(".display_text_showmore").css("display", "none");
            $(".display_text_showmore_space").css("display", "block");
            if (page > 1 && !0 != target.data('show_pagination')) {
                $(results).before('<a class="load_more_jobs load_previous" href="#"><strong>' + job_manager_ajax_filters.i18n_load_prev_listings + '</strong></a>')
            } else {
                target.find('.load_previous').remove()
            }
            target.find('.load_more_jobs').data('page', page)
        }
        var filter_job_type = [];
        var filter_job_new = [];
        var filter_job_shift = [];
        var filter_job_shift_type = [];
        var filter_job_shift_length = [];
        $(':input[name="filter_job_type[]"]:checked, :input[name="filter_job_type[]"][type="hidden"], :input[name="filter_job_type"]', form).each(function() {
            filter_job_type.push($(this).val())
        });
        $(':input[name="filter_job_new[]"]:checked, :input[name="filter_job_new[]"][type="hidden"], :input[name="filter_job_new"]', form).each(function() {
            filter_job_new.push($(this).val())
        });
        $(':input[name="filter_job_shift[]"]:checked, :input[name="filter_job_shift[]"][type="hidden"], :input[name="filter_job_shift"]', form).each(function() {
            filter_job_shift.push($(this).val())
        });
        $(':input[name="filter_job_shift_type[]"]:checked, :input[name="filter_job_shift_type[]"][type="hidden"], :input[name="filter_job_shift_type"]', form).each(function() {
            filter_job_shift_type.push($(this).val())
        });
        $(':input[name="filter_job_shift_length[]"]:checked, :input[name="filter_job_shift_length[]"][type="hidden"], :input[name="filter_job_shift_length"]', form).each(function() {
            filter_job_shift_length.push($(this).val())
        });
        var categories = form.find(':input[name^="search_categories"]').map(function() {
            return $(this).val()
        }).get();
        var keywords = '';
        var location = '';
        var location_region = '';
        var gross = '';
        var date = '';
        var $keywords = form.find(':input[name="search_keywords"]');
        var $location = form.find(':input[name="search_location"]');
        var $location_region = form.find(':input[name="search_location_region"]');
        var $gross = form.find(':input[name="search_gross"]');
        var $date = form.find(':input[name="search_date"]');
        if ($keywords.val() !== $keywords.attr('placeholder')) {
            keywords = $keywords.val()
        }
        // alert(keywords);

        if ($location.val() !== $location.attr('placeholder')) {
            location = $location.val()
        }
        if ($location_region.val() !== $location_region.attr('placeholder')) {
            location_region = $location_region.val()
        }
        if ($gross.val() !== $gross.attr('placeholder')) {
            gross = $gross.val()
        }
        if ($date.val() !== $date.attr('placeholder')) {
            date = $date.val()
        }
        var search_keywords_para = '';
        var search_keywords_para_data = '';
        var page_loaded = getParameterByName("loadedpage");
        var search_keywords_para = getParameterByName("search_keywords");
        var final_loaded_page = 0;
        if (page == 1 && page_loaded) {
            if (!isNaN(page_loaded)) {
                per_page = page_loaded * per_page;
                $(".display_text_showmore span.display_post").text(per_page);
                target.find('.load_more_jobs').data('page', page_loaded);
                $(".display_post").html(per_page);
                $("#display_post_no").val(per_page)
            }
        }
        if (filter_job_type.length <= 1) {
            $('input[name="filter_job_type[]"]').each(function(){
                filter_job_type.push($(this).val())
            });
            // console.log(filter_job_type);
        }
        keywords = $('.mainSearch input[name="search_keywords"]').val();
        data = {
            lang: job_manager_ajax_filters.lang,
            search_keywords: keywords,
            search_location: location,
            search_location_region: location_region,
            search_gross: gross,
            search_date: date,
            search_categories: categories,
            filter_job_type: filter_job_type,
            filter_job_new: filter_job_new,
            filter_job_shift: filter_job_shift,
            filter_job_shift_type: filter_job_shift_type,
            filter_job_shift_length: filter_job_shift_length,
            per_page: per_page,
            orderby: orderby,
            order: order,
            page: page,
            featured: featured,
            filled: filled,
            show_pagination: target.data('show_pagination'),
            form_data: form.serialize()
        };
        $('.load_more_jobs i', target).removeClass('fa fa-plus-circle').addClass('fa fa-refresh fa-spin');
        xhr[index] = $.ajax({
            type: 'POST',
            url: job_manager_ajax_filters.ajax_url.toString().replace("%%endpoint%%", "get_listings"),
            data: data,
            success: function(result) {
                if (result) {
                    try {
                        $('#titlebar .count_jobs').html(result.post_count)
                        // $(showing).show().html(result.showing);
                        if (result.showing) {
                            $(showing).show().html(result.showing);
                            $('.sidebar .job_filters_links').html(result.showing_links)
                            $('#titlebar .count_jobs').html(result.post_count)
                        } else {
                            $(showing).hide();
                            $('.sidebar .job_filters_links').hide()
                        }
                        if (result.showing_all) {
                            $(showing).addClass('wp-job-manager-showing-all')
                        } else {
                            $(showing).removeClass('wp-job-manager-showing-all')
                        }
                        if (result.html) {
                            if (append && loading_previous) {
                                $(results).prepend(result.html)
                            } else if (append) {
                                $(results).append(result.html)
                            } else {
                                $(results).html(result.html)
                            }
                        }
                        if (result.max_num_pages <= 1) {
                            console.log(result.max_num_pages);
                            $('.load_more_jobs:not(.load_previous)', target).hide();
                            $('.display_text_showmore').hide();
                            $('.load_more_jobs').css("display", "none");
                            $(".display_text_showmore_space").css("display", "none");
                            $(".display_text_showmore").css("display", "none")
                        }
                        if (!0 == target.data('show_pagination')) {
                            target.find('.job-manager-pagination').remove();
                            if (result.pagination) {
                                target.append(result.pagination)
                            }
                        } else {
                            if (!result.found_jobs || result.max_num_pages <= page) {
                                $('.load_more_jobs:not(.load_previous)', target).hide();
                                $('.display_text_showmore').hide();
                                $('.load_more_jobs').css("display", "none");
                                $(".display_text_showmore_space").css("display", "none");
                                $(".display_text_showmore").css("display", "none")
                            } else if (!loading_previous) {
                                $('.load_more_jobs', target).show();
                                $('.display_text_showmore').show();
                                $(".display_text_showmore_space").css("display", "none");
                                $(".display_text_showmore").css("display", "block")
                            }
                            $('.load_more_jobs i', target).removeClass('fa fa-refresh fa-spin').addClass('fa fa-plus-circle');
                            $('li.job_listing', results).css('visibility', 'visible')
                        }
                        $(results).removeClass('loading');
                        $(spinner).fadeOut();
                        $('.tooltip').tooltipster();
                        var h = '';
                        if (search_keywords_para != null) {
                            var search_keywords_para_data = '&search_keywords='+search_keywords_para;
                        } else {
                            var search_keywords_para_data = '';
                        }
                        if (page_loaded && page == 1) {
                            if (window.location.hash) {
                                h = window.location.hash;
                                location.href = "#"
                            }
                            if (page_loaded != 1)
                                history.pushState(window.location.href, null, '?loadedpage=' + page_loaded + search_keywords_para_data);
                            var dispapv = $('#display_post_no2').val();
                            if (dispapv > 2) {
                                var totla_val = parseInt(dispapv) * page_loaded;
                            } else {
                                var totla_val = 10 * page_loaded;
                            }
                            $(".display_post").html(totla_val);
                            $("#display_post_no").val(totla_val);
                            if (totla_val > result.post_count) {
                                $(".display_post").html(result.post_count);
                                $("#display_post_no").val(result.post_count)
                            }
                            $("#count_post").html(' out of ' + result.post_count)
                        } else {
                            if (page != 1)
                                history.pushState(window.location.href, null, '?loadedpage=' + page + search_keywords_para_data);
                            var dispapv = $('#display_post_no2').val();
                            if (dispapv > 2) {
                                var totla_val = parseInt(dispapv) * page;
                            } else {
                                var totla_val = 10 * page;
                            }
                            $(".display_post").html(totla_val);
                            $("#display_post_no").val(totla_val);
                            if (totla_val > result.post_count) {
                                $(".display_post").html(result.post_count);
                                $("#display_post_no").val(result.post_count)
                            }
                            $("#count_post").html(' out of ' + result.post_count)
                        }
                        if (page != 1 && h == '') {
                            target.triggerHandler('updated_results', result)
                        }
                        if (h != '')
                            $(window).scrollTop($(h).offset().top)
                    } catch (err) {
                        if (window.console) {
                            console.log(err)
                        }
                    }
                }
            },
            error: function(jqXHR, textStatus, error) {
                if (window.console && 'abort' !== textStatus) {
                    console.log(textStatus + ': ' + error)
                }
            },
            statusCode: {
                404: function() {
                    if (window.console) {
                        console.log("Error 404: Ajax Endpoint cannot be reached. Go to Settings > Permalinks and save to resolve.")
                    }
                }
            }
        })
    });
    var current_min_price = parseInt(job_manager_ajax_filters.salary_min, 10),
        current_max_price = parseInt(job_manager_ajax_filters.salary_max, 10);
    $("#salary-range").slider({
        range: !0,
        min: parseInt(job_manager_ajax_filters.salary_min, 10),
        max: parseInt(job_manager_ajax_filters.salary_max, 10),
        values: [current_min_price, current_max_price],
        step: 1000,
        slide: function(event, ui) {
            $("input#salary_amount").val(ui.values[0] + "-" + ui.values[1]);
            $(".salary_amount .from").text(job_manager_ajax_filters.currency + ui.values[0]);
            $(".salary_amount .to").text(job_manager_ajax_filters.currency + ui.values[1])
        }
    });
    $(".salary_amount .from").text(job_manager_ajax_filters.currency + $("#salary-range").slider("values", 0));
    $(".salary_amount .to").text(job_manager_ajax_filters.currency + $("#salary-range").slider("values", 1));
    $("input#salary_amount").val($("#salary-range").slider("values", 0) + "-" + $("#salary-range").slider("values", 1));
    var current_min_price = parseInt(job_manager_ajax_filters.rate_min, 10),
        current_max_price = parseInt(job_manager_ajax_filters.rate_max, 10);
    $("#rate-range").slider({
        range: !0,
        min: parseInt(job_manager_ajax_filters.rate_min, 10),
        max: parseInt(job_manager_ajax_filters.rate_max, 10),
        values: [current_min_price, current_max_price],
        step: 1,
        slide: function(event, ui) {
            $("input#rate_amount").val(ui.values[0] + "-" + ui.values[1]);
            $(".rate_amount .from").text(job_manager_ajax_filters.currency + ui.values[0]);
            $(".rate_amount .to").text(job_manager_ajax_filters.currency + ui.values[1])
        }
    });
    $(".rate_amount .from").text(job_manager_ajax_filters.currency + $("#rate-range").slider("values", 0));
    $(".rate_amount .to").text(job_manager_ajax_filters.currency + $("#rate-range").slider("values", 1));
    $("input#rate_amount").val($("#rate-range").slider("values", 0) + "-" + $("#rate-range").slider("values", 1));
    $('#search_keywords, #search_location, #search_location_region, #search_gross, #search_date, .job_types :input, .job_new :input, .job_shifts :input, #search_categories, .job-manager-filter, .filter_by_check').change(function() {
        var target = $('div.job_listings');
        target.triggerHandler('update_results', [1, !1]);
        job_manager_store_state(target, 1)
    }).on("keyup", function(e) {
        if (e.which === 13) {
            $(this).trigger('change')
        }
    });
    $('.filter_by_check').change(function(event) {
        $(this).parents('.widget').find('.widget_range_filter-inside').toggle()
    });
    $("#salary-range,#rate-range").on("slidestop", function(event, ui) {
        var target = $('div.job_listings');
        target.triggerHandler('update_results', [1, !1]);
        job_manager_store_state(target, 1)
    });
    $('.job_filters').on('click', '.reset', function() {
        var target = $('div.job_listings');
        var form = $(this).closest('form');
        form.find(':input[name="search_keywords"], :input[name="search_location"], :input[name="search_location_region"], :input[name="search_date"], :input[name="search_gross"], .job-manager-filter').not(':input[type="hidden"]').val('').trigger('chosen:updated');
        form.find(':input[name^="search_categories"]').not(':input[type="hidden"]').val(0).trigger('chosen:updated');
        $(':input[name="filter_job_type[]"]', form).not(':input[type="hidden"]').attr('checked', 'checked');
        $(':input[name="filter_job_new[]"]', form).not(':input[type="hidden"]').attr('checked', 'checked');
        $(':input[name="filter_job_shift[]"]', form).not(':input[type="hidden"]').removeAttr('checked');
        $('.search_keywords #search_keywords').val("");
        $('.job_filters #search_keywords').val("");
        $('.filter_by_check').prop('checked', !1);
        $('.widget_range_filter-inside').hide();
        $("#salary-range,#rate-range").each(function() {
            var options = $(this).slider('option');
            $(this).slider('values', [options.min, options.max])
        });
        target.triggerHandler('reset');
        target.triggerHandler('update_results', [1, !1]);
        job_manager_store_state(target, 1);
        return !1
    });
    $(document.body).on('click', '.load_more_jobs', function() {
        var target = $(this).closest('div.job_listings');
        var page = parseInt($(this).data('page') || 1);
        var loading_previous = !1;
        $(this).find('i').removeClass('fa fa-plus-circle').addClass('fa fa-refresh fa-spin');
        $(".display_text_showmore").css("display", "none");
        $(".display_text_showmore_space").css("display", "block");
        if ($(this).is('.load_previous')) {
            page = page - 1;
            loading_previous = !0;
            if (page === 1) {
                $(this).remove()
            } else {
                $(this).data('page', page)
            }
        } else {
            page = page + 1;
            $(this).data('page', page);
            job_manager_store_state(target, page)
        }
        target.triggerHandler('update_results', [page, !0, loading_previous]);
        return !1
    });
    $('div.job_listings').on('click', '.job-manager-pagination a', function() {
        var target = $(this).closest('div.job_listings');
        var page = $(this).data('page');
        job_manager_store_state(target, page);
        target.triggerHandler('update_results', [page, !1]);
        $("body, html").animate({
            scrollTop: target.offset().top - 40
        }, 600);
        return !1
    });
    if ($.isFunction($.fn.chosen)) {
        if (job_manager_ajax_filters.is_rtl == 1) {
            $('select[name^="search_categories"]').addClass('chosen-rtl');
            $('select[name^="search_location_region"]').addClass('chosen-rtl')
        }
        $('select[name^="search_categories"]').chosen({
            search_contains: !0,
            max_selected_options: 5
        });
        $('select[name^="search_location_region"]').chosen({
            search_contains: !0
        })
    }
    if (window.history && window.history.pushState) {
        $supports_html5_history = !0
    } else {
        $supports_html5_history = !1
    }
    var location = document.location.href.split('#')[0];

    function job_manager_store_state(target, page) {
        if ($supports_html5_history) {
            var form = $('.job_filters');
            var data = $(form).serialize();
            var index = $('div.job_listings').index(target);
            window.history.replaceState({
                id: 'job_manager_state',
                page: page,
                data: data,
                index: index
            }, '', location + '#s=1')
        }
    }
    $(window).on("load", function(event) {
        $('.job_filters').each(function() {
            var target = $('div.job_listings');
            var form = $('.job_filters');
            var inital_page = 1;
            var index = $('div.job_listings').index(target);
            if (window.history.state && window.location.hash) {
                var state = window.history.state;
                if (state.id && 'job_manager_state' === state.id && index == state.index) {
                    inital_page = state.page;
                    form.deserialize(state.data);
                    form.find(':input[name^="search_categories"]').not(':input[type="hidden"]').trigger('chosen:updated')
                }
            }
            target.triggerHandler('update_results', [inital_page, !1])
        })
    })
})