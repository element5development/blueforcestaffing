var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SHOW NAV ON SCROLL UP
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var currentTop = $(window).scrollTop();
		if (currentTop <= 350) {
			$("header#main-header .container:first-of-type").removeClass('is-visible');
			$("header#main-header .container:first-of-type").removeClass('not-visible');
		} else {
			$("header#main-header .container:first-of-type").removeClass('not-visible');
			$("header#main-header .container:first-of-type").addClass('is-visible');
		}
		this.previousTop = currentTop;
	});

});