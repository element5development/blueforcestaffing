<?php
/**
 * The template for displaying all single jobs.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WorkScout
 */

get_header(); ?>
<style type="text/css">
.company-info img { width: 450px; max-width: 100%; }
</style>
<?php while ( have_posts() ) : the_post(); ?>
<!-- Titlebar
================================================== -->
<?php
$header_image = get_post_meta($post->ID, 'pp_job_header_bg', TRUE);

if(!empty($header_image)) { ?>
	<div id="titlebar" class="photo-bg" style="background: url('<?php echo esc_url($header_image); ?>')">
<?php } else { ?>
	<div id="titlebar">
<?php } ?>

		<div class="container">
			<div class="ten columns">

			<?php
			$terms = get_the_terms( $post->ID, 'job_listing_category' );

			if ( $terms && ! is_wp_error( $terms ) ) :

				$jobcats = array();

				foreach ( $terms as $term ) {
					$term_link = get_term_link( $term );
					// $jobcats[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
				}

				$print_cats = join( " / ", $jobcats ); ?>
				<?php $theTi = explode(',', get_the_title()); ?>
				<?php if (count($theTi) > 1): ?>
				<a href="javascript:void(0);" style="color: #fff;"><?php echo $theTi[0].','; ?></a>
				<?php endif ?>
			 	<?php echo '<span>'.$print_cats.'</span>'; ?>
			<?php
			endif; ?>
				<h2>
				<?php if (count($theTi) > 1): ?>
				<a href="javascript:void(0);" style="color: #fff;"><?php echo $theTi[1]; ?></a>
				<?php else : ?>
					<?php the_title(); ?>
				<?php endif ?>
                <div class="job-listing-job-tags">
					<?php $types = wp_get_post_terms( get_the_id(), 'job_listing_type' ); ?>
					<?php foreach ($types as $key => $value): ?>
						<span class="job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>">
							<?php echo $value->name; ?>
						</span>
					<?php endforeach; ?>
					<?php if(workscout_newly_posted()) { echo '<span class="new_job">'.esc_html__('NEW','workscout').'</span>'; } ?></div>
				</h2>
			</div>

			<div class="six columns">
			<?php do_action('workscout_bookmark_hook') ?>
			</div>

		</div>
	</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="sixteen columns">
		<?php do_action('job_content_start'); ?>
	</div>

<?php if(class_exists( 'WP_Job_Manager_Applications' )) : ?>
	<?php if ( is_position_filled() ) : ?>
			<div class="sixteen columns"><div class="notification closeable notice "><?php esc_html_e( 'This position has been filled', 'workscout' ); ?></div><div class="margin-bottom-35"></div></div>
	<?php elseif ( ! candidates_can_apply() && 'preview' !== $post->post_status && get_post_meta(get_the_id(), '_orientation_date_asap', true) == 0 ) : ?>
			<div class="sixteen columns"><div class="notification closeable notice "><?php esc_html_e( 'Applications have closed', 'workscout' ); ?></div></div>
	<?php endif; ?>
<?php  endif;  ?>

	<!-- Recent Jobs -->
	<div class="eleven columns">
		<div class="padding-right">
			<?php //if ( get_the_company_name() ) { ?>
				<!-- Company Info -->
				<div class="company-info" itemscope itemtype="http://data-vocabulary.org/Organization">
					<?php /*if(class_exists('Astoundify_Job_Manager_Companies')) { echo workscout_get_company_link(the_company_name('','',false)); } */ ?>
					<?php //if (get_post_meta(get_the_id(), 'company_logo', true) != '') {
						the_company_logo( $size = 'full');
					//} else {
						//echo "<img src='/wp-content/uploads/2016/05/logo.png'>";
					//} ?>
					<?php /*if(class_exists('Astoundify_Job_Manager_Companies')) { echo "</a>"; }*/ ?>
					<div class="content" style="display:none;">
						<h4>
							<?php /*if(class_exists('Astoundify_Job_Manager_Companies')) { echo workscout_get_company_link(the_company_name('','',false)); } */?>
							<?php /*the_company_name( '<strong itemprop="name">', '</strong>' );*/  $job_facility = get_post_meta( $post->ID, '_facility', true ); if($job_facility!=''){ echo $job_facility;}  ?>
							<?php /*if(class_exists('Astoundify_Job_Manager_Companies')) { echo "</a>"; }*/ ?>
						<?php /*the_company_tagline( '<span class="company-tagline">- ', '</span>' );*/ ?></h4>
						<?php if ( $website = get_the_company_website() ) : ?>
							<!-- <span><a class="website" href="<?php echo esc_url( $website ); ?>" itemprop="url" target="_blank" rel="nofollow"><i class="fa fa-link"></i> <?php esc_html_e( 'Website', 'workscout' ); ?></a></span> -->
						<?php endif; ?>
						<?php if ( get_the_company_twitter() ) : ?>
							<span><a href="http://twitter.com/<?php echo get_the_company_twitter(); ?>">
								<i class="fa fa-twitter"></i>
								@<?php echo get_the_company_twitter(); ?>
							</a></span>
						<?php endif; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			<?php //} ?>
		<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
			<div class="job-manager-info"><?php esc_html_e( 'This listing has expired.', 'workscout' ); ?></div>
		<?php endif; ?>

			<div class="single_job_listing" itemscope itemtype="http://schema.org/JobPosting">
				<meta itemprop="title" content="<?php echo esc_attr( $post->post_title ); ?>" />

				<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
					<div class="job-manager-info"><?php esc_html_e( 'This listing has expired.', 'workscout' ); ?></div>
				<?php else : ?>
					<div class="job_description" itemprop="description">
						<?php do_action('workscout_single_job_before_content'); ?>
						<?php the_company_video(); ?>
                        <p><strong><?php esc_html_e('Description','workscout'); ?>:</strong></p>
						<?php echo do_shortcode(apply_filters( 'the_job_description', get_the_content() )); ?>
					</div>
                    <?php
                        $jobRequirements = get_post_meta( $post->ID, '_job_requirements', true );
                        if (!empty($jobRequirements)):
                    ?>
                    <div>
                        <p><strong><?php esc_html_e('Requirements','workscout'); ?>:</strong></p>
                        <span class="job_requirements" itemprop="jobRequirements">
                            <?php echo $jobRequirements; ?>
                        </span>
                    </div>
                    <?php endif; ?>
					<?php
						/**
						 * single_job_listing_end hook
						 */
						do_action( 'single_job_listing_end' );
					?>

				<?php endif; ?>
			</div>

		</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">
		<?php dynamic_sidebar( 'sidebar-job-before' ); ?>
		<!-- Sort by -->
		<div class="widget">

			<h4><?php esc_html_e('Job Overview','workscout') ?></h4>

			<div class="job-overview">
				<?php do_action( 'single_job_listing_meta_before' ); ?>
				<ul>

					<?php 
					 if (get_post_meta( $post->ID, 'bullhorn_job_id', true )) {
					 	?>
					<li>
						<i class="fa fa-th"></i>
						<div>
							<strong><?php esc_html_e('Job ID:','workscout'); ?></strong>
							<span><?php  echo get_post_meta( $post->ID, 'bullhorn_job_id', true ) ?> </span>
						</div>
					</li>
					<?php } ?>
					<?php do_action( 'single_job_listing_meta_start' ); ?>
					<li>
						<i class="fa fa-calendar"></i>
						<div>
							<strong><?php esc_html_e('Date Posted','workscout'); ?>:</strong>
							<span><?php printf( esc_html__( 'Posted %s ago', 'workscout' ), human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
						</div>
					</li>
					<?php if (get_post_meta( $post->ID, '_job_location', true )): ?>
						
                	<li>
						<i class="fa fa-map-marker"></i>
						<div>
							<strong><?php esc_html_e('Location','workscout'); ?>:</strong>
							<span class="location" itemprop="jobLocation"><?php echo get_post_meta( $post->ID, '_job_location', true ); ?></span>
						</div>
					</li>
					<?php endif ?>
                     <?php

						$array_speciality_list = wp_get_post_terms( $post->ID, 'job_listing_category', array( 'fields' => 'names' ) );
						$job_speciality_list  ='';
						if(is_array($array_speciality_list) && 0<count($array_speciality_list)){
						$job_speciality_list  = implode(',',$array_speciality_list);

						}
						if($job_speciality_list  !='')
						{
 					?>

                            <li>
                                <i class="fa fa-money"></i>
                                <div>
                                    <strong><?php esc_html_e('Specialty','workscout'); ?>:</strong>
                                    <span><?php echo $job_speciality_list; ?></span>
                                 </div>
                        	</li>
                    <?php } ?>
                    <?php
                    if (get_post_meta( $post->ID, '_orientation_date_asap', true )) {
                    	if (get_post_meta( $post->ID, 'text_for_orientation', true )) {
						echo '<li class="ws-application-deadline"><i class="fa fa-calendar"></i>
							<div>
								<strong>Orientation Date:</strong><span>'.get_post_meta( $post->ID, 'text_for_orientation', true ).'</span></div></li>';
                    	} else {
						echo '<li class="ws-application-deadline"><i class="fa fa-calendar"></i>
							<div>
								<strong>' . ( $expired ? __( 'Orientation Date', 'workscout' ) : __( 'Orientation Date', 'workscout' ) ) . ':</strong><span>ASAP</span></div></li>';
                    	}
	                } else {
						if ( $deadline = get_post_meta( $post->ID, '_application_deadline', true ) ) {
							$expiring_days = apply_filters( 'job_manager_application_deadline_expiring_days', 2 );
							$expiring = ( floor( ( time() - strtotime( $deadline ) ) / ( 60 * 60 * 24 ) ) >= $expiring_days );
							$expired  = ( floor( ( time() - strtotime( $deadline ) ) / ( 60 * 60 * 24 ) ) >= 0 );

							echo '<li class="ws-application-deadline ' . ( $expiring ? 'expiring' : '' ) . ' ' . ( $expired ? 'expired' : '' ) . '"><i class="fa fa-calendar"></i>
							<div>
								<strong>' . ( $expired ? __( 'Orientation Date', 'workscout' ) : __( 'Orientation Date', 'workscout' ) ) . ':</strong><span>' . date_i18n( __( 'M j, Y', 'workscout' ), strtotime( $deadline ) ) . '</span></div></li>';
						}
                    } ?>
                    <?php $Weekly_Gross = get_post_meta( $post->ID, '_job_weekly_gross', true );
					 if ( $Weekly_Gross ) {
					 	?>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong><?php esc_html_e('Weekly Gross:','workscout'); ?></strong>
							<span>$<?php echo esc_html( $Weekly_Gross ) ?></span>
						</div>
					</li>
					<?php } ?>

                    	<?php //$terms2 = get_the_term_list( get_the_id(), 'job_listing_tag', '', apply_filters( 'job_manager_tag_list_sep', ', ' ), '' );
                    	$terms3 = '';
                    	$terms2 = json_decode(get_post_meta( $post->ID, 'actual_shift', true ));
                    	foreach ($terms2 as $key => $value) {
                    		$terms3 .= $value.', ';
                    	}

					 if ( $terms3 ) {
					?>
					<li>
						<i class="fa fa-clock-o"></i>
						<div>
							<strong><?php esc_html_e('Job Shifts','workscout'); ?>:</strong>
							<span><?php echo strip_tags( rtrim($terms3, ', ') ) ?></span>
						</div>
					</li>
					<?php } ?>
                    	<?php $hours = get_post_meta( $post->ID, '_hours', true );
					 if ( $hours ) { ?>
					<li>
						<i class="fa fa-clock-o"></i>
						<div>
							<strong><?php esc_html_e('Hours','workscout'); ?>:</strong>
							<span><?php echo esc_html( $hours ) ?><?php esc_html_e(' hr / week','workscout'); ?></span>
						</div>
					</li>
					<?php } ?>
					<?php
					$expired_date = get_post_meta( $post->ID, '_job_expires', true );
					$hide_expiration = get_post_meta( $post->ID, '_hide_expiration', true );

					if(empty($hide_expiration )) {
						if(!empty($expired_date)) { ?>
					<!-- <li>
						<i class="fa fa-calendar"></i>
						<div>
							<strong><?php esc_html_e('Expiration date','workscout'); ?>:</strong>
							<span><?php echo date_i18n( get_option( 'date_format' ), strtotime( get_post_meta( $post->ID, '_job_expires', true ) ) ) ?></span>
						</div>
					</li> -->
					<?php }
					} ?>






					<?php $rate_min = get_post_meta( $post->ID, '_rate_min', true );
					 if ( $rate_min ) {
					 	$rate_max = get_post_meta( $post->ID, '_rate_max', true );  ?>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong><?php esc_html_e('Rate:','workscout'); ?></strong>
							<span>
								<?php echo get_workscout_currency_symbol(); echo esc_html( $rate_min ) ?> <?php if(!empty($rate_max)) { echo '- '.$rate_max; } ?><?php esc_html_e(' / hour','workscout'); ?>
							</span>
						</div>
					</li>
					<?php } ?>

					<?php $salary = get_post_meta( $post->ID, '_salary_min', true );
					 if ( $salary ) {
						$salary_max = get_post_meta( $post->ID, '_salary_max', true );
					 	?>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong><?php esc_html_e('Salary:','workscout'); ?></strong>
							<span><?php  echo get_workscout_currency_symbol();  echo esc_html( $salary ) ?> - <?php echo esc_html($salary_max); ?></span>
						</div>
					</li>
					<?php } ?>

					<?php $travel = get_post_meta( $post->ID, '_job_type_travel', true );
					 if ( $travel > 0 ) {
					 	?>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong><?php esc_html_e('Travel Stipend:','workscout'); ?></strong>
							<span>Up to <?php  echo get_workscout_currency_symbol();  echo esc_html( $travel ) ?> </span>
						</div>
					</li>
					<?php } ?>

					<?php $travel = get_post_meta( $post->ID, '_job_type_meals-and-incidentals', true );
					 if ( $travel > 0 ) {
					 	?>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong><?php esc_html_e('Meals & Incidentals:','workscout'); ?></strong>
							<span><?php  echo get_workscout_currency_symbol();  echo esc_html( $travel ) ?> </span>
						</div>
					</li>
					<?php } ?>

					<?php $travel = get_post_meta( $post->ID, '_job_type_housing-stipend', true );
					 if ( $travel > 0 ) {
					 	?>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong><?php esc_html_e('Housing Stipend:','workscout'); ?></strong>
							<span><?php  echo get_workscout_currency_symbol();  echo esc_html( $travel ) ?> </span>
						</div>
					</li>
					<?php } ?>





					<?php do_action( 'single_job_listing_meta_end' ); ?>
				</ul>

				<?php do_action( 'single_job_listing_meta_after' ); ?>

				<?php if ( candidates_can_apply() ) : ?>
					<?php
						$external_apply = get_post_meta( $post->ID, '_apply_link', true );
						if(!empty($external_apply)) {
							echo '<a class="button" target="_blank" href="'.esc_url($external_apply).'">'.esc_html__( 'Apply for job', 'workscout' ).'</a>';
						} else {
							get_job_manager_template( 'job-application.php' );
						}
					?>

				<?php endif; ?>


			</div>

		</div>
		<?php dynamic_sidebar( 'sidebar-job-after' ); ?>

	</div>
	<!-- Widgets / End -->


</div>
<div class="clearfix"></div>
<div class="margin-top-55"></div>
<script type="application/ld+json">
	<?php $locati = explode(', ', get_post_meta( $post->ID, '_job_location', true )); ?>
{
	"@context": "http://schema.org",
	"@type": "JobPosting",
	"title": "<?php the_title(); ?>",
	"description": "<?php echo strip_tags(do_shortcode(apply_filters( 'the_job_description', get_the_content() ))); ?>",
	"datePosted": "<?php echo date('Y-m-d', get_post_time( 'U' )); ?>",
	"jobLocation": {
		"@type": "Place",
		"address": {
			"@type": "PostalAddress",
			"streetAddress": "<?php echo get_post_meta( $post->ID, '_job_location', true ); ?>",
			"addressLocality": "<?php echo get_post_meta( $post->ID, '_job_location', true ); ?>",
			"addressRegion": "<?php echo get_post_meta( $post->ID, '_job_location', true ); ?>",
			"postalCode": "<?php echo end($locati); ?>",
			"addressCountry": "US"
		}
	},
	"hiringOrganization": {
		"@type": "Organization",
		"name": "Blueforce Staffing",
		"logo": "<?php echo get_bloginfo('url'); ?>/wp-content/uploads/2016/05/logo.png"
	},
	"baseSalary": {
		"@type": "MonetaryAmount",
		"currency": "USD",
		"value": {
			"@type": "QuantitativeValue",
			"value": <?php echo get_post_meta( $post->ID, '_job_weekly_gross', true ); ?>,
			"unitText": "WEEK"
		}
	},
	"employmentType": "CONTRACTOR",
	"validThrough": "<?php echo date('Y-m-d', strtotime('+ 1 day')); ?>"
}
</script>
<?php 
$bull_cats = wp_get_post_terms($post->ID, 'bullhorn_categories', array("fields" => "slugs"));
$mainCat = '';
if (in_array('registered-nurse-rn', $bull_cats)) {
	$mainCat = 'nursing';
}
if (in_array('licensed-practical-vocational-nurse-lpn-lvn', $bull_cats)) {
	$mainCat = 'nursing';
}
if (in_array('certified-nursing-assistant-cna', $bull_cats)) {
	$mainCat = 'nursing';
}
if (in_array('allied-health-professional', $bull_cats)) {
	$mainCat = 'allied';
}
if (in_array('service-health-professional', $bull_cats)) {
	$mainCat = 'allied';
}
if (in_array('physician-do', $bull_cats)) {
	$mainCat = 'providers';
}
if (in_array('physician-md', $bull_cats)) {
	$mainCat = 'providers';
}
if (in_array('physician-assistant-pa', $bull_cats)) {
	$mainCat = 'providers';
}
if (in_array('nurse-practitioner-np', $bull_cats)) {
	$mainCat = 'providers';
}
?>

<?php if ($mainCat == 'nursing') { ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('#input_11_26').html('<option value="Registered Nurse (RN)">Registered Nurse (RN)</option><option value="Licensed Practical / Vocational Nurse (LPN / LVN)">Licensed Practical / Vocational Nurse (LPN / LVN)</option><option value="Certified Nursing Assistant (CNA)">Certified Nursing Assistant (CNA)</option>');
		});
	</script>
<?php } ?>
<?php if ($mainCat == 'allied') { ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('#input_11_26').html('<option value="Allied Health Professional">Allied Health Professional</option><option value="Service Health Professional">Service Health Professional</option>');
		});
	</script>
<?php } ?>
<?php if ($mainCat == 'providers') { ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('#input_11_26').html('<option value="Physician (MD)">Physician (MD)</option><option value="Physician (DO)">Physician (DO)</option><option value="Physician Assistant (PA)">Physician Assistant (PA)</option><option value="Nursing Practitioner (NP)">Nursing Practitioner (NP)</option>');
		});
	</script>
<?php } ?>
<script>
setTimeout(function(){
	if(jQuery('#input_2_9').length>0){
/*	jQuery('#input_2_9').val('<?php echo get_the_ID()?>');
jQuery('#input_2_9').val('<?php echo get_the_company_name();?>');
		jQuery('#input_2_10').val('<?php echo get_the_title()?>');
		*/
		}
	},5000);
if (jQuery('#gform_wrapper_11').hasClass('gform_validation_error')) {
	// if () {
	
setTimeout(function(){
		jQuery('.job_application.application a').click();
	},1000);
	// } else {
		// jQuery('a.small-dialog.popup-with-zoom-anim.apply_link').click();
	// }
}
	</script>
<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
