<?php /* Template Name: Landing Template */ ?>
<?php get_header('landing'); ?>

<?php $background = get_field('background_image'); ?>

<div class="landing" style="background-image: url(<?php echo $background['url']; ?>);">
	<div class="landing_header flex-container max-width">
		<div class="one-half">
			<?php $image = get_field('header_branding'); ?>
			<?php if( !empty($image) ): ?>
				<a href="<?php echo get_site_url(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
			<?php endif; ?>
		</div>
		<div class="one-half">
			<?php 
				$phone = get_field('header_phone'); 
				$phone_ext = get_field('header_ext');
				$phone_number = '+1'. preg_replace('/[^0-9]/','',$phone) . ',' . $phone_ext;
				$email = get_field('header_email');
			?>
			<?php if( get_field('header_contact') == 'phone' ) { ?>
				<a href="tel:<?php echo $phone_number; ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="50" height="51" viewBox="0 0 50 51">
						<path d="M10.02 21.82a41.98 41.98 0 0 0 18.36 18.36l6.12-6.12c.88-.88 1.82-1.1 2.86-.66a31.28 31.28 0 0 0 9.9 1.56A2.67 2.67 0 0 1 50 37.7v9.76a2.67 2.67 0 0 1-2.74 2.74c-13.02 0-24.14-4.62-33.4-13.86C4.62 27.08 0 15.96 0 2.94A2.67 2.67 0 0 1 2.74.2h9.76a2.67 2.67 0 0 1 2.74 2.74c0 3.46.52 6.76 1.56 9.9a2.68 2.68 0 0 1-.66 2.86z"></path>
					</svg>
					<?php echo $phone; ?> ext. <?php echo $phone_ext; ?>
				</a>
			<?php } else { ?>
				<a href="mailto:<?php echo $email; ?>">
					<?php echo $email; ?>
				</a>
			<?php } ?>
		</div>
	</div>
	<div class="landing_contents max-width">
		<div class="landing_main-contents">
			<?php if( get_field('main_content_heading') ): ?>
				<?php the_field('main_content_heading'); ?>
				<?php 
					$phone = get_field('cta_phone'); 
					$phone_ext = get_field('cta_phone_ext');
					$phone_number = '+1'. preg_replace('/[^0-9]/','',$phone) . ',' . $phone_ext;
					$email = get_field('cta_email');
				?>
				<?php if( get_field('cta_contact') == 'phone' ) { ?>
					<a href="tel:<?php echo $phone_number; ?>" class="button">
						<svg xmlns="http://www.w3.org/2000/svg" width="50" height="51" viewBox="0 0 50 51">
							<path d="M10.02 21.82a41.98 41.98 0 0 0 18.36 18.36l6.12-6.12c.88-.88 1.82-1.1 2.86-.66a31.28 31.28 0 0 0 9.9 1.56A2.67 2.67 0 0 1 50 37.7v9.76a2.67 2.67 0 0 1-2.74 2.74c-13.02 0-24.14-4.62-33.4-13.86C4.62 27.08 0 15.96 0 2.94A2.67 2.67 0 0 1 2.74.2h9.76a2.67 2.67 0 0 1 2.74 2.74c0 3.46.52 6.76 1.56 9.9a2.68 2.68 0 0 1-.66 2.86z"></path>
						</svg>
						Call Now
					</a>
				<?php } else { ?>
					<a href="mailto:<?php echo $email; ?>" class="button">
						<?php echo $email; ?>
					</a>
				<?php } ?>
				<?php the_field('main_content'); ?>
			<?php endif; ?>
		</div>
		<?php if( get_field('cta_heading') ): ?>
			<?php $bg = get_field('cta_image'); ?>
			<div class="landing_cta flex-container">
				<div class="one-half" style="background-image: url(<?php echo $bg['url']; ?>)"></div>
				<div class="one-half">
					<?php if( get_field('cta_heading') ): ?>
						<h2><?php the_field('cta_heading'); ?></h2>
					<?php endif; ?>
					<?php if( get_field('cta_contents') ): ?>
						<p><?php the_field('cta_contents'); ?></p>
					<?php endif; ?>
					<?php if( get_field('cta_link') ): ?>
						<a href="<?php the_field('cta_link'); ?>" class="button"><?php the_field('cta_button'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if( get_field('contact_form') ): ?>
		<div class="landing_contact">
			<div class="flex-container max-width">
				<div class="one-half">
					<?php the_field('contact_form'); ?>
				</div>
				<div class="one-half">
					<h2>Want to talk right now?</h2>
					<?php 
						$phone = get_field('cta_phone'); 
						$phone_ext = get_field('cta_phone_ext');
						$phone_number = '+1'. preg_replace('/[^0-9]/','',$phone) . ',' . $phone_ext;
						$email = get_field('cta_email');
					?>
					<?php if( get_field('cta_contact') == 'phone' ) { ?>
						<a href="tel:<?php echo $phone_number; ?>" class="button">
							<svg xmlns="http://www.w3.org/2000/svg" width="50" height="51" viewBox="0 0 50 51">
								<path d="M10.02 21.82a41.98 41.98 0 0 0 18.36 18.36l6.12-6.12c.88-.88 1.82-1.1 2.86-.66a31.28 31.28 0 0 0 9.9 1.56A2.67 2.67 0 0 1 50 37.7v9.76a2.67 2.67 0 0 1-2.74 2.74c-13.02 0-24.14-4.62-33.4-13.86C4.62 27.08 0 15.96 0 2.94A2.67 2.67 0 0 1 2.74.2h9.76a2.67 2.67 0 0 1 2.74 2.74c0 3.46.52 6.76 1.56 9.9a2.68 2.68 0 0 1-.66 2.86z"></path>
							</svg>
							Call Now
						</a>
					<?php } else { ?>
						<a href="mailto:<?php echo $email; ?>" class="button">
							<?php echo $email; ?>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="landing_footer">
		<div class="flex-container max-width">
			<div class="one-half">
				<?php if( have_rows('footer_links') ):
					while ( have_rows('footer_links') ) : the_row(); ?>
	        	<a href="<?php the_sub_field('page_link'); ?>"><?php the_sub_field('page_text'); ?></a>
			    <?php endwhile;
				endif; ?>
			</div>
			<div class="one-half">
				<p>© <?php echo date("Y"); ?> Huffmaster Inc. All rights reserved.</p>
			</div>
		</div>
	</div>
</div>

<?php get_footer('landing'); ?>