<?php
/**
 * Template Name: Page with Jobs Search
 *
 * @package WordPress
 * @subpackage workscout
 * @since workscout 1.0
 */

get_header(); ?>
<style type="text/css">
.search-choice {
  font-size: 13px;
}
#bottomSearch #search_categories,#bottomSearch #search_location_region {
        height: 52.4px;
    }
.chosen-container-multi .chosen-results li {
  font-size: 14px;
  padding: 5px;
}
.chosen-container-multi .chosen-choices {
  padding: 18.5px 18px;
  margin: 0 !important;
}
.daterangepicker .ranges {
  float: none !important;
}
.about-us-right li:hover {
  border-color: #1d3465;
  background: #f0f0f0;
}
</style>
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
<div id="titlebar" class="single" style="margin-bottom: 0;"><div class="container" <?php if($_COOKIE['cat_selection'] == 'clients') { echo 'style="padding: 0 8px;"'; } ?>><div class="sixteen columns"><h1 id="selectionTitle"><?php if($_COOKIE['cat_selection'] == 'clients') { echo 'For clients'; } else { echo ucwords($_COOKIE['cat_selection']); }; ?></h1></div></div></div>
<div id="banner" class="workscout-search-banner" style="display: none;">
    <div class="container">
        <div class="sixteen columns">

            <div class="search-container">

                <!-- Form -->
                <h1><?php esc_html_e('Search For Open Travel Nursing Jobs','workscout') ?></h1><div style="background: rgba(205,219,235,.6); border-radius: 5px;">
                <form method="GET" class="clearfix" action="<?php echo get_permalink(get_option('job_manager_jobs_page_id')); ?>" style="background: none;">
                      <input type="text" id="search_keywords" name="search_keywords" placeholder="Keyword" value=""/>

                        <?php
                        if ( ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) :
                        $show_category_multiselect = get_option( 'job_manager_enable_default_category_multiselect', false );
                        if ( ! empty( $_GET['search_category'] ) ) { $selected_category = sanitize_text_field( $_GET['search_category'] ); } else { $selected_category = ""; }
                      ?>
                      <div class="search_categories">
                        <?php /* if ( $show_category_multiselect ) : ?>
                          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
                        <?php else : ?>
                          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => esc_html__( 'Specialty', 'workscout' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
                        <?php endif;*/ ?>


                        <div class="lcotion"><?php do_action('job_region_custom_location_dropdown_action'); ?></div>


                         <?php

                       /*job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_region', 'hierarchical' => 1, 'show_option_all' => esc_html__( 'Location', 'workscout' ), 'name' => 'search_location_region', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) );*/ ?>
                      </div>
                      <?php else: ?>
                        <input type="hidden" name="search_location_region" value="<?php echo sanitize_title( get_query_var('job_listing_category') ); ?>" />
                      <?php endif; ?>
                    <?php /*?>  <input type="text" id="search_location_region" name="search_location_region" placeholder="Location" value=""/><?php */ ?>
                      <?php
                        if ( ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) :
                        $show_category_multiselect = get_option( 'job_manager_enable_default_category_multiselect', false );
                        if ( ! empty( $_GET['search_category'] ) ) { $selected_category = sanitize_text_field( $_GET['search_category'] ); } else { $selected_category = ""; }
                      ?>
                      <div class="search_categories">
                        <?php /* if ( $show_category_multiselect ) : ?>
                          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
                        <?php else : ?>
                          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => esc_html__( 'Specialty', 'workscout' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
                        <?php endif;*/ ?>

                         <?php /*job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 100, 'show_option_all' => esc_html__( 'Specialty', 'workscout' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) );*/ ?>
                         <?php do_action('job_specialty_custom_job_dropdown_action'); ?>
                      </div>
                      <?php else: ?>
                        <input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( get_query_var('job_listing_category') ); ?>" />
                      <?php endif; ?>
                      <input type="text" id="search_gross" name="search_gross"  placeholder="Min Weekly Gross" value=""/>
                      <button><i class="fa fa-search"></i></button>
                </form>
                <!-- Browse Jobs -->
                <div class="browse-jobs" style="background: none;">
                    <?php $categoriespage = ot_get_option('pp_categories_page');
                    //if(!empty($categoriespage)) {
                        printf( __( ' Or browse job offers by <a href="%s">Specialty</a>, <a href="/travel-nurse-opportunities/">Location</a> or <a href="/travel-nurse-jobs/?new_job">New Listings (7 Days)</a>', 'workscout' ), get_permalink($categoriespage) );
                    //} ?>
                </div>
                </div>
                <!-- Announce -->
                <div class="announce">
                  Your dream job awaits you.
                    <?php $count_jobs = wp_count_posts( 'job_listing', 'readable' );
                    //printf( esc_html__( 'We have %s job offers for you!', 'workscout' ), '<strong>' . $count_jobs->publish . '</strong>' ) ?>
                </div>

            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($){

    
  $('.search_categories').ready(function(){
    setTimeout(function(){
      $('.chosen-choices .search-field input').val('Specialty');
      $('.chosen-choices .search-field input').attr('style', 'width: 100% !important;');
    }, 500);
    $(document).click(function(){
      $('.chosen-choices .search-field input').val('');
      $('.chosen-choices .search-field input').attr('placeholder', 'Specialty');
      $('.chosen-choices .search-field input').attr('style', 'width: 100% !important;');
    });
  });
  $('.about-us-right li').hover(function(){
    //$('.about-us-right li').removeClass('active');
    //$(this).addClass('active');
  });
});
</script>

<?php if ($_COOKIE['cat_selection'] != 'clients'): ?>
  
<!--section-map-location-->
<section class="location-section">
<a name="id3"/></a>
<div class="wrapper-inner clearfix">

  <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>

<div class="map-imag"><div class="iamges-area-here">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/main-map.png" width="532" height="356" usemap="#Map" id="map-image">
<!---<map name="Map">
  <area shape="poly" name="West" class="dualhover s" coords="184,349,125,349,78,351,40,344,17,335,17,315,14,285,15,246,17,204,13,170,13,142,18,103,21,75,23,44,23,18,34,6,38,3,50,2,59,2,70,0,98,1,113,3,127,6,140,7,152,9,159,12,167,12,181,12,189,14,193,15,193,15,214,17,215,17,216,31,213,70,213,90,212,104,216,111,226,112,231,112,230,157,216,158,205,157,189,157,175,156,166,156,163,156,162,169,162,182,160,196,161,214,162,222,167,228,174,279,177,294,178,300,186,304,193,307,205,310,214,313,224,318,214,313,217,311,226,311,239,316,251,320,259,326,270,332,276,337,278,344,279,347,269,351" href="<?php echo site_url();?>/job-filter/?region=West">
  <area shape="poly"  class="dualhover s" name="Midwest" coords="217,18,291,16,293,13,322,17,329,25,342,25,349,28,366,31,386,32,396,40,401,51,402,56,402,61,405,66,412,71,416,77,420,83,425,85,426,95,428,106,429,109,436,113,444,110,451,109,448,117,441,123,437,128,434,137,426,140,419,139,413,139,409,146,405,147,395,150,382,154,367,153,363,158,355,159,351,159,344,159,333,159,323,159,314,159,307,161,300,155,293,155,281,157,247,157,239,155,232,153,231,136,232,128,234,122,236,115,234,112,233,111,228,110,221,108,218,108,215,102,215,94,214,79,214,61,217,20,217,20" href="<?php echo site_url();?>/job-filter/?region=Midwest">
 <area shape="poly"  class="dualhover s" name="Northeast" coords="504,5,516,5,519,10,525,15,528,17,530,22,529,27,524,31,521,39,521,51,520,56,515,54,511,51,510,50,506,55,509,63,510,67,511,70,506,76,497,80,488,88,485,94,485,101,485,108,479,118,479,129,476,133,472,126,471,118,466,117,460,110,457,110,456,109,453,108,445,107,438,109,434,109,430,108,429,105,427,99,425,90,425,87,425,84,425,79,434,70,440,61" href="<?php echo site_url();?>/job-filter/?region=Northeast">
 <area shape="poly"  name="Southwest" class="dualhover s" coords="336,225,341,213,340,202,339,196,340,186,341,181,348,176,348,171,351,164,355,160,363,158,370,157,377,154,384,153,391,153,399,152,407,151,420,148,422,142,415,145,420,143,428,139,433,137,440,132,445,125,451,119,453,115,458,113,463,115,467,123,473,129,476,136,479,140,481,146,478,152,477,156,476,160,471,168,467,177,459,185,454,189,448,196,444,203,440,209,440,215,441,222,444,227,446,234,448,241,452,246,456,252,461,262,461,266,461,273,458,277,457,279,451,279,445,273,443,265,437,260,432,254,431,245,429,239,425,235,422,233,414,230,406,232,402,234,396,231,394,229,374,230,367,233,363,233,357,229,354,227,352,227,343,227,336,223,337,218,340,216" href="<?php echo site_url();?>/job-filter/?region=Southwest">
 <area shape="poly"  class="dualhover s" name="Southeast" coords="360,247,349,248,338,250,336,244,329,245,319,245,311,245,307,247,304,250,299,253,294,254,290,260,285,261,279,267,277,273,277,279,278,286,277,292,265,290,257,283,252,274,250,266,243,258,240,252,232,248,227,245,218,246,217,249,217,252,208,253,207,249,205,245,202,240,195,232,187,220,181,219,178,218,168,222,166,222,162,217,162,200,164,180,164,163,166,155,180,155,193,157,211,157,232,157,245,157,257,157,278,159,293,159,301,160,302,163,310,163,331,160,341,161,350,163,346,172,342,182,340,186,338,189,338,198,339,207,338,216,336,222,343,225,352,227,357,228,359,241" href="<?php echo site_url();?>/job-filter/?region=Southeast">
</map>--><div class="hover-image">
<a id="west-img" class="lowzindex" href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/west/">
<img class="dualhover t" src="<?php echo get_stylesheet_directory_uri(); ?>/images/west.png"  name="West">
</a>
<a id="midwest-img" class="lowzindex" href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/midwest/">
<img class="dualhover t" src="<?php echo get_stylesheet_directory_uri(); ?>/images/mid-west.png"  name="Midwest">
</a>
<a id="northeast-img" class="lowzindex" href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/northeast/">
<img class="dualhover t" src="<?php echo get_stylesheet_directory_uri(); ?>/images/north-east.png"  name="Northeast">
</a>
<a id="southwest-img" class="lowzindex" href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/southwest/">
<img class="dualhover t" src="<?php echo get_stylesheet_directory_uri(); ?>/images/south-west.png"  name="Southeast">
</a>
<a  id="southeast-img" class="lowzindex" href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/southeast/">
<img class="dualhover t" src="<?php echo get_stylesheet_directory_uri(); ?>/images/south-east.png"  name="Southwest">
</a></div>
<div id="West" class="regionbubble" name="West">
    <a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/west/">West</a>
    </div>

    <div id="Midwest" class="regionbubble" name="Midwest">
    <a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/midwest/">Midwest</a>
    </div>

    <div id="Southwest" class="regionbubble" name="Southwest">
    <a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/southwest/">Southwest</a>
    </div>

    <div id="Southeast" class="regionbubble" name="Southeast">
    <a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/southeast/">Southeast</a>
    </div>

    <div id="Northeast" class="regionbubble" name="Northeast">
    <a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/northeast/" class="underline">Northeast</a>
    </div>
</div>
<div class="select-state">
<span class="type">
<!-- <input type="checkbox" class="dependend" id="school-statement5"> -->
<label for="school-statement5"></label><a href="<?php bloginfo('url'); ?>/travel-nurse-jobs-opportunities/compact-state-multi-state-license/" style="text-decoration:none;"><!-- <input type="checkbox" name="is_in_region" id="is_in_region" value="1"> --> Compact State (Multi-State License)*</a>
</span></div></div>


  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'providers'): ?>
    <style type="text/css">@media (min-width: 768px) { .paddingTopH {padding-top: 0px;}.search-job-location{margin-top: 0;} }</style>
    <div style="width:100%;" class="map-imag"><?php echo '<h2 style="color: #1d3465;font-size: 1.75rem; margin:0;    font-weight: 600;" class="paddingTopH">Select your specialty to see our available provider opportunities.</h2>'; ?></div>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'allied'): ?>
    <style type="text/css">@media (min-width: 768px) { .paddingTopH {padding-top: 0px;}.search-job-location{margin-top: 0;} }</style>
    <div style="width:100%;" class="map-imag"><?php echo '<h2 style="color: #1d3465;font-size: 1.75rem; margin:0;    font-weight: 600;" class="paddingTopH">Select your specialty to see our available allied opportunities.</h2>'; ?></div>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'clients'): ?>
  <?php endif ?>

<div class="search-job-location <?php if($_COOKIE['cat_selection'] == 'allied' || $_COOKIE['cat_selection'] == 'providers') { echo 'spec'; }; ?>" style="<?php if($_COOKIE['cat_selection'] == 'allied' || $_COOKIE['cat_selection'] == 'providers') { echo 'width:100%;padding-left:0;'; }; ?>">
  <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
<h3>
    <?php echo get_field('map_area_heading_nursing'); ?>
</h3>
  <?php endif ?>
<p>
  <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
    <?php echo get_field('map_area_text_nursing'); ?>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'providers'): ?>
    <?php echo get_field('map_area_text_providers'); ?>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'allied'): ?>
    <?php echo get_field('map_area_text_allied'); ?>
  <?php endif ?></p>
 <?php
global $wpdb;

$values = $wpdb->get_results("SELECT DISTINCT meta_value FROM $wpdb->postmeta  WHERE meta_key  = 'geolocation_city' ",ARRAY_A);

//print_r($values);

 ?>

<div class="serch-lo">
  <form id="bottomSearch" method="GET" class="clearfix" action="<?php echo get_permalink(get_option('job_manager_jobs_page_id')); ?>">
    <div class="midsec midloc clearfix">
      <input type="text" id="search_keywords" name="search_keywords" placeholder="Keyword" value=""/>
     <div class="search_categories"> <div class="lcotion"><?php do_action('job_region_custom_location_dropdown_action'); ?></div></div><?php /*?>
     <input type="text" id="search_location_region" name="search_location_region" placeholder="Location" value=""/>
     <?php */?>
    </div>
    <div class="midsec clearfix">
      <?php
      /*  if ( ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) :
        $show_category_multiselect = get_option( 'job_manager_enable_default_category_multiselect', false );
        if ( ! empty( $_GET['search_category'] ) ) { $selected_category = sanitize_text_field( $_GET['search_category'] ); } else { $selected_category = ""; }
      ?>
      <div class="search_categories">
        <?php if ( $show_category_multiselect ) : ?>
          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
        <?php else : ?>
          <?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => esc_html__( 'Any category', 'workscout' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
        <?php endif; ?>
      </div>
      <?php else: ?>
        <input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( get_query_var('job_listing_category') ); ?>" />
      <?php endif;*/ ?>

         <div class="search_categories jobdp"> <?php do_action('job_specialty_custom_job_dropdown_action'); ?></div>


      <input type="text" id="search_gross" name="search_gross"  placeholder="Min Weekly Gross" value=""/>
    </div>
    <button style="color:#fff;margin-top:5px;">Search</button>
  </form>
</div>
</div>

</div>

</section>
<div class="clearfix">
<?php endif ?>

<section class="about-us-section">
<div class="wrapper-inner">
<div class="about-us-text">
  <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
    <?php the_field('about_us_nursing'); ?>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'providers'): ?>
    <?php the_field('about_us_providers'); ?>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'allied'): ?>
    <?php the_field('about_us_allied'); ?>
  <?php endif ?>
  <?php if ($_COOKIE['cat_selection'] == 'clients'): ?>
    <?php the_field('about_us_client'); ?>
  <?php endif ?>
</div>

<div class="about-us-right">
<br>
  <?php if ($_COOKIE['cat_selection'] == 'clients'): ?>
    <style type="text/css">#titlebar, #titlebar.single {
    background: #f5f5f5 !important;
}</style>
  <?php endif ?>
<ul>
  <?php if ($_COOKIE['cat_selection'] == 'clients'): ?>
<li><a href="/our-solutions/"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-05.png" alt="" /> <h4>OUR SOLUTIONS</h4></a></li>
<li><a href="/what-we-provide/"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-06.png" alt="" /> <h4>WHAT WE PROVIDE</h4></a></li>
<li><a href="/our-process/"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-07.png" alt="" /> <h4>OUR PROCESS</h4></a></li>
<li><a href="/quality/"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-08.png" alt="" /> <h4>QUALITY</h4></a></li>
  <?php else : ?>
<li><a href="/about-blueforce/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-us.png" alt="" /> <h4>About Us</h4></a></li>
<?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
<li><a href="/subscribe/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mail-icon.png" alt="" /> <h4>Subscribe for Updates</h4></a></li>
<?php endif ?>
<li><a href="/our-process/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/apply.png" alt="" /> <h4>Our process</h4></a></li>
<?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
<li><a href="/apply-now/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dream-job.png" alt="" /> <h4>Dream job</h4></a></li>
<?php endif ?>
  <?php endif ?>
</ul>
</div>
</div>
</section>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script>
jQuery(document).ready(function ($) {
  setTimeout(function(){
  if($('.job_listings.job-list > li.no_job_listings_found').length) {
    $('.container.page-container.home-page-container').remove();
  }
}, 3000);
$('#is_in_region').change(function(){
    if (this.checked) {
      // $('#bottomSearch').prepend('<input type="hidden" id="search_location_region" name="search_location_region" value="162" />');
      $('#bottomSearch #search_location_region option[value="compact-state-multi-state-license"]').attr('selected','selected');
    } else {
      $('#bottomSearch #search_location_region option[value="compact-state-multi-state-license"]').removeAttr('selected','selected');
      // $('#bottomSearch #search_location_region').remove();
    }
});
function isiPhone(){
    return (
        (navigator.platform.indexOf("iPhone") != -1) ||
        (navigator.platform.indexOf("iPod") != -1)
    );
}
if(isiPhone()){
   // alert('iPhone detected');
  $('.search_categories select').css('text-indent', '0px');
  $('.search_categories select').css('padding-left', '20px');
  // $('.search_categories.jobdp select').prepend('<option disabled selected class="hidden">Specialty</option>');
  // $('.search_categories .lcotion select').prepend('<option disabled selected class="hidden">Location</option>');
  setTimeout(function(){
    $('.search_categories select').on('focus', function() {
        $(this).children(':first-child').removeProp('selected');
    });
  }, 1000);
}
// if ($(window).width() < 768) {
// }
//$('.dualhover.t').hide();
var image = $('#map-image');
 $('.dualhover.s').bind('hover', function(){
var area = $(this).attr("name");
//alert(area);
$("img[name="+area+"]").toggle();
$("img[name="+area+"]").toggleClass('hovereffect');

});
  //$('#West').hover(function(){
   $('.hover-image .lowzindex').mouseenter(function(){
jQuery(this).removeClass('lowzindex');
 jQuery(this).addClass('highzindex');
 }).mouseleave(function() {
  jQuery(this).addClass('lowzindex');
  jQuery(this).removeClass('highzindex');
});

     $('.regionbubble a').mouseenter(function(){
     //alert('hh');
    var  bubul_a=jQuery(this).attr('href');
    //alert(bubul_a);
     $('.hover-image a').each(function(){
       var hover_a = jQuery(this).attr('href');
     // alert(hover_a);

      if(bubul_a==hover_a)
      {
     // alert('matech');
      jQuery(this).removeClass('lowzindex');
      jQuery(this).addClass('highzindex');
      }
      });
}).mouseleave(function() {
 var  bubul_a=jQuery(this).attr('href');
    //alert(bubul_a);
     $('.hover-image a').each(function(){
       var hover_a = jQuery(this).attr('href');
     // alert(hover_a);

      if(bubul_a==hover_a)
      {
     // alert('matech');
     jQuery(this).addClass('lowzindex');
  jQuery(this).removeClass('highzindex');
      }
      });
});

    $( "#orientation1" ).datepicker({

        dateFormat: 'm/d/yy',

    });

    $( "#orientation2" ).datepicker({

        dateFormat: 'm/d/yy',


    });
    $( "#orientation3" ).datepicker({

        dateFormat: 'm/d/yy',

    });

    $( "#orientation4" ).datepicker({

        dateFormat: 'm/d/yy',


    });



        });
        var expanded = false;
    function showCheckboxes() {
    //alert('weekly-gross');
        var checkboxes = document.getElementById("weekly_gross");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
</script>
<div class="clearfix"></div>



<?php
while ( have_posts() ) : the_post(); ?>
<!-- 960 Container -->
<div style=" <?php if($_COOKIE['cat_selection'] == 'clients') { echo 'display:none'; } ?>" class="container page-container home-page-container">
    <article <?php post_class("sixteen columns"); ?>>
      <?php error_reporting(E_ERROR | E_PARSE); ?>
      <style type="text/css">.job_filters {display: none !important;}
a.load_more_jobs {
    display: block;
    padding: 10px;
    margin: 30px auto;
    background: red;
    width: 200px;
    text-align: center;
    color: #fff;
    text-decoration: none !important;
}
a.load_more_jobs strong {
    color: #fff;
}
    </style>
        <div class="margin-top-40"></div>
        <?php
        if($_COOKIE['cat_selection'] == 'providers') {
            $title_jobs = get_field('recent_jobs_heading_providers');
        } else if($_COOKIE['cat_selection'] == 'allied') {
            $title_jobs = get_field('recent_jobs_heading_allied');
        } else {
           $title_jobs = get_field('recent_jobs_heading_nursing');
        }
        ?>
        
        <?php if($_COOKIE['cat_selection'] != 'clients') {
            echo do_shortcode('[column width="eleven" place="first"] [headline]'.$title_jobs.'[/headline] [jobs show_filters="true" show_description="true" per_page="4" orderby="_job_weekly_gross"] [/column] [column width="five" place="last"] [spotlight_jobs][/column]');
        } ?>
      <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
        <?php echo do_shortcode('[testimonials_wide background="" include_posts="'.implode(',', get_field('select_testimonials_nursing')).'" per_page="4"]'); wp_reset_query(); ?>
        <?php //echo do_shortcode('[actionbox wide="true" title="'.get_field('apply_now_heading_nursing').'" url="/apply-now" buttontext="Apply Now" ]'); ?>
      <?php endif ?>
      <?php if ($_COOKIE['cat_selection'] == 'clients'): ?>
        <?php echo do_shortcode('[testimonials_wide background="" include_posts="'.implode(',', get_field('select_testimonials_client')).'" per_page="-1"]'); wp_reset_query(); ?>
        <?php //echo do_shortcode('[actionbox wide="true" title="'.get_field('apply_now_heading_client').'" url="/apply-now" buttontext="Apply Now" ]'); ?>
      <?php endif ?>
    </article>
</div>
 <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
  <div class="infobox">
    <div class="container">
        <div class="sixteen columns"> 
        <?php
        echo get_field('apply_now_heading_nursing').'<a class="small-dialog popup-with-zoom-anim" href="#apply-dialog">Apply Now</a>';
        ?>        
        </div>
    </div>
</div>
<?php endif ?>
<div class="container">
                <article class="sixteen columns" style="width: 100%;">                  <div class="vc_row wpb_row vc_row-fluid container" style="width: 100%;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
  <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper">
      <div class="three-section-divide clearfix" style="margin-top: 50px; vertical-align: top; text-align: center;">
<div class="first-column" style="display: inline-block; float: none;">
<div class="ask-im"></div>
<h3>Have a question?</h3>
<p>Send us a message and receive a quick<br/> response</p>
<div class="ask-questi"><a href="/have-a-question">Ask A Question</a></div>
</div>
  <?php if ($_COOKIE['cat_selection'] == 'nursing'): ?>
<div class="first-column" style="display: inline-block; float: none;">
    
<div class="ask-im"></div>
<h3>Get an answer!</h3>
<p>View our FAQs and answers to&nbsp;the questions we hear the most</p>
<div class="ask-questi"><a href="/get-an-answer">Frequently Asked Questions</a></div>
</div>
  <?php endif ?>
<div class="first-column" style="display: inline-block; float: none;">
<div class="ask-im"></div>
<h3>Contact Us!</h3>
<p>Reach out directly to BlueForce Healthcare Staffing</p>
<div class="ask-questi"><a href="/contact">Contact BlueForce</a></div>
</div>
</div>

    </div>
  </div>
</div></div></div></div>
    </article>
</div>
<style type="text/css">
#dontShowIt {display: none !important;}
.ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content {display: none !important;}
@media (max-width: 768px) {
  #search_keywords {
    width: 100% !important;
  }
  body #bottomSearch .midsec.midloc .search_categories {
    margin-right: 0px;
    width: 100%;
  }
  .chosen-container-single .chosen-single span {
    color: #909090;
  }
  .search-container input, .search-container select, #bottomSearch input, #bottomSearch select,
  .search-container .chosen-container, .search-container select, .search-container input {
    color: #909090;
  }
}
</style>
<?php endwhile; // end of the loop.

get_footer(); ?>