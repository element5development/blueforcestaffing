<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WorkScout
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta name="p:domain_verify" content="2416aa20e429f4ba7c04dcdd2f7dcb95"/>

<?php wp_head(); ?>

<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
<!-- Hotjar Tracking Code for http://blueforcestaffing.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:231620,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<script>
function count_my_fun()
{
	var txt_val = $("#display_post_no").val();
	var adding_pag = 15;
	var totla_val = +txt_val + +adding_pag;
	$(".display_post").html(totla_val);
	$("#display_post_no").val(totla_val);
}
</script>
<!--Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1034725683306429'); // Insert your pixel ID here.
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1034725683306429&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<?php $layout = Kirki::get_option( 'workscout','pp_body_style','fullwidth' ); ?>
<body <?php body_class($layout); ?>>
<div id="wrapper">
<?php if (!is_page('selecting-page')) { ?>
	<header id="main-header" class="<?php $style = Kirki::get_option( 'workscout','pp_header_style', 'default' ); echo $style; ?>">
		<div class="container">
			<div class="sixteen columns">
				<!-- Logo -->
				<div id="logo" style="position: relative;">
					<div style="position: absolute; left: 15px; right: 0; width: 80px; height: 20px; margin: 0 auto; background: #fff; border-radius: 50%; top: 35%; z-index: -9; box-shadow: 0px 1px 90px 70px #fff;"></div>
					<?php
						$logo = Kirki::get_option( 'workscout', 'pp_logo_upload', '' );
					if($logo) {
							if(is_front_page()){ ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
							<?php } else { ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
							<?php }
					} else {
							if(is_front_page()) { ?>
							<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php } else { ?>
							<h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
							<?php }
					}
					?>
					<?php if(get_theme_mod('workscout_tagline_switch','hide') == 'show') { ?><div id="blogdesc"><?php bloginfo( 'description' ); ?></div><?php } ?>
				</div>
				<div class="huffmaster0">
					<?php if ($_COOKIE['cat_selection'] != 'clients'): ?>
					<a class="small-dialog popup-with-zoom-anim apply_link" href="#apply-dialog2">Apply Now</a>
					<?php endif ?>
				</div>
				<style>.phone-no0 a {color: #fff !important; text-decoration: none !important; border: 0px !important; width: auto !important; height: auto !important; padding: 0px !important; margin: 0px !important;}</style>
				<!-- Menu -->
				<nav id="navigation" class="menu">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'responsive','container' => false ) ); ?>
						<?php
						$minicart_status = Kirki::get_option( 'workscout', 'pp_minicart_in_header', false );
						if(Kirki::get_option( 'workscout', 'pp_login_form_status', true ) ) { ?>
						<ul class="float-right">
						<?php if($minicart_status == 'on') {  get_template_part( 'inc/mini_cart'); } ?>
							<?php
							if ( is_user_logged_in() ) { ?>
							<?php $loginpage = ot_get_option('pp_login_page');
							if( ! empty( $loginpage )) { $loginlink = get_permalink($loginpage); ?>
							<li><a href="<?php echo esc_url($loginlink); ?>"><i class="fa fa-sign-out"></i> <?php esc_html_e('User Page','workscout') ?></a></li>
							<?php } ?>
							<li><a href="<?php echo wp_logout_url( home_url() );  ?>"><i class="fa fa-sign-out"></i> <?php esc_html_e('Log Out','workscout') ?></a></li>
						</ul>
							<?php
							} else {
							if(ot_get_option('pp_login_form','on') == 'off') {
								$loginpage = ot_get_option('pp_login_page');
								if( ! empty( $loginpage )) {
									$loginlink = get_permalink($loginpage);
								} else {
									$loginlink = wp_login_url( get_permalink() );
								} ?>
									<li><a href="<?php echo esc_url($loginlink); ?>#tab-register"><i class="fa fa-user"></i> <?php esc_html_e('Sign Up','workscout') ?></a></li>
									<li><a href="<?php echo esc_url($loginlink); ?>"><i class="fa fa-lock"></i> <?php esc_html_e('Log In','workscout') ?></a></li>
								<?php
							} else { ?>
								<li><a href="#singup-dialog" class="small-dialog popup-with-zoom-anim"><i class="fa fa-user"></i> <?php esc_html_e('Sign Up','workscout') ?></a></li>
								<li><a href="#login-dialog" class="small-dialog popup-with-zoom-anim"><i class="fa fa-lock"></i> <?php esc_html_e('Log In','workscout') ?></a></li>
							<?php } ?>
						</ul>
						<?php if(ot_get_option('pp_login_form','on') == 'on') { ?>
						<div id="singup-dialog" class="small-dialog zoom-anim-dialog mfp-hide apply-popup">
							<div class="small-dialog-headline">
								<h2><?php esc_html_e('Sign Up','workscout'); ?></h2>
							</div>
							<div class="small-dialog-content">
								<?php echo do_shortcode('[register_form]'); ?>
							</div>
						</div>
						<div id="login-dialog" class="small-dialog zoom-anim-dialog mfp-hide apply-popup">
							<div class="small-dialog-headline">
								<h2><?php esc_html_e('Login','workscout'); ?></h2>
							</div>
							<div class="small-dialog-content">
								<?php echo do_shortcode('[login_form]');  ?>
							</div>
						</div>
						<?php }
							}
					}?>
				</nav>
				<!-- Navigation -->
				<div id="mobile-navigation">
					<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i><?php esc_html_e('Menu','workscout'); ?></a>
				</div>
			</div>
		</div>
		<div class="social-i container">
			<div class="footer-bottom socialSection">
				<style type="text/css">.hideDesktop {display: none !important;}  @media (max-width: 550px) {body .selectingSection .column-12.client .sp a{    font-size: 20px; line-height:26px;} .phone-no0{padding-top: 5px!important; }body .social-i .footer-bottom.landingfooterbottom+.footer-bottom img{margin-top: 0!important;} .page #titlebar {margin-top: -190px !important;} .page .social-i .footer-bottom .social-icons li a {padding: 0px !important;} #banner {margin-top: -182px;} .footer-bottom .social-icons li {float: none; text-align: center;} .twitter i, .twitter:before, .pinterest i, .pinterest:before, .instagram i, .instagram:before, .facebook i, .facebook:before, .linkedin i, .linkedin:before {margin: 13px 0 0 -5px;} .social-icons li a i {margin-left: -5px;} .social-icons li.hideDesktop {width: 100%;} .social-icons li.hideDesktopBottom {margin-bottom: 15px; margin-top: -10px;}}</style>
				<?php /* get the slider array */
				$footericons = ot_get_option( 'pp_footericons', array() );
				if ( !empty( $footericons ) ) {
					echo '<ul class="social-icons mobileChange">';
					foreach( $footericons as $icon ) {
						echo '<li><a target="_blank" class="' . $icon['icons_service'] . '" title="' . esc_attr($icon['title']) . '" href="' . esc_url($icon['icons_url']) . '"><i class="icon-' . $icon['icons_service'] . '"></i></a></li>';
					}
					// echo '<li class="hideDesktop"><a href="/apply-now" target="_blank" style="width: auto; border: 0px; padding: 10px; height: auto;" class="scriptTriggerApply"><img style="max-width:100px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt="" /></a></li>';
					echo '<li class="hideDesktop hideDesktopBottom"><div style="display: inline-block; line-height: 20px; padding-top: 2px; color: #fff;" class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none" style="font-family: inherit;"></i></div></li>';
					echo '</ul>';
				}
				?>
			</div>
			<div class="huffmaster1">
				<a style="color:#fff;" href="https://www.jointcommission.org/">
				<img src="/wp-content/uploads/2016/05/huff.png" alt="" />
				<span class="top-text0">Blueforce has earned<br>The Joint Commission's Gold<br>Seal of Approval</span>
				<!-- <a href="/apply-now" target="_blank" class="scriptTriggerApply"><img style="max-width:160px; margin: 8px 0 0 10px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt="" /></a> -->
				<!-- <img src="/wp-content/uploads/2016/05/phone.png" alt="" class="ndimg" />
				<div class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none">1 (248) 588-1600 (Outside US)</i></div> -->
				</a>
			</div>
			<div class="footer-bottom mobileHide numberSection" style="width: 160px; color: #fff; padding: 4px 0 0 0; position: relative; text-align: right;">
				<img src="/wp-content/uploads/2016/05/phone.png" alt="" class="ndimg" style="float: left; margin-top: 12px;display: inline-block;float: left;position: absolute;left: 0px;top: 4px;" />
				<div style="display: inline-block; line-height: 20px; padding-top: 16px;padding-bottom:10px;" class="phone-no0" x-ms-format-detection="none" data-callMe="true">1 (866) 795-2583<br><i x-ms-format-detection="none"></i></div>
			</div>
		</div>
		<h1><?php the_title();?></h1>
	</header>
	<div class="clearfix"></div>
	<script>
			jQuery('#header-newsletter-close').on('click', function(){
					// alert();
					jQuery(this).parent().parent().parent().parent().remove();
			});

			jQuery('.huffmaster0 a:eq(1)').on('click',function(e) {
				// e.preventDefault();
				ga('send', 'event', 'Lead', 'Click', 'Apply Now Lead', 1);
				fbq('track', 'Lead',{content_name: 'Apply Now'});
				fbq('track', 'ViewContent',{button:'Apply Now'});
				// window.open('/apply-now/', '_blank');
			});
			
			jQuery('#mobile-navigation .menu-trigger').on('click',function() {
					function s(){
							// jQuery('#jPanelMenu-menu').append('<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://chk.tbe.taleo.net/chk05/ats/careers/v2/applyRequisition?org=HUFFMASTERS&amp;cws=45&amp;rid=741" target="_blank" class="scriptTriggerApply"><img style="max-width:160px; margin: 2px 0 0 10px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt=""></a></li>');
							jQuery('#jPanelMenu-menu').append('<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="/apply-now/"><img style="max-width:160px; margin: 2px 0 0 10px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt=""></a></li>');
					}
					setTimeout(s,500);
			});
	</script>
	<?php } ?>
	<script type="text/javascript">
    jQuery(document).ready(function($){
			$('body').on('click', 'div[data-callMe="true"]', function(e) {
				location.href = 'tel:+18667952583';
			});
    });
	</script>
	<style type="text/css">
			#main-header #logo > div {
					display: none;
			}
			#main-header #logo img {
					z-index: 1;
					position: relative;
			}
			#main-header #logo:before {
					content: '';
					background: url('/wp-content/uploads/2018/10/logo-white-bg-2x.png');
					display: block;
					width: 100%;
					height: 100%;
					position: absolute;
					left: 0;
					/*filter: blur(30px);*/
					top: 0;
					background-position: center;
					background-repeat: no-repeat;
					background-size: 100%;
			}
			#wrapper {
					/*margin-top: -22px;*/
			}
	</style>
	<?php if (strpos($_SERVER['HTTP_REFERER'], get_bloginfo('url')) === false): ?>
			<?php if (isset($_SERVER['HTTP_REFERER'])): 
							$parse = parse_url($_SERVER['HTTP_REFERER']);
							$sourceHost = $parse['host'];
							if (isset($_GET['li_source'])) {
									$sourceHost = $_GET['li_source'];
							} ?>
					<script type="text/javascript">
							jQuery(document).ready(function($){
									$.cookie("main_source", "<?php echo $sourceHost; ?>", { expires: 7, path: '/' });
							});
					</script>
			<?php endif ?>
	<?php endif ?>