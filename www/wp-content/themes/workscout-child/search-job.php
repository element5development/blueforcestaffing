<?php

/* Template Name: Search Job */

get_header();

//print_r($_GET);



function curPageURL() {

 $pageURL = 'http';

 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

 $pageURL .= "://";

 if ($_SERVER["SERVER_PORT"] != "80") {

  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];

 } else {

  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

 }

 return $pageURL;

}



$currentpageurl = curPageURL();



global $paged;

if( get_query_var( 'paged' ) )

$paged = get_query_var( 'paged' );

else {

if( get_query_var( 'page' ) )

$paged = get_query_var( 'page' );

else

$my_page = 1;

set_query_var( 'paged', $my_page );

$paged = $my_page;

}



$post_tit = $_GET['title'];

$location = $_GET['location'];

$speciality = $_GET['speciality'];

$orientation = $_GET['orientation'];

$weekly_gross = $_GET['weekly_gross'];

$contract_len= $_GET['contract'];

$hours= $_GET['hours'];

$travel= $_GET['travel'];

$housing= $_GET['housing'];

$state= $_GET['state'];

 $city= $_GET['city'];

$zip= $_GET['zip-code'];

$relationarray1 = array();

$relationarray2 = array();

$relationarray3 = array();

$relationarray4 = array();

$relationarray5 = array();

$relationarray6 = array();

$relationarray7 = array(); 

$relationarray8 = array();

$relationarray9 = array(); 

$relationarray10= array(); 

$relationarray11= array(); 

$odate = array();

if(!empty($location)){

$relationarray1 = array('key' => '_job_location','value' => $location, 'compare' => 'LIKE');

}

if(!empty($speciality)){

//$relationarray2 = array('key' => 'Specialty','value' => $speciality, 'compare' => 'LIKE');

}

if(!empty($orientation)){

$relationarray3 = array('key' => 'Orientation','value' => $orientation, 'compare' => 'LIKE');

}

if(!empty($weekly_gross) && $weekly_gross != 'anyrate' ){

$rusult_gross =explode("-",$weekly_gross);

   if(count($rusult_gross) == 2) {

	$relationarray4 = array('key' => 'WeeklyGross','value' => $rusult_gross, 'compare' => 'BETWEEN', 'type' => 'NUMERIC');

	}

}

if(!empty($contract_len)){

$relationarray5 = array('key' => 'ContractLength','value' => $contract_len, 'compare' => '=');

}

if(!empty($hours)){

$relationarray6 = array('key' => 'Guarantee','value' => $hours, 'compare' => '=');

}

if(!empty($housing)){

$relationarray7 = array('key' => 'HousingStipend','value' =>'','compare' => '!=');

}

if(!empty($travel))

{

$relationarray8 = array('key' => 'TravelStipend', 'value' =>'','compare' => '!=');

}

if(!empty($state))

{

$relationarray9 = array('key' => 'geolocation_state_long', 'value' =>$state,'compare' => 'LIKE');

}

if(!empty($city))

{

$relationarray10 = array('key' => 'geolocation_city', 'value' =>$city,'compare' => 'LIKE');

}

if(!empty($zip))

{

$relationarray11 = array('key' => 'job_zipcode', 'value' =>$zip,'compare' => '=');

}
$valtype = array($_GET['odate1'], $_GET['odate2'] );
if(!empty($_GET['odate1']) && !empty($_GET['odate2'])) {

	 $valtype = array($_GET['odate1'], $_GET['odate2'] );

	$odate = array('key' => 'Orientation','value' => $valtype, 'compare' => 'BETWEEN');

}

$ordertype = 'DESC';

if(!empty($_GET['sortby'])){

	if($_GET['sortby'] == 'highest'){

	     $ordertype = 'DESC';

	}

	if($_GET['sortby'] == 'lowest'){

		$ordertype = 'ASC';

	}

  	

}	

if(!empty($_GET['specialities'])){

		$job_specialities = $_GET['specialities'];

		// print_r($job_specialities);

		//$job_spe= explode( ',', $job_specialities );

		

		$job_list_type=array(array( 'taxonomy' => 'job_listing_category', 'field' => 'slug', 

	'terms' => $job_specialities )); 

	}		



	

  	$args = array( 

	'meta_query' => array(

	  'relation' => 'AND',

			$relationarray1,

			$relationarray2,

			$relationarray3,

			$relationarray4,

			$relationarray5,

			$relationarray6,

			$relationarray7,

			$relationarray8,

			$relationarray9,

			$relationarray10,

			$relationarray11,

			$odate

	   ),

	   'meta_key' =>'WeeklyGross',

	'orderby'   =>'meta_value', 

	'post_type' => 'job_listing', 

	'paged' =>$paged,    

	'posts_per_page' => 10,	

	 'tax_query' => $job_list_type,

	'order' => $ordertype ,

	"s" => $post_tit

	) ;

	

	

	





$my_query = new WP_Query($args);

$jobcount = $my_query->post_count;

$max_num_pages = $my_query->max_num_pages;





$jobtypes = get_terms('job_listing_type');

$specialities = get_terms('job_listing_category');

	

	/* 	$arg = array( 

	'post_type' => 'job_listing', 

	'paged' =>$paged,    

	'posts_per_page' => 10,	

	'tax_query' => array(

		'relation' => 'AND',

		array(

			'taxonomy' => 'job_listing_category',

			'field'    => 'slug',

			'terms'    => $job_specialities

		)

	),

	) ;

	$my = new WP_Query($arg);

	echo "<pre>";

	print_r($my); */

	

?>



<div class="banner-area inner-pages-search-listing">

<div class="search-res">

<div class="wrapper-inner">

<div class="lect-banner-content">



<h3>We found <?php echo $jobcount; ?> jobs matching:</h3>

<h1>Jobs  <?php echo $location;  if(isset($_GET['specialities'])) { echo ' - '.$_GET['specialities'] ;} else {echo "- All";}?></h1>

</div>

<div class="serch-fav-job"><a href="#">Apply For Your Dream Job</a></div>



<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>

</div>

</div>

</div>



 <div class="search-section">

  <div class="wrapper-inner">

  

   <div class="search-section-left">

       <div class="search-bar">

           <div class="search-left">

           <input type='text' placeholder='keywords, job title or company name' id='search-text-input' /></div>

            <div class="search-right"> <i class="fa fa-search"></i></div>



        </div> <!-- ed of search bar -->
<div class="start_job_list">
<?php 
if($jobcount=='0')
{
echo '<li class="no_job_listings_found">There are no listings matching your search.</li>';
}


?>



<?php $jobposts = $my_query->posts;

    foreach($jobposts as $job) {

	

		$permalink = get_permalink( $job->ID ) ;

		$jobmeta= get_post_meta($job->ID);	

     	$location_name= $jobmeta['geolocation_city'][0].', '.$jobmeta['geolocation_state_long'][0];

		$WeeklyGross = $jobmeta['WeeklyGross'][0];

		$orientation = $jobmeta['Orientation'][0];

		$shift = $jobmeta['Shift'][0];

		$Specialty = $jobmeta['Specialty'][0];	

$jobcats = array();			

	    $terms = get_the_terms( $job->ID, 'job_listing_type' );	  

		if ( $terms && ! is_wp_error( $terms ) ) { 

			$jobcats = array();	

			foreach ( $terms as $term ) {

				$term_link = get_term_link( $term );

				if($term->name == 'Full Time') {					

					$jobcats[] = '<li class="f-t"><a href="'.$term_link.'">'.$term->name.'</a></li>';

				 } 

				else if($term->name == 'Part Time') {						

					$jobcats[] = '<li class="p-t"><a href="'.$term_link.'">'.$term->name.'</a></li>';

				 } 

				 else {

				    $jobcats[] = '<li class="p-t"><a href="'.$term_link.'">'.$term->name.'</a></li>';

				 }

				

			}

		}

 ?>



    

<div class="search-result searching-get">
	<a href="<?php echo $permalink; ?>">
    <div class="search-result-left">

     

    <?php if (has_post_thumbnail($job->ID))  {echo get_the_post_thumbnail( $job->ID, array(160, 98) );}

     else {

	 echo '<img src="'.site_url().'/wp-content/plugins/wp-job-manager/assets/images/company.png">';

	  } ?>  

     

     </div>

    <div class="search-result-right">

         <h2><?php echo apply_filters( 'the_title', $job->post_title, $job->ID ); ?>

         <?php if(!empty($Specialty)) { echo  ' - '.$Specialty ; } ?>

         </h2>

    

    

        <?php if(!empty($jobcats)) { ?>

        <ul class="search-catagory">

          <?php foreach($jobcats as $jobtype) {

              echo $jobtype;	  

          } ?>

        </ul>

        <?php 



		} ?>

    

        <ul class="search-location">

        

        <li><i class="fa fa-user" aria-hidden="true"></i> <?php if(!empty($Specialty)) { echo $Specialty ; } ?></li>

        <li><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $shift;?></li>

        <li><i class="fa fa-calendar-check-o" aria-hidden="true"></i><?php echo $orientation;?></li>

        <li><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location_name;?></li>

        <li><i class="fa fa-money" aria-hidden="true"></i><?php echo $WeeklyGross;?>/wk</li>

        </ul>

    </div>
    </a>

</div>  <!-- search result -->

   

   <?php  } ?>

   

   <?php 	echo wpbeginner_numeric_posts_nav($max_num_pages); ?>

   </div>

 </div>

    <!-- search-section-left -->

    

  <div class="search-section-sidebar">

  <a class="reset" href="<?php echo site_url();?>/job-filter/?location=&speciality=&orientation=&weekly_gross=">Reset</a>

    <div class="short-by">

    <h2>Sort by</h2>

     <select name="sortby" id="sortby">

       <option value="">--Select--</option>

       <option value="highest" <?php if(isset($_GET['sortby']) && $_GET['sortby'] =='highest') {  echo 'selected ="selected"'; } ?> >Total Earning -Hightest First</option>

        <option value="lowest" <?php if(isset($_GET['sortby']) && $_GET['sortby'] =='lowest') {  echo 'selected ="selected"'; } ?> >Total Earning -Lowest First</option>

     </select> 

    </div>

    <form method="get" action="" />

    <div class="short-by location">

    <h2>Location</h2>

    <input type="text" placeholder="State/Province" id="state" name="state">

    <input type="text" placeholder="City" id="city" name="city"> </div>

    <div class="miles-from-zip">

    <ul class="miles">

    <li>

    <select>    

    <option value="total">Miles</option>

    <option value="100">100</option>

    <option value="200">200</option>

    <option value="300">300</option>

    <option value="400">400</option>

    <option value="500">500</option>

    </select>

    </li>

   <?php /*?> <li></li><?php */?>

    <li><input type="text" placeholder="Zip-Code" id="zip-code" name="zip-code"></li>

    </ul>

    <input type="submit" value="filter" id="filter-btn">

    </div>

</form>





  <div class="short-by Specialties">

      <h2>Specialties</h2>

	   <select name="specialities" id="specialties" multiple SIZE=10> 

	   <option value="">--Select--</option>

	   <?php foreach($specialities as $key => $special) { 

	

	     echo '<option value="'.$special->slug.'">'.$special->name.'</option>';	  

	     }

	  ?>

      </select> 

</div>

    



    

    

    <div class="short-by weekly-gross">

       <h2>Weekly Gross</h2>

	   

        <span class="type">

        <input id="school-weeklyg4" class="dependend" type="checkbox" checked="" value="anyrate">

        <label for="school-weeklyg4"></label>Any rate

        </span>

        <span class="type">

        <input id="school-weeklyg5" class="dependend" type="checkbox" value="1-25">

        <label for="school-weeklyg5"></label>$0-$25(258)

        </span>

        <span class="type">

        <input id="school-weeklyg6" class="dependend" type="checkbox" value="25-50">

        <label for="school-weeklyg6"></label>$25-$50(123)

        </span>

        <span class="type">

        <input id="school-weeklyg7" class="dependend" type="checkbox" value="50-100">

        <label for="school-weeklyg7"></label>$50-$100(342)

        </span>

        <span class="type">

        <input id="school-weeklyg8" class="dependend" type="checkbox" value="100-200">

        <label for="school-weeklyg8"></label>$100-$200(58)

        </span>

        <span class="type">

        <input id="school-weeklyg9" class="dependend" type="checkbox" value="200-3000">

        <label for="school-weeklyg9"></label>$200+(18)

        </span>

    </div>

    

    <div class="short-by">

    <h2>Orientation Date</h2>

       <h5 class="date_between"> Between</h5><div class="calender-start">

           <input type="text" id="orientation1" value="" placeholder="04/18/2016">

           <i class="fa fa-calendar" aria-hidden="true"></i>

        </div>

        <span class="and_date">And</span><div class="calender-start end">

           <input type="text" id="orientation2" value=""  placeholder="04/18/2017">

           <i class="fa fa-calendar" aria-hidden="true"></i>

        </div>

     

    </div>

    

    

    <div class="short-by contract_len">

      <h2>Contract Length</h2>

        <span class="type">

        <input id="school-contract10" class="dependend" type="checkbox" value="3">

        <label for="school-contract10"></label>3 month

        </span>

        <span class="type">

        <input id="school-contract11" class="dependend" type="checkbox" value="4">

        <label for="school-contract12"></label>4 month

        </span>

        <span class="type">

        <input id="school-contract13" class="dependend" type="checkbox" value="6">

        <label for="school-contract13"></label>6 month

        </span>

        <span class="type">

        <input id="school-contract14" class="dependend" type="checkbox" value="9">

        <label for="school-contract14"></label>9 month

        </span>

        <span class="type">

        <input id="school-contract15" class="dependend" type="checkbox" value="12">

        <label for="school-contract15"></label>12 month

        </span>

    </div>

    

    

    <div class="short-by hour_guarantee">

      <h2>Hours Guarantee</h2>

        <span class="type">

        <input id="school-gurantee16" class="dependend" type="checkbox" value="20">

        <label for="school-gurantee16"></label>20 hours

        </span>

        <span class="type">

        <input id="school-gurantee17" class="dependend" type="checkbox" value="30">

        <label for="school-gurantee17"></label>30 hours

        </span>

        <span class="type">

        <input id="school-gurantee18" class="dependend" type="checkbox" value="40">

        <label for="school-gurantee18"></label>40 hours

        </span>

        <span class="type">

        <input id="school-gurantee19" class="dependend" type="checkbox" value="50">

        <label for="school-gurantee19"></label>50 hours

        </span>

    </div>

    

     <div class="short-by shift">

       <h2>Shift</h2>

        <div class="shift_list">

		<li><span class="type">

        <input id="school-shift20" class="dependend" type="checkbox" checked="" value="ALL">

        <label for="school-shift20"></label>All

        </span></li>

       <li> <span class="type">

        <input id="school-shift21" class="dependend" type="checkbox" value="D">

        <label for="school-shift21"></label>Days

        </span></li>

       <li> <span class="type">

        <input id="school-shift22" class="dependend" type="checkbox" value="D/N">

        <label for="school-shift22"></label>Days & Nights

        </span></li>

        <li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D/N-10">

        <label for="school-shift23"></label>Days & Nights (10 hours)

        </span></li>

		 <li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D/N-12">

        <label for="school-shift23"></label>Days & Nights (12 hours)

        </span></li>

		 <li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D/S">

        <label for="school-shift23"></label>Days & Swing

        </span></li>

		 <li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D/S-10">

        <label for="school-shift23"></label>Days & Swing (10 hours)

        </span></li>

		 <li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D/S-12">

        <label for="school-shift23"></label>Days & Swing (12 hours)

        </span></li>

		 <li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D-10">

        <label for="school-shift23"></label>Days (10 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="D-12">

        <label for="school-shift23"></label>Days (12 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="N">

        <label for="school-shift23"></label>Nights

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="N-10">

        <label for="school-shift23"></label>Nights (10 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="N-12">

        <label for="school-shift23"></label>Nights (12 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="N/A">

        <label for="school-shift23"></label>Not listed 

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="S">

        <label for="school-shift23"></label>Swing

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="S/N">

        <label for="school-shift23"></label>Swing & Nights

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="S/N-10">

        <label for="school-shift23"></label>Swing & Nights (10 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="S/N-12">

        <label for="school-shift23"></label>Swing & Nights (12 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="S-10">

        <label for="school-shift23"></label>Swing (10 hours)

        </span></li>

		<li><span class="type">

        <input id="school-shift23" class="dependend" type="checkbox" value="S-12">

        <label for="school-shift23"></label>Swing (12 hours)

        </span></li> </div>

        

		<button id="loadMore">Load More</button>

    </div>

    

        <div class="short-by bonus_stipend">

           <h2>Bonus & Stipend</h2>

            <span class="type">

              <input id="school-statement22" class="dependend" type="checkbox" value="housing">

              <label for="school-statement22"></label>Housing Stipend

            </span>

            <span class="type">

              <input id="school-statement23" class="dependend" type="checkbox" value="travel">

              <label for="school-statement23"></label>Travel Bonus

            </span>      

            

        </div>

        

     </div> <!-- end of serch rightsection -->

   </div>

 </div>

    

    

<script>

var str = "";

  $(document).ready(function(e) {

  

	$(".shift_list li").hide();  

    size_li = $(".shift_list li").size();

	//// alert(size_li);

    x=5;

    $('.shift_list li:lt('+x+')').show();

    $('#loadMore').click(function () {

        x= size_li;

        $('.shift_list li:lt('+x+')').fadeIn('slow');

    });

    

  function $_GET(param) {

	var vars = {};

	window.location.href.replace( location.hash, '' ).replace( 

		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp

		function( m, key, value ) { // callback

			vars[key] = value !== undefined ? value : '';

		}

	);



	if ( param ) {

		return vars[param] ? vars[param] : null;	

	}

	return vars;

}



var $_GET = $_GET();

var   time = $_GET['time'];	

//// alert(time);

var   odate1 = $_GET['odate1'];	

var   odate2 = $_GET['odate2'];	

var   sort = $_GET['sortby'];	

var   sort1 = $_GET['sortby'];

var   job = $_GET['jobtype'];

var   gross = $_GET['weekly_gross'];

var   contract=	$_GET['contract'];

var   hour=	$_GET['hours'];

var   travel=$_GET['travel'];

var   housing=$_GET['housing'];

var   special=$_GET['specialities'];

var   state =$_GET['state'];

var   city =$_GET['city'];

var   title =$_GET['title'];

var   shift=$_GET['shift'];



	$( "#orientation1" ).datepicker({

		dateFormat: 'm/d/yy',

	    onSelect: function(date) {

            var orientation1  = $('#orientation1').val();

			var orientation2  = $('#orientation2').val();

			if(orientation2 != '' && orientation1 != ''){

			   url = '<?php echo $currentpageurl;?>&odate1='+orientation1+'&odate2='+orientation2;						  

			    window.location=url;

			}		  

        },	

	});

	$( "#orientation2" ).datepicker({

		dateFormat: 'm/d/yy',

	    onSelect: function(date) {

            var orientation1  = $('#orientation1').val();

			var orientation2  = $('#orientation2').val();

			if(orientation2 != '' && orientation1 != ''){

				if(odate1 == null && odate2 == null){

			    url = '<?php echo $currentpageurl;?>&odate1='+orientation1+'&odate2='+orientation2;		 

			    window.location=url;

				}

				else

				{

				url = '<?php echo $currentpageurl;?>';

				var newurl=url.replace('&odate1='+odate1+'&odate2='+odate2,'&odate1='+orientation1+'&odate2='+orientation2);

	             window.location=newurl;

				}

			}		  

        },	

	});

	

     $('#sortby').change(function(){
	var sortby= $("#sortby").val();
	var search_query= $("#search-text-input").val();
	  $.ajax({
      url: '<?php echo bloginfo('template_url')?>/ajax_job_listing.php',
      type: 'post',
      data: {'action': 'ajax_job', 'title': search_query, 'sortby': sortby},
      success: function(data, status) {
        if(data) {
         //// alert(data);
		 $('.start_job_list').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });


	   });

	  $('.weekly-gross .dependend').on('change', function() {
		$('.weekly-gross .dependend').not(this).prop('checked', false); 
	      var sortby= $("#sortby").val();
		  var search_query= $("#search-text-input").val();
		   var weekly_gross= $(".weekly-gross .dependend:checked").val();
		//   // alert(weekly_gross);
	  $.ajax({
      url: '<?php echo bloginfo('template_url')?>/ajax_job_listing.php',
      type: 'post',
      data: {'action': 'ajax_job', 'title': search_query, 'sortby': sortby,'weekly_gross' :weekly_gross},
      success: function(data, status) {
        if(data) {
       //  // alert(data);
		 $('.start_job_list').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });

			

	});
	
	 $('.shift_list .dependend').on('change', function() {

	      if($(this).prop("checked") == true){

			 var shift_list=$(this).val();

			url = '<?php echo $currentpageurl;?>';

			 if(shift == null)

				  {

				  newurl = '<?php echo $currentpageurl;?>'+'&shift='+shift_list;		

				  window.location=newurl;	

				  }

				  else

				  {

			var newurl=url.replace('&shift='+shift,"&shift="+shift_list);

			window.location=newurl;

			}

			}

			else

			{

			url = '<?php echo $currentpageurl;?>';

			 var newurl=url.replace('&shift='+shift,"");

			 window.location=newurl;

			}

	});

		  $('input[name=jobtype]').on('change', function() {

		    if($(this).prop("checked") == true){

					 var job_type = $(this).val();

					  url = '<?php echo $currentpageurl;?>';

					  if(job == null)

					  {

					newurl = '<?php echo $currentpageurl;?>'+'&jobtype='+job_type;		

					  window.location=newurl;	

					}	

					else{

					  var newurl=url.replace('&jobtype='+job,"&jobtype="+job_type);

					  window.location=newurl;	

					}		

			}

			else

			{

					  url = '<?php echo $currentpageurl;?>';

					  var newurl=url.replace('&jobtype='+job,"");

						 window.location=newurl;	

			

			}

			

	});

	

	  $('.contract_len .dependend').on('change', function() {

	  	if($(this).prop("checked") == true){	  

		 var contract_len=$(this).val();

			$('.contract_len .dependend').not(this).prop('checked', false); 

			url = '<?php echo $currentpageurl;?>';

			 if(contract == null)

				  {

				  newurl = '<?php echo $currentpageurl;?>'+'&contract='+contract_len;		

				  window.location=newurl;	

				  }

				  else

				  {

			var newurl=url.replace('&contract='+contract,"&contract="+contract_len);

			window.location=newurl;

			}	  

			}



			else

			{

			url = '<?php echo $currentpageurl;?>';

			var newurl=url.replace('&contract='+contract,"");

			window.location=newurl;

			}



	  });

	  

	    $('.hour_guarantee .dependend').on('change', function() {

	  	if($(this).prop("checked") == true){	  	  

		 var hour_guarantee=$(this).val();

			$('.hour_guarantee .dependend').not(this).prop('checked', false); 

			url = '<?php echo $currentpageurl;?>';

			 if(hour == null)

				  {

				  newurl = '<?php echo $currentpageurl;?>'+'&hours='+hour_guarantee;		

				  window.location=newurl;	

				  }

				  else

				  {

			var newurl=url.replace('&hours='+hour,"&hours="+hour_guarantee);

			window.location=newurl;

			}

			}

			else

			{

			url = '<?php echo $currentpageurl;?>';

			var newurl=url.replace('&hours='+hour,"");

			window.location=newurl;

			}

	  });

	  

	 $('#school-statement22').click(function(){

	 url = '<?php echo $currentpageurl;?>';

	   if(housing == null)

	   {

	   newurl = '<?php echo $currentpageurl;?>'+'&housing=1';		

	  window.location=newurl;

	   }

	    else

	   {

	  var newurl=url.replace('&housing='+housing,"");

			window.location=newurl;

	   }

	 

	 });

	  $('#school-statement23').click(function(){

	 url = '<?php echo $currentpageurl;?>';

	

	   if(travel == null)

	   {

	   newurl = '<?php echo $currentpageurl;?>'+'&travel=1';		

	  window.location=newurl;

	  }

	   else

	   {

	   var newurl=url.replace('&travel='+travel,"");

			window.location=newurl;

	   }

	  

	 

	 });

	 

	  $('#filter-btn').click(function(){

	 url = '<?php echo $currentpageurl;?>';

	

	   if(state == null)

	   {

	   newurl = '<?php echo $currentpageurl;?>'+'&state='+state;

	  window.location=newurl;

	  }

	   if(city==null)

	   {

	  newurl = '<?php echo $currentpageurl;?>'+'&city='+city;

	  window.location=newurl;

	   }

	  

	 

	 });

	  

	   $('#specialties').click(function(event){
	   
	    if(event.ctrlKey)
				  {
				 var specialities= $("#specialties option:selected").val();	
				  }
				  else
				  {
				     var search_query=$('#search-text-input').val();
	   var sortby= $("#sortby option:selected").val();
	   var specialities= $("#specialties option:selected").val();
    $.ajax({
      url: '<?php echo bloginfo('template_url')?>/ajax_job_listing.php',
      type: 'post',
      data: {'action': 'ajax_job', 'title': search_query, 'sortby': sortby, 'specialities': specialities},
      success: function(data, status) {
        if(data) {
      //   // alert(data);
		 $('.start_job_list').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });
        }	

	 });

	   $('.search-right').click(function(){

	   var search_query=$('#search-text-input').val();
	   var sortby= $("#sortby option:selected").val();
    $.ajax({
      url: '<?php echo bloginfo('template_url')?>/ajax_job_listing.php',
      type: 'post',
      data: {'action': 'ajax_job', 'title': search_query, 'sortby': sortby},
      success: function(data, status) {
        if(data) {
        // // alert(data);
		 $('.start_job_list').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });

	    if(title == null)

		{

		  newurl = '<?php echo $currentpageurl;?>'+'?title='+search_query;		

				//  window.location=newurl;	

		  }

				  

		  else

			 {

			

				url = '<?php echo $currentpageurl;?>';

				 var newurl=url.replace('&title='+title,"&title="+search_query);

			      //window.location=newurl;

				  }

	   });



/******************* Each function **************************/ 

 $('input[name=jobtype]').each(function() {

	  var job_type = $(this).val();

	  if(job_type==job)

	  {

	  $(this).prop('checked', true);

	  }

 });

  $('.weekly-gross .dependend').each(function() {

		var weekly_gross=$(this).val();

	   if(weekly_gross==gross)

	   {

	   $('.weekly-gross .dependend').not(this).prop('checked', false); 

	   $(this).prop('checked', true);

	   }

   });
   
     $('.shift_list .dependend').each(function() {

		var shift_list=$(this).val();

	   if(shift_list==shift)

	   {

	   $('.shift_list .dependend').not(this).prop('checked', false); 

	   $(this).prop('checked', true);

	   }

   });

    $('.contract_len .dependend').each(function() {

		var contract_len=$(this).val();

	   if(contract_len==contract)

	   {

	   $(this).prop('checked', true);

	   }

   });

    $('.hour_guarantee .dependend').each(function() {

		var hour_guarantee=$(this).val();

	   if(hour_guarantee==hour)

	   {

	   $(this).prop('checked', true);

	   }

   });



   if(travel == 1)

	   {

	   $('#school-statement23').prop('checked', true);

	   }

	   if(housing == 1)

	   {

	   $('#school-statement22').prop('checked', true);

	   }

 

   

});

    $(document).on('keyup', function(event) {

	var specialities= $("#specialties").val();

        if(specialities && !event.ctrlKey ) {

           //// alert(specialities); 

						     var search_query=$('#search-text-input').val();
	   var sortby= $("#sortby").val();
	   var specialities= $("#specialties").val();
    $.ajax({
      url: '<?php echo bloginfo('template_url')?>/ajax_job_listing.php',
      type: 'post',
      data: {'action': 'ajax_job', 'title': search_query, 'sortby': sortby, 'specialities': specialities},
      success: function(data, status) {
        if(data) {
         // alert(data);
		 $('.start_job_list').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });

        } 

		

         specialities = '';

    });







</script>

	

	<?php get_footer();?>