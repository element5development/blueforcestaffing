<li <?php job_listing_class(); ?>>
	<a href="<?php the_job_permalink(); ?>">
		<div class="position">
			<h3><?php the_title(); ?></h3>
		</div>
		<ul class="meta">
			<li class="location" style="margin-bottom: 0px;padding-bottom: 0;"><i class="fa fa-map-marker"></i> <?php echo get_post_meta( get_the_ID(), '_job_location', true ); ?></li>
			<?php $Weekly_Gross = get_post_meta( get_the_id(), '_job_weekly_gross', true );
					 if ( $Weekly_Gross ) {
					 	?>
					<li class="company"><i class="fa fa-money"></i> $<?php echo esc_html( $Weekly_Gross ) ?></li>
					<?php } ?>
			<!-- <li class="company"><i class="fa fa-building-o"></i> <?php the_company_name(); ?></li> -->
			<li class="job-type <?php echo get_the_job_type() ? sanitize_title( get_the_job_type()->slug ) : ''; ?>"><?php $types = wp_get_post_terms( get_the_ID(), 'job_listing_type' ); ?>
<?php foreach ($types as $key => $value): ?>
	<span title="<?php if($value->slug == 'travel'){echo "Up to ";}?>$<?php echo get_post_meta(get_the_ID(), '_job_type_'.$value->slug, true); ?>" class="tooltip job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>" style="color: #fff; padding: 2px;">
		<?php echo $value->name; ?>
	</span>
<?php endforeach; ?></li>
		</ul>
	</a>
</li>