<?php
function get_job_listing_tags_length( $fields = 'all' ) {
	$terms =  get_terms( "job_listing_tag", array(
		'orderby'    => 'ID',
		'order'      => 'ASC',
		'hide_empty' => false,
		'fields'     => $fields,
		'include' => array(859,858,857)
	) );
	return $terms;
	// $types = [];
	// foreach ( $terms as $key => $type ) 
	// {
	// 	list($k, $v) = explode('-', $type->slug, 2);
	// 	$types[$v] = ucwords(str_replace("-", " ", $v));
	// }
	// return $types;
}

// $selected_job_shifts =implode( ',', array_values( get_job_listing_tags( 'id=>slug' ) ) );
// $selected_job_shifts = is_array( $selected_job_shifts ) ? $selected_job_shifts : array_filter( array_map( 'trim', explode( ',', $selected_job_shifts ) ) );
// $sft = explode(',', $_GET['filter_job_shift']);

 ?>
<?php if ( ! is_tax( 'job_listing_tag' ) && empty( $job_shifts ) ) : ?>
	<ul class="job_shifts checkboxes">
		<?php foreach ( get_job_listing_tags_length() as $key => $type ) : ?>
			<li style="list-style:none;">
				<input type="checkbox" name="filter_job_shift[]" value="<?php echo esc_attr($type->slug); ?>"
<?php if ($sft): ?>
	<?php if (in_array($type->slug, $sft)): ?>
	checked="checked"
	<?php endif ?>
<?php endif ?>
				 id="job_shift_length_<?php echo esc_attr($type->slug); ?>" />
				<label for="job_shift_length_<?php echo esc_attr($type->slug); ?>" class="<?php echo sanitize_title( $type->name ); ?>"> <?php echo esc_attr($type->name); ?></label>
			</li>
		<?php endforeach; ?>
	</ul>
	<input type="hidden" name="filter_job_shift[]" value="" />
<?php elseif ( $job_shifts ) : ?>
	<?php foreach ( $job_shifts as $job_shift ) : ?>
		<input type="hidden" name="filter_job_shift[]" value="<?php echo sanitize_title( $job_shift ); ?>" />
	<?php endforeach; ?>
<?php endif; ?>