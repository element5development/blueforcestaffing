<?php global $post; ?>
<li id="job_listing_search_<?php echo get_the_id(); ?>" <?php job_listing_class(); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
	<div class="job-block" onclick="history.pushState(window.location.href, null, '#job_listing_search_<?php echo get_the_id(); ?>'); location.href='<?php the_job_permalink(); ?>'" style="cursor: pointer;">
		<?php //if (get_post_meta(get_the_id(), 'company_logo', true) != '') {
			the_company_logo( $size = 'job-manager-search-thumb');
		// } else {
			//echo "<img class='company_logo' src='/wp-content/uploads/2016/05/logo.png'>";
		// } ?>
		<div class="job-list-content">
			<h4><?php the_title(); ?>

        	<?php $types = wp_get_post_terms( get_the_id(), 'job_listing_type' ); ?>
					
				<div class="job-list-job-tags">
			
			<?php foreach ($types as $key => $value): ?>
            	
					<span title="<?php if($value->slug == 'travel'){echo "Up to ";}?>$<?php echo get_post_meta($post->ID, '_job_type_'.$value->slug, true); ?>" class="tooltip job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>">
						<?php echo $value->name; ?>
					</span>
			
			<?php endforeach; ?>
			
			<?php if(workscout_newly_posted()) { echo '<span class="new_job">'.esc_html__('NEW','workscout').'</span>'; } ?> 
			</div>	
			</h4>

			<div class="job-icons">
				<?php do_action( 'workscout_job_listing_meta_start' ); ?>
				<?php if (get_post_meta( $post->ID, '_job_location', true )): ?>
					
                <span><i class="fa fa-map-marker"></i> <?php echo get_post_meta( $post->ID, '_job_location', true ); ?></span>
				<?php endif ?>

                <?php $Weekly_Gross = get_post_meta( $post->ID, '_job_weekly_gross', true );
					 if ( $Weekly_Gross ) {
					 	?>
					<span><i class="fa fa-money"></i> $<?php echo esc_html( $Weekly_Gross ) ?></span>
					<?php } ?>

                    <?php $terms2 = get_the_term_list( get_the_id(), 'job_listing_tag', '', apply_filters( 'job_manager_tag_list_sep', ', ' ), '' );

					 if ( $terms2 ) {
					?>
					<span><i class="fa fa-clock-o"></i> <?php echo strip_tags( $terms2 ) ?></span>
					<?php } ?>
				<?php /* ?><span><i class="fa fa-briefcase"></i> <?php the_company_name();?></span>				<?php */ ?>


				<?php
				$rate_min = get_post_meta( $post->ID, '_rate_min', true );
				if ( $rate_min) {
					$rate_max = get_post_meta( $post->ID, '_rate_max', true );  ?>
					<span>
						<i class="fa fa-money"></i> <?php echo get_workscout_currency_symbol(); echo esc_html( $rate_min ); if(!empty($rate_max)) { echo '- '.$rate_max; } ?> <?php esc_html_e('/ hour','workscout'); ?>
					</span>
				<?php } ?>

				<?php
				$salary_min = get_post_meta( $post->ID, '_salary_min', true );
				if ( $salary_min ) {
					$salary_max = get_post_meta( $post->ID, '_salary_max', true );  ?>
					<span>
						<i class="fa fa-money"></i>
						<?php echo get_workscout_currency_symbol(); echo esc_html( $salary_min ) ?> <?php if(!empty($salary_max)) { echo '- '.$salary_max; } ?>
					</span>
				<?php } ?>


				<?php do_action( 'workscout_job_listing_meta_end' ); ?>
			</div>
			<div class="listing-desc"><?php the_excerpt(); ?></div>
		</div>
	</div>
<div class="clearfix"></div>
</li>