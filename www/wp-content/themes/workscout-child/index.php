<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WorkScout
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

<?php $header_image = Kirki::get_option( 'workscout', 'pp_blog_header_upload', '' );  
if(!empty($header_image)) { ?>
		<div id="titlebar" class="photo-bg single" style="background: url('<?php echo esc_url($header_image); ?>')">
	<?php } else { ?>
		<div id="titlebar" class="single">
<?php } ?>
		<div class="container">

			<div class="sixteen columns">
				<h1><?php echo Kirki::get_option( 'workscout', 'pp_blog_title' ); ?></h1>
				<?php if(function_exists('bcn_display')) { ?>
		        <nav id="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
					<ul>
			        	<?php bcn_display_list(); ?>
			        </ul>
				</nav>
				<?php } ?>
			</div>

		</div>
	</div>
<?php 

$layout = Kirki::get_option( 'workscout', 'pp_blog_layout' ); ?>
<!-- Content
================================================== -->
<div class="container <?php echo esc_attr($layout); ?>">

	<!-- Blog Posts -->
	<div class="eleven columns">
		<?php if (is_home()): ?>
			<select id="postSorting" style="margin-bottom: 20px; width: 150px; padding: 5px">
				<option value="1" data-linkk="<?php bloginfo('url'); ?>/blog/">Newest to Oldest</option>
				<option value="2" data-linkk="<?php bloginfo('url'); ?>/blog/?sort=2" <?php if (isset($_GET['sort'])): ?>
					selected="selected"
				<?php endif ?>>Oldest to Newest</option>
			</select>
		<?php endif ?>
		<div class="padding-right">
		<?php while ( have_posts() ) : the_post(); ?>
				
			<?php
				get_template_part( 'template-parts/content', 'blog' );
			?>

			<?php endwhile; ?>

			<?php if(function_exists('wp_pagenavi')) { 
				wp_pagenavi(array(
					'next_text' => '<i class="fa fa-chevron-right"></i>',
					'prev_text' => '<i class="fa fa-chevron-left"></i>',
					'use_pagenavi_css' => false,
					));
			} else {
				workscout_posts_navigation(array(
		 			'prev_text'  => ' ',
		            'next_text'  => ' ',
				)); 
			} ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>


		</div>
	</div>
	<!-- Blog Posts / End -->
	<div class="five columns sidebar">
		<?php dynamic_sidebar('blog_sidebar'); ?>
	</div>
	<?php //get_sidebar(); ?>

</div>

<?php get_footer(); ?>
