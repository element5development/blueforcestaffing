<?php
/**
 * Template Name: Team
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WPVoyager
 */

get_header(); 

while ( have_posts() ) : the_post(); 
?>
<!-- Titlebar
================================================== -->
<?php 
$submit_job_page = get_option('job_manager_submit_job_form_page_id');
$resume_job_page = get_option('resume_manager_submit_resume_form_page_id');
if (!empty($submit_job_page) && is_page($submit_job_page) || !empty($resume_job_page) && is_page($resume_job_page)) { ?>
	<!-- Titlebar
	================================================== -->
	
	<?php $header_image = get_post_meta($post->ID, 'pp_job_header_bg', TRUE); 
	if(!empty($header_image)) { ?>
		<div id="titlebar" class="photo-bg single submit-page" style="background: url('<?php echo esc_url($header_image); ?>')">
	<?php } else { ?>
		<div id="titlebar" class="single submit-page">
	<?php } ?>
		<div class="container">

			<div class="sixteen columns">
				<h2><i class="fa fa-plus-circle"></i> <?php the_title(); ?></h2>
			</div>

		</div>
	</div>
<?php } else { ?>
	<?php $header_image = get_post_meta($post->ID, 'pp_job_header_bg', TRUE); 
	if(!empty($header_image)) { ?>
		<div id="titlebar" class="photo-bg single" style="background: url('<?php echo esc_url($header_image); ?>')">
	<?php } else { ?>
		<div id="titlebar" class="single">
	<?php } ?>
		<div class="container">

			<div class="sixteen columns">
				<h1><?php the_title(); ?></h1>
				<?php if(function_exists('bcn_display')) { ?>
		        <nav id="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
					<ul>
			        	<?php bcn_display_list(); ?>
			        </ul>
				</nav>
				<?php } ?>
			</div>
		</div>
	</div>
<?php 
}

$layout  = get_post_meta($post->ID, 'pp_sidebar_layout', true);
if(empty($layout)) { $layout = 'full-width'; }
$class = ($layout !="full-width") ? "eleven columns" : "sixteen columns" ;

?>
<a href="#" tabindex="1" id="focusHere"></a>
<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
		<div class="clearfix teamMember">
			<div class="bigDiv">
				
			</div>
		<?php if( have_rows('team_members') ): while ( have_rows('team_members') ) : the_row(); ?>
			<div class="">
				<a href="javascript:void(0);" class="readMoreeCol">X</a>
				<img src="<?php the_sub_field('image'); ?>" style="max-width: 100%;">
				<h3><?php the_sub_field('name'); ?></h3>
				<h5><?php the_sub_field('title'); ?></h5>
				<div class="contentss">
				<?php the_sub_field('content'); ?>
				</div>
				<a class="mail" href="mailto:<?php the_sub_field('contact_email'); ?>"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php the_sub_field('contact_email'); ?></a>
				<a href="javascript:void(0);" class="readMoree">Read More</a>
			</div>
		<?php endwhile; endif; ?>
		<br clear="all">
		</div>
	</article>
</div>

<?php

endwhile; // End of the loop. 

get_footer(); 
?>
<script type="text/javascript">
	jQuery(function($){
		$('.teamMember > div a.readMoree, .teamMember > div img').click(function(e){
            e.preventDefault();
			if($(window).width() > 767) {
                $('.teamMember > div').show();
                $('.teamMember .bigDiv').hide();
                var first = $(this).parent();
                $(this).parent().hide();
                $(this).parent().parent().find('.bigDiv').show();
                $(this).parent().parent().find('.bigDiv').html($(this).parent().html());
                if($(window).width() > 992) {
                   $(this).parent().parent().find('.bigDiv').css('height', $('.teamMember').css('height'));
                }     
                
                $('#focusHere').focus();
                $('.teamMember > div a.readMoreeCol').click(function(){
                    first.show();
                    $('.bigDiv').html('');
                    $('.bigDiv').hide();
                });
            } else {
                $('body .teamMember > div').removeAttr('style').find('.contentss').removeAttr('style');
                $(this).parent().css('height', '100%');
                $(this).parent().find('.contentss').css('height', '100%');
            }
			// $(this).parent().css('width', '50%');
			// $(this).parent().css('height', 'auto');
			// $(this).parent().find('.contentss').css('height', 'auto');
			// $(this).parent().find('.readMoreeCol').show();
		});
		$('.teamMember > div a.readMoreeCol').click(function(){
			// $(this).hide();
			// $(this).parent().css('width', '25%');
			// $(this).parent().css('height', '470px');
			// $(this).parent().find('.contentss').css('height', '125px');
			// $(this).parent().find('.readMoree').show();
		});
	});
</script>