<?php
// function defer_parsing_of_js ( $url ) {
// if ( FALSE === strpos( $url, '.js' ) ) return $url;
// if ( strpos( $url, 'jquery.js' ) ) return $url;
// return "$url' defer ";
// }
// if (!is_admin()) {
// add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
// }

add_action( 'init', 'create_bullhorn_category_nonhierarchical_taxonomy', 0 );
 
function create_bullhorn_category_nonhierarchical_taxonomy() {
  $labels = array(
    'name' => _x( 'Bullhorn Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Bullhorn Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Bullhorn Categories' ),
    'popular_items' => __( 'Popular Bullhorn Categories' ),
    'all_items' => __( 'All Bullhorn Categories' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Bullhorn Category' ), 
    'update_item' => __( 'Update Bullhorn Category' ),
    'add_new_item' => __( 'Add New Bullhorn Category' ),
    'new_item_name' => __( 'New Bullhorn Category Name' ),
    'separate_items_with_commas' => __( 'Separate bullhorn categories with commas' ),
    'add_or_remove_items' => __( 'Add or remove bullhorn categories' ),
    'choose_from_most_used' => __( 'Choose from the most used bullhorn categories' ),
    'menu_name' => __( 'Bullhorn Categories' ),
  ); 
 
  register_taxonomy('bullhorn_categories','job_listing',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'bullhorn_categories' ),
  ));
}

function delete_form_fields($fields) {
	unset($fields['_hourly_max']);
	unset($fields['_hourly']);
	unset($fields['_address']);
	unset($fields['_compact_state']);
	unset($fields['_apply_link']);
	unset($fields['_application']);
	unset($fields['_job_expires']);
	unset($fields['_company_name']);
	unset($fields['_company_website']);
	unset($fields['_company_tagline']);
	unset($fields['_company_twitter']);
	unset($fields['_company_video']);
	// echo "<pre>";
	// print_r($fields);
	// die;
	return $fields;
}
add_filter('job_manager_job_listing_data_fields', 'delete_form_fields');



function change_post_menu_label() {
    global $menu;
    global $submenu;
    $submenu['edit.php?post_type=job_listing'][16][0] = 'Specialties';
    $submenu['edit.php?post_type=job_listing'][17][0] = 'Benefits & Incidentals';
    echo '';
}
add_action( 'admin_menu', 'change_post_menu_label' );

register_nav_menus( array(
	'footer_menu' => 'Footer Menu',
) );
function file_get_contents_curl($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function cron_action() {
    // create curl resource
	if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
		file_get_contents_curl("http://e5blueforce.staging.wpengine.com/webServices/bullhorn-api.php");
	}
	else {
		// wp_mail('ahsanr@addiedigital.com', 'Alert: Cron Runs', 'Testing Cron');
		file_get_contents_curl("http://blueforcestaffing.com/webServices/bullhorn-api.php?".rand());
	}
}
add_action('cron_action', 'cron_action');

function cron_action2() {
	global $wpdb;
	$dd = date('Y-m-d', strtotime('-30 days'));
	$oldate = $dd.' 00:00:00';
	$adcdscz  = $wpdb->query( 
		$wpdb->prepare( 
			"
			 DELETE FROM wp_expired_jobs
			 WHERE added_at < %s
			",
		        $oldate 
	        )
	);
}
add_action('cron_action2', 'cron_action2');


function cron_action3() {
	if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
		file_get_contents_curl("http://e5blueforce.staging.wpengine.com/webServices/delete-old-post.php");
	}
	else {
		file_get_contents_curl("http://blueforcestaffing.com/webServices/delete-old-post.php?".rand());
	}
}
add_action('cron_action3', 'cron_action3');


function remove_menus(){
	remove_menu_page( 'edit.php?post_type=job_application' );
}
add_action( 'admin_menu', 'remove_menus' );


function rudr_post_tags_meta_box_remove() {
	$id = 'tagsdiv-job_listing_tag'; // you can find it in a page source code (Ctrl+U)
	$post_type = 'job_listing'; // remove only from post edit screen
	$position = 'side';
	remove_meta_box( $id, $post_type, $position );
}
add_action( 'admin_menu', 'rudr_post_tags_meta_box_remove');



function new_add_new_tags_metabox(){
	$id = 'tagsdiv-job_listing_tag'; // it should be unique
	$heading = 'Job Shifts'; // meta box heading
	$callback = 'new_metabox_content'; // the name of the callback function
	$post_type = 'job_listing';
	$position = 'side';
	$pri = 'default'; // priority, 'default' is good for us
	add_meta_box( $id, $heading, $callback, $post_type, $position, $pri );
}
add_action( 'admin_menu', 'new_add_new_tags_metabox');

/*
 * Fill
 */
function new_metabox_content($post) {

	// get all blog post tags as an array of objects
	$all_tags = get_terms('job_listing_tag', array('hide_empty' => 0) );

	// get all tags assigned to a post
	$all_tags_of_post = get_the_terms( $post->ID, 'job_listing_tag' );

	// create an array of post tags ids
	$ids = array();
	if ( $all_tags_of_post ) {
		foreach ($all_tags_of_post as $tag ) {
			$ids[] = $tag->term_id;
		}
	}

	// HTML
	echo '<div id="taxonomy-job_listing_tag" class="categorydiv">';
	echo '<input type="hidden" name="tax_input[job_listing_tag][]" value="0" />';
	echo '<ul>';
	foreach( $all_tags as $tag ){
		// unchecked by default
		$checked = "";
		// if an ID of a tag in the loop is in the array of assigned post tags - then check the checkbox
		if ( in_array( $tag->term_id, $ids ) ) {
			$checked = " checked='checked'";
		}
		$id = 'job_listing_tag-' . $tag->term_id;
		echo "<li id='{$id}'>";
		echo "<label><input type='checkbox' name='tax_input[job_listing_tag][]' id='in-$id'". $checked ." value='$tag->slug' /> $tag->name</label><br />";
		echo "</li>";
	}
	echo '</ul></div>'; // end HTML
}

/*----------------------------------------------------------------*\
    ADDITIONAL FILE FORMATS
\*----------------------------------------------------------------*/
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	$mimes['zip'] = 'application/zip';
	return $mimes;
 }
 add_filter('upload_mimes', 'cc_mime_types');

/*----------------------------------------------------------------*\
		ENQUEUE JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
	wp_enqueue_style('ebook', get_stylesheet_directory_uri() . '/css/ebook.css', array('workscout-base'), '1.0', 'all');
	wp_enqueue_style('featherlight', get_stylesheet_directory_uri() . '/css/featherlight.css', array('workscout-base'), '1.0', 'all');
	wp_enqueue_script('main', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script('featherlight', get_stylesheet_directory_uri() . '/js/featherlight.js', array( 'jquery' ), '1.0', true );
}
add_action('wp_enqueue_scripts', 'wp_main_assets');


add_action( 'wp_enqueue_scripts', 'workscout_enqueue_styles' );
function workscout_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css',array('workscout-base','workscout-responsive','workscout-font-awesome') );

}

add_filter( 'job_manager_get_listings', 'workscout_filter_by_bulljobid', 10, 2 );
function workscout_filter_by_bulljobid( $query_args, $args ) {
	if($query_args['_keyword'] != '') {
		if (count(get_posts(array('post_type' => 'job_listing', 'meta_key' => 'bullhorn_job_id', 'meta_value' => $query_args['_keyword'])))) {
		 	$query_args['meta_query'][] = array(
		 		'relation' => 'AND',
			 	array(
	                'key'   => 'bullhorn_job_id',
	                'value' => $query_args['_keyword']
	            )
	        );
	        
	        add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
    return $query_args;
}


// add_filter( 'job_manager_get_listings', 'workscout_filter_by_mera_location', 10, 2 );

function workscout_filter_by_mera_location( $query_args, $args ) {
	if ( isset( $_GET['search_keywords'] ) ) {
	 	// $query_args['meta_query'][] = array(
		 // 	'relation' => 'OR',
			//  	array(
	  //               'key'   => '_job_location',
	  //               'value' => $_GET['search_keywords'],
   //                  'compare' => 'LIKE',
	  //           )
   //      );
	}
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if( isset( $form_data['search_keywords']) ) {
		 	// $query_args['meta_query'][] = array(
			 // 	'relation' => 'OR',
				//  	array(
		  //               'key'   => '_job_location',
		  //               'value' => $form_data['search_keywords'],
	   //                  'compare' => 'LIKE',
		  //           )
	   //      );
	 	}

		// This will show the 'reset' link
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
	} else {
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
	}
	// print_r($_POST['form_data']);
	// print_r($args);
	// die;
	return $query_args;
	// die();
	// if ( isset( $_POST['form_data'] ) ) {
	// 	parse_str( $_POST['form_data'], $form_data );
	// 	if( isset( $form_data['company_field']) ) {
	// 		$query_args['meta_query'] = array(
	// 		 	array(
	//                 'key'   => '_company_name',
	//                 'value' => $form_data['company_field']
	//             )
	//         );
	// 	}
	// }
	// add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
	// return $query_args;

}
add_filter( 'job_manager_get_listings', 'workscout_filter_by_gross', 10, 2 );

function workscout_filter_by_gross( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		$form_datasearch_gross = str_replace('$', '', $form_data['search_gross']);
		// If this is set, we are filtering by salary
		if( isset( $form_data['search_gross']) ) {
			if ( ! empty( $form_data['search_gross'] ) ) {

				 	$query_args['meta_query'][] = array(
					 	'relation' => 'OR',

						 	array(
				                'key'   => '_job_weekly_gross',
				                'value' => $form_datasearch_gross,
                                'compare' => '>=',
                                'type' => 'NUMERIC'
				            )
			        );

				// This will show the 'reset' link
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
			}
		} else {
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
	// die();
	// if ( isset( $_POST['form_data'] ) ) {
	// 	parse_str( $_POST['form_data'], $form_data );
	// 	if( isset( $form_data['company_field']) ) {
	// 		$query_args['meta_query'] = array(
	// 		 	array(
	//                 'key'   => '_company_name',
	//                 'value' => $form_data['company_field']
	//             )
	//         );
	// 	}
	// }
	// add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
	// return $query_args;

}
					// echo date('Y-m-d', strtotime($Date. ' + 30 days'));
					// die();

add_filter( 'job_manager_get_listings', 'workscout_filter_by_date_o', 10, 2 );

function workscout_filter_by_date_o( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );

		// If this is set, we are filtering by salary
		if( isset( $form_data['search_date']) ) {
			if ( ! empty( $form_data['search_date'] ) ) {
				$selected_range = sanitize_text_field( $form_data['search_date'] );

					$range = str_replace(' ', '', $selected_range);
					$range = explode( 'to', $range );
					$range0 = explode('-', $range[0]);
					$range1 = explode('-', $range[1]);
					$range00 = $range0[2].'-'.$range0[0].'-'.$range0[1];
					$range01 = $range1[2].'-'.$range1[0].'-'.$range1[1];
					$range = array(0=>$range00, 1=>$range01);
					// print_r($range);
				 	$query_args['meta_query'][] = array(
						'relation' => 'OR', // Optional, defaults to "AND"
						array(
				            'relation' => 'OR',
                            array(
                                'key' => '_application_deadline',
                                'value' => $range,
                                'compare' => 'BETWEEN',
                                'type' => 'DATE',
                            ),
						 	array(
				                'key'   => '_orientation_date_asap',
				                'value' => 1,
                                'compare' => '=',
                                'type' => 'NUMERIC'
				            )
						),
					 	array(
			                'key'   => '_orientation_date_asap',
			                'value' => 1,
                            'compare' => '=',
                            'type' => 'NUMERIC'
			            )
					);
				// This will show the 'reset' link
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
			}
		} else {
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;

}


add_filter( 'job_manager_get_listings', 'workscout_filter_by_location_region', 10, 2 );

function workscout_filter_by_location_region( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
			# code...
// echo "<pre>";
// print_r($form_data['search_location_region']);
// die();
		// If this is set, we are filtering by salary
		if( isset( $form_data['search_location_region_all']) ) {
			if ( ! empty( $form_data['search_location_region_all'] ) ) {
				$query_args['tax_query'] = array(
				 	'relation' => 'AND',
					array(
						'taxonomy' => 'job_listing_region',
						'terms' => $form_data['search_location_region_all'],
						'field' => 'slug',
					),
				);
			}
		}
		
		if( isset( $form_data['search_location_region']) ) {
			if ( ! empty( $form_data['search_location_region'] ) ) {


				if ($form_data['search_location_region'] == 'northeast') { 
				    $locationArr = array(
				        'Connecticut (CT)',
				        'Maine (ME)',
				        'Massachusetts (MA)',
				        'New Hampshire (NH',
				        'Rhode Island (RI)',
				        'Vermont (VT)',
				        'New Jersey (NJ)',
				        'New York (NY)',
				        'Pennsylvania (PA)',
				        'Delaware (DE)',
				        'Maryland (MD)'
				    );
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => $locationArr,
							'field' => 'name',
						),
					);
				} else if ($form_data['search_location_region'] == 'midwest') { 
				    $locationArr = array(
				        'Illinois (IL)',
				        'Indiana (IN)',
				        'Michigan (MI)',
				        'Ohio (OH)',
				        'South Dakota (SD)',
				        'Wisconsin (WI)',
				        'Iowa (IA)',
				        'Kansas (KS)',
				        'Minnesota (MN)',
				        'Missouri (MO)',
				        'Nebraska (NE)',
				        'North Dakota (ND)'
				    );
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => $locationArr,
							'field' => 'name',
						),
					);
				} else if ($form_data['search_location_region'] == 'southwest') { 
				    $locationArr = array(
				        'Texas (TX)',
				        'Oklahoma (OK)',
				        'New Mexico (NM)',
				        'Arizona (AZ)'
				    );
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => $locationArr,
							'field' => 'name',
						),
					);
				} else if ($form_data['search_location_region'] == 'southeast') { 
				    $locationArr = array(
				        'West Virginia (WV)',
				        'Virginia (VA)',
				        'Kentucky (KY)',
				        'Tennessee (TN)',
				        'North Carolina (NC)',
				        'South Carolina (SC)',
				        'Georgia (GA)',
				        'Alabama (AL)',
				        'Mississippi (MS)',
				        'Arkansas (AR)',
				        'Louisiana (LA)',
				        'Florida (FL)'
				    );
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => $locationArr,
							'field' => 'name',
						),
					);
				} else if ($form_data['search_location_region'] == 'west') { 
				    $locationArr = array(
				        'Colorado (CO)',
				        'Idaho (ID)',
				        'Montana (MT)',
				        'Nevada (NV)',
				        'New Mexico (NM)',
				        'Utah (UT)',
				        'Wyoming (WY)',
				        'Alaska (AK)',
				        'California (CA)',
				        'Hawaii (HI)',
				        'Oregon (OR)',
				        'Washington (WA)'
				    );
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => $locationArr,
							'field' => 'name',
						),
					);
				} else if ($form_data['search_location_region'] == 'compact-state-multi-state-license') { 
				    $locationArr = array(
						'Arizona (AZ)',
						'Arkansas (AR)',
						'Colorado (CO)',
						'Delaware (DE)',
						'Florida (FL)',
						'Georgia (GA)',
						'Idaho (ID)',
						'Iowa (IA)',
						'Kansas (KS)',
						'Kentucky (KY)',
						'Louisiana (LA)',
						'Maine (ME)',
						'Maryland (MD)',
						'Mississippi (MS)',
						'Missouri (MO)',
						'Montana (MT)',
						'Nebraska (NE)',
						'New Hampshire (NH',
						'New Mexico (NM)',
						'North Carolina (NC)',
						'North Dakota (ND)',
						'Oklahoma (OK)',
						'South Carolina (SC)',
						'South Dakota (SD)',
						'Tennessee (TN)',
						'Texas (TX)',
						'Utah (UT)',
						'Virginia (VA)',
						'West Virginia (WV)',
						'Wisconsin (WI)',
						'Wyoming (WY)'
				    );
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => $locationArr,
							'field' => 'name',
						),
					);
				} else {
					$query_args['tax_query'] = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_listing_region',
							'terms' => array($form_data['search_location_region']),
							'field' => 'slug',
						),
					);
				}
				 	/*$query_args['meta_query'][] = array(
					 	'relation' => 'OR',

						 	array(
				                'key'   => '_job_location',
				                'value' => $form_data['search_location_region'],
				                'compare' => 'LIKE'
				            )
			        );*/


				/*	$location_info = get_term_by('slug',$form_data['search_location_region'],'job_listing_region');



					if(isset($location_info->parent) &&  $location_info->parent == 0){
						 $job_child_terms = get_terms( array(
							'taxonomy' => 'job_listing_region',
							'hide_empty' => false,
							'exclude'=>array(0),
							'parent'=>$location_info->term_id,
						) );
						if(0<count($job_child_terms)){
							$array_jobs_region =array();
							$array_jobs_region['relation'] ='OR';
							  foreach($job_child_terms as $key=>$Job_states)
							  {

								  $array_jobs_region[] =  array(
											'key'   => '_state',
											'value' => $Job_states->slug,
											'compare' => 'LIKE'
										);

							  }

							  $query_args['meta_query'][] = $array_jobs_region;
						 }
						}else{
					$query_args['meta_query'][] = array(
					 	'relation' => 'OR',

						 	array(
				                'key'   => '_state',
				                'value' => $form_data['search_location_region'],
				                'compare' => 'LIKE'
				            )
			        );

					}
			*/

				// This will show the 'reset' link
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
			}
		} else {
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;

}




// add_filter( 'job_manager_get_listings', 'workscout_filter_by_region_location', 10, 2 );

function workscout_filter_by_region_location( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );

		// If this is set, we are filtering by salary
		if( isset( $form_data['search_location_region'])) {
			if ( ! empty( $form_data['search_location_region'] ) ) {

					$query_args['tax_query'][] = array(
					 	'relation' => 'OR',

						 	array(
						'taxonomy'         => 'job_listing_region',
						'field'            => 'name',
						'terms'            => 'New York',
					)
					);
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );

			}
		}
	}
	return $query_args;
}


add_filter( 'job_manager_get_listings', 'workscout_filter_by_search_specialty', 10, 2 );

function workscout_filter_by_search_specialty( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
			# code...
// echo "<pre>";
// print_r($form_data['search_categories']);

$selected_specialties = $form_data['search_categories'];
global $wpdb;

// $web_specialties = $wpdb->get_results( "SELECT * FROM specialty_mapping WHERE web_specialty_1 = '".$selected_specialties[0]."' OR web_specialty_2 = '".$selected_specialties[0]."' OR web_specialty_3 = '".$selected_specialties[0]."' OR web_specialty_4 = '".$selected_specialties[0]."' OR web_specialty_5 = '".$selected_specialties[0]."'", OBJECT );
$web_specialties = $wpdb->get_results( "SELECT * FROM specialty_mapping WHERE '".$selected_specialties[0]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) ", OBJECT );

if (isset($selected_specialties[1])) {
	$web_specialties = $wpdb->get_results( "SELECT * FROM specialty_mapping WHERE '".$selected_specialties[0]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[1]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5)", OBJECT );
}
if (isset($selected_specialties[2])) {
	$web_specialties = $wpdb->get_results( "SELECT * FROM specialty_mapping WHERE '".$selected_specialties[0]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[1]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[2]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5)", OBJECT );
}
if (isset($selected_specialties[3])) {
	$web_specialties = $wpdb->get_results( "SELECT * FROM specialty_mapping WHERE '".$selected_specialties[0]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[1]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[2]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[3]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5)", OBJECT );
}
if (isset($selected_specialties[4])) {
	$web_specialties = $wpdb->get_results( "SELECT * FROM specialty_mapping WHERE '".$selected_specialties[0]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[1]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[2]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[3]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5) AND '".$selected_specialties[4]."' IN (web_specialty_1,web_specialty_2,web_specialty_3,web_specialty_4,web_specialty_5)", OBJECT );
}

$selected_items = [];
foreach ($web_specialties as $key => $value) {
	array_push($selected_items, trim($value->specialty));
}

// print_r($web_specialties);
// print_r($selected_items);
		// If this is set, we are filtering by salary
		if( isset( $form_data['search_categories']) && 0<count($form_data['search_categories']) && $form_data['search_categories'][0]!='') {
unset($query_args['tax_query']);

						$query_args['tax_query'][] = array(
					 	'relation' => 'AND',
							array(
								'taxonomy' => 'job_listing_category',
								'terms' => $selected_items,
								'field' => 'name',
							),
						);



				// This will show the 'reset' link
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );


		}
	}
// print_r($query_args);
	return $query_args;

}

add_filter( 'job_manager_get_listings', 'workscout_filter_by_job_shifts', 10, 2 );

function workscout_filter_by_job_shifts( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// if (isset($form_data['filter_job_shift_type']) || isset($form_data['filter_job_shift_length'])) {
		if (isset($form_data['filter_job_shift'])) {
			$terms = '';
			$shift = '';
			$shift_type = '';
			$shift_length = '';

			// if (isset($form_data['filter_job_shift_type'])) {
			// 	$shift_type = array_filter($form_data['filter_job_shift_type']);
			// }
			// if (isset($form_data['filter_job_shift_length'])) {
			// 	$shift_length = array_filter($form_data['filter_job_shift_length']);
			// }
			if (isset($form_data['filter_job_shift'])) {
				$shift = array_filter($form_data['filter_job_shift']);
			}

			// if ($shift || $shift_type || $shift_length) {
			if ($shift) {

				$terms =  get_terms( "job_listing_tag", array(
					'orderby'    => 'ID',
					'order'      => 'ASC',
					'hide_empty' => false,
					'fields'     => 'id=>slug'
				) );

			}

			//echo "<pre>";
			// print_r($shift_type);
			// if($shift_type) {
			// 	$terms = array_filter($terms, function($v, $k) use($shift_type) {
			// 		// list($type, $length) = explode('-', $v, 2);
			// 	    return in_array($type , $shift_type);
			// 	}, ARRAY_FILTER_USE_BOTH);
			// }


			// // print_r($shift_length);
			// if($shift_length) {
			// 	$terms = array_filter($terms, function($v, $k) use($shift_length) {
			// 		// list($type, $length) = explode('-', $v, 2);
			// 	    return in_array($length , $shift_length);
			// 	}, ARRAY_FILTER_USE_BOTH);
			// }

			// print_r($shift);
			if($shift) {
				$terms = array_filter($terms, function($v, $k) use($shift) {
				    return in_array($v , $shift);
				}, ARRAY_FILTER_USE_BOTH);
				// $terms = $shift;
			}


				// print_r($terms);


				// die();


			if($terms) {
				$shift1 = [];
				$shift2 = [];
				$cond1 = [];
				$cond2 = [];
				if (in_array('days', $shift)) {
					array_push($shift1,'days');
				}
				if (in_array('swing', $shift)) {
					array_push($shift1,'swing');
				}
				if (in_array('nights', $shift)) {
					array_push($shift1,'nights');
				}
				if (in_array('rotating', $shift)) {
					array_push($shift1,'rotating');
				}
				if (in_array('flexible', $shift)) {
					array_push($shift1,'flexible');
				}
				if (in_array('to-be-determined', $shift)) {
					array_push($shift1,'to-be-determined');
				}
				if (in_array('8-hours', $shift)) {
					array_push($shift2,'8-hours');
				}
				if (in_array('10-hours', $shift)) {
					array_push($shift2,'10-hours');
				}
				if (in_array('12-hours', $shift)) {
					array_push($shift2,'12-hours');
				}
				// print_r(count($shift1));
				// if (count($shift1) == 0) {
				// 	$shift1 = ['days','swing','nights','rotating','flexible','to-be-determined'];
				// }
				// if (count($shift2) == 0) {
				// 	$shift2 = ['8-hours', '10-hours', '12-hours'];
				// }
				// print_r($shift2);
				if (count($shift1)) {
					$query_args['tax_query'][] = array(
						 	'relation' => 'AND',
							 	array(
									'taxonomy'         => 'job_listing_tag',
									'field'            => 'slug',
									'terms'            => $shift1,
									// 'operator'         => 'OR'
								)
					);
				}
				if (count($shift2)) {
					$query_args['tax_query'][] = array(
						 	'relation' => 'AND',
							 	array(
									'taxonomy'         => 'job_listing_tag',
									'field'            => 'slug',
									'terms'            => $shift2,
									// 'operator'         => 'OR'
								)
					);
				}
				if (count($shift1) && count($shift2)) {
					$query_args['tax_query'][] = array(
						 	'relation' => 'AND',
							 	array(
									'taxonomy'         => 'job_listing_tag',
									'field'            => 'slug',
									'terms'            => $shift1,
									// 'operator'         => 'OR'
								),
							 	array(
									'taxonomy'         => 'job_listing_tag',
									'field'            => 'slug',
									'terms'            => $shift2,
									// 'operator'         => 'OR'
								)
					);
				}
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
			}


			/*if ($form_data['filter_job_shift'][0] != '') {
				# code...
				echo "<pre>";
				print_r($form_data['filter_job_shift']); die();
				// If this is set, we are filtering by salary
				if( isset( $form_data['filter_job_shift'])) {
					if ( ! empty( $form_data['filter_job_shift'] ) ) {

							$query_args['tax_query'][] = array(
							 	'relation' => 'OR',
								 	array(
										'taxonomy'         => 'job_listing_tag',
										'field'            => 'slug',
										'terms'            => $form_data['filter_job_shift'],
									)
							);
						add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );

					}
				}
			}*/
		}
	}
	$query_args['meta_key'] = '_job_weekly_gross';
	$query_args['meta_type'] = 'DECIMAL';
	// print_r($query_args);
	// die;
	return $query_args;
}

function workscout_featured_jobs22( $atts ) {
    ob_start();

    extract( $atts = shortcode_atts( apply_filters( 'job_manager_output_jobs_defaults', array(
        'per_page'                  => get_option( 'job_manager_per_page' ),
        'orderby'                   => 'featured',
        'order'                     => 'DESC',
        'title'                     => 'Job Spotlight',
        'visible'                   => '1,1,1,1',

        // Limit what jobs are shown based on category and type
        'categories'                => '',
        'job_types'                 => '',
        'featured'                  => true, // True to show only featured, false to hide featured, leave null to show both.
        'filled'                    => null, // True to show only filled, false to hide filled, leave null to show both/use the settings.


    ) ), $atts ) );

    $randID = rand(1, 99);

    if ( ! is_null( $filled ) ) {
        $filled = ( is_bool( $filled ) && $filled ) || in_array( $filled, array( '1', 'true', 'yes' ) ) ? true : false;
    }

    // Array handling
    $categories         = is_array( $categories ) ? $categories : array_filter( array_map( 'trim', explode( ',', $categories ) ) );
    $job_types          = is_array( $job_types ) ? $job_types : array_filter( array_map( 'trim', explode( ',', $job_types ) ) );
    if ( ! is_null( $featured ) ) {
        $featured = ( is_bool( $featured ) && $featured ) || in_array( $featured, array( '1', 'true', 'yes' ) ) ? true : false;
    }
	$catsss = '';
	$bullhorn_categories = $_COOKIE['cat_selection'];
	if ('nursing' == $bullhorn_categories) {
		if ($catsss != '') {
			$catsss .= ',';
		}
		$catsss .= 'registered-nurse-rn,licensed-practical-vocational-nurse-lpn-lvn,certified-nursing-assistant-cna';
	}
	if ('providers' == $bullhorn_categories) {
		if ($catsss != '') {
			$catsss .= ',';
		}
		$catsss .= 'provider,physician-do,physician-md,physician-assistant-pa,nurse-practitioner-np';
	}
	if ('allied' == $bullhorn_categories) {
		if ($catsss != '') {
			$catsss .= ',';
		}
		$catsss .= 'allied-health-professional,service-health-professional';
	}
    $jobs = get_job_listings(  array(
            'search_categories' => $categories,
            'job_types'         => $job_types,
            'orderby'           => $orderby,
            'order'             => $order,
            'posts_per_page'    => 12,
            'featured'          => $featured,
            'filled'            => $filled,
            'bullhorn_categories' => explode(",", $catsss)
        )  );

   if ( $jobs->have_posts() ) : ?>

        <h1 class="margin-bottom-5"><?php echo esc_html($title); ?></h1>
        <!-- Navigation -->
        <div class="showbiz-navigation">
            <div id="showbiz_left_001" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
            <div id="showbiz_right_001" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
        </div>
        <div class="clearfix"></div>

        <!-- Showbiz Container -->
        <div id="job-spotlight" class="job-spotlight-car2 showbiz-container" data-visible="[<?php echo $visible; ?>]">
            <div class="showbiz" data-left="#showbiz_left_<?php echo esc_attr($randID); ?>" data-right="#showbiz_right_<?php echo esc_attr($randID); ?>" data-play="#showbiz_play_<?php echo esc_attr($randID); ?>" >
                <div class="overflowholder2">
                    <ul>
                      <?php while ( $jobs->have_posts() ) : $jobs->the_post();
                        $id = get_the_id(); ?>
                        <li>
                            <div class="job-spotlight">
                                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?><br>
									<div class="job-spotlight-job-tags">
                                	<?php $types = wp_get_post_terms( $id, 'job_listing_type' ); ?>
									<?php foreach ($types as $key => $value): ?>
										<span title="<?php if($value->slug == 'travel'){echo "Up to ";}?>$<?php echo get_post_meta(get_the_id(), '_job_type_'.$value->slug, true); ?>" class="tooltip job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>" style="opacity: 1;">
											<?php echo $value->name; ?>
										</span>
									<?php endforeach; ?>
									</div>
                                </h4></a>
                                <?php $Weekly_Gross = get_post_meta( get_the_id(), '_job_weekly_gross', true );
					 if ( $Weekly_Gross ) {
					 	?>
					<span><i class="fa fa-money"></i> $<?php echo esc_html( $Weekly_Gross ) ?></span><br>
					<?php } ?>
                                <span><i class="fa fa-map-marker"></i> <?php echo get_post_meta( get_the_id(), '_job_location', true ); ?></span><br>

                                <?php
                                $rate_min = get_post_meta( $id, '_rate_min', true );
                                if ( $rate_min) {
                                    $rate_max = get_post_meta( $id, '_rate_max', true );  ?>
                                    <span>
                                        <i class="fa fa-money"></i> <?php  echo get_workscout_currency_symbol();  echo esc_html( $rate_min ); if(!empty($rate_max)) { echo '- '.$rate_max; } ?> / hour
                                    </span><br>
                                <?php } ?>

                                <?php
                                $salary_min = get_post_meta( $id, '_salary_min', true );
                                if ( $salary_min ) {
                                    $salary_max = get_post_meta( $id, '_salary_max', true );  ?>
                                    <span>
                                        <i class="fa fa-money"></i>
                                        <?php echo get_workscout_currency_symbol(); echo esc_html( $salary_min ) ?> <?php if(!empty($salary_max)) { echo '- '.$salary_max; } ?>
                                    </span><br>
                                <?php } ?>

                                <a href="<?php the_permalink(); ?>" class="button"><?php esc_html_e('Apply For This Job','workscout') ?></a>
                            </div>
                        </li>
                        <?php endwhile; ?>

                    </ul>
                    <div class="clearfix"></div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php
    endif;
    $job_listings_output =  ob_get_clean();

    return $job_listings_output;

}
	add_shortcode('spotlight_jobs2', 'workscout_featured_jobs22');
function remove_parent_theme_features() {
	remove_shortcode( 'spotlight_jobs' );
	add_shortcode('spotlight_jobs', 'workscout_featured_jobs2');
}
add_action( 'init', 'remove_parent_theme_features', 10 );

function workscout_featured_jobs2( $atts ) {
    ob_start();

    extract( $atts = shortcode_atts( apply_filters( 'job_manager_output_jobs_defaults', array(
        'per_page'                  => get_option( 'job_manager_per_page' ),
        'orderby'                   => 'date',
        'order'                     => 'DESC',
        'title'                     => 'Job Spotlight',
        'visible'                   => '1,1,1,1',

        // Limit what jobs are shown based on category and type
        'categories'                => '',
        'yotitle'                => false,
        'job_types'                 => '',
        'featured'                  => true, // True to show only featured, false to hide featured, leave null to show both.
        'filled'                    => null, // True to show only filled, false to hide filled, leave null to show both/use the settings.


    ) ), $atts ) );

    $randID = rand(1, 99);

    if ( ! is_null( $filled ) ) {
        $filled = ( is_bool( $filled ) && $filled ) || in_array( $filled, array( '1', 'true', 'yes' ) ) ? true : false;
    }

    // Array handling
    $categories         = is_array( $categories ) ? $categories : array_filter( array_map( 'trim', explode( ',', $categories ) ) );
    $job_types          = is_array( $job_types ) ? $job_types : array_filter( array_map( 'trim', explode( ',', $job_types ) ) );
    if ( ! is_null( $featured ) ) {
        $featured = ( is_bool( $featured ) && $featured ) || in_array( $featured, array( '1', 'true', 'yes' ) ) ? true : false;
    }
	$catsss = '';
	$bullhorn_categories = $_COOKIE['cat_selection'];
	if ('nursing' == $bullhorn_categories) {
		if ($catsss != '') {
			$catsss .= ',';
		}
		$catsss .= 'registered-nurse-rn,licensed-practical-vocational-nurse-lpn-lvn,certified-nursing-assistant-cna';
	}
	if ('providers' == $bullhorn_categories) {
		if ($catsss != '') {
			$catsss .= ',';
		}
		$catsss .= 'provider,physician-do,physician-md,physician-assistant-pa,nurse-practitioner-np';
	}
	if ('allied' == $bullhorn_categories) {
		if ($catsss != '') {
			$catsss .= ',';
		}
		$catsss .= 'allied-health-professional,service-health-professional';
	}
    $jobs = get_job_listings(  array(
            'search_categories' => $categories,
            'job_types'         => $job_types,
            'orderby'           => $orderby,
            'order'             => $order,
            'posts_per_page'    => -1,
            'featured'          => $featured,
            'filled'            => $filled,
            'bullhorn_categories' => explode(",", $catsss)
        )  );

   if ( $jobs->have_posts() ) : ?>
   		<?php if ($yotitle): ?>
        <h1 class="margin-bottom-5" style="line-height: 38px; min-height: 95px;">Recent Jobs</h1>
   		<?php else : ?>
        <h3 class="margin-bottom-5"><?php echo esc_html($title); ?></h3>
   		<?php endif ?>
        <!-- Navigation -->
        <div class="showbiz-navigation">
            <div id="showbiz_left_<?php echo esc_attr($randID); ?>" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
            <div id="showbiz_right_<?php echo esc_attr($randID); ?>" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
        </div>
        <div class="clearfix"></div>

        <!-- Showbiz Container -->
        <div id="job-spotlight" class="job-spotlight-car showbiz-container" data-visible="[<?php echo $visible; ?>]">
            <div class="showbiz" data-left="#showbiz_left_<?php echo esc_attr($randID); ?>" data-right="#showbiz_right_<?php echo esc_attr($randID); ?>" data-play="#showbiz_play_<?php echo esc_attr($randID); ?>" >
                <div class="overflowholder">
                    <ul>
                      <?php while ( $jobs->have_posts() ) : $jobs->the_post();
                        $id = get_the_id(); ?>
                        <li>
                            <div class="job-spotlight">
                                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?><br>
	<div class="job-spotlight-job-tags">
                                	<?php $types = wp_get_post_terms( $id, 'job_listing_type' ); ?>
									<?php foreach ($types as $key => $value): ?>
										<span title="<?php if($value->slug == 'travel'){echo "Up to ";}?>$<?php echo get_post_meta(get_the_id(), '_job_type_'.$value->slug, true); ?>" class="tooltip job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>" style="opacity: 1;">
											<?php echo $value->name; ?>
										</span>
									<?php endforeach; ?>
									</div>
                                </h4></a>
                                <?php $Weekly_Gross = get_post_meta( get_the_id(), '_job_weekly_gross', true );
					 if ( $Weekly_Gross ) {
					 	?>
					<span><i class="fa fa-money"></i> $<?php echo esc_html( $Weekly_Gross ) ?></span>
					<?php } ?>
                                <span><i class="fa fa-map-marker"></i> <?php echo get_post_meta( get_the_id(), '_job_location', true ); ?></span>

                                <?php
                                $rate_min = get_post_meta( $id, '_rate_min', true );
                                if ( $rate_min) {
                                    $rate_max = get_post_meta( $id, '_rate_max', true );  ?>
                                    <span>
                                        <i class="fa fa-money"></i> <?php  echo get_workscout_currency_symbol();  echo esc_html( $rate_min ); if(!empty($rate_max)) { echo '- '.$rate_max; } ?> / hour
                                    </span>
                                <?php } ?>

                                <?php
                                $salary_min = get_post_meta( $id, '_salary_min', true );
                                if ( $salary_min ) {
                                    $salary_max = get_post_meta( $id, '_salary_max', true );  ?>
                                    <span>
                                        <i class="fa fa-money"></i>
                                        <?php echo get_workscout_currency_symbol(); echo esc_html( $salary_min ) ?> <?php if(!empty($salary_max)) { echo '- '.$salary_max; } ?>
                                    </span>
                                <?php } ?>

                                <p><?php
                                    $excerpt = get_the_excerpt();
                                    echo workscout_string_limit_words($excerpt,20); ?>...
                                </p>
                                <a href="<?php the_permalink(); ?>" class="button"><?php esc_html_e('Apply For This Job','workscout') ?></a>
                            </div>
                        </li>
                        <?php endwhile; ?>

                    </ul>
                    <div class="clearfix"></div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php
    endif;
    $job_listings_output =  ob_get_clean();

    return $job_listings_output;

}
















add_filter('uwpqsftaxo_field', 'add_multiselect_admin');
function add_multiselect_admin($fields){

	$fields['multiselect'] = 'Multi Select';
	return $fields;
}

add_filter('uwpqsf_addtax_field_multiselect','multiselect_front','',11);
function multiselect_front($type,$exc,$hide,$taxname,$taxlabel,$taxall,$opt,$c,$defaultclass,$formid,$divclass){
	$eid = explode(",", $exc);
	$args = array('hide_empty'=>$hide,'exclude'=>$eid );
	$taxoargs = apply_filters('uwpqsf_taxonomy_arg',$args,$taxname,$formid);
    $terms = get_terms($taxname,$taxoargs);
	$count = count($terms);
	$html  = '<div class="'.$defaultclass.' '.$divclass.'" id="tax-select-'.$c.'"><span class="taxolabel-'.$c.'">'.$taxlabel.'</span>';
	$html .= '<input  type="hidden" name="taxo['.$c.'][name]" value="'.$taxname.'">';
	$html .= '<input  type="hidden" name="taxo['.$c.'][opt]" value="'.$opt.'">';
	$html .=  '<select multiple id="tdp-'.$c.'" class="tdp-class-'.$c.'" name="taxo['.$c.'][term]">';
		if(!empty($taxall)){
			$html .= '<option selected value="uwpqsftaxoall">'.$taxall.'</option>';
		}
			if ( $count > 0 ){
					foreach ( $terms as $term ) {
					$selected = (isset($_GET['taxo'][$c]['term']) && $_GET['taxo'][$c]['term'] == $term->slug) ? 'selected="selected"' : '';
					$html .= '<option value="'.$term->slug.'" '.$selected.'>'.$term->name.'</option>';}
		}
	$html .= '</select>';
	$html .= '</div>';
	return $html;
}



add_action('uwpqsf_form_bottom','injecting_buttons','',4);
function injecting_buttons(){
echo '<select name="filter-specialty">';
echo '<option value="max-sal-desc">Sort By Salary - Highest First</option>';
echo '<option value="max-sal-asc">Sort By Salary - lowest First</option>';
echo '<option value="min-sal-asc">Sort By Min Salary - lowest First</option>';
echo '<option value="min-sal-desc">Sort By Min Salary - Highest First</option>';
echo '</select>';
}

add_filter('uwpqsf_query_args','changing_result_order','',3);
function changing_result_order($args, $id,$getdata){

	$orderby_value = $getdata['filter-specialty'];
	switch( $orderby_value ){
		 case 'max-sal-desc':
		 $args['meta_key'] = '_salary_max';
     $args['orderby'] = 'meta_value';
     $args['order'] = 'DESC';
		 break;

		 case 'max-sal-asc':
		 $args['meta_key'] = '_salary_max';
     $args['orderby'] = 'meta_value';
     $args['order'] = 'ASC';
		 break;

		 case 'min-sal-desc':
		 $args['meta_key'] = '_salary_min';
     $args['orderby'] = 'meta_value';
     $args['order'] = 'DESC';
		 break;

		 case 'min-sal-asc':
		 $args['meta_key'] = '_salary_min';
     $args['orderby'] = 'meta_value';
     $args['order'] = 'ASC';
		 break;

   }

    return $args;
}

add_filter('uwpqsf_pagination', 'custom_pagi','',4);
function custom_pagi($html,$max_num_pages,$pagenumber,$id){
	$range = 2;//this is the range, you can change it to 1, 2 or 3
	$showitems = (1 * $range)+1;
	$pages = $max_num_pages;
	$paged = $pagenumber;
	if(empty($paged)) $paged = 1;

	if($pages == '')
	 {

   $pages = $max_num_pages;

	    if(!$pages)
		 {
				 $pages = 1;
		 }
	}

	if(1 != $pages)
	 {
	  $html = "<div class=\"uwpqsfpagi\">  ";
	  $html .= '<input type="hidden" id="curuform" value="#uwpqsffrom_'.$id.'">';

	 if($paged > 2 && $paged > $range+1 && $showitems < $pages)
	 $html .= '<a id="1" class="upagievent" href="#">&laquo; '.__("First","UWPQSF").'</a>';
	 $previous = $paged - 1;
	 if($paged > 1 && $showitems < $pages) $html .= '<a id="'.$previous.'" class="mobile-hidden upagievent" style="float:left; position: absolute; left: 0;" "href="#"> '.__("Previous","UWPQSF").'</a>';

	 for ($i=1; $i <= $pages; $i++)
	  {
		 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
		 {
		 $html .= ($paged == $i)? '<span class="upagicurrent">'.$i.'</span>': '<a id="'.$i.'" href="#" class="upagievent inactive">'.$i.'</a>';
		 }
	 }

	 if ($paged < $pages && $showitems < $pages){
		 $next = $paged + 1;
		 $html .= '<a id="'.$next.'" class="upagievent mobile-hidden" style="float:right; position: absolute; right: 0;" href="#">'.__("Next","UWPQSF").' </a>';}
		 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) {
		 $html .= '<a id="'.$pages.'" class="upagievent"  href="#">'.__("Last","UWPQSF").' &raquo;</a>';}
		 $html .= "</div>\n";$max_num_pages = $pages;
		 return $html;
	 }

}

add_filter('uwpqsf_result_tempt', 'customize_output', '', 4);
function customize_output($results , $arg, $id, $getdata ){
	 // The Query
            $apiclass = new uwpqsfprocess();
            $query = new WP_Query( $arg );
		        ob_start();	$result = '';
			// The Loop

		if ( $query->have_posts() ) {
			echo '<ul class="job_listings job-list full">';
			while ( $query->have_posts() ) {

				$query->the_post();

      ?>

      <?php get_job_manager_template_part( 'content', 'job_listing' ); ?>

      <?php

			 }

        echo '</ul>';

       echo  $apiclass->ajax_pagination($arg['paged'],$query->max_num_pages, 4, $id, $getdata);


		   }

		 else {

					 echo  'No Job Found';
				}
				/* Restore original Post Data */
				wp_reset_postdata();

		$results = ob_get_clean();
			return $results;
  }



 //Permalink Base
  function change_job_listing_slug( $args ) {
  $args['rewrite']['slug'] = _x( 'travel-nurse-jobs', 'Job permalink - resave permalinks after changing this', 'job_manager' );
  return $args;
  }

  add_filter( 'register_post_type_job_listing', 'change_job_listing_slug' );


  //Job Category

 add_filter( 'register_taxonomy_job_listing_category_args', 'change_category_slug');
  function change_category_slug ( $args ) {
  global $query_args;
  $args['rewrite']['slug'] = _x( 'travel-nurse-jobs', 'Reset Your Permalinks After', 'job_manager' );
  $args['rewrite']['with_front'] = _x( false, 'Reset Your Permalinks After', 'job_manager' );
  return $args;
  }



function custom_rewrite_rule( $args ) {
	  global $post;
    $taxonomy = 'job_listing_category';
    $queried_term = get_query_var($taxonomy);
    $terms = get_terms($taxonomy, 'slug='.$queried_term);
    //$term = ( $terms ) ? $terms[0]->term_id : false;
    $permalink = $term->slug;
    global $wp_rewrite;
    $job_listing_structure = '/job_listing/%year%/%monthnum%/%day%/%movies%';
    $wp_rewrite->add_rewrite_tag("%movies%", '([^/]+)', "movies=");
    $wp_rewrite->add_permastruct('movies', $movies_structure, false);
    $args['rewrite']['slug'] = _x( $job_listing_structure.'-jobs', 'Reset Your Permalinks After', 'job_manager' );
    return $args;
  }
 // add_filter('register_taxonomy_job_listing_type_object_type', 'custom_rewrite_rule');

   //ID to job URL

   function custom_job_post_type_link( $post_id, $post ) {

    	// don't add the id if it's already part of the slug
    	$permalink = $post->post_name;
    	if ( strpos( $permalink, strval( $post_id ) ) ) {
    		return;
    	}

    	// unhook this function to prevent infinite looping
    	remove_action( 'save_post_job_listing', 'custom_job_post_type_link', 10, 2 );

 	   // add the id to the slug
    	$permalink .= '-' . $post_id;

    	// update the post slug
    	wp_update_post( array(
        	'ID' => $post_id,
        	'post_name' => $permalink
    	));

    	// re-hook this function
    	add_action( 'save_post_job_listing', 'custom_job_post_type_link', 10, 2 );
     }
     add_action( 'save_post_job_listing', 'custom_job_post_type_link', 10, 2 );





function wpbeginner_numeric_posts_nav($max_num_pages = 0) {

/** Stop execution if there's only 1 page */
if( $max_num_pages <= 1 )
    return;
$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

/** Add current page to the array */
if ( $paged >= 1 )
    $links[] = $paged;

/** Add the pages around the current page to the array */
if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
}

if ( ( $paged + 2 ) <= $max_num_pages ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
}
echo '<div class="navigation"><ul class="pagination">' . "\n";

/** Previous Post Link */
if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

/** Link to first page, plus ellipses if necessary */
if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
    if ( ! in_array( 2, $links ) )
        echo '<li>…</li>';
}

/** Link to current page, plus 2 pages in either direction if necessary */
sort( $links );
foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
}

/** Link to last page, plus ellipses if necessary */
if ( ! in_array( $max_num_pages, $links ) ) {
    if ( ! in_array( $max_num_pages - 1, $links ) )
        echo '<li>…</li>' . "\n";
    $class = $paged == $max_num_pages ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max_num_pages ) ), $max_num_pages );
}

/** Next Post Link */
if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );
echo '</ul></div>' . "\n";
}



add_filter( 'job_manager_get_listings', 'workscout_sort_by_gross', 10, 2 );
function workscout_sort_by_gross( $query_args, $args ) {
	// if ( isset( $_POST['form_data'] ) ) {
		$query_args['meta_key'] = '_job_weekly_gross';
		$query_args['orderby'] = 'meta_value';
	// }
	// print_r($query_args);
	// die();
	return $query_args;
}




function job_orientation_daily_date_update_for_asap() {
    $ID = null;
    $job = new WP_Query( array(
					   'post_type' => 'job_listing',
	                    'posts_per_page'=>-1,
                        'meta_key' => '_orientation_date_asap',
                        'meta_value' => 1)
        );
    $posts = $job->posts;
    $post = null;
    if (!empty($posts)) {
        foreach($posts as $post) {
            $ID = $post->ID;
            $nextDay = date("Y-m-d", strtotime("+1 day"));
            update_post_meta($ID, "_application_deadline", $nextDay);
        }
    }
}
add_action('job_orientation_daily_date_update_for_asap', 'job_orientation_daily_date_update_for_asap');

add_filter('the_company_logo','site_logo_when_no_location_logo');
function site_logo_when_no_location_logo($logo){
	global $post;
	$post_location_image = get_post_meta($post->ID,'location_image_url',true);
	// echo "<pre>";
	// print_r(get_post_meta($post->ID));
	// echo "</pre>";
	// $term_list = wp_get_post_terms($post->ID, 'job_listing_region', array("fields" => "names"));
	// print_r($term_list);

	//$logo = wp_get_attachment_url(attachment_url_to_postid(str_replace('http://blueforcestaffing.com', 'http://e5blueforce.staging.wpengine.com', $logo)), 'medium');

	if($post_location_image==''){
	 $logo = JOB_MANAGER_PLUGIN_URL . '/assets/images/company.png';
	// } else {
	// 	if (function_exists('is_wpe_snapshot') && is_wpe_snapshot())
	// 		$logo = wp_get_attachment_url(attachment_url_to_postid(str_replace('http://blueforcestaffing.com', 'http://e5blueforce.staging.wpengine.com', $post_location_image)), 'medium');
	// 	else
	// 		$logo = wp_get_attachment_url(attachment_url_to_postid(str_replace('http://e5blueforce.staging.wpengine.com', 'http://blueforcestaffing.com', $post_location_image)), 'medium');

	}
	
	return $logo;
}


// function load_webservices_cronhooks() {
//     $home_path = ABSPATH;
//     $resource_path = $home_path . 'webServices/cron-hooks.php';
//     require_once $resource_path;

// }
// load_webservices_cronhooks();


add_filter( 'manage_edit-job_listing_columns', 'elm5_jobcolumns',  20, 2);
 function elm5_jobcolumns( $columns, $post_id  = "") {
	$columns["bullhorn_jobid"]  = __( "Bullhorn ID", 'wp-job-manager' );
	return $columns;
}

add_action( 'manage_job_listing_posts_custom_column', 'elm5_jobcustom_columns', 20, 2);
function elm5_jobcustom_columns( $column,  $post_id = "" ) {
	global $post;
	if (empty($post_id)) {
		$post_id = $post->ID;
	}
	if ( 'bullhorn_jobid' === $column ) {
		echo get_post_meta( $post_id, 'bullhorn_job_id', true );
	}
}

add_filter( 'manage_edit-job_listing_sortable_columns', 'elm5_jobsortable_columns' , 10, 2);
function elm5_jobsortable_columns( $columns, $post_id = "" ) {
	$columns['bullhorn_jobid'] = 'bullhorn_jobid';
	return $columns;
}


add_filter( 'job_manager_get_listings', 'workscout_filter_by_job_new', 10, 2 );
function workscout_filter_by_job_new( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		$job_listing_type = $form_data['filter_job_new'];
		if($job_listing_type) {
			$query_args['date_query'] = array(
				'relation' => 'OR',
		        array(
		            'after' => '1 week ago'
		        )
		    );

			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	if (is_array($form_data['filter_job_type'])) {
		if (count(array_filter($form_data['filter_job_type']))) {
			// $taxC = $query_args['tax_query'];
			$query_args['tax_query'][] = array(
	            "taxonomy" => 'job_listing_type',
	            "field" => 'slug',
	            "terms" => $form_data['filter_job_type']
			);
		}
	}

	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_filter_by_bullhorn_categories', 10, 2 );
function workscout_filter_by_bullhorn_categories( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		$catsss = '';
		// $bullhorn_categories = [];
		// $bullhorn_categories = $form_data['filter_bullhorn_categories'];
		// if (!count($bullhorn_categories)) {
			$bullhorn_categories[] = $_COOKIE['cat_selection'];
		// }
		if (count($form_data['filter_bullhorn_categories']) > 1) {
			$bullhorn_categories = $form_data['filter_bullhorn_categories'];
		}
		if (in_array('nursing', $bullhorn_categories)) {
			if ($catsss != '') {
				$catsss .= ',';
			}
			$catsss .= 'registered-nurse-rn,licensed-practical-vocational-nurse-lpn-lvn,certified-nursing-assistant-cna';
		}
		if (in_array('providers', $bullhorn_categories)) {
			if ($catsss != '') {
				$catsss .= ',';
			}
			$catsss .= 'provider,physician-do,physician-md,physician-assistant-pa,nurse-practitioner-np';
		}
		if (in_array('allied', $bullhorn_categories)) {
			if ($catsss != '') {
				$catsss .= ',';
			}
			$catsss .= 'allied-health-professional,service-health-professional';
		}
		if($bullhorn_categories) {
			$query_args['tax_query'][] = array(
	            "taxonomy" => 'bullhorn_categories',
	            "field" => 'slug',
	            "terms" => explode(",", $catsss)
			);

			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	// if (is_array($form_data['filter_bullhorn_categories'])) {
	// 	echo "here";
	// 	if (count(array_filter($form_data['filter_bullhorn_categories'])) > 2) {
	// 		// $taxC = $query_args['tax_query'];
	// 		$query_args['tax_query'][] = array(
	//             "taxonomy" => 'bullhorn_categories',
	//             "field" => 'slug',
	//             "terms" => $form_data['filter_bullhorn_categories']
	// 		);
	// 	}
	// }
	// echo "<pre>";
	// print_r($form_data['filter_bullhorn_categories']);
	// echo "<br>";
	// print_r($query_args);
	// echo "</pre>";

	return $query_args;
}

// add_filter( 'job_manager_get_listings', 'workscout_filter_by_bullhorn_categories', 10, 2 );
// function workscout_filter_by_bullhorn_categories( $query_args, $args ) {
// 	if ( isset( $_POST['form_data'] ) ) {
// 		parse_str( $_POST['form_data'], $form_data );
// 		$catsss = '';
// 		$bullhorn_categories = [];
// 		$bullhorn_categories = $form_data['filter_bullhorn_categories'];
// 		if (!count($bullhorn_categories)) {
// 			$bullhorn_categories[] = $_COOKIE['cat_selection'];
// 		}
// 		if (in_array('nursing', $bullhorn_categories)) {
// 			if ($catsss != '') {
// 				$catsss .= ',';
// 			}
// 			$catsss .= 'registered-nurse-rn,licensed-practical-vocational-nurse-lpn-lvn,certified-nursing-assistant-cna';
// 		}
// 		if (in_array('providers', $bullhorn_categories)) {
// 			if ($catsss != '') {
// 				$catsss .= ',';
// 			}
// 			$catsss .= 'provider';
// 		}
// 		if (in_array('allied', $bullhorn_categories)) {
// 			if ($catsss != '') {
// 				$catsss .= ',';
// 			}
// 			$catsss .= 'allied-health-professional,service-health-professional';
// 		}
// 		if($bullhorn_categories) {
// 			$query_args['tax_query'][] = array(
// 	            "taxonomy" => 'bullhorn_categories',
// 	            "field" => 'slug',
// 	            "terms" => explode(",", $catsss)
// 			);

// 			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
// 		}
// 	}
// 	if (is_array($form_data['filter_bullhorn_categories'])) {
// 		if (count(array_filter($form_data['filter_bullhorn_categories'])) > 2) {
// 			// $taxC = $query_args['tax_query'];
// 			$query_args['tax_query'][] = array(
// 	            "taxonomy" => 'bullhorn_categories',
// 	            "field" => 'slug',
// 	            "terms" => $form_data['filter_bullhorn_categories']
// 			);
// 		}
// 	}

// 	return $query_args;
// }

add_action( 'gform_post_submission_14', 'candidate_attach_to_job_14', 10, 2 );
function candidate_attach_to_job_14( $entry, $form ) {

	$address = new stdClass();
	$category = new stdClass();

	$mainSource = $_COOKIE['main_source'];
	$allSource = array(	
		'google.com' => 'Google Search',
		'bing.com' => 'Bing Search', 
		'ziprecruiter.com' => 'Job Board: ZipRecruiter',
		'indeed.com' => 'Job Board: Indeed',
		'nurserecruiter.com' => 'Job Board: Nurse Recruiter',
		'thegypsynurse.com' => 'Job Board: The Gypsy Nurse',
		'facebook.com' => 'Social Media: Facebook',
		'twitter.com' => 'Social Media: Twitter',
		't.co' => 'Social Media: Twitter',
		'linkedin.com' => 'Social Media: LinkedIn',
		'pinterest.com' => 'Social Media: Pinterest',
		'instagram.com' => 'Social Media: Instagram',
		'practicelink.com' => 'Job Board: Practice Link',
		'practicematch.com' => 'Job Board: Practice Match',
		'doccafe.com' => 'Job Board: DocCafe',
		'physemp.com' => 'Job Board: Physemp'
	);

	$getSo = $allSource[str_replace('www.', '', $mainSource)];
	if ($getSo) {
		$getSo = $getSo;
	} else {
		$getSo = str_replace('www.', '', $mainSource);
	}
	if (is_page('apply-now')) {
		$getSo = 'Apply now page';
	}
	if ($getSo == '') {
		$getSo = 'Company Website';
	}

	$candidate = [];
	$candidate['name'] = $entry['1.3']. ' ' . $entry['1.6'];
	$candidate['firstName'] = $entry['1.3'];
	$candidate['middleName'] = $entry['1.4'];
	$candidate['lastName'] = $entry['1.6'];
	$candidate['status'] = 'New Lead';
	// $candidate['source'] = $entry['15'];
	$candidate['source'] = $getSo;
	$candidate['customText8'] = $entry['27'];
	// $candidate['recruiterUserID'] = 'Scott Cassady';
	$candidate['mobile'] = $entry['2'];
	$candidate['email'] = $entry['3'];

	$address->address1 = $entry['17'];
	$address->address2 = $entry['18'];
	$address->city = $entry['19'];
	$address->state = ($entry['20'] != '') ? $entry['20'] : $entry['21'];
	$address->zip = $entry['24'];
	$address->countryName = $entry['23'];
	$candidate['address'] = $address;

	if ($candidate['jobID'] == '') {
		if ($entry['26'] == 'Registered Nurse (RN)') {
			$candidate['jobID'] = 377;
		}
		if ($entry['26'] == 'Licensed Practical / Vocational Nurse (LPN / LVN)') {
			$candidate['jobID'] = 828;
		}
		if ($entry['26'] == 'Certified Nursing Assistant (CNA)') {
			$candidate['jobID'] = 764;
		}
		if ($entry['26'] == 'Allied Health Professional') {
			$candidate['jobID'] = 3124;
		}
		if ($entry['26'] == 'Service Health Professional') {
			$candidate['jobID'] = 3127;
		}
		if ($entry['26'] == 'Nursing Practitioner (NP)') {
			$candidate['jobID'] = 3128;
		}
		if ($entry['26'] == 'Physician Assistant (PA)') {
			$candidate['jobID'] = 3128;
		}
		if ($entry['26'] == 'Physician (MD)') {
			$candidate['jobID'] = 3128;
		}
		if ($entry['26'] == 'Physician (DO)') {
			$candidate['jobID'] = 3128;
		}
	}
	// $candidate['countryID'] = 'United States';
	if ($entry['26'] == 'Registered Nurse (RN)') {
		$category = new stdClass();
		$category->id = '2000000';
		$category->name = 'Registered Nurse (RN)';
		$candidate['category'] = $category;
	}
	if ($entry['8'] == 'Yes') {
		$category = new stdClass();
		$category->id = '2000000';
		$category->name = 'Registered Nurse (RN)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Licensed Practical / Vocational Nurse (LPN / LVN)') {
		$category = new stdClass();
		$category->id = '2000001';
		$category->name = 'Licensed Practical / Vocational Nurse (LPN / LVN)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Certified Nursing Assistant (CNA)') {
		$category = new stdClass();
		$category->id = '2000002';
		$category->name = 'Certified Nursing Assistant (CNA)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Allied Health Professional') {
		$category = new stdClass();
		$category->id = '2000003';
		$category->name = 'Allied Health Professional';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Service Health Professional') {
		$category = new stdClass();
		$category->id = '2000771';
		$category->name = 'Service Health Professional';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Nursing Practitioner (NP)') {
		$category = new stdClass();
		$category->id = '2001013';
		$category->name = 'Nursing Practitioner (NP)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician Assistant (PA)') {
		$category = new stdClass();
		$category->id = '2001171';
		$category->name = 'Physician Assistant (PA)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician (MD)') {
		$category = new stdClass();
		$category->id = '2000853';
		$category->name = 'Physician (MD)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician (DO)') {
		$category = new stdClass();
		$category->id = '2001783';
		$category->name = 'Physician (DO)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Other') {
		$category = new stdClass();
		$category->id = '2000447';
		$category->name = 'Other';
		$candidate['category'] = $category;
	}

	while (true) {
	    try {
			require getcwd().'/webServices/blueforce-bullhorn/candidate.php';
			$newCandID = '';
			$newCandIDCatID = '';
			$searchCandidate = $clientnew->getCandidate('isDeleted:0 AND email:'.$candidate['email']);

			if (isset($searchCandidate->data[0]->id)) {
				$newCandID = $searchCandidate->data[0]->id;
				if (isset($searchCandidate->data[0]->categories->data[0]->id)) {
					$newCandIDCatID = $searchCandidate->data[0]->categories->data[0]->id;
				}
			}

			if ($newCandID == '') {
				// echo "creating new";
				$candidateRes = $clientnew->post(
				    'entity/Candidate',
				    $candidate, [
				        'connect_timeout' => '20'
				    ]
				);

				$newCandID = $candidateRes->changedEntityId;
				
				$clientnew->request('PUT',
				    'entity/Candidate/'.$newCandID.'/businessSectors/1100064',
				    [], [
				        'Content-Type' => 'application/json'
				    ]
				);

				$candidateFile = [];
				if ($entry['4']) {
					$candidateID = $newCandID;
					$candidateFile['externalID'] = "portfolio";
					$candidateFile['fileContent'] = base64_encode(file_get_contents($entry['4']));
					$candidateFile['fileType'] = "SAMPLE";
					$ext = explode('.', $entry['4']);
					$candidateFile['name'] = 'resume-'.rand().'.'.end($ext);

					$response = $clientnew->post(
					    'file/Candidate/'.$candidateID,
					    $candidateFile, [
					        'Content-Type' => 'application/json'
					    ]
					);
				}
			}

			$jobSubmission = [];
			$jobSubmission['status'] = 'New Lead';
			$jobSubmission['dateWebResponse'] = strtotime(date('Y-m-d h:i:s'));
			$jobSubmission['candidate'] = [
				'id' => $newCandID
			];

			if ($newCandIDCatID != '') {
				$newJobIDDD = 377;
				if ($newCandIDCatID == 2000000) {
					$newJobIDDD = 377;
				}
				if ($newCandIDCatID == 2000001) {
					$newJobIDDD = 828;
				}
				if ($newCandIDCatID == 2000002) {
					$newJobIDDD = 764;
				}
				if ($newCandIDCatID == 2000003) {
					$newJobIDDD = 3124;
				}
				if ($newCandIDCatID == 2000771) {
					$newJobIDDD = 3127;
				}
				if ($newCandIDCatID == 2000853) {
					$newJobIDDD = 3128;
				}
				if ($newCandIDCatID == 2001783) {
					$newJobIDDD = 3128;
				}
				if ($newCandIDCatID == 2001171) {
					$newJobIDDD = 3128;
				}
				if ($newCandIDCatID == 2001013) {
					$newJobIDDD = 3128;
				}
				$jobSubmission['jobOrder'] = [
					'id' => $newJobIDDD
				];
			} else {
				$newJobIDDD = 377;
				if ($category->id == 2000000) {
					$newJobIDDD = 377;
				}
				if ($category->id == 2000001) {
					$newJobIDDD = 828;
				}
				if ($category->id == 2000002) {
					$newJobIDDD = 764;
				}
				if ($category->id == 2000003) {
					$newJobIDDD = 3124;
				}
				if ($category->id == 2000771) {
					$newJobIDDD = 3127;
				}
				if ($category->id == 2000853) {
					$newJobIDDD = 3128;
				}
				if ($category->id == 2001783) {
					$newJobIDDD = 3128;
				}
				if ($category->id == 2001171) {
					$newJobIDDD = 3128;
				}
				if ($category->id == 2001013) {
					$newJobIDDD = 3128;
				}
				$jobSubmission['jobOrder'] = [
					'id' => $newJobIDDD
				];
			}

			$jobRes = $clientnew->post(
			    'entity/JobSubmission',
			    $jobSubmission, [
			    ]
			);

	        break;
	    } 
	    catch (Exception $e) {
	    	$headers[] = 'From: Blueforce <alert@blueforcestaffing.com>';
			 
			wp_mail( 'ahsan@addiedigital.com', 'Failed Candidate Submission', 'Alert for failed candidate submission.', $headers );
	    	// echo $e->getMessage();
	    	break;
	    	if ($e->getCode() != 500) {
				// echo 'Message: ' .$e->getMessage();
				break;
	    	}
	    }
	    usleep(200000);
	}

}

add_action( 'gform_post_submission_13', 'candidate_attach_to_job_13', 10, 2 );
function candidate_attach_to_job_13( $entry, $form ) {

	$address = new stdClass();
	$category = new stdClass();

	$mainSource = $_COOKIE['main_source'];
	$allSource = array(	
		'google.com' => 'Google Search',
		'bing.com' => 'Bing Search', 
		'ziprecruiter.com' => 'Job Board: ZipRecruiter',
		'indeed.com' => 'Job Board: Indeed',
		'nurserecruiter.com' => 'Job Board: Nurse Recruiter',
		'thegypsynurse.com' => 'Job Board: The Gypsy Nurse',
		'facebook.com' => 'Social Media: Facebook',
		'twitter.com' => 'Social Media: Twitter',
		't.co' => 'Social Media: Twitter',
		'linkedin.com' => 'Social Media: LinkedIn',
		'pinterest.com' => 'Social Media: Pinterest',
		'instagram.com' => 'Social Media: Instagram',
		'practicelink.com' => 'Job Board: Practice Link',
		'practicematch.com' => 'Job Board: Practice Match',
		'doccafe.com' => 'Job Board: DocCafe',
		'physemp.com' => 'Job Board: Physemp'
	);

	$getSo = $allSource[str_replace('www.', '', $mainSource)];
	if ($getSo) {
		$getSo = $getSo;
	} else {
		$getSo = str_replace('www.', '', $mainSource);
	}
	if (is_page('apply-now')) {
		$getSo = 'Apply now page';
	}
	if ($getSo == '') {
		$getSo = 'Company Website';
	}

	$candidate = [];
	$candidate['name'] = $entry['1.3']. ' ' . $entry['1.6'];
	$candidate['firstName'] = $entry['1.3'];
	$candidate['middleName'] = $entry['1.4'];
	$candidate['lastName'] = $entry['1.6'];
	$candidate['status'] = 'New Lead';
	// $candidate['source'] = $entry['15'];
	$candidate['source'] = $getSo;
	$candidate['customText8'] = $entry['27'];
	// $candidate['recruiterUserID'] = 'Scott Cassady';
	$candidate['mobile'] = $entry['2'];
	$candidate['email'] = $entry['3'];

	$address->address1 = $entry['17'];
	$address->address2 = $entry['18'];
	$address->city = $entry['19'];
	$address->state = ($entry['20'] != '') ? $entry['20'] : $entry['21'];
	$address->zip = $entry['24'];
	$address->countryName = $entry['23'];
	$candidate['address'] = $address;

	if ($candidate['jobID'] == '') {
		if ($entry['26'] == 'Registered Nurse (RN)') {
			$candidate['jobID'] = 377;
		}
		if ($entry['26'] == 'Licensed Practical / Vocational Nurse (LPN / LVN)') {
			$candidate['jobID'] = 828;
		}
		if ($entry['26'] == 'Certified Nursing Assistant (CNA)') {
			$candidate['jobID'] = 764;
		}
		if ($entry['26'] == 'Allied Health Professional') {
			$candidate['jobID'] = 3124;
		}
		if ($entry['26'] == 'Service Health Professional') {
			$candidate['jobID'] = 3127;
		}
		if ($entry['26'] == 'Nursing Practitioner (NP)') {
			$candidate['jobID'] = 3128;
		}
		if ($entry['26'] == 'Physician Assistant (PA)') {
			$candidate['jobID'] = 3128;
		}
		if ($entry['26'] == 'Physician (MD)') {
			$candidate['jobID'] = 3128;
		}
		if ($entry['26'] == 'Physician (DO)') {
			$candidate['jobID'] = 3128;
		}
	}
	// $candidate['countryID'] = 'United States';
	if ($entry['26'] == 'Registered Nurse (RN)') {
		$category = new stdClass();
		$category->id = '2000000';
		$category->name = 'Registered Nurse (RN)';
		$candidate['category'] = $category;
	}
	if ($entry['8'] == 'Yes') {
		$category = new stdClass();
		$category->id = '2000000';
		$category->name = 'Registered Nurse (RN)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Licensed Practical / Vocational Nurse (LPN / LVN)') {
		$category = new stdClass();
		$category->id = '2000001';
		$category->name = 'Licensed Practical / Vocational Nurse (LPN / LVN)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Certified Nursing Assistant (CNA)') {
		$category = new stdClass();
		$category->id = '2000002';
		$category->name = 'Certified Nursing Assistant (CNA)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Allied Health Professional') {
		$category = new stdClass();
		$category->id = '2000003';
		$category->name = 'Allied Health Professional';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Service Health Professional') {
		$category = new stdClass();
		$category->id = '2000771';
		$category->name = 'Service Health Professional';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Nursing Practitioner (NP)') {
		$category = new stdClass();
		$category->id = '2001013';
		$category->name = 'Nursing Practitioner (NP)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician Assistant (PA)') {
		$category = new stdClass();
		$category->id = '2001171';
		$category->name = 'Physician Assistant (PA)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician (MD)') {
		$category = new stdClass();
		$category->id = '2000853';
		$category->name = 'Physician (MD)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician (DO)') {
		$category = new stdClass();
		$category->id = '2001783';
		$category->name = 'Physician (DO)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Other') {
		$category = new stdClass();
		$category->id = '2000447';
		$category->name = 'Other';
		$candidate['category'] = $category;
	}

	while (true) {
	    try {
			require getcwd().'/webServices/blueforce-bullhorn/candidate.php';
			$newCandID = '';
			$newCandIDCatID = '';
			$searchCandidate = $clientnew->getCandidate('isDeleted:0 AND email:'.$candidate['email']);

			if (isset($searchCandidate->data[0]->id)) {
				$newCandID = $searchCandidate->data[0]->id;
				if (isset($searchCandidate->data[0]->categories->data[0]->id)) {
					$newCandIDCatID = $searchCandidate->data[0]->categories->data[0]->id;
				}
			}

			if ($newCandID == '') {
				// echo "creating new";
				$candidateRes = $clientnew->post(
				    'entity/Candidate',
				    $candidate, [
				        'connect_timeout' => '20'
				    ]
				);

				$newCandID = $candidateRes->changedEntityId;
				
				$clientnew->request('PUT',
				    'entity/Candidate/'.$newCandID.'/businessSectors/1100064',
				    [], [
				        'Content-Type' => 'application/json'
				    ]
				);

				$candidateFile = [];
				if ($entry['4']) {
					$candidateID = $newCandID;
					$candidateFile['externalID'] = "portfolio";
					$candidateFile['fileContent'] = base64_encode(file_get_contents($entry['4']));
					$candidateFile['fileType'] = "SAMPLE";
					$ext = explode('.', $entry['4']);
					$candidateFile['name'] = 'resume-'.rand().'.'.end($ext);

					$response = $clientnew->post(
					    'file/Candidate/'.$candidateID,
					    $candidateFile, [
					        'Content-Type' => 'application/json'
					    ]
					);
				}
			}

			$jobSubmission = [];
			$jobSubmission['status'] = 'New Lead';
			$jobSubmission['dateWebResponse'] = strtotime(date('Y-m-d h:i:s'));
			$jobSubmission['candidate'] = [
				'id' => $newCandID
			];

			if ($newCandIDCatID != '') {
				$newJobIDDD = 377;
				if ($newCandIDCatID == 2000000) {
					$newJobIDDD = 377;
				}
				if ($newCandIDCatID == 2000001) {
					$newJobIDDD = 828;
				}
				if ($newCandIDCatID == 2000002) {
					$newJobIDDD = 764;
				}
				if ($newCandIDCatID == 2000003) {
					$newJobIDDD = 3124;
				}
				if ($newCandIDCatID == 2000771) {
					$newJobIDDD = 3127;
				}
				if ($newCandIDCatID == 2000853) {
					$newJobIDDD = 3128;
				}
				if ($newCandIDCatID == 2001783) {
					$newJobIDDD = 3128;
				}
				if ($newCandIDCatID == 2001171) {
					$newJobIDDD = 3128;
				}
				if ($newCandIDCatID == 2001013) {
					$newJobIDDD = 3128;
				}
				$jobSubmission['jobOrder'] = [
					'id' => $newJobIDDD
				];
			} else {
				$newJobIDDD = 377;
				if ($category->id == 2000000) {
					$newJobIDDD = 377;
				}
				if ($category->id == 2000001) {
					$newJobIDDD = 828;
				}
				if ($category->id == 2000002) {
					$newJobIDDD = 764;
				}
				if ($category->id == 2000003) {
					$newJobIDDD = 3124;
				}
				if ($category->id == 2000771) {
					$newJobIDDD = 3127;
				}
				if ($category->id == 2000853) {
					$newJobIDDD = 3128;
				}
				if ($category->id == 2001783) {
					$newJobIDDD = 3128;
				}
				if ($category->id == 2001171) {
					$newJobIDDD = 3128;
				}
				if ($category->id == 2001013) {
					$newJobIDDD = 3128;
				}
				$jobSubmission['jobOrder'] = [
					'id' => $newJobIDDD
				];
			}

			$jobRes = $clientnew->post(
			    'entity/JobSubmission',
			    $jobSubmission, [
			    ]
			);

	        break;
	    } 
	    catch (Exception $e) {
	    	$headers[] = 'From: Blueforce <alert@blueforcestaffing.com>';
			 
			wp_mail( 'ahsan@addiedigital.com', 'Failed Candidate Submission', 'Alert for failed candidate submission.', $headers );
	    	// echo $e->getMessage();
	    	break;
	    	if ($e->getCode() != 500) {
				// echo 'Message: ' .$e->getMessage();
				break;
	    	}
	    }
	    usleep(200000);
	}

}

add_action( 'gform_post_submission_11', 'candidate_attach_to_job_11', 10, 2 );
function candidate_attach_to_job_11( $entry, $form ) {

	$address = new stdClass();
	$category = new stdClass();

	$mainSource = $_COOKIE['main_source'];
	$allSource = array(	
		'google.com' => 'Google Search',
		'bing.com' => 'Bing Search', 
		'ziprecruiter.com' => 'Job Board: ZipRecruiter',
		'indeed.com' => 'Job Board: Indeed',
		'nurserecruiter.com' => 'Job Board: Nurse Recruiter',
		'thegypsynurse.com' => 'Job Board: The Gypsy Nurse',
		'facebook.com' => 'Social Media: Facebook',
		'twitter.com' => 'Social Media: Twitter',
		't.co' => 'Social Media: Twitter',
		'linkedin.com' => 'Social Media: LinkedIn',
		'pinterest.com' => 'Social Media: Pinterest',
		'instagram.com' => 'Social Media: Instagram',
		'practicelink.com' => 'Job Board: Practice Link',
		'practicematch.com' => 'Job Board: Practice Match',
		'doccafe.com' => 'Job Board: DocCafe',
		'physemp.com' => 'Job Board: Physemp'
	);

	$getSo = $allSource[str_replace('www.', '', $mainSource)];
	if ($getSo) {
		$getSo = $getSo;
	} else {
		$getSo = str_replace('www.', '', $mainSource);
	}
	if (is_page('apply-now')) {
		$getSo = 'Apply now page';
	}
	if ($getSo == '') {
		$getSo = 'Company Website';
	}

	$candidate = [];
	$candidate['name'] = $entry['1.3']. ' ' . $entry['1.6'];
	$candidate['firstName'] = $entry['1.3'];
	$candidate['middleName'] = $entry['1.4'];
	$candidate['lastName'] = $entry['1.6'];
	$candidate['status'] = 'New Lead';
	// $candidate['source'] = $entry['15'];
	$candidate['source'] = $getSo;
	$candidate['customText8'] = $entry['27'];
	// $candidate['recruiterUserID'] = 'Scott Cassady';
	$candidate['mobile'] = $entry['2'];
	$candidate['email'] = $entry['3'];

	$address->address1 = $entry['17'];
	$address->address2 = $entry['18'];
	$address->city = $entry['19'];
	$address->state = ($entry['20'] != '') ? $entry['20'] : $entry['21'];
	$address->zip = $entry['24'];
	$address->countryName = $entry['23'];
	$candidate['address'] = $address;


	$candidate['jobID'] = $entry['9'];
	// $candidate['countryID'] = 'United States';
	// 

	if ($entry['26'] == 'Licensed Practical / Vocational Nurse (LPN / LVN)') {
		$category = new stdClass();
		$category->id = '2000001';
		$category->name = 'Licensed Practical / Vocational Nurse (LPN / LVN)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Certified Nursing Assistant (CNA)') {
		$category = new stdClass();
		$category->id = '2000002';
		$category->name = 'Certified Nursing Assistant (CNA)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Allied Health Professional') {
		$category = new stdClass();
		$category->id = '2000003';
		$category->name = 'Allied Health Professional';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Service Health Professional') {
		$category = new stdClass();
		$category->id = '2000771';
		$category->name = 'Service Health Professional';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Nursing Practitioner (NP)') {
		$category = new stdClass();
		$category->id = '2001013';
		$category->name = 'Nursing Practitioner (NP)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician Assistant (PA)') {
		$category = new stdClass();
		$category->id = '2001171';
		$category->name = 'Physician Assistant (PA)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician (MD)') {
		$category = new stdClass();
		$category->id = '2000853';
		$category->name = 'Physician (MD)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Physician (DO)') {
		$category = new stdClass();
		$category->id = '2001783';
		$category->name = 'Physician (DO)';
		$candidate['category'] = $category;
	}
	if ($entry['26'] == 'Other') {
		$category = new stdClass();
		$category->id = '2000447';
		$category->name = 'Other';
		$candidate['category'] = $category;
	}

	while (true) {
	    try {

			require getcwd().'/webServices/blueforce-bullhorn/candidate.php';
			$newCandID = '';
			$searchCandidate = $clientnew->getCandidate('isDeleted:0 AND email:'.$candidate['email']);


			if (isset($searchCandidate->data[0]->id)) {
				$newCandID = $searchCandidate->data[0]->id;
			}

			if ($newCandID == '') {
				// echo "creating new";
				$candidateRes = $clientnew->post(
				    'entity/Candidate',
				    $candidate, [
				        'connect_timeout' => '20'
				    ]
				);

				$newCandID = $candidateRes->changedEntityId;
				
				$clientnew->request('PUT',
				    'entity/Candidate/'.$newCandID.'/businessSectors/1100064',
				    [], [
				        'Content-Type' => 'application/json'
				    ]
				);

				$candidateFile = [];
				if ($entry['4']) {
					$candidateID = $newCandID;
					$candidateFile['externalID'] = "portfolio";
					$candidateFile['fileContent'] = base64_encode(file_get_contents($entry['4']));
					$candidateFile['fileType'] = "SAMPLE";
					$ext = explode('.', $entry['4']);
					$candidateFile['name'] = 'resume-'.rand().'.'.end($ext);

					$response = $clientnew->post(
					    'file/Candidate/'.$candidateID,
					    $candidateFile, [
					        'Content-Type' => 'application/json'
					    ]
					);
				}
			}

			$jobSubmission = [];
			$jobSubmission['status'] = 'New Lead';
			$jobSubmission['dateWebResponse'] = strtotime(date('Y-m-d h:i:s'));
			$jobSubmission['candidate'] = [
				'id' => $newCandID
			];
			$jobSubmission['jobOrder'] = [
				'id' => $candidate['jobID']
			];

			$jobRes = $clientnew->post(
			    'entity/JobSubmission',
			    $jobSubmission, [
			    ]
			);

	        break;
	    } 
	    catch (Exception $e) {
	    	// echo "trying<br>";
	    	break;
	    }
	    usleep(200000);
	}

}

add_action( 'gform_post_submission_9', 'candidate_attach_to_job9', 10, 2 );
function candidate_attach_to_job9( $entry, $form ) {

	$address = new stdClass();
	$category = new stdClass();

	$candidate = [];
	$candidate['name'] = $entry['1.3']. ' ' . $entry['1.6'];
	$candidate['firstName'] = $entry['1.3'];
	$candidate['middleName'] = $entry['1.4'];
	$candidate['lastName'] = $entry['1.6'];
	$candidate['status'] = 'New Lead';
	$candidate['source'] = $entry['15'];
	$candidate['customText8'] = $entry['16'];
	// $candidate['recruiterUserID'] = 'Scott Cassady';
	$candidate['mobile'] = $entry['2'];
	$candidate['email'] = $entry['3'];

	$address->address1 = $entry['17'];
	$address->address2 = $entry['18'];
	$address->city = $entry['19'];
	$address->state = ($entry['20'] != '') ? $entry['20'] : $entry['21'];
	$address->zip = $entry['24'];
	$address->countryName = $entry['23'];
	$candidate['address'] = $address;


	$candidate['jobID'] = $entry['9'];
	// $candidate['countryID'] = 'United States';
	// 
	if ($entry['8'] == 'No') {
		$category = new stdClass();
		$category->id = '2000447';
		$category->name = 'Other';
		$candidate['category'] = $category;
	}

	$industry = new stdClass();
	$industry->id = '1100059';
	$industry->name = 'Healthcare Travel';
	$candidate['businessSectors'] = $industry;

	while (true) {
	    try {
			require getcwd().'/webServices/blueforce-bullhorn/candidate.php';

			$candidateRes = $clientnew->post(
			    'entity/Candidate',
			    $candidate, [
			        'connect_timeout' => '20'
			    ]
			);

			if ($entry['4']) {
				$candidateFile = [];

				$candidateID = $candidateRes->changedEntityId;
				$candidateFile['externalID'] = "portfolio";
				$candidateFile['fileContent'] = base64_encode(file_get_contents($entry['4']));
				$candidateFile['fileType'] = "SAMPLE";
				$candidateFile['name'] = 'resume-'.rand().'.doc';

				$response = $clientnew->post(
				    'file/Candidate/'.$candidateID,
				    $candidateFile, [
				        'Content-Type' => 'application/json'
				    ]
				);
			}
			break;
		} catch(Exception $e) {
		}
	    usleep(200000);
	}

}

add_action( 'gform_post_submission_1', 'candidate_attach_to_job2', 10, 2 );
function candidate_attach_to_job2( $entry, $form ) {

	$candidate = [];
	$candidate['name'] = $entry['1.3']. ' ' . $entry['1.6'];
	$candidate['firstName'] = $entry['1.3'];
	$candidate['middleName'] = '';
	$candidate['lastName'] = $entry['1.6'];
	$candidate['status'] = 'New Lead';
	// $candidate['source'] = $entry['15'];
	// $candidate['customText8'] = $entry['16'];
	// $candidate['recruiterUserID'] = 'Scott Cassady';
	$candidate['mobile'] = $entry['2'];
	$candidate['email'] = $entry['3'];
	// $candidate['address'] = [
	// 	'address1' => $entry['17'],
	// 	'address2' => $entry['18'],
	// 	'city' => $entry['19'],
	// 	'state' => ($entry['20'] != '') ? $entry['20'] : $entry['21'],
	// 	'zip' => $entry['20'],
	// 	'country' => $entry['23']
	// ];
	// $candidate['jobID'] = $entry['9'];
	// $candidate['countryID'] = 'United States';
	// $candidate['categoryID'] = $entry['8'];

	// echo "<pre>";
	// print_r($entry);
	// die;


	$industry = new stdClass();
	$industry->id = '1100059';
	$industry->name = 'Healthcare Travel';
	$candidate['businessSectors'] = $industry;

	while (true) {
	    try {
			require getcwd().'/webServices/blueforce-bullhorn/candidate.php';

			$candidateRes = $clientnew->post(
			    'entity/Candidate',
			    $candidate, [
			        'connect_timeout' => '20'
			    ]
			);

			if ($entry['4']) {
				$candidateFile = [];

				$candidateID = $candidateRes->changedEntityId;
				$candidateFile['externalID'] = "portfolio";
				$candidateFile['fileContent'] = base64_encode(file_get_contents($entry['4']));
				$candidateFile['fileType'] = "SAMPLE";
				$candidateFile['name'] = 'resume-'.rand().'.doc';

				$response = $clientnew->post(
				    'file/Candidate/'.$candidateID,
				    $candidateFile, [
				        'Content-Type' => 'application/json'
				    ]
				);
			}
			break;
		} catch(Exception $e) {
		}
	    usleep(200000);
	}

}

function location_images_add_page(){
    global $wpdb;
    $table_name = 'wp_location_images';
    if (isset($_GET['delete'])) {
	    $results = $wpdb->delete($table_name, array('id' => $_GET['delete']));
    }
    if (isset($_GET['edit'])) {
	    $editResult = $wpdb->get_results("SELECT * FROM $table_name WHERE id=".$_GET['edit']);
    }
    if (isset($_POST['hiddenId'])) {
    	$editPost = $_POST['hiddenId'];
    	unset($_POST['hiddenId']);
    	unset($_POST['submit']);
    	// print_r($_POST);
    	// die;
	    $results = $wpdb->update($table_name, $_POST, array('id' => $editPost));
    }
    if (isset($_POST['submit']) && !isset($_POST['hiddenId'])) {
	    unset($_POST['submit']);
	    $results = $wpdb->insert($table_name, $_POST);
    }
    $all = $wpdb->get_results( 
		"
		SELECT * 
		FROM $table_name
		"
	);
    ?>
    <div class="wrap">
	    <h1>Add Location Images</h1>
	    <br>
	    <br>
	    <?php if (isset($_GET['edit'])): ?>
	    <form method="POST" action="?page=location_images">
	    	<input type="hidden" name="hiddenId" value="<?php echo $_GET['edit']; ?>">
			<label>Zip: </label><input type="text" name="li_zip" value="<?php echo $editResult[0]->li_zip; ?>">
			<label>City: </label><input type="text" name="li_city" value="<?php echo $editResult[0]->li_city; ?>">
			<label>State: </label>
			<select id="li_state" name="li_state" required="required">
				<option value="Alabama">Alabama</option>
				<option value="Alaska">Alaska</option>
				<option value="Arizona">Arizona</option>
				<option value="Arkansas">Arkansas</option>
				<option value="California">California</option>
				<option value="Colorado">Colorado</option>
				<option value="Connecticut">Connecticut</option>
				<option value="Delaware">Delaware</option>
				<option value="District Of Columbia">District Of Columbia</option>
				<option value="Florida">Florida</option>
				<option value="Georgia">Georgia</option>
				<option value="Guam">Guam</option>
				<option value="Hawaii">Hawaii</option>
				<option value="Idaho">Idaho</option>
				<option value="Illinois">Illinois</option>
				<option value="Indiana">Indiana</option>
				<option value="Iowa">Iowa</option>
				<option value="Kansas">Kansas</option>
				<option value="Kentucky">Kentucky</option>
				<option value="Louisiana">Louisiana</option>
				<option value="Maine">Maine</option>
				<option value="Marshall Islands">Marshall Islands</option>
				<option value="Maryland">Maryland</option>
				<option value="Massachusetts">Massachusetts</option>
				<option value="Michigan">Michigan</option>
				<option value="Minnesota">Minnesota</option>
				<option value="Mississippi">Mississippi</option>
				<option value="Missouri">Missouri</option>
				<option value="Montana">Montana</option>
				<option value="Nebraska">Nebraska</option>
				<option value="Nevada">Nevada</option>
				<option value="New Hampshire">New Hampshire</option>
				<option value="New Jersey">New Jersey</option>
				<option value="New Mexico">New Mexico</option>
				<option value="New York">New York</option>
				<option value="North Carolina">North Carolina</option>
				<option value="North Dakota">North Dakota</option>
				<option value="Northern Mariana Islands">Northern Mariana Islands</option>
				<option value="Ohio">Ohio</option>
				<option value="Oklahoma">Oklahoma</option>
				<option value="Oregon">Oregon</option>
				<option value="Palau">Palau</option>
				<option value="Pennsylvania">Pennsylvania</option>
				<option value="Puerto Rico">Puerto Rico</option>
				<option value="Rhode Island">Rhode Island</option>
				<option value="South Carolina">South Carolina</option>
				<option value="South Dakota">South Dakota</option>
				<option value="Tennessee">Tennessee</option>
				<option value="Texas">Texas</option>
				<option value="Utah">Utah</option>
				<option value="Vermont">Vermont</option>
				<option value="Virgin Islands">Virgin Islands</option>
				<option value="Virginia">Virginia</option>
				<option value="Washington">Washington</option>
				<option value="West Virginia">West Virginia</option>
				<option value="Wisconsin">Wisconsin</option>
				<option value="Wyoming">Wyoming</option>
			</select>
			<label>Image URL: </label><input type="text" name="li_image_url" value="<?php echo $editResult[0]->li_image_url; ?>" required="required">
			<input type="submit" name="submit" value="submit" />
	    </form>
	    <script type="text/javascript">
	    	jQuery(function(){
	    		jQuery('#li_state option[value="<?php echo $editResult[0]->li_state; ?>"]').attr('selected', 'selected');
	    	});
	    </script>
	    <?php else : ?>
	    <form method="POST" action="?page=location_images">
			<label>Zip: </label><input type="text" name="li_zip">
			<label>City: </label><input type="text" name="li_city">
			<label>State: </label>
			<select name="li_state" required="required">
				<option value="Alabama">Alabama</option>
				<option value="Alaska">Alaska</option>
				<option value="Arizona">Arizona</option>
				<option value="Arkansas">Arkansas</option>
				<option value="California">California</option>
				<option value="Colorado">Colorado</option>
				<option value="Connecticut">Connecticut</option>
				<option value="Delaware">Delaware</option>
				<option value="District Of Columbia">District Of Columbia</option>
				<option value="Florida">Florida</option>
				<option value="Georgia">Georgia</option>
				<option value="Guam">Guam</option>
				<option value="Hawaii">Hawaii</option>
				<option value="Idaho">Idaho</option>
				<option value="Illinois">Illinois</option>
				<option value="Indiana">Indiana</option>
				<option value="Iowa">Iowa</option>
				<option value="Kansas">Kansas</option>
				<option value="Kentucky">Kentucky</option>
				<option value="Louisiana">Louisiana</option>
				<option value="Maine">Maine</option>
				<option value="Marshall Islands">Marshall Islands</option>
				<option value="Maryland">Maryland</option>
				<option value="Massachusetts">Massachusetts</option>
				<option value="Michigan">Michigan</option>
				<option value="Minnesota">Minnesota</option>
				<option value="Mississippi">Mississippi</option>
				<option value="Missouri">Missouri</option>
				<option value="Montana">Montana</option>
				<option value="Nebraska">Nebraska</option>
				<option value="Nevada">Nevada</option>
				<option value="New Hampshire">New Hampshire</option>
				<option value="New Jersey">New Jersey</option>
				<option value="New Mexico">New Mexico</option>
				<option value="New York">New York</option>
				<option value="North Carolina">North Carolina</option>
				<option value="North Dakota">North Dakota</option>
				<option value="Northern Mariana Islands">Northern Mariana Islands</option>
				<option value="Ohio">Ohio</option>
				<option value="Oklahoma">Oklahoma</option>
				<option value="Oregon">Oregon</option>
				<option value="Palau">Palau</option>
				<option value="Pennsylvania">Pennsylvania</option>
				<option value="Puerto Rico">Puerto Rico</option>
				<option value="Rhode Island">Rhode Island</option>
				<option value="South Carolina">South Carolina</option>
				<option value="South Dakota">South Dakota</option>
				<option value="Tennessee">Tennessee</option>
				<option value="Texas">Texas</option>
				<option value="Utah">Utah</option>
				<option value="Vermont">Vermont</option>
				<option value="Virgin Islands">Virgin Islands</option>
				<option value="Virginia">Virginia</option>
				<option value="Washington">Washington</option>
				<option value="West Virginia">West Virginia</option>
				<option value="Wisconsin">Wisconsin</option>
				<option value="Wyoming">Wyoming</option>
			</select>
			<label>Image URL: </label><input type="text" name="li_image_url" required="required">
			<input type="submit" name="submit" value="submit" />
	    </form>
	    <?php endif ?>
	    <br>
	    <br>
		<table id="location_images" class="display" style="width:100%">
	        <thead>
	            <tr>
	                <th>ID</th>
	                <th>Zip</th>
	                <th>State</th>
	                <th>City</th>
	                <th>Image URL</th>
	                <th>Action</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php foreach ($all as $key => $value) {
	        		echo "            <tr>
	                <td>".$value->id."</td>
	                <td>".$value->li_zip."</td>
	                <td>".$value->li_state."</td>
	                <td>".$value->li_city."</td>
	                <td>".$value->li_image_url."</td>
	                <td>
	                	<a href='?page=location_images&edit=".$value->id."'>edit</a>
	                	<a href='?page=location_images&delete=".$value->id."'>delete</a>
                	</td>
	            </tr>";
	        	} ?>
	    	</tbody>
	        <tfoot>
	            <tr>
	                <th>ID</th>
	                <th>Zip</th>
	                <th>State</th>
	                <th>City</th>
	                <th>Image URL</th>
	                <th>Action</th>
	            </tr>
	        </tfoot>
	    </table>
	    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
	    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	    <script type="text/javascript">
	    	jQuery(document).ready(function($) {
			    $('#location_images').DataTable();
			});
	    </script>
	    <style type="text/css">
	    	table.dataTable tfoot th, table.dataTable tfoot td,
	    	table.dataTable thead th, table.dataTable thead td {
	    		text-align: left;
	    	}
	    </style>
    </div>
    <?php
}

function location_images_plugin_menu(){
    add_menu_page('Location Images Page', 'Location Images', 'manage_options', 'location_images', 'location_images_add_page');
}
add_action('admin_menu','location_images_plugin_menu');


add_action( 'wp_ajax_candidate_check_form', 'candidate_check_form' );
add_action( 'wp_ajax_nopriv_candidate_check_form', 'candidate_check_form' );

function candidate_check_form() {
	// $abc = [];
	while (true) {
	    try {
			require $_SERVER['DOCUMENT_ROOT'].'/webServices/blueforce-bullhorn/candidate.php';
			$searchCandidate = $clientnew->getCandidate('isDeleted:0 AND email:'.$_POST["email"]);
			if (isset($searchCandidate->data[0]->id)) {
				$abc = $searchCandidate->data[0]->id;
			} else {
				$abc = 'notfound';
			}
			break;
		} catch(Exception $e) {
		}
	    usleep(200000);
	}
	echo $abc;
    exit();
}

function order_posts_by_asc( $query ) {
    if ( $query->is_home() && $query->is_main_query() && isset($_GET['sort'])) {
        $query->set( 'order', 'ASC' );
    }
}
add_action( 'pre_get_posts', 'order_posts_by_asc' );


function js_variables(){
    $variables = array (
        'ajax_url' => admin_url('admin-ajax.php'),
    );
    echo '<script type="text/javascript">window.wp_data = ' . json_encode($variables) . ';</script>';
}
add_action('wp_head','js_variables');


function ajax_request() {

$value = $_POST['post_name'];
setcookie('cat_selection', $value , time() + (86400 * 30), "/"); // 86400 = 1 day

return $value;
 
wp_die();
}
 
add_action( 'wp_ajax_nopriv_ajax_request', 'ajax_request' );
add_action( 'wp_ajax_ajax_request', 'ajax_request' );





add_action( 'admin_menu', 'expired_jobs_function' );
function expired_jobs_function() {
add_menu_page( 'Expired Jobs', 'Expired Jobs', 'manage_options', 'expired-jobs', 'expired_jobs_page', '',25);
}
function expired_jobs_page() {
global $wpdb;
$results = $wpdb->get_results( "SELECT * FROM wp_expired_jobs ORDER BY added_at DESC", OBJECT );
?>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript">
   $(document).ready(function() {
       $('#expired-jobs-data').DataTable();
       $('button[data-toggle="modal"]').click(function(){
         var allData = $(this).parent().find('.more-data').html();
         $('#show-data').html(allData);
       });
   } );
</script>
<!-- Modal -->
<br>
<h3>Expired Jobs:</h3>
<br>
<br>
<table id="expired-jobs-data" class="display" style="width:100%">
   <thead>
      <tr>
         <th>URL</th>
         <th>Name</th>
         <th>Category</th>
         <th>Specialty</th>
         <th>Expired At</th>
      </tr>
   </thead>
   <tbody>
      <?php foreach ($results as $key => $value) { ?>
      <tr>
         <td><?php echo $value->url; ?></td>
         <td><?php echo $value->name; ?></td>
         <td><?php $newcategory = json_decode($value->category); echo $newcategory[0]->name; ?></td>
         <td><?php $newspecialty = json_decode($value->specialty); echo $newspecialty[0]->name; ?></td>
         <td><?php echo $value->added_at; ?></td>
      </tr>
      <?php } ?>
   </tbody>
</table>
<style type="text/css">    
   th {
   text-align: left;
   }
</style>
<?php
   }
add_filter( 'manage_edit-job_listing_category_columns' , 'blueforce_job_listing_category_columns' );
function blueforce_job_listing_category_columns( $columns ) {
	
	// add carousel shortcode column
	$columns['job_listing_category'] = __('Category');
	return $columns;
}

add_filter( 'manage_job_listing_category_custom_column', 'blueforce_job_listing_category_column_content', 20, 3 );
function blueforce_job_listing_category_column_content( $content, $column_name, $term_id ) {
	$term = get_term( $term_id, 'job_listing_category' );
	if ( 'job_listing_category' == $column_name ) {
		$pods = pods( 'job_listing_category', $term_id );
		$cat = $pods->field( 'type_of_category' );
		$content = '';
		if (is_array($cat)) {
			foreach ($cat as $key => $value) {
				$content .= $value.', ';
			}
			$content = rtrim($content, ', ');
		} else {
			$content = $cat;
		}
	}
	return $content;
}

function blueforce_change_taxonomy_vehicle_type_label() {
	$t = get_taxonomy('job_listing_category'); 
	$t->labels->name                            = 'Job Specialties';
	$t->labels->singular_name                   = 'Job Specialty';
	$t->labels->menu_name                       = 'Job Specialties';
	$t->labels->all_items                       = 'All Job Specialties';
	$t->labels->parent_item                     = 'Parent Job Specialty';
	$t->labels->parent_item_colon               = 'Parent Job Specialty:';
	$t->labels->new_item_name                   = 'New Job Specialty Name';
	$t->labels->add_new_item                    = 'Add New Job Specialty';
	$t->labels->edit_item                       = 'Edit Job Specialty';
	$t->labels->update_item                     = 'Update Job Specialty';
	$t->labels->separate_items_with_commas      = 'Separate Job Specialties with commas';
	$t->labels->search_items                    = 'Search Job Specialties';
	$t->labels->add_or_remove_items             = 'Add or remove Job Specialties';
	$t->labels->choose_from_most_used           = 'Choose from the most used Job Specialties';
}
add_action( 'wp_loaded', 'blueforce_change_taxonomy_vehicle_type_label', 20);

add_action( 'admin_head-edit-tags.php', 'blueforce_remove_parent_category' );
add_action( 'admin_head-term.php', 'blueforce_remove_parent_category' );

function blueforce_remove_parent_category()
{
    if ( 'job_listing_category' != $_GET['taxonomy'] )
        return;

    $parent = 'parent()';

    if ( isset( $_GET['tag_ID'] ) )
        $parent = 'parent().parent()';

    ?>
        <script type="text/javascript">
            jQuery(document).ready(function($)
            {     
                $('label[for="parent"]').<?php echo $parent; ?>.hide();       
                $('label[for="term_meta[fa_icon]"]').<?php echo $parent; ?>.hide();       
                $('label[for="term_meta[upload_icon]"]').<?php echo $parent; ?>.hide();       
                $('label[for="term_meta[upload_header]"]').<?php echo $parent; ?>.hide();       
                $('#wpseo_meta').hide();       
            });
        </script>
    <?php
}


add_action( 'admin_menu', 'complex_specialty_mapping_menu' );
function complex_specialty_mapping_menu() {
	add_menu_page( 'Specialty Mapping', 'Specialty Mapping', 'manage_options', 'specialty-mapping-page.php', 'complex_specialty_mapping_page', 'dashicons-tickets', 6  );
}

function complex_specialty_mapping_page() { ?>
<?php 
	global $wpdb;
	$webCat = [
		'Allied Health Professional' => 'Allied', 
		'Service Health Professional' => 'Allied',
		'Registered Nurse (RN)' => 'Nursing',
		'Licensed Practical / Vocational Nurse (LPN / LVN)' => 'Nursing',
		'Certified Nursing Assistant (CNA)' => 'Nursing',
		'Physician (MD)' => 'Provider',
		'Physician (DO)' => 'Provider',
		'Physician (DPM)' => 'Provider',
		'Physician Assistant (PA)' => 'Provider',
		'Nurse Practitioner (NP)' => 'Provider',
		'Certified Registered Nurse Anesthetist (CRNA)' => 'Provider'
	];
	
	if (isset($_GET['delete']) && $_GET['type'] == 'mapping') {
		$wpdb->delete('specialty_mapping', array('id' => $_GET['delete']));
	}
	
	if (isset($_GET['delete']) && $_GET['type'] == 'specialty') {
		$wpdb->delete('specialties', array('id' => $_GET['delete']));
	}
	
	if (isset($_POST['submit']) && $_POST['type'] == 'mapping') {
		$dataArr = [
			'web_category' => $webCat[$_POST['category']],
			'category' => $_POST['category'],
			'specialty' => $_POST['specialty']
		];
		$webSpecialties = $_POST['web_specialty'];
		if (isset($webSpecialties[0])) {
			$dataArr['web_specialty_1'] = $webSpecialties[0];
		}
		if (isset($webSpecialties[1])) {
			$dataArr['web_specialty_2'] = $webSpecialties[1];
		}
		if (isset($webSpecialties[2])) {
			$dataArr['web_specialty_3'] = $webSpecialties[2];
		}
		if (isset($webSpecialties[3])) {
			$dataArr['web_specialty_4'] = $webSpecialties[3];
		}
		if (isset($webSpecialties[4])) {
			$dataArr['web_specialty_5'] = $webSpecialties[4];
		}

		if ($_POST['id']) {
			$wpdb->update('specialty_mapping', $dataArr, ['id' => $_POST['id']]);
		} else {
			$wpdb->insert('specialty_mapping', $dataArr);
		}
	}

	if (isset($_POST['submit']) && $_POST['type'] == 'specialty') {
		$dataArr = [
			'web_category' => $_POST['web_category'],
			'title' => $_POST['title']
		];
		if ($_POST['id']) {
			$wpdb->update('specialties', $dataArr, ['id' => $_POST['id']]);
		} else {
			$wpdb->insert('specialties', $dataArr);
		}
	}
	// foreach ($mappings as $key => $value) {
		// $results = $wpdb->update('specialty_mapping', ['web_category' => $webCat[$value->category]], array('id' => $value->id));
	// }

	$specialties = get_terms( array('taxonomy' => 'job_listing_category','hide_empty' => false,'orderby'=>'name'));
	$all_webspecialties = $wpdb->get_results( "SELECT * FROM specialties", OBJECT );
	$specialtyArr = [];
	foreach ($specialties as $key => $value) {
		array_push($specialtyArr, $value->name);
	}
	// echo "<pre>";
	// print_r($specialties);
	// echo "</pre>";
	$mappings = $wpdb->get_results( "SELECT * FROM specialty_mapping", OBJECT );
?>
	<style type="text/css">.notice {display: none !important;}</style>
	<script type="text/javascript">
		jQuery(function($){
			$('.complex_specialty_mapping .add-btn').click(function(){
				$('#complex_specialty_form input[name="id"]').val('');
				$('#complex_specialty_form select[name="category"] option').removeAttr('selected');
				$('#complex_specialty_form select[name="specialty"] option').removeAttr('selected');
				$('#complex_specialty_form').show();
				return false;
			});

			$('.complex_specialty_mapping .edit-btn').click(function(){
				$('#complex_specialty_form input[name="id"]').val('');
				$('#complex_specialty_form select[name="category"] option').removeAttr('selected');
				$('#complex_specialty_form select[name="specialty"] option').removeAttr('selected');
				$('#complex_specialty_form').show();
				$('#complex_specialty_form input[name="id"]').val($(this).attr('data-id'));
				$('#complex_specialty_form select[name="category"]').val($(this).attr('data-category'));
				$('#complex_specialty_form select[name="specialty"]').val($(this).attr('data-specialty'));
				return false;
			});

			$('.web_specialty .add-btn').click(function(){
				$('#web_specialty_form input[name="id"]').val('');
				$('#web_specialty_form select[name="web_category"] option').removeAttr('selected');
				$('#web_specialty_form input[name="title"]').val('');
				$('#web_specialty_form').show();
				return false;
			});

			$('.web_specialty .edit-btn').click(function(){
				$('#web_specialty_form input[name="id"]').val('');
				$('#web_specialty_form select[name="web_category"] option').removeAttr('selected');
				$('#web_specialty_form input[name="title"]').val('');
				$('#web_specialty_form').show();
				$('#web_specialty_form input[name="id"]').val($(this).attr('data-id'));
				$('#web_specialty_form select[name="web_category"]').val($(this).attr('data-web_category'));
				$('#web_specialty_form input[name="title"]').val($(this).attr('data-title'));
				return false;
			});
		});
	</script>

	<br>
	<div class="wrap complex_specialty_mapping" style="padding: 20px; background: #fff; border-radius: 10px;">
		<h1 class="wp-heading-inline">Complex Specialty Mapping</h1>
		<a href="#" class="page-title-action add-btn">Add New</a>
		<hr class="wp-header-end">
		
		<form id="complex_specialty_form" action="" method="post" style="display: none;">
			<input type="hidden" name="id" value="">
			<br>
			<label style="display: block; margin-top: 5px">Category</label>			
			<select name="category" style="width: 400px">
				<?php foreach ($webCat as $key => $value) { ?>
					<option value="<?php echo $key ?>"><?php echo $key.' ('.$value.')' ?></option>
				<?php } ?>
			</select>
			<br>
			<label style="display: block; margin-top: 5px">Specialty</label>			
			<select name="specialty" style="width: 400px">
				<?php foreach ($specialties as $key => $value) { ?>
					<option value="<?php echo $value->name ?>"><?php echo $value->name ?></option>
				<?php } ?>
			</select>
			<br>
			<div style="width: 400px">
				<label style="display: block; margin-top: 5px">Mapped Specialities (select upto 5)</label>			
				<select class="web_specialty_chosen" name="web_specialty[]" multiple="multiple" max="5" maxlength="5" style="height: 110px; width: 400px">
					<?php foreach ($all_webspecialties as $key => $value) { ?>
						<option value="<?php echo $value->title ?>"><?php echo $value->title ?></option>
					<?php } ?>
				</select>
			</div>
			<br>
			<input type="hidden" name="type" value="mapping">
			<input type="submit" name="submit" class="button" value="Submit" style="margin-top: 5px;">
		</form>

		<br>
		<br>

		<table class="display dataTable" id="complex_specialty_mapping_table" style="border: 1px solid #000">
			<thead>
				<tr>
					<!-- <th style="text-align: left;">Category</th> -->
					<th style="text-align: left;">ID</th>
					<th style="text-align: left;">Web Category</th>
					<th style="text-align: left;">Specialty</th>
					<th style="text-align: left; width: 100px padding-left: 0px; padding-right: 10px; padding-left: 10px;">Web Specialty 1</th>
					<th style="text-align: left; width: 100px padding-left: 0px; padding-right: 10px; padding-left: 10px;">Web Specialty 2</th>
					<th style="text-align: left; width: 100px padding-left: 0px; padding-right: 10px; padding-left: 10px;">Web Specialty 3</th>
					<th style="text-align: left; width: 100px padding-left: 0px; padding-right: 10px; padding-left: 10px;">Web Specialty 4</th>
					<th style="text-align: left; width: 100px padding-left: 0px; padding-right: 10px; padding-left: 10px;">Web Specialty 5</th>
					<th style="text-align: left;">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($mappings as $key => $map) { ?>
					<?php 
						// if (!in_array(trim($map->specialty), $specialtyArr)) {
						// 	$insertedTerm = wp_insert_term($map->specialty, 'job_listing_category');
						// 	if (is_array($insertedTerm)) {
						// 		if (isset($insertedTerm['term_id'])) {
						// 			$pod = pods('job_listing_category', $insertedTerm['term_id']);
						// 			$pod->save(['type_of_category' => $map->web_category]);
						// 		}
						// 	}
						// }
					?>
					<!-- <tr style="background: <?php echo (in_array(trim($map->specialty), $specialtyArr)) ? 'blue' : 'red'; ?>"> -->
					<tr>
						<!-- <td><?php echo $map->category ?></td> -->
						<td><?php echo $map->id ?></td>
						<td><?php echo $map->web_category ?></td>
						<td><?php echo $map->specialty ?></td>
						<td><?php echo $map->web_specialty_1 ?></td>
						<td><?php echo $map->web_specialty_2 ?></td>
						<td><?php echo $map->web_specialty_3 ?></td>
						<td><?php echo $map->web_specialty_4 ?></td>
						<td><?php echo $map->web_specialty_5 ?></td>
						<td><a href="#" class="edit-btn" data-id="<?php echo $map->id ?>" data-category="<?php echo $map->category ?>" data-specialty="<?php echo $map->specialty ?>" data-web_specialty_1="<?php echo $map->web_specialty_1 ?>" data-web_specialty_2="<?php echo $map->web_specialty_2 ?>" data-web_specialty_3="<?php echo $map->web_specialty_3 ?>" data-web_specialty_4="<?php echo $map->web_specialty_4 ?>" data-web_specialty_5="<?php echo $map->web_specialty_5 ?>">Edit</a> | <a href="?page=specialty-mapping-page.php&type=mapping&delete=<?php echo $map->id ?>" onClick="return confirm('Are you sure?')">Delete</a></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<!-- <th style="text-align: left;">Category</th> -->
					<th style="text-align: left;">ID</th>
					<th style="text-align: left;">Web Category</th>
					<th style="text-align: left;">Specialty</th>
					<th style="text-align: left;">Web Specialty 1</th>
					<th style="text-align: left;">Web Specialty 2</th>
					<th style="text-align: left;">Web Specialty 3</th>
					<th style="text-align: left;">Web Specialty 4</th>
					<th style="text-align: left;">Web Specialty 5</th>
					<th style="text-align: left;">Action</th>
				</tr>
			</tfoot>
		</table>
	</div>

	<br>
	<br>

	<?php $web_specialties = $wpdb->get_results( "SELECT * FROM specialties", OBJECT ); ?>
	
	<div class="wrap web_specialty" style="padding: 20px; background: #fff; border-radius: 10px;">
		<h1 class="wp-heading-inline">Web Specialties</h1>
		<a href="#" class="page-title-action add-btn">Add New</a>
		<hr class="wp-header-end">
		
		<form id="web_specialty_form" action="" method="post" style="display: none;">
			<input type="hidden" name="id" value="">
			<br>
			<label style="display: block; margin-top: 5px">Category</label>			
			<select name="web_category" style="width: 170px;">
				<option value="<?php echo 'allied' ?>"><?php echo 'Allied' ?></option>
				<option value="<?php echo 'nursing' ?>"><?php echo 'Nursing' ?></option>
				<option value="<?php echo 'provider' ?>"><?php echo 'Provider' ?></option>
			</select>
			<br>
			<label style="display: block; margin-top: 5px">Specialty</label>			
			<input type="text" name="title" style="width: 170px;">
			<br>
			<input type="hidden" name="type" value="specialty">
			<input type="submit" name="submit" class="button" value="Submit" style="margin-top: 5px;">
		</form>

		<br>
		<br>

		<table class="display dataTable" id="web_specialty_table" style="border: 1px solid #000">
			<thead>
				<tr>
					<th style="text-align: left;">ID</th>
					<th style="text-align: left;">Web Category</th>
					<th style="text-align: left;">Specialty</th>
					<th style="text-align: left;">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($web_specialties as $key => $webSpecialty) { ?>
					<tr>
						<td><?php echo $webSpecialty->id ?></td>
						<td><?php echo ucwords($webSpecialty->web_category) ?></td>
						<td><?php echo $webSpecialty->title ?></td>
						<td><a href="#" class="edit-btn" data-id="<?php echo $webSpecialty->id ?>" data-web_category="<?php echo strtolower($webSpecialty->web_category) ?>" data-title="<?php echo $webSpecialty->title ?>">Edit</a> | <a href="?page=specialty-mapping-page.php&type=specialty&delete=<?php echo $webSpecialty->id ?>" onClick="return confirm('Are you sure?')">Delete</a></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<th style="text-align: left;">ID</th>
					<th style="text-align: left;">Web Category</th>
					<th style="text-align: left;">Specialty</th>
					<th style="text-align: left;">Action</th>
				</tr>
			</tfoot>
		</table>
	</div>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" />
	<script type="text/javascript">
		jQuery(document).ready(function($) {
		    $('#complex_specialty_mapping_table').DataTable();
		    $('#web_specialty_table').DataTable();
		    $('.web_specialty_chosen').chosen({
		    	max_selected_options: 5
		    });
		});
	</script>
	<style type="text/css">
		.chosen-container.chosen-container-multi {
			width: 400px !important;
		}
		.chosen-container-multi .chosen-choices {
			border-color: #7e8993;
			background: none;
			border-radius: 3px;
		}
		.dataTables_wrapper .dataTables_length {
			margin-bottom: 10px;
		}
		table.dataTable tfoot th, table.dataTable tfoot td,
		table.dataTable thead th, table.dataTable thead td {
			text-align: left;
		}
		table.dataTable#complex_specialty_mapping_table thead .sorting {
			width: 100px !important;
		}
		table.dataTable#complex_specialty_mapping_table thead .sorting:last-child {
			width: 50px !important;
		}
		table.dataTable#complex_specialty_mapping_table thead .sorting:first-child {
			width: 20px !important;
		}
	</style>
<?php }