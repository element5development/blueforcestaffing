<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WorkScout
 */

get_header(); ?>
	<style type="text/css">body .menu ul.sp>li>a {background: rgba(255,255,255,.2) !important;}</style>
	<!-- Titlebar
	================================================== -->
	<div id="titlebar" class="single submit-page">
		<div class="container">

			<div class="sixteen columns">
				<?php 
					$url404 = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
					global $wpdb;
					$getResults2 = $wpdb->get_row( "SELECT * FROM wp_expired_jobs WHERE url = '".$url404."'" );
					if (count($getResults2) > 0) { ?>
					<h1><?php esc_html_e( 'The opportunity you selected is no longer available.', 'workscout' ); ?></h1>
					<?php } else { ?>
					<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'workscout' ); ?></h1>
					<?php }
				 ?>
				<?php //if (strpos($_SERVER['REQUEST_URI'], 'travel-nurse-jobs') !== false): ?>
				<?php //else : ?>
				<?php //endif ?>
			</div>

		</div>
	</div>
	<div class="container full-width">
		<article id="post-<?php the_ID(); ?>">
			<div class="columns sixteen">
				<?php
					$getResults = $wpdb->get_row( "SELECT * FROM wp_expired_jobs WHERE url = '".$url404."'" );
					$showOther = false;
					if (count($getResults) > 0) {
						$specialty = json_decode($getResults->specialty);
						$category = json_decode($getResults->category);
						$pgTitle = 'Recommended Jobs';
						$topContent = "<h2>The job listing you requested has expired or been filled. Don't worry, we can still help!</h2>
						<p>Below are some similar job listings to the one you were looking for. If you don't see a job you're interested in, you can also submit an application for your Dream Job by clicking the button below.</p>
						<a href=\"#apply-dialog\" class=\"button small-dialog popup-with-zoom-anim\">Apply for your dream job</a>";
						$finalPosts = get_posts(
						    array(
						        'post_type' => 'job_listing',
						        'numberposts' => -1,
						        'orderby' => 'date',
	                          	'order' => 'DESC',
							    'date_query' => array(
							        'after' => date('Y-m-d', strtotime('-10 days')) 
							    ),
						        'tax_query' => array(
						        	'relation' => 'AND',
						            array(
						                'taxonomy' => 'job_listing_category',
						                'field' => 'term_id',
						                'terms' => $specialty[0]->term_id,
						            ),
						            array(
						                'taxonomy' => 'bullhorn_categories',
						                'field' => 'term_id',
						                'terms' => $category[0]->term_id,
						            )
						         )
						    )
						);
						if (count($finalPosts) < 1) {
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    ),
							        'tax_query' => array(
							            array(
							                'taxonomy' => 'job_listing_category',
							                'field' => 'term_id',
							                'terms' => $specialty[0]->term_id,
							            )
							         )
							    )
							);
						}
						if (count($finalPosts) < 1) {
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    ),
							        'tax_query' => array(
							            array(
							                'taxonomy' => 'bullhorn_categories',
							                'field' => 'term_id',
							                'terms' => $category[0]->term_id,
							            )
							        )
							    )
							);
						}
						if (count($finalPosts) < 1) {
							$pgTitle = 'Recent Jobs';
							$topContent = "<h2>The job listing you requested has expired or been filled. Don't worry, we can still help!</h2>
							<p>You can submit an application for your Dream Job by clicking the button below.</p>
							<a href=\"#apply-dialog\" class=\"button small-dialog popup-with-zoom-anim\">Apply for your dream job</a>";
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    )
							    )
							);
						}
					} else {
						$pgTitle = 'Recommended Jobs';
						$topContent = "<h2>The page you requested has expired or been removed.</h2>
						<p>You can submit an application for your Dream Job by clicking the button below.</p>
						<a href=\"#apply-dialog\" class=\"button small-dialog popup-with-zoom-anim\">Apply for your dream job</a><br>
						<h2>Looking for staffing for your facility?</h2>
						<p>Click here to find out more about our services.</p>
						<div class=\"clientButton1 \"><a href=\"/prospective-clients/\" id=\"clientButton2\" class=\"button small-dialog popup-with-zoom-anim\">FOR CLIENTS</a></div>";

						$finalPosts = array();
						$bullhorn_speciality = $_COOKIE['specialty_cookie'];
						$bullhorn_categories = $_COOKIE['cat_selection'];

						if ($bullhorn_categories != '' && $bullhorn_speciality != '') {
							if ('nursing' == $bullhorn_categories) {
								if ($catsss != '') {
									$catsss .= ',';
								}
								$catsss .= 'registered-nurse-rn,licensed-practical-vocational-nurse-lpn-lvn,certified-nursing-assistant-cna';
							}
							if ('providers' == $bullhorn_categories) {
								if ($catsss != '') {
									$catsss .= ',';
								}
								$catsss .= 'provider';
							}
							if ('allied' == $bullhorn_categories) {
								if ($catsss != '') {
									$catsss .= ',';
								}
								$catsss .= 'allied-health-professional,service-health-professional';
							}
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    ),
							        'tax_query' => array(
							        	'relation' => 'AND',
							            array(
							                'taxonomy' => 'job_listing_category',
							                'field' => 'term_id',
							                'terms' => $bullhorn_speciality,
							            ),
							            array(
							                'taxonomy' => 'bullhorn_categories',
							                'field' => 'slug',
							                'terms' => explode(',', $catsss),
							            )
							         )
							    )
							);
						} else if ($bullhorn_speciality != '') {
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    ),
							        'tax_query' => array(
							            array(
							                'taxonomy' => 'job_listing_category',
							                'field' => 'term_id',
							                'terms' => $bullhorn_speciality,
							            )
							         )
							    )
							);
						} else if ($bullhorn_categories != '') {
							if ('nursing' == $bullhorn_categories) {
								if ($catsss != '') {
									$catsss .= ',';
								}
								$catsss .= 'registered-nurse-rn,licensed-practical-vocational-nurse-lpn-lvn,certified-nursing-assistant-cna';
							}
							if ('providers' == $bullhorn_categories) {
								if ($catsss != '') {
									$catsss .= ',';
								}
								$catsss .= 'provider';
							}
							if ('allied' == $bullhorn_categories) {
								if ($catsss != '') {
									$catsss .= ',';
								}
								$catsss .= 'allied-health-professional,service-health-professional';
							}
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    ),
							        'tax_query' => array(
							            array(
							                'taxonomy' => 'bullhorn_categories',
							                'field' => 'slug',
							                'terms' => explode(',', $catsss),
							            )
							         )
							    )
							);
						}

						if (count($finalPosts) < 1) {
							$showOther = true;
							$pgTitle = 'Recommended Jobs';
							$topContent = "<h2>The page you requested has expired or been removed.</h2>
							<p>Search our website for available job postings.</p>
							<a href=\"". get_bloginfo('url') ."\" class=\"button\" rel=\"home\">available healthcare jobs</a>";
							$finalPosts = get_posts(
							    array(
							        'post_type' => 'job_listing',
							        'numberposts' => -1,
							        'orderby' => 'date',
		                          	'order' => 'DESC',
								    'date_query' => array(
								        'after' => date('Y-m-d', strtotime('-10 days')) 
								    ),
							        'tax_query' => array(
							            array(
							                'taxonomy' => 'bullhorn_categories',
							                'field' => 'slug',
							                'terms' => explode(',', $catsss),
							            )
							        )
							    )
							);
						}
					}
				?>
				<?php echo $topContent; ?>
				<div class="margin-bottom-25"></div>
			</div>

			<?php if (!$showOther): ?>
				
  	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
  	<style type="text/css">.overflowholder2 .bx-wrapper {max-width: 100% !important;} /*li .job-spotlight {min-height: 410px;}*/ .showbiz ul li {margin-top: 0px;} .overflowholder2 .bx-wrapper .bx-viewport {height: auto !important;}</style>
	<script>
		jQuery(document).ready(function(){
			var theslider = jQuery('.overflowholder2 ul').bxSlider({
				minSlides: 1,
				maxSlides: 3,
				moveSlides: 1,
				slideWidth: 379,
				pager: false,
				controls: false,
				infiniteLoop: false
			});
			jQuery('#showbiz_left_001').click(function(){
				theslider.goToPrevSlide();
			});
			jQuery('#showbiz_right_001').click(function(){
				theslider.goToNextSlide();
			});
			console.log(theslider);
			if (theslider != undefined) {
				if (theslider.getSlideCount() < 4) {
					jQuery('.showbiz-navigation').remove();
				}
			}
		});
	</script>
			<?php endif ?>

			<?php if ($showOther): ?>
  	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
  	<style type="text/css">.overflowholder2 .bx-wrapper {max-width: 100% !important;} /*li .job-spotlight {min-height: 410px;}*/ .showbiz ul li {margin-top: 0px;} .overflowholder2 .bx-wrapper .bx-viewport {height: auto !important;}</style>
	<script>
		jQuery(document).ready(function(){
			var theslider = jQuery('.overflowholder2 ul').bxSlider({
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				slideWidth: 375,
				pager: false,
				controls: false,
				infiniteLoop: false
			});
			jQuery('#showbiz_left_001').click(function(){
				theslider.goToPrevSlide();
			});
			jQuery('#showbiz_right_001').click(function(){
				theslider.goToNextSlide();
			});
		});
	</script>
				<?php 
					$finalPosts2 = get_posts(
					    array(
					        'post_type' => 'job_listing',
					        'numberposts' => 50,
					        'orderby' => 'date',
                          	'order' => 'DESC',
					    )
					);
				 ?>
				<style type="text/css">.showbiz .overflowholder ul li {margin-top: 0px !important;}</style>
				<style type="text/css">.showbiz-navigation {margin-top: -89px;} .about-us-right li {width: 49%;}</style>
		        <div class="columns one-third">
				<h1 class="margin-bottom-5" style="line-height: 38px; min-height: 95px;">Recent Jobs</h1>
		        <!-- Navigation -->
		        <div class="showbiz-navigation">
		            <div id="showbiz_left_001" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
		            <div id="showbiz_right_001" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
		        </div>
		        <div class="clearfix"></div>
		        <!-- Showbiz Container -->
		        <div id="job-spotlight" class="job-spotlight-car2 showbiz-container" data-visible="[<?php echo $visible; ?>]">
		            <div class="showbiz" data-left="#showbiz_left_001" data-right="#showbiz_right_001" data-play="#showbiz_play_001" >
		                <div class="overflowholder2">
		                    <ul>
	                    	<?php foreach ($finalPosts2 as $key => $value): ?>
		                      <?php $id = $value->ID; ?>
		                        <li>
		                            <div class="job-spotlight">
		                                <a href="<?php echo get_the_permalink($id); ?>"><h4><?php echo get_the_title($id); ?><br>
											<div class="job-spotlight-job-tags">
		                                	<?php $types = wp_get_post_terms( $id, 'job_listing_type' ); ?>
											<?php foreach ($types as $key => $value): ?>
												<span title="<?php if($value->slug == 'travel'){echo "Up to ";}?>$<?php echo get_post_meta($id, '_job_type_'.$value->slug, true); ?>" class="tooltip job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>" style="opacity: 1;">
													<?php echo $value->name; ?>
												</span>
											<?php endforeach; ?>
											</div>
		                                </h4></a>
		                                <?php $Weekly_Gross = get_post_meta( $id, '_job_weekly_gross', true );
							 if ( $Weekly_Gross ) {
							 	?>
							<span><i class="fa fa-money"></i> $<?php echo esc_html( $Weekly_Gross ) ?></span><br>
							<?php } ?>
		                                <span><i class="fa fa-map-marker"></i> <?php echo get_post_meta( $id, '_job_location', true ); ?></span><br>

		                                <?php
		                                $rate_min = get_post_meta( $id, '_rate_min', true );
		                                if ( $rate_min) {
		                                    $rate_max = get_post_meta( $id, '_rate_max', true );  ?>
		                                    <span>
		                                        <i class="fa fa-money"></i> <?php  echo get_workscout_currency_symbol();  echo esc_html( $rate_min ); if(!empty($rate_max)) { echo '- '.$rate_max; } ?> / hour
		                                    </span><br>
		                                <?php } ?>

		                                <?php
		                                $salary_min = get_post_meta( $id, '_salary_min', true );
		                                if ( $salary_min ) {
		                                    $salary_max = get_post_meta( $id, '_salary_max', true );  ?>
		                                    <span>
		                                        <i class="fa fa-money"></i>
		                                        <?php echo get_workscout_currency_symbol(); echo esc_html( $salary_min ) ?> <?php if(!empty($salary_max)) { echo '- '.$salary_max; } ?>
		                                    </span><br>
		                                <?php } ?>

		                                <a href="<?php echo get_the_permalink($id); ?>" class="button"><?php esc_html_e('Apply For This Job','workscout') ?></a>
		                            </div>
		                        </li>
		                    	<?php endforeach; ?>
		                    </ul>
		                    <div class="clearfix"></div>

		                </div>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        </div>
		        <div class="columns one-third">
					<div class="widget">
						<h1 class="widget-title" style="line-height: 38px; min-height: 95px;">Looking for staffing for your facility?</h1>
						<div class="about-us-right" style="width: 100%; margin-top: 5px;">
						<ul>
						  <li><a href="/our-solutions/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-05.png" alt=""> <h4>OUR SOLUTIONS</h4></a></li>
						<li><a href="/what-we-provide/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-06.png" alt=""> <h4>WHAT WE PROVIDE</h4></a></li>
						<li><a href="/our-process/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-07.png" alt=""> <h4>OUR PROCESS</h4></a></li>
						<li><a href="/quality/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/08/ico-08.png" alt=""> <h4>QUALITY</h4></a></li>
						  </ul>
						</div>
					</div>
		        </div>
		        <div class="columns one-third">
					<div class="widget widget_categories">
						<h1 class="widget-title" style="line-height: 38px; min-height: 95px;">Contact Us</h1>
						<p>Have a question? Reach out directly to<br>BlueForce.</p>
						<a href="/contact/" class="button">CONTACT US</a>
					</div>
		        </div>

		        <div class="clearfix"></div>
			<?php else : ?>


			<div class="columns sixteen">
				<?php //echo do_shortcode('[spotlight_jobs2 title="Recommended Jobs"]'); ?>
		       <h1 class="margin-bottom-5"><?php echo $pgTitle; ?></h1>
		        <!-- Navigation -->
		        <div class="showbiz-navigation">
		            <div id="showbiz_left_001" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
		            <div id="showbiz_right_001" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
		        </div>
		        <div class="clearfix"></div>
		        <!-- Showbiz Container -->
		        <div id="job-spotlight" class="job-spotlight-car2 showbiz-container" data-visible="[<?php echo $visible; ?>]">
		            <div class="showbiz" data-left="#showbiz_left_001" data-right="#showbiz_right_001" data-play="#showbiz_play_001" >
		                <div class="overflowholder2">
		                    <ul>
	                    	<?php foreach ($finalPosts as $key => $value): ?>
		                      <?php $id = $value->ID; ?>
		                        <li>
		                            <div class="job-spotlight">
		                                <a href="<?php echo get_the_permalink($id); ?>"><h4><?php echo get_the_title($id); ?><br>
											<div class="job-spotlight-job-tags">
		                                	<?php $types = wp_get_post_terms( $id, 'job_listing_type' ); ?>
											<?php foreach ($types as $key => $value): ?>
												<span title="<?php if($value->slug == 'travel'){echo "Up to ";}?>$<?php echo get_post_meta($id, '_job_type_'.$value->slug, true); ?>" class="tooltip job-type <?php echo $value->slug ? sanitize_title( $value->slug ) : ''; ?>" style="opacity: 1;">
													<?php echo $value->name; ?>
												</span>
											<?php endforeach; ?>
											</div>
		                                </h4></a>
		                                <?php $Weekly_Gross = get_post_meta( $id, '_job_weekly_gross', true );
							 if ( $Weekly_Gross ) {
							 	?>
							<span><i class="fa fa-money"></i> $<?php echo esc_html( $Weekly_Gross ) ?></span><br>
							<?php } ?>
		                                <span><i class="fa fa-map-marker"></i> <?php echo get_post_meta( $id, '_job_location', true ); ?></span><br>

		                                <?php
		                                $rate_min = get_post_meta( $id, '_rate_min', true );
		                                if ( $rate_min) {
		                                    $rate_max = get_post_meta( $id, '_rate_max', true );  ?>
		                                    <span>
		                                        <i class="fa fa-money"></i> <?php  echo get_workscout_currency_symbol();  echo esc_html( $rate_min ); if(!empty($rate_max)) { echo '- '.$rate_max; } ?> / hour
		                                    </span><br>
		                                <?php } ?>

		                                <?php
		                                $salary_min = get_post_meta( $id, '_salary_min', true );
		                                if ( $salary_min ) {
		                                    $salary_max = get_post_meta( $id, '_salary_max', true );  ?>
		                                    <span>
		                                        <i class="fa fa-money"></i>
		                                        <?php echo get_workscout_currency_symbol(); echo esc_html( $salary_min ) ?> <?php if(!empty($salary_max)) { echo '- '.$salary_max; } ?>
		                                    </span><br>
		                                <?php } ?>

		                                <a href="<?php echo get_the_permalink($id); ?>" class="button"><?php esc_html_e('Apply For This Job','workscout') ?></a>
		                            </div>
		                        </li>
		                    	<?php endforeach; ?>
		                    </ul>
		                    <div class="clearfix"></div>

		                </div>
		                <div class="clearfix"></div>
		            </div>
		        </div>
			</div>
			<?php endif ?>

			<div style="display: none;">
				<div class="columns one-third"><?php the_widget( 'WP_Widget_Recent_Posts' ); ?></div>
						
				<div class="columns one-third"><?php if ( workscout_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
						<div class="widget widget_categories">
							<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'workscout' ); ?></h2>
							<ul>
							<?php
								wp_list_categories( array(
									'orderby'    => 'count',
									'order'      => 'DESC',
									'show_count' => 1,
									'title_li'   => '',
									'number'     => 10,
								) );
							?>
							</ul>
						</div><!-- .widget -->
						<?php endif; ?>
				</div>

				<div class="columns one-third">

					<?php
						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'workscout' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>
				</div>
			</div>
		</article>
	</div>

	<script type="text/javascript">
		// jQuery(document).ready(function($){
		// 	$.removeCookie("cat_selection");
  //           $.cookie("cat_selection", "clients", { expires: 7, path: '/' });
		// });
	</script>
<?php get_footer(); ?>