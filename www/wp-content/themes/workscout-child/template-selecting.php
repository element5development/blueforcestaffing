<?php
/**
 * Template Name: Selecting
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WPVoyager
 */

get_header(); ?>

		<header id="main-header" class="<?php $style = Kirki::get_option( 'workscout','pp_header_style', 'default' ); echo $style; ?> selectingHeader">
			<div class="container" style="padding: 0; border: 0px;">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo" style="position: relative;">
						 <?php

			                $logo = Kirki::get_option( 'workscout', 'pp_logo_upload', '' );
			                if($logo) {
			                    if(is_front_page()){ ?>
			                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
			                    <?php } else { ?>
			                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
			                    <?php }
			                } else {
			                    if(is_front_page()) { ?>
			                    <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			                    <?php } else { ?>
			                    <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
			                    <?php }
			                }
			                ?>
			                <?php if(get_theme_mod('workscout_tagline_switch','hide') == 'show') { ?><div id="blogdesc"><?php bloginfo( 'description' ); ?></div><?php } ?>

					</div>

					<div class="social-i" style="margin-top: 15px;">
						<div class="footer-bottom">
							<style type="text/css">.hideDesktop {display: none !important;} @media (max-width: 991px) {.hideDesktop {display: inline-block !important;}} @media (max-width: 550px) {/*.mobileChange {position: absolute;}*/ .page #titlebar {margin-top: -190px !important;} .page .social-i .footer-bottom .social-icons li a {padding: 0px !important;} #banner {margin-top: -182px;} .footer-bottom .social-icons li {float: none; text-align: center;} .twitter i, .twitter:before, .pinterest i, .pinterest:before, .instagram i, .instagram:before, .facebook i, .facebook:before {margin: 13px 0 0 -5px;} .social-icons li a i {margin-left: -5px;} .social-icons li.hideDesktop {width: 100%;} .social-icons li.hideDesktopBottom {margin-bottom: 15px; margin-top: -10px;}}</style>
			                <?php /* get the slider array */
			                $footericons = ot_get_option( 'pp_footericons', array() );
			                if ( !empty( $footericons ) ) {
			                    echo '<ul class="social-icons mobileChange">';
			                    foreach( $footericons as $icon ) {
			                        echo '<li><a target="_blank" class="' . $icon['icons_service'] . '" title="' . esc_attr($icon['title']) . '" href="' . esc_url($icon['icons_url']) . '"><i class="icon-' . $icon['icons_service'] . '"></i></a></li>';
			                    }
			                    // echo '<li class="hideDesktop"><a href="/dream-job" target="_blank" style="width: auto; border: 0px; padding: 10px; height: auto;" class="scriptTriggerApply"><img style="max-width:100px;" src="/wp-content/themes/workscout-child/images/apply-now-btn.png" alt="" /></a></li>';
			                    echo '<li class="hideDesktop hideDesktopBottom"><div style="display: inline-block; line-height: 20px; padding-top: 2px; color: #fff;" class="phone-no0" x-ms-format-detection="none">1 (866) 795-2583<br><i x-ms-format-detection="none" style="font-family: inherit;">1 (248) 588-1600 (Outside US)</i></div></li>';
			                    echo '</ul>';
			                }
			                ?>
						</div>
						<div class="footer-bottom footerBottom2" style="width: 160px; color: #fff; padding: 4px 0 0 0; position: relative; text-align: right;">
							<img src="/wp-content/uploads/2016/05/phone.png" alt="" class="ndimg" style="float: left; margin-top: 12px;display: inline-block;
						    float: left;     position: absolute;
						    left: 0px;
						    top: 4px;" />
							<div style="display: inline-block; line-height: 20px; padding-top: 10px;" class="phone-no0" x-ms-format-detection="none">1 (866) 795-2583<br><i x-ms-format-detection="none">1 (248) 588-1600 (Outside US)</i></div>
						</div>
					</div>
				</div>
			</div>
		</header>

<section class="selectingSection clearfix">
	<div class="column-4 nursing">
		<div></div>
		<a href="#" id="nursingButton">I’m a nursing professional</a>
	</div>
	<div class="column-4 allied">
		<div></div>
		<a href="#" id="alliedButton">I’m an allied professional</a>
	</div>
	<div class="column-4 provider">
		<div></div>
		<a href="#" id="providerButton">I’m a provider</a>
	</div>
	<div class="column-12 client">
		<span>For Clients</span>
		<a href="#" id="clientButton">Looking for staffing solutions?</a>
	</div>
</section>
<style type="text/css">
	body {
		overflow: hidden;
	}
	header#main-header .container {
		padding: 0 40px !important;
		width: 100% !important;
	}
	.page .social-i .footer-bottom {
		padding: 0 0 0 45px;
	}
	.selectingHeader {
		position: absolute !important;
		top: 0px;
		left: 0px;
		right: 0px;
		margin: 0 auto;
	}
	.selectingHeader #logo {
		border: 0px !important;
		padding: 0px !important;
		background: transparent !important;
	}
	.selectingSection * {
		box-sizing: border-box;
	}
	.selectingSection a {
		color: #FFFFFF;
		font-family: "Lato";
		font-size: 18px;
		line-height: 22px;
		text-align: right;
	}
	.selectingSection {
		height: 100vh;
	}
	.selectingSection .column-4 {
		width: 33.33%;
		float: left;
		padding: 0;
		height: 80%;
		text-align: center;
		position: relative;
		background-size: cover !important;
	}
	.selectingSection .column-4 > div {
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background: rgba(0,0,0,0.5);
		z-index: 0;
	}
	.selectingSection .column-4 a {
		display: inline-block;
		border: 2px solid #fff;
		padding: 8px 30px;
		margin-top: 80%;
		position: relative;
		z-index: 9;
	}
	.selectingSection .column-4 a:hover {
		background: #fff;
		color: #1D3465;
	}
	.selectingSection .column-12 {
		text-align: center;
		width: 100%;
		float: left;
		height: 20%;
	}
	.selectingSection .column-4.nursing {
		background: url("/wp-content/uploads/2018/08/Bitmap4.jpg");
	}
	.selectingSection .column-4.allied {
		background: url("/wp-content/uploads/2018/08/Bitmap5.png");
	}
	.selectingSection .column-4.provider {
		background: url("/wp-content/uploads/2018/08/Bitmap6.png");
	}
	.selectingSection .column-12.client {
		background: #1D3465;
		padding: 0;
		position: relative;
	}
	.selectingSection .column-12.client a {
		display: inline-block;
		margin-top: 4%;
	}
	.selectingSection .column-12.client span {
		position: absolute;
		left: 50px;
		top: 40%;
		float: left;
		color: #FFFFFF;
		font-family: "Montserrat";
		font-size: 36px;
		font-weight: 600;
		line-height: 30px;
	}
	@media (max-width: 991px) {
		.footerBottom2 {
			display: none !important;
		}
	}
</style>
<?php while ( have_posts() ) : the_post();  ?>


<?php endwhile; // End of the loop.  ?>

<?php get_footer(); ?>
