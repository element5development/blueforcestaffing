<?php
/**
 * WorkScout functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WorkScout
 */

remove_filter( 'the_title','add_breadcrumb_to_the_title'  );
include_once( get_template_directory() . '/kirki/kirki.php' );

function workscout_kirki_update_url( $config ) {

    $config['url_path'] = get_template_directory_uri() . '/kirki/';
    return $config;

}
add_filter( 'kirki/config', 'workscout_kirki_update_url' );

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );


/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );

/**
 * Show New Layout
 */
add_filter( 'ot_show_new_layout', '__return_false' );


/**
 * Custom Theme Option page
 */
add_filter( 'ot_use_theme_options', '__return_true' );

/**
 * Meta Boxes
 */
add_filter( 'ot_meta_boxes', '__return_true' );

/**
 * Loads the meta boxes for post formats
 */
add_filter( 'ot_post_formats', '__return_true' );

/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );

/**
 * Theme Options
 */
load_template( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );

/**
 * Meta Boxes
 */
load_template( trailingslashit( get_template_directory() ) . 'inc/meta-boxes.php' );


if ( ! function_exists( 'workscout_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */


add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

function workscout_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WorkScout, use a find and replace
	 * to change 'workscout' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'workscout', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'resume-manager-templates' );
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	do_action( 'purethemes-testimonials' );
	
	/*
	 * Enabling Full Template Support for WP Job Manager
	 */
	add_theme_support( 'job-manager-templates' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size(840, 430, true); //size of thumbs
	add_image_size('workscout-small-thumb', 96, 105, true);     //slider
	add_image_size('workscout-small-blog', 498, 315, true);     //slider
	add_image_size('workscout-resume', 110, 110, true);     //slider

	add_image_size('job-manager-search-thumb', 160, 180,true);
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'workscout' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'workscout_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // workscout_setup
add_action( 'after_setup_theme', 'workscout_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function workscout_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'workscout_content_width', 860 );
}
add_action( 'after_setup_theme', 'workscout_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function workscout_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'workscout' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );	
	register_sidebar( array(
		'name'          => esc_html__( 'Jobs page sidebar', 'workscout' ),
		'id'            => 'sidebar-jobs',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Single job sidebar before', 'workscout' ),
		'id'            => 'sidebar-job-before',
		'description'   => 'This widgets will be displayed before the Job Overview on single job page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );	
	register_sidebar( array(
		'name'          => esc_html__( 'Single job sidebar after', 'workscout' ),
		'id'            => 'sidebar-job-after',
		'description'   => 'This widgets will be displayed after the Job Overview on single job page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar(array(
		'id' => 'pay-benefits-page',
		'name' => esc_html__('Pay & Benefits', 'workscout' ),
		'description' => esc_html__('Pay & Benefits Page', 'workscout' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		));
		
			register_sidebar(array(
		'id' => 'about-us-page',
		'name' => esc_html__('About Us', 'workscout' ),
		'description' => esc_html__('About Us Page', 'workscout' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		));
		
				register_sidebar(array(
		'id' => 'resources-page',
		'name' => esc_html__('Resources', 'workscout' ),
		'description' => esc_html__('Resources Page', 'workscout' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		));
	
		register_sidebar(array(
		'id' => 'for-employers-page',
		'name' => esc_html__('For Employers', 'workscout' ),
		'description' => esc_html__('For Employers Page', 'workscout' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		));	
	register_sidebar(array(
		'id' => 'contact-us-page',
		'name' => esc_html__('Contact Us', 'workscout' ),
		'description' => esc_html__('Contact Us Page', 'workscout' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		));
	register_sidebar(array(
		'id' => 'footer4',
		'name' => esc_html__('Footer Menu', 'workscout' ),
		'description' => esc_html__('Menu in Footer', 'workscout' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		));
	if (ot_get_option('incr_sidebars')):
		$pp_sidebars = ot_get_option('incr_sidebars');
		foreach ($pp_sidebars as $pp_sidebar) {
			register_sidebar(array(
				'name' => $pp_sidebar["title"],
				'id' => $pp_sidebar["id"],
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
				));
		}
	endif;
}
add_action( 'widgets_init', 'workscout_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function workscout_scripts() {

	wp_register_style( 'workscout-base', get_template_directory_uri(). '/css/base.css' );
    wp_register_style( 'workscout-responsive', get_template_directory_uri(). '/css/responsive.css' );
    wp_register_style( 'workscout-font-awesome', get_template_directory_uri(). '/css/font-awesome.css' );
    wp_register_style( 'workscout-tooltip-css', get_template_directory_uri(). '/css/tooltipster.bundle.min.css' );
	wp_enqueue_style( 'workscout-style', get_stylesheet_uri(), array('workscout-base','workscout-responsive','workscout-font-awesome') );
	wp_enqueue_style( 'workscout-woocommerce', get_template_directory_uri(). '/css/woocommerce.css' );
	wp_enqueue_style('workscout-tooltip-css');
	wp_dequeue_style('wp-job-manager-frontend');
	wp_dequeue_style('wp-job-manager-resume-frontend');
	wp_dequeue_style('chosen');


	wp_deregister_script( 'wp-job-manager-bookmarks-bookmark-js');
	wp_dequeue_style( 'wp-job-manager-bookmarks-frontend' );
	wp_dequeue_style( 'wp-job-manager-applications-frontend' );


	if ( defined( 'JOB_MANAGER_VERSION' ) ) {
	    global $wpdb;
		$ajax_url  = WP_Job_Manager_Ajax::get_endpoint();
		$min = floor( $wpdb->get_var(
	    $wpdb->prepare('
	            SELECT min(meta_value + 0)
	            FROM %1$s
	            LEFT JOIN %2$s ON %1$s.ID = %2$s.post_id
	            WHERE meta_key IN ("' . implode( '","', array( '_salary') ) . '")
	            AND meta_value != ""
	        ', $wpdb->posts, $wpdb->postmeta )
	    ) );
	            
		$max = ceil( $wpdb->get_var(
		$wpdb->prepare('
		    SELECT max(meta_value + 0)
		    FROM %1$s
		    LEFT JOIN %2$s ON %1$s.ID = %2$s.post_id
		    WHERE meta_key IN ("' . implode( '","', array( '_salary' ) ) . '")
		', $wpdb->posts, $wpdb->postmeta, '_price' )
		) );

		$ratemin = floor( $wpdb->get_var(
	    $wpdb->prepare('
	            SELECT min(meta_value + 0)
	            FROM %1$s
	            LEFT JOIN %2$s ON %1$s.ID = %2$s.post_id
	            WHERE meta_key IN ("' . implode( '","',  array( '_rate') ) . '")
	            AND meta_value != ""
	        ', $wpdb->posts, $wpdb->postmeta )
	    ) );	

	    $ratemax = ceil( $wpdb->get_var(
		$wpdb->prepare('
		    SELECT max(meta_value + 0)
		    FROM %1$s
		    LEFT JOIN %2$s ON %1$s.ID = %2$s.post_id
		    WHERE meta_key IN ("' . implode( '","',  array( '_rate' ) ) . '")
		', $wpdb->posts, $wpdb->postmeta, '_price' )
		) );
		wp_dequeue_script('wp-job-manager-ajax-filters' );
		wp_deregister_script('wp-job-manager-ajax-filters');
		wp_register_script( 'workscout-wp-job-manager-ajax-filters', get_stylesheet_directory_uri() . '/js/workscout-ajax-filters.js', array( 'jquery', 'jquery-deserialize' ), '20150705', true );
		wp_localize_script( 'workscout-wp-job-manager-ajax-filters', 'job_manager_ajax_filters', array(
				'ajax_url'                	=> $ajax_url,
				'is_rtl'                  	=> is_rtl() ? 1 : 0,
				'lang'                    	=> defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : '', // WPML workaround until this is standardized
				'i18n_load_prev_listings' 	=> esc_html__( 'Load previous listings', 'workscout' ),
				'salary_min'		      	=> $min,
				'salary_max'		      	=> $max,
				'rate_min'		      		=> $ratemin,
				'rate_max'		      		=> $ratemax,
				'currency'		      		=> get_workscout_currency_symbol(),

			) );
		$ajax_url = admin_url( 'admin-ajax.php', 'relative' );
		//$ajax_url = get_bloginfo('url').'/wp-admin/admin-ajax.php';

		
		$resume_ratemin = floor( $wpdb->get_var(
	    $wpdb->prepare('
	            SELECT min(meta_value + 0)
	            FROM %1$s
	            LEFT JOIN %2$s ON %1$s.ID = %2$s.post_id
	            WHERE meta_key IN ("' . implode( '","',  array( '_rate') ) . '")
	            AND meta_value != ""
	        ', $wpdb->posts, $wpdb->postmeta )
	    ) );	

	    $resume_ratemax = ceil( $wpdb->get_var(
		$wpdb->prepare('
		    SELECT max(meta_value + 0)
		    FROM %1$s
		    LEFT JOIN %2$s ON %1$s.ID = %2$s.post_id
		    WHERE meta_key IN ("' . implode( '","',  array( '_rate' ) ) . '")
		', $wpdb->posts, $wpdb->postmeta, '_price' )
		) );
		wp_dequeue_script('wp-resume-manager-ajax-filters' );
		wp_deregister_script('wp-resume-manager-ajax-filters');
		wp_register_script( 'workscout-wp-resume-manager-ajax-filters', get_template_directory_uri() . '/js/workscout-resumes-ajax-filters.js', array( 'jquery', 'jquery-deserialize' ), '20150705', true );
		wp_localize_script( 'workscout-wp-resume-manager-ajax-filters', 'resume_manager_ajax_filters', array(
			'ajax_url' => $ajax_url,
			'rate_min'		      		=> $ratemin,
			'rate_max'		      		=> $ratemax,
			'currency'		      		=> get_workscout_currency_symbol(),
		) );

	}
	
	//wp_enqueue_script( 'suggest');
	wp_enqueue_script( 'jquery-ui-autocomplete' );

	wp_enqueue_script( 'workscout-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	
	wp_enqueue_script('jquery-ui-slider'); 
	
	wp_enqueue_script( 'workscout-chosen', get_template_directory_uri() . '/js/chosen.jquery.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-hoverIntent', get_template_directory_uri() . '/js/hoverIntent.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-counterup', get_template_directory_uri() . '/js/jquery.counterup.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array('jquery'), '20150705', true );
	
	wp_enqueue_script( 'google-maps', 'https://maps.google.com/maps/api/js' );
	wp_enqueue_script( 'workscout-gmaps', get_template_directory_uri() . '/js/jquery.gmaps.min.js', array('jquery'), '20150705', true );
	
	
	wp_enqueue_script( 'workscout-jpanelmenu', get_template_directory_uri() . '/js/jquery.jpanelmenu.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array(), '20150705', true );
	wp_enqueue_script( 'workscout-magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-superfish', get_template_directory_uri() . '/js/jquery.superfish.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-tools', get_template_directory_uri() . '/js/jquery.themepunch.tools.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-showbizpro', get_template_directory_uri() . '/js/jquery.themepunch.showbizpro.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-stacktable', get_template_directory_uri() . '/js/stacktable.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-tooltip-js', get_template_directory_uri() . '/js/tooltipster.bundle.min.js', array('jquery'), '20150705', true );
	wp_enqueue_script( 'workscout-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '20150705', true );

	wp_localize_script( 'workscout-custom', 'ws',
    array(
        'logo'=> Kirki::get_option( 'workscout','pp_logo_upload', ''),
        'retinalogo'=> Kirki::get_option( 'workscout','pp_retina_logo_upload',''),
        )
    );

	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'workscout_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom registration form
 */

require get_template_directory() . '/inc/registration.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Load Job Menager related stuff
 */
require get_template_directory() . '/inc/wp-job-manager.php';

/**
 * Load shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Load ptshortcodes
 */
require get_template_directory() . '/inc/ptshortcodes.php';

/**
 * Load woocommerce 
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load TGMPA.
 */
require get_template_directory() . '/inc/tgmpa.php';

/**
 * Load widgets.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Load Envato.
 */
require get_template_directory() . '/inc/github.php';

  
/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
add_action( 'vc_before_init', 'workscout_vcSetAsTheme' );
function workscout_vcSetAsTheme() {
    vc_set_as_theme( $disable_updater = true );
}

function workscout_remove_frontend_links() {
    vc_disable_frontend(); // this will disable frontend editor
}
//add_action( 'vc_after_init', 'workscout_remove_frontend_links' );

/**
 * Load Visual Composer compatibility file.
 */

if(function_exists('vc_map')) {
    require_once get_template_directory() . '/inc/vc.php';
    //require_once get_template_directory() . '/inc/vc_modified_shortcodes.php';
}


//if(is_singular( 'job_listing' ) ){

	add_action( 'init', 'my_custom_rewrites' );
//}
function my_custom_rewrites(){

     add_rewrite_tag( '%job_listing_category%', '([^/]+)' );

    add_rewrite_rule(
        '^travel-nurse-jobs/([a-zA-Z_-]+)/?$',
        'index.php?job_listing_category=$matches[1]',
        'top' );
		add_rewrite_tag( '%job_listing%', '([^/]+)' );
		add_rewrite_rule(
        '^travel-nurse-jobs/([a-zA-Z_0-9\-]+)-([0-9]+)/?$',
        'index.php?job_listing=$matches[1]-$matches[2]',
        'top' );
		
		add_rewrite_tag( '%job_listing_region%', '([^/]+)' );
		add_rewrite_rule(
        '^travel-nurse-jobs-opportunities/([^/]+)/?$',
        'index.php?job_listing_region=$matches[1]',
        'top' );
}


add_filter('job_manager_get_listings_custom_filter_text','change_travel_search_jobs_heading_text');

function change_travel_search_jobs_heading_text($heading_text)
{ 
   $heading_text = str_replace('Showing all','Showing all Travel Nursing',$heading_text);
	return  $heading_text;
}

add_filter('widget_text', 'do_shortcode');


add_action('job_region_custom_location_dropdown_action','job_region_custom_location_dropdown');
function job_region_custom_location_dropdown()
{
	$northeastarray = array(
"Connecticut (CT)",
"Maine (ME)",
"Massachusetts (MA)",
"New Hampshire (NH",
"Rhode Island (RI",
"Vermont (VT)",
"New Jersey (NJ)",
"New York (NY)",
"Pennsylvania (PA)",
"Delaware (DE)",
"Maryland (MD)",
);
$midwestarray = array(
"Illinois (IL)",
"Indiana (IN)",
"Michigan (MI)",
"Ohio (OH)",
"South Dakota (SD)",
"Wisconsin (WI)",
"Iowa (IA)",
"Kansas (KS)",
"Minnesota (MN)",
"Missouri (MO)",
"Nebraska (NE)",
"North Dakota (ND)",
);
$southwestarray = array(
"Texas (TX)",
"Oklahoma (OK)",
"New Mexico (NM)",
"Arizona (AZ)",
);
$southeastarray = array(
"West Virginia (WV)",
"Virginia (VA)",
"Kentucky (KY)",
"Tennessee (TN)",
"North Carolina (NC)",
"South Carolina (SC)",
"Georgia (GA)",
"Alabama (AL)",
"Mississippi (MS)",
"Arkansas (AR)",
"Louisiana (LA)",
"Florida (FL)",
);
$westarray = array(
"Colorado (CO)",
"Idaho (ID)",
"Montana (MT)",
"Nevada (NV)",
"New Mexico (NM)",
"Utah (UT)",
"Wyoming (WY)",
"Alaska (AK)",
"California (CA)",
"Hawaii (HI)",
"Oregon (OR)",
"Washington (WA)",
);
			  $job_parent_terms = get_terms( array(
				'taxonomy' => 'job_listing_region',
				'hide_empty' => false,
				'exclude'=>array(1169,1170,1172,1171,1168),
				'parent'=>0,
			) );

			$array_parent_regions = array();
			$array_region_terms = array();
			// $job_region_select_html = '<select name="search_location_region" id="search_location_region"><option value="">Location</option><optgroup label="By Region"></optgroup>';
			$job_region_select_html = '<select name="search_location_region" id="search_location_region"><option value="">Location</option>';
			$job_region_select_html .= '<option value="midwest">Midwest</option>';
			$job_region_select_html .= '<option value="northeast">Northeast</option>';
			$job_region_select_html .= '<option value="southwest">Southwest</option>';
			$job_region_select_html .= '<option value="southeast">Southeast</option>';
			$job_region_select_html .= '<option value="west">West</option>';
			foreach($job_parent_terms as $key=>$parent_term) {
					$job_region_select_html .='<option '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$parent_term->slug?'selected="selected"':'').' value="'.$parent_term->slug.'">'.ucfirst($parent_term->name).'</option>';
				}
			// $job_region_select_html .= '<optgroup label="Northeast">';
			// foreach($job_parent_terms as $key=>$parent_term)
			// {
			// 	$array_region_terms[] = $parent_term;
			// 	if (in_array($parent_term->name, $northeastarray)) {
			// 		$job_region_select_html .='<option '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$parent_term->slug?'selected="selected"':'').' value="'.$parent_term->slug.'">'.ucfirst($parent_term->name).'</option>';
			// 	}
			// 	$array_parent_regions[] = $parent_term->term_id;
			// }
			// $job_region_select_html .= '</optgroup>';

			// $job_region_select_html .= '<optgroup label="Midwest">';
			// foreach($job_parent_terms as $key=>$parent_term)
			// {
			// 	$array_region_terms[] = $parent_term;
			// 	if (in_array($parent_term->name, $midwestarray)) {
			// 		$job_region_select_html .='<option '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$parent_term->slug?'selected="selected"':'').' value="'.$parent_term->slug.'">'.ucfirst($parent_term->name).'</option>';
			// 	}
			// 	$array_parent_regions[] = $parent_term->term_id;
			// }
			// $job_region_select_html .= '</optgroup>';

			// $job_region_select_html .= '<optgroup label="Southwest">';
			// foreach($job_parent_terms as $key=>$parent_term)
			// {
			// 	$array_region_terms[] = $parent_term;
			// 	if (in_array($parent_term->name, $southwestarray)) {
			// 		$job_region_select_html .='<option '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$parent_term->slug?'selected="selected"':'').' value="'.$parent_term->slug.'">'.ucfirst($parent_term->name).'</option>';
			// 	}
			// 	$array_parent_regions[] = $parent_term->term_id;
			// }
			// $job_region_select_html .= '</optgroup>';

			// $job_region_select_html .= '<optgroup label="Southeast">';
			// foreach($job_parent_terms as $key=>$parent_term)
			// {
			// 	$array_region_terms[] = $parent_term;
			// 	if (in_array($parent_term->name, $southeastarray)) {
			// 		$job_region_select_html .='<option '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$parent_term->slug?'selected="selected"':'').' value="'.$parent_term->slug.'">'.ucfirst($parent_term->name).'</option>';
			// 	}
			// 	$array_parent_regions[] = $parent_term->term_id;
			// }
			// $job_region_select_html .= '</optgroup>';

			// $job_region_select_html .= '<optgroup label="West">';
			// foreach($job_parent_terms as $key=>$parent_term)
			// {
			// 	$array_region_terms[] = $parent_term;
			// 	if (in_array($parent_term->name, $westarray)) {
			// 		$job_region_select_html .='<option '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$parent_term->slug?'selected="selected"':'').' value="'.$parent_term->slug.'">'.ucfirst($parent_term->name).'</option>';
			// 	}
			// 	$array_parent_regions[] = $parent_term->term_id;
			// }
			// $job_region_select_html .= '</optgroup>';

			   $job_states_terms = get_terms( array(
						'taxonomy' => 'job_listing_region',
						'orderby'=>'name',
						'hide_empty' => false,
						'exclude'=>$array_parent_regions,
						
					) );
   //            $job_region_select_html .= '<optgroup label="By State">';
			// foreach($job_states_terms as $key=>$job_states)
			// {
			// 	if(strpos($job_states->slug,'multi-state-license') ===false){
					
			// 	  $job_region_select_html .='<option  '.(isset($_REQUEST['search_location_region']) && $_REQUEST['search_location_region']==$job_states->slug?'selected="selected"':'').' value="'.$job_states->slug.'">'.ucfirst($job_states->name).'</option>';
				  
			// 	}
			// }
			// $job_region_select_html .= '</optgroup>';
			$job_region_select_html .= '</select>';
			echo   $job_region_select_html;	
	
}



add_action('job_specialty_custom_job_dropdown_action','job_specialty_custom_job_dropdown');
function job_specialty_custom_job_dropdown()
{
	
			  $job_specialty_terms = get_terms( array(
				'taxonomy' => 'job_listing_category',
				'hide_empty' => false,
				'orderby'=>'name',
				'exclude'=>array(0),
				
			) );

if(strpos($_SERVER['REQUEST_URI'],'travel-nurse-jobs')!==false) { 
	$array_cat = explode('travel-nurse-jobs/',$_SERVER['REQUEST_URI']);
	$category = rtrim($array_cat[1],'/');
	$job_cat = get_term_by('slug',$category,'job_listing_category');
}

			// $job_specialty_select_html = '<select name="search_categories[]" id="search_categories" max="5" maxlength="5" multiple><option value="">Specialty</option>';
			$job_specialty_select_html = '<select name="search_categories[]" id="search_categories" max="5" maxlength="5" multiple>';
			
             
			foreach($job_specialty_terms as $key=>$specialty_term)
			{
                  $pods = pods( 'job_listing_category', $specialty_term->term_id ); 
                  $cat = $pods->field( 'type_of_category' );
                  if (is_array($cat)) {
                  	foreach ($cat as $key => $value) {
					  if(strpos(strtolower($value),strtolower(($_COOKIE["cat_selection"]))) !== FALSE) {
					 	 $job_specialty_select_html .='<option  '.(isset($_REQUEST['search_categories']) && in_array($specialty_term->term_id,$_REQUEST['search_categories'])?'selected="selected"':'').(isset($job_cat->term_id) && ($specialty_term->term_id==$job_cat->term_id)?'selected="selected"':'').' value="'.$specialty_term->term_id.'">'.ucfirst($specialty_term->name).'</option>';
	                  }
                  	}
                  } else {
					  if(strpos(strtolower($cat),strtolower(($_COOKIE["cat_selection"]))) !== FALSE) {
					 	 $job_specialty_select_html .='<option  '.(isset($_REQUEST['search_categories']) && in_array($specialty_term->term_id,$_REQUEST['search_categories'])?'selected="selected"':'').(isset($job_cat->term_id) && ($specialty_term->term_id==$job_cat->term_id)?'selected="selected"':'').' value="'.$specialty_term->term_id.'">'.ucfirst($specialty_term->name).'</option>';
	                  }
                  }

				  
				
			}
			$job_specialty_select_html .= '</select>';
			// echo   $job_specialty_select_html;	

	global $wpdb;
	// $catset = ($_COOKIE["cat_selection"] == 'providers') ? 'provider' : $_COOKIE["cat_selection"];
	$web_specialties = $wpdb->get_results( "SELECT * FROM specialties", OBJECT );
	$specialties_array = [];
	$pspecialties_array = [];
	$nspecialties_array = [];
	$aspecialties_array = [];

	foreach ($web_specialties as $key => $value) {
		if ($value->web_category == 'provider') {
			array_push($pspecialties_array, $value->title);
		}
		if ($value->web_category == 'nursing') {
			array_push($nspecialties_array, $value->title);
		}
		if ($value->web_category == 'allied') {
			array_push($aspecialties_array, $value->title);
		}
		$specialties_array[$value->title] = $value->title;
	}
	if (is_front_page()) {
		if ($_COOKIE["cat_selection"] == 'nursing') {
			$specialties_array = $nspecialties_array;
		} elseif ($_COOKIE["cat_selection"] == 'allied') {
			$specialties_array = $aspecialties_array;
		} elseif ($_COOKIE["cat_selection"] == 'providers') {
			$specialties_array = $pspecialties_array;
		}
	}

	sort($specialties_array);


	$job_specialty_select_html = '<select name="search_categories[]" id="search_categories" max="5" maxlength="5" multiple>';
	foreach ($specialties_array as $key => $value) {
	 	$providerD = (in_array($value, $pspecialties_array)) ? '1' : '0';
	 	$nursingD = (in_array($value, $nspecialties_array)) ? '1' : '0';
	 	$alliedD = (in_array($value, $aspecialties_array)) ? '1' : '0';

	 	if ($_COOKIE['specialty_cookie'] != '' && $_COOKIE['specialty_cookie'] != 'null') {
			$job_specialty_select_html .= '<option value="'.$value.'" data-provider="'.$providerD.'" data-nursing="'.$nursingD.'" data-allied="'.$alliedD.'">'.ucfirst($value).'</option>';
	 	} else {
			$job_specialty_select_html .= '<option value="'.$value.'" '.(isset($_REQUEST['search_categories']) && in_array($value, $_REQUEST['search_categories'])?'selected="selected"':'').' data-provider="'.$providerD.'" data-nursing="'.$nursingD.'" data-allied="'.$alliedD.'">'.ucfirst($value).'</option>';
	 	}

	}
	$job_specialty_select_html .= '</select>';
	echo  $job_specialty_select_html;	

}



add_filter('the_company_logo','site_logo_when_no_company_logo',10,2);

function site_logo_when_no_company_logo($logo,$post)
{	
	$file_contents = '';
	if ($logo) {
		$file_contents = file_get_contents($logo);
	}
	if($file_contents =='')
	{ 
		$logo =get_template_directory_uri().'/images/blueforce-logo-image.png'; 
	}
	return $logo;
}





/*global $wp_filter;
echo "<pre>";
print_r($wp_filter['the_company_logo']);
$reflFunc = new ReflectionFunction('site_logo_when_no_location_logo');
print $reflFunc->getFileName() . ':' . $reflFunc->getStartLine();
die;*/
