
	<!-- Titlebar
================================================== -->

<?php $header_image = get_post_meta($post->ID, 'pp_job_header_bg', TRUE); 
if(!empty($header_image)) { ?>
	<div id="titlebar" class="photo-bg single" style="background: url('<?php echo esc_url($header_image); ?>')">
<?php } else { ?>
	<div id="titlebar" class="single">
<?php } ?>
	<div class="container">
		<div class="">
			<div class="ten columns">
				<h1 id="selectionTitle"><?php echo ucwords($_COOKIE['cat_selection']); ?></h1>
				<?php if(function_exists('bcn_display')) { ?>
				<?php } ?>
				<?php 
				$count_jobs = wp_count_posts( 'job_listing', 'readable' );
				?>
				<span>
				<?php printf( esc_html__( 'We have %s job offers for you', 'workscout' ), '<em class="count_jobs">' . $count_jobs->publish . '</em>' ) ?>
				</span>
				<!-- <h2 class="showing_jobs"><?php esc_html_e('Showing All Travel Nursing Jobs','workscout') ?></h2> -->
			</div>
			<?php if(get_option('workscout_enable_add_job_button')) { ?>
		        <div class="six columns">
		        <nav id="breadcrumbs" style="margin-top: -22px;" xmlns:v="http://rdf.data-vocabulary.org/#">
					<ul>
			        	<?php bcn_display_list(); ?>
			        </ul>
				</nav>
					     <?php  $option_tree = get_option('option_tree');?>
					<a href="#apply-dialog" class="button small-dialog popup-with-zoom-anim"><?php esc_html_e('Apply for your dream job','workscout'); ?></a>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php 
	$layout  = get_post_meta($post->ID, 'pp_sidebar_layout', true);
	if(empty($layout)) { $layout = 'right-sidebar'; }

	wp_dequeue_script('wp-job-manager-ajax-filters' );
	wp_enqueue_script( 'workscout-wp-job-manager-ajax-filters' );
?>

<div class="container <?php echo esc_attr($layout); ?>">

	
	<?php  get_sidebar('jobs');?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('eleven columns jobs-listing-content-align-left'); ?>>
		<div class="padding-right">
			<?php 
			if ( ! empty( $_GET['search_keywords'] ) ) {
				$keywords = sanitize_text_field( $_GET['search_keywords'] );
			} else {
				$keywords = '';
			}
			?>
			<!-- <form class="list-search"  method="GET" action="<?php echo get_permalink(get_option('job_manager_jobs_page_id')); ?>"> -->
			<form class="list-search"  method="GET" action="javascript:void(0);">
				<div class="search_keywords mainSearch">
					<button><i class="fa fa-search"></i></button>
					<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'job title, keywords or company name', 'workscout' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
					<div class="clearfix"></div>
                    <div class="job-listing-disclaimer"><p>See a job you like but the orientation date doesn't fit, discuss your options with a recruiter now: 1-866-795-2583!</p></div>
				</div>
			</form>
     
			<?php the_content(); ?>
			<footer class="entry-footer">
				<?php edit_post_link( esc_html__( 'Edit', 'workscout' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article>

</div>
