<?php
/**
 * Template Name: Skills checklists
**/
get_header();
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


<?php

while ( have_posts() ) : the_post(); 

	$job_page = get_option('job_manager_jobs_page_id');
	$resume_page = get_option('resume_manager_resumes_page_id');
	
	if(!empty($job_page) && is_page($job_page)){
		get_template_part( 'jobs' );	
	} elseif (!empty($resume_page) && is_page($resume_page)) {
		get_template_part( 'resumes' );
	}
	else {
		get_template_part( 'template-parts/content', 'page' ); 
	}

endwhile; // End of the loop. 


$args = array(
    'taxonomy' => 'skills_checklists',
    'hide_empty' => false,
    'order' => 'DESC'
);
?>
<div class="checklists">
    <div class="container">
        <?
        $terms = get_terms( $args );
        $count = count($terms);
        $count2 = 0;
        foreach($terms as $term) {
            $pods = pods( 'skills_checklists', $term->slug ); 
            $categories_sp = $pods->field('choose_category');            
             if(gettype($categories_sp) == 'array') {
                foreach($categories_sp as $item) {
                    if(strtolower($item) == strtolower($_COOKIE["cat_selection"])) {
                        $count2++;
                    };
                };
            } else if(strtolower($categories_sp) == strtolower($_COOKIE["cat_selection"])) {
                $count2++;
            }           
        }
        $count2 = floor($count2 / 2);
        foreach($terms as $term) {
            $pods = pods( 'skills_checklists', $term->slug ); 
            $categories_sp = $pods->field('choose_category');
            $can = 0;
            if(gettype($categories_sp) == 'array') {
                foreach($categories_sp as $item) {
                    if(strtolower($item) == strtolower($_COOKIE["cat_selection"])) {
                        $can = 1;
                    };
                };
            } else if(strtolower($categories_sp) == strtolower($_COOKIE["cat_selection"])) {
                $can = 1;
            }
            if($can == 1) {
                $c++;
        ?>
                <!-- <div class="col"><p><i class="far fa-file-pdf"></i> <a  target="_blank" href="<?php echo get_template_directory_uri(); ?>/filesave.php?filename=<?php echo $pods->field( 'pdf_file')['guid']; ?>"><?php echo $term->name ?></a></p></div> -->
                <div class="col"><p><i class="far fa-file-pdf"></i> <a  target="_blank" href="<?php echo $pods->field( 'pdf_file')['guid']; ?>"><?php echo $term->name ?></a></p></div>
        <?php
            }
        }
        ?>
    </div>
</div>



<?php get_footer(); ?>