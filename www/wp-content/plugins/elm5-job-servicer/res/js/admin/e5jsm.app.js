(function(){
    var UNDEFINED, 
        $ = jQuery,
        container,
        processingClass = 'processing',
        UNFN = function(){},
        blockUI = function(txt){
            if (txt) {
                $.blockUI({ message: txt });
            }
            else {
                $.blockUI();
            }            
        },
        unblockUI = function(){
            $.unblockUI();
        },
        blockButtons = function() {
            container.find("button").prop('disabled', true);            
        },
        unblockButtons = function() {
            container.find("button").prop('disabled', false);            
        },
        reloadPage = function(){
            location.reload();
        };
        
    $(document).ready(function($) {
        container = $('.E5JSMContainer');
        
        
        $(".showDetailsButton").click(function(){     
            if($(".subDetails").is(":visible")) {
                $(".showDetailsButton").text('Show more details');
                $(".subDetails").slideUp();
            }
            else {
                $(".showDetailsButton").text('Hide details');
                $(".subDetails").slideDown();
            }            
        });
        $(".updateChangeListButton").click(function(){     
               var data = {
                   'action': 'E5JSM_updatechangetable'
               };
               blockUI("Please wait... it may take some time to aggregate...");
               $.post(ajaxurl, data, function(response) {
                   reloadPage();
               });     
            return false;
        });

         $(".cleanChangeListButton").click(function(){     
               var data = {
                   'action': 'E5JSM_flushchangetable'
               };
               blockUI();
               $.post(ajaxurl, data, function(response) {
                   reloadPage();
               });     
            return false;
        });
         $(".processDatesButton").click(function(){  
             var resetDate = parseInt($("#resetDate").val()) || 10,
                batchItemCount = parseInt($("#batchItemCount").val()) || 1,
                data = {
                   'action': 'E5JSM_updatesubscriptiondates',
                   'reset_date': resetDate,
                   'batch_item_count': batchItemCount
                };
            hideNotice();
            cleanLog();
            processData(data);
            return false;
        });
    });    
    
    function showError(msg) {
        showNotice(msg, 'error');
    }
    function showNotice(msg, ntype) {
        ntype = ntype || 'success';
        container.find('.notice')
            .html(msg)
            .removeClass('hiddden')
            .addClass(ntype);
    }
    function hideNotice() {
            container.find('.actionLogsContainer .content').html("");
    }
    function showActionProgress() {
            container.find('.actionLogsContainer h3, .mainActionsContainer h3').addClass(processingClass);
    }
    function hideActionProgress() {
            container.find('.actionLogsContainer h3, .mainActionsContainer h3').removeClass(processingClass);
    }
    function cleanLog() {
        container.find('.notice').addClass('hiddden');
    }
    function showResultData(result) {
        var data = result.data, 
            i,
            row,
            cols,
            col,
            logContainer = container.find('.actionLogsContainer .content'), 
            table = logContainer.find('table'),
            tableBody,
            mapKey,
            dataType,
            dataValue,
            dataClass,
            dataMap = {
                'id': 'number',
                'update_status': 'yesnno',
                'order_id': 'number',
                'product_id': 'number',
                'old_period': 'string',
                'old_length': 'number',
                'old_status': 'string',
                'old_start_date': 'date',
                'old_renewal_date': 'date',
                'new_renewal_date': 'date',
                'old_expiry_date': 'date',
                'new_expiry_date': 'date'
            };
    
        if (!table.length) {
            table = $('<table class="wp-list-table widefat fixed striped logTable"></table>').appendTo(logContainer);
            table.append("<thead>" +
                    "<tr>" +
                    "<th>ID</th>" +
                    "<th>Success</th>" +
                    "<th class='highlight'>Order ID</th>" +
                    "<th>Product ID</th>" +
                    "<th>Period</th>" +
                    "<th>Length</th>" +
                    "<th>Status</th>" +
                    "<th>Start</th>" +
                    "<th>Old Renewal</th>" +
                    "<th class='highlight'>New Renewal</th>" +
                    "<th>Old Expiration</th>" +
                    "<th class='highlight'>New Expiration</th>" +
                    "</tr></thead>");
            
            table.append("<tbody></tobdy>");
        }
        
        tableBody = table.find('tbody');
        
        for(i in data) {
            cols = data[i];
            row = [];
            for(mapKey in dataMap) {
                dataClass = "";
                dataType = dataMap[mapKey];                
                dataValue = cols[mapKey];                
                if (dataValue == null || dataValue == UNDEFINED) {
                    dataValue = "";
                }
                switch (dataType) {
                    case 'yesnno':
                        dataValue = dataValue == 1 ? "Yes" : "No";
                        break;
                    case 'date':
                        dataValue = getDateString(dataValue);
                        break;
                }
                switch(mapKey) {
                    case "old_length":
                        dataValue = dataValue == 0 ? "Never Expire" : dataValue;
                        break;
                    case "order_id":
                    case "new_renewal_date":
                    case "new_expiry_date":
                        dataClass = "highlight";
                        break;
                    default:
                        dataClass = "";
                        
                }
                row.push("<td class='" +dataClass+ "'>" +dataValue+ "</td>");
                
            }
            
            reCalculateCount(cols);
            tableBody.append("<tr>" + row.join('') + "</tr>"); 
        }
        
    }
    
    function reCalculateCount(data) {
        var currentPendingNet = parseInt(container.find('.tspn').text()),
            currentPending = parseInt(container.find('.tsp').text()),
            currentUpdated = parseInt(container.find('.tsu').text()),
            status = data.update_status;
    
        if (status == 1) {
            currentPendingNet--;
            currentPending--;
            currentUpdated++;
            container.find('.tspn').text(currentPendingNet);
            container.find('.tsp').text(currentPending);
            container.find('.tsu').text(currentUpdated);
        }
    }


    function getDateString(d) {
        if (!d) {
            return "";
        }
        var date = new Date(Date.parse(d)).toString().replace(/\s\d{2}\:.+$/, '')
        return date;
        
    }

    function processData(data) {
        blockButtons(); 
        showActionProgress();
        $.post(ajaxurl, data, function(response) {
            if (response.error) {
                showError(response.error);
            }
            if (response.message) {
                showNotice(response.message);
            }
            if (response.data) {
                showResultData(response);
            }
            if (response.success) {
                processData(data);
                unblockButtons();
                hideActionProgress();
            }
            else {
                unblockButtons();
                hideActionProgress();
            }

        }, 'json');     
    }

}());
