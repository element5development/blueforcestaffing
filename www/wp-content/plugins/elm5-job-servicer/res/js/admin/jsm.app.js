(function(){
    var UNDEFINED, 
        $ = jQuery,
        container,
        processingClass = 'processing',
        UNFN = function(){},
        blockUI = function(txt){
            if (txt) {
                $.blockUI({ message: txt });
            }
            else {
                $.blockUI();
            }            
        },
        unblockUI = function(){
            $.unblockUI();
        },
        blockButtons = function() {
            container.find("button").prop('disabled', true);            
        },
        unblockButtons = function() {
            container.find("button").prop('disabled', false);            
        },
        reloadPage = function(){
            location.reload();
        };
        
    $(document).ready(function($) {
        container = $('.e5jsmContainer');
        var table = container.find('table');
        
        $(".checkUncheckAll").change(function(){     
            var isChecked = $(this).prop('checked');
            table.find('tbody input[type=checkbox]:visible').prop('checked', isChecked);
        });
        
        $(".selectNotListed").click(function(){
            table.find('tbody input[type=checkbox]').prop('checked', false);
            table.find('tbody tr.notlisted input[type=checkbox]').prop('checked', true);
        });
        $(".selectActive").click(function(){
            table.find('tbody input[type=checkbox]').prop('checked', false);
            table.find('tbody tr.activejob input[type=checkbox]').prop('checked', true);
        });
        $(".selectInactive").click(function(){
            table.find('tbody input[type=checkbox]').prop('checked', false);
            table.find('tbody tr.inactivejob input[type=checkbox]').prop('checked', true);
        });
        $(".selectDuplicates").click(function(){
            table.find('tbody input[type=checkbox]').prop('checked', false);
            table.find('tbody tr.hasDuplicates').not('.activejob').find('input[type=checkbox]').prop('checked', true);
        });

        $(".showNotListed").click(function(){
            table.find('tbody tr').hide();
            table.find('tbody tr.notlisted').show();
        });
        $(".showActive").click(function(){
            table.find('tbody tr').hide();
            table.find('tbody tr.activejob').show();
        });
        $(".showInactive").click(function(){
            table.find('tbody tr').hide();
            table.find('tbody tr.inactivejob').show();
        });
        $(".showWithDuplicates").click(function(){
            table.find('tbody tr').hide();
            table.find('tbody tr.hasDuplicates').show();
        });
        $(".showAll").click(function(){     
            table.find('tbody tr').show();
        });
        

        $(".trashIt").click(function(){
            var ids = getCheckedItems(table.find('tbody'));                
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_remove_post');
            return false;             
        });
        $(".scrapIt").click(function(){
            var ids = getCheckedItems(table.find('tbody'));                
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_scrap_post');
            return false;                          
        });
        $(".removeActive").click(function(){
            var ids = getCheckedItems(table.find('tbody'), '.activejob');
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_remove_post');
            return false;            
        });
        $(".removeNotListed").click(function(){
            var ids = getCheckedItems(table.find('tbody'), '.notlisted');
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_remove_post');
            return false;            
        });
        $(".removeInactive").click(function(){
            var ids = getCheckedItems(table.find('tbody'), '.inactivejob');
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_remove_post');
            return false;            
        });
        $(".removeDuplicate").click(function(){
            var ids = getCheckedItems(table.find('tbody'), '.hasDuplicates', '.activejob');
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_remove_post');
            return false;
        });
        
        $(".addToList").click(function(){
            var ids = getCheckedItems(table.find('tbody'), '.notlisted');
            buildAjaxQueueBasedOnIDs(ids, 'e5jsm_enlist');
            return false;
        });

        $(".gotoFirstPage").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_tableoffset_first'}, function() { reloadPage(); });            
            return false;            
        });
        $(".gotoLastPage").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_tableoffset_last', total: totalRecords || 0}, function() { reloadPage(); });            
            return false;            
        });
        $(".gotoPreviousPage").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_tableoffset_decrease'}, function() { reloadPage(); });            
            return false;            
        });
        $(".gotoNextPage").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_tableoffset_increase'}, function() { reloadPage(); });            
            return false;            
        });
        $(".increasePageLimit").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_tablelimit_increase'}, function() { reloadPage(); });            
            return false;            
        });
        $(".decreasePageLimit").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_tablelimit_decrease'}, function() { reloadPage(); });            
            return false;            
        });
        $(".processDownwards").click(function(){  
            $.post(ajaxurl, {action: 'e5jsm_processdownwards_toggle'}, function(data) { 
                if (data == 1) {
                    console.log("auto-initiate previous page");
                    $('.gotoPreviousPage').click();
                }
                else {
                    console.log("Not auto-processing downwanrds");
                }
            });            
            return false;            
        });
        $(".searchCustomIDs").click(function(){  
            var ids = (''+$('#customIDs').val()).replace(/\s/, '');
            if (!ids) {
                return;
            }
            $.post(ajaxurl, {action: 'e5jsm_set_customids', ids: ids}, function(data) { 
                if (data == 1) {
                    reloadPage();                    
                }
                else {
                    
                }
            });            
            return false;            
        });
        
    });    
    
    function getCheckedItems(parent, filter, notFilter) {
        if (!filter) {
            filter = '';
        }
        else {
            filter += ' ';
        }
        
        var idsArray = [], 
            elms = parent.find(filter + "input[type=checkbox]:checked:visible");

        if (notFilter) {
            elms = elms.not(notFilter);
        }
    
        elms.each(function(){
            var id = $(this).attr('data-id');
            idsArray.push(id);
        });        
        
        return idsArray;
    }
    
    function showError(msg) {
        showNotice(msg, 'error');
    }
    function showNotice(msg, ntype) {
        ntype = ntype || 'success';
        $('.notice').removeClass('hidden success error').addClass(ntype);
        $('.notice').html(msg);
    }
    function hideNotice() {
            container.find('.actionLogsContainer .content').html("");
    }
    function showActionProgress() {
            container.find('.actionLogsContainer h3, .mainActionsContainer h3').addClass(processingClass);
    }
    function hideActionProgress() {
            container.find('.actionLogsContainer h3, .mainActionsContainer h3').removeClass(processingClass);
    }
    function cleanLog() {
        $('.notice').addClass('hiddden');
    }

    function processData(data, callback, failback) {
        blockButtons(); 
        showActionProgress();
        $.post(ajaxurl, data, function(response) {
            if (response.error) {
                showError(response.error);
                if (failback) {
                    failback(response, data);
                }
            }
            if (response.message) {
                showNotice(response.message);
            }
            //if (response.success) {
                if (callback) {
                    callback(response, data);
                }                
            //}
            
            unblockButtons();
            hideActionProgress();

        }, 'json');     
    }
    
    
    function buildAjaxQueueBasedOnIDs(ids, action, filter, nextCallback) {
        var id = ids.shift(),
            elm = $((filter ? 'tr.'+filter:'tr')+'[data-id='+id+']'),
            gid = elm.data('gid'),
            data = {
                'action': action,
                'ids': id,
                'gid': gid,
            },
           failback = function (result, fdata) {
               failAjaxCall(result, fdata);
           },
           callback = function(result, cdata) {
               var celm, cgid;
               if (result) {
                   endAjaxCall(result, cdata);
               }
               id = ids.shift();
               celm = $((filter ? 'tr.'+filter:'tr')+'[data-id='+id+']');
               if (celm.length && id) {
                   cgid = elm.data('gid');
                   data.ids = id;
                   data.gid = cgid;
                   initAjaxCall(data);
                   processData(data, callback, failback);
               }
               else {
                   if (nextCallback) {
                       nextCallback();
                   }
               }
           };

       hideNotice();
       cleanLog();
       if (elm.length && id) {
           initAjaxCall(data);
           processData(data, callback, failback);
       }        
        else {
            if (nextCallback) {
                nextCallback();
            }
        }       
    }
    function endAjaxCall(result, requestdata) {
        var data = result.data,
            id = data &&  data.ids,
            value = data && data.value,
            action = requestdata.action,
            target = result.target,
            container = $('tr[data-id='+id+']'),
            cell = container.find('td.postid');

        if (id) {
            container.removeClass('actionprocessing').addClass('actionsuccess');
            cell.removeClass('actionprocessing');
            if (/scrap/.test(action)) {
                container.remove();
            }
            else if (/remove/.test(action)) {
                container.removeClass('activejob publish').addClass('trash');
            }
            else if (/enlist/.test(action)) {
                container.removeClass('notlisted');
            }
        }

    }
    function failAjaxCall(result, requestdata) {
        var data = result.data,
            id = data && data.ids,
            target = result.target,
            container = $('tr[data-id='+id+']'),
            cell = container.find('td.postid');
                       
        container.removeClass('actionprocessing').addClass('actionfailed');
        cell.removeClass('actionprocessing');      
    }
    
    function initAjaxCall(data, requestdata) {
        var target = data.target,
            id = data && data.ids,
            container = $('tr[data-id='+id+']'),
            cell = container.find('td.postid');
            
        container.addClass('actionprocessing');
        cell.addClass('actionprocessing');
    }
    
}());
