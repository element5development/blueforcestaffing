<?php
/**
 * Plugin Name: Elm5 Job Servicer
 * Plugin URI: http://www.element5digital.com/
 * Description: Check and manage Jobs which are created from Google Sheet
 * Author: Element 5 Digital
 * Author URI: http://www.element5digital.com/
 * Version: 1.0.0
 * License: GPLv2 or later
 */
?>
<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
  
global $wpdb;
DEFINE ("ELM5JSM_PLUGINFILE", __FILE__);

require_once( 'includes/admin/install.php' );
require_once( 'includes/admin/plugin.php' );  
require_once( 'includes/admin/hooks.php' );  
require_once( 'includes/admin/ajax.php' );  
require_once( 'includes/admin/helper.php' );  
require_once( 'includes/admin/class_jobmanager.php' );
