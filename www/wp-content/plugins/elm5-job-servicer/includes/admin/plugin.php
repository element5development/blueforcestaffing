<?php

/* Add Menu under Settins in Admin */
function elm5_jobServiceManager() {
    include('page.php');
}
function E5JSM_admin_actions() {
    add_options_page("Job Service Manager", "Job Service Manager", 1, "JobServiceManager", "elm5_jobServiceManager");
}
add_action('admin_menu', 'E5JSM_admin_actions');

/* Add JS files */
add_action( 'admin_enqueue_scripts', 'E5JSM_admin_enqueue_scripts' );
function E5JSM_admin_enqueue_scripts($hook) {
    if( 'index.php' != $hook ) {
    	//return;
    }       
	wp_enqueue_script( 'E5JSM-script',
            plugins_url( 'elm5-job-servicer/res/js/admin/e5jsm.app.js'),
            array('jquery') );	
	wp_enqueue_script( 'JSM-script',
            plugins_url( 'elm5-job-servicer/res/js/admin/jsm.app.js'),
            array('jquery') );	
	wp_enqueue_script( 'E5JSM-blockui',
            plugins_url( 'elm5-job-servicer/res/js/jquery.blockUI.js'),
            array('jquery') );	
	wp_localize_script( 'E5JSM-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
    
//    wp_enqueue_style( 'E5JSM-wc-admin-style', plugins_url('woocommerce/assets/css/admin.css' ) );
    wp_enqueue_style( 'E5JSM-admin-style', plugins_url('elm5-job-servicer/res/css/styles.css' ) );
}
