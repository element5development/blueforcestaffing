<div class="wrap e5jsmContainer" id="poststuff">
    <?php    echo "<h1>" . __( 'Element5 - Jobs Service Manager', 'E5JSM' ) . "</h1>"; ?>
<?php
    @set_time_limit( 0 );
    @ini_set( 'memory_limit', -1);
    global $customIDs, $totalSub, $lastCount; 
    $timeStart = microtime(true);
    
    $jobs = ELM5JSM::getJobListData();
    $list = ELM5JSM::getRecordedGSIDs();

    $timeEnd = microtime(true);
    $timeDiff = round($timeEnd - $timeStart);
    $timeDiffHuman = ''.E5JSMH::secondsToHumanTime($timeDiff);
    $timeTaken = "Time taken: $timeDiffHuman ";
    

    $htmlArr = array();
    $htmlArr[] = "<table class='widefat jobListTableElm5' >";
    
    $count = 0;
    foreach($jobs as $gid => $row){
    foreach($row as $pid => $item){

        if ($count == 0) {
            $htmlArr[] = "<thead>";
            $htmlArr[] = "<tr>";
            foreach ($item as $title => $col) {
                if ($title == 'checkbox') {
                    $htmlArr[] = "<th><input type='checkbox' class='checkUncheckAll' /></th>";
                }
                elseif ($title == 'Count') {
                    $htmlArr[] = "<th>#</th>";
                }
                else {
                    if (strpos($title, '_') !== 0)  {
                        $htmlArr[] = "<th>$title</th>";
                    }
                }
            }
            $htmlArr[] = "</tr>";
            $htmlArr[] = "</thead>";
            $htmlArr[] = "<tbody>";
        }
        $count++;
        $dCount = $item['Duplicate Count'];
        
        $classNamesArray = array();
        $classNamesArray[] = $item['STATUS'];
        $classNamesArray[] = isset($list[$gid]) && $list[$gid] == $pid ? 'activejob' : 'inactivejob';
        $classNamesArray[] = !isset($list[$gid]) ? 'notlisted' : '';
        $classNamesArray[] = empty($dCount) || $dCount == 1 ? 'noDuplicates' : 'hasDuplicates';
        $classNamesArray[] = 'pid-'.$pid;
        
        $rowHTML = array();
        foreach ($item as $title => $val) {
            $sanitizedTitle = E5JSMH::sanitizeTitleToId($title);
            if ($title == 'checkbox') {
                $rowHTML[] = "<td class='$sanitizedTitle' data-id='$pid' data-gid='$gid'>
                    <input type='checkbox' data-id='$pid' data-gid='$gid' class='subselector' />
                    </td>";
            }
            else {
                if (strpos($title, '_') !== 0)  {
                    $cleanVal = strip_tags($val);
                    switch ($sanitizedTitle) {
                        case 'duplicatecount':
                            $val = empty($val) || $val == 1 ? "NONE" : $val;
                            break;
                        default:

                    }
                    $cleanVal = strip_tags($val);
                    $rowHTML[] = "<td class='$sanitizedTitle' data-title='$title' data-value='$cleanVal'>$val</td>";
                }
            }
        }
        $classNames = implode(' ', $classNamesArray);
        $htmlArr[] = "<tr class='$classNames' 
            data-duplicates='$dCount'
            data-gid='$gid'
            data-id='$pid'>";
        $htmlArr[] = implode("\r\n", $rowHTML);
        $htmlArr[] = "</tr>";
        }
    }
    if ($count > 0) {
        $htmlArr[] = "</tbody>";    
    }
    $htmlArr[] = "</table>";    
    
    $tablehtml = implode("\r\n", $htmlArr);

?>
    (<?php echo $timeTaken; ?>)
    <div class="notice hidden" style="padding: 10px;">
    </div>
    <div class="postbox mainActionsContainer" style="padding:5px;">
        <h3>List of Jobs</h3>
        <hr/>        
        <p> SHOW:
            <button class="button-secondary showAll">All</button>
            <button class="button-secondary showWithDuplicates">Having Duplicates</button>
            <button class="button-secondary showInactive">Inactive</button>
            <button class="button-secondary showActive">Active</button>
            <button class="button-secondary showNotListed">Not Listed</button>
        </p>        
        <p> SELECT:
            <button class="button-secondary selectDuplicates">Duplicates</button>
            <button class="button-secondary selectInactive">Inactive</button>
            <button class="button-secondary selectActive">Active</button>
            <button class="button-secondary selectNotListed">Not Listed</button>
        </p>        
        <hr/>
        <p> ACTION:
            <button class="button-primary removeDuplicate">Remove Duplicates</button>
            <button class="button-primary removeNotListed">Remove Not Listed</button>
            <button class="button-primary removeActive">Remove Active</button>
            <button class="button-primary removeInactive">Remove Inactive</button>
            <button class="button-primary trashIt">Trash Selected</button>
            <button class="button-primary scrapIt">Trash and Remove Selected</button>
        </p>
        <hr/>
        <p>
            <button class="button-primary addToList">Include In List</button>
        </p>
        <hr/>
        <p class='legendButtons'>Legends: 
            <button class="noDuplicates">Has no Duplicates</button>
            <button class="activejob">ACTIVE JOB</button>
            <button class="publish">Published</button>
            <button class="trash">Trash</button>
            <button class="notlisted">NOT CACHED</button>            
        </p>        
        <?php echo  $tablehtml; ?>
        <hr/>
    </div>

    <div class="postbox actionLogsContainer">
        <h3 class="hndle">Action Logs</h3>
        <div class="content inside">
            <?php            
                $tz = date_default_timezone_get();
                echo $tz . " " . current_time( 'mysql') . " [ GMT: " . current_time( 'mysql', 1) . "]";                                
            ?>
        </div>
    </div>
</div>

<?php
    $timeEnd = microtime(true);
    $timeDiff = round($timeEnd - $timeStart);
    $timeDiffHuman = ''.E5JSMH::secondsToHumanTime($timeDiff);
    $timeTaken = "Time taken: $timeDiffHuman ";
?>
<?php echo $timeTaken; ?>

<script type="text/javascript">

    jQuery(document).ready(function() {

    });

</script>