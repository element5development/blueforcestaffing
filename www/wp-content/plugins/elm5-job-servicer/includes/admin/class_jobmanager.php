<?php

class ELM5JSM {

    public static function getJobListData() {
        $data = array();
        $map = array(
            'checkbox' => 'checkbox',
            'post_count' => 'Duplicate Count',
            'meta_value' => 'Google JOB ID',
            'post_id' => 'POST ID',
            'post_modified' => 'UPDATED ON',
            'post_status' => 'STATUS',
            'post_title' => 'TITLE',
        );
        $records = self::getPostWithMeta();
        if (!empty($records)) {
            $lastGID = '';
            foreach($records as $item) {
                $val = $item->meta_value;
                $postId = $item->post_id;
                if (!isset($data[$val])) {
                    $data[$val] = array();
                }
                $datum = json_decode(json_encode($item), true);
                $newDatum = array();
                foreach($map as $key => $title) {
                    $newDatum[$title] = isset($datum[$key]) ? $datum[$key] : '';
                }                
                $data[$val][$postId] = $newDatum;
            }
        }
        foreach ($data as &$row) {
            $duplicateCount = count($row);
            foreach($row as &$item) {
                $item['Duplicate Count'] = $duplicateCount;
            }
        }
        //var_dump($data);
        return $data;
        
    }
    private static function getPostWithMeta($meta = '') {
        global $wpdb;
        
        if (empty($meta)) {
            $meta = 'googlesheet_openorders_id';
        }
        $sql = "SELECT p.post_title, p.post_status, p.post_modified, pm.*
            FROM `wp_postmeta` pm
            LEFT JOIN wp_posts p ON p.id=pm.post_id
            WHERE `meta_key` LIKE '%$meta%'
            order by meta_value asc, post_id asc, post_modified asc";

        $records = $wpdb->get_results($sql);
        return $records;
    }
    public static function getRecordedGSIDs() {
        return self::get_my_option('job_gs_ids', array());
    }
    public static function saveRecordedGSIDs($values=array()) {
        return update_option('job_gs_ids', $values);
    }
    public static function removeFromRecordedGSIDs($id, $gid) {
        if (!empty($id) && !empty($gid)) {
            $list = ELM5JSM::getRecordedGSIDs();
            if (isset($list[$gid]) && $list[$gid] == $id) {
                unset($list[$gid]);
                self::saveRecordedGSIDs($list);
            }
        }
        return $id;
    }
    public static function addToRecordedGSIDs($id, $gid) {   
        if (!empty($id) && !empty($gid)) {
            $list = ELM5JSM::getRecordedGSIDs();
            if (!empty($list) && is_array($list)) {
                $list[$gid] = $id;
                ELM5JSM::saveRecordedGSIDs($list);
            }
        }
        return $id;
    }

    public static function get_my_option($option, $default = "") {
        global $wpdb;
        $value = $default;
        $suppress = $wpdb->suppress_errors();
        $options = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name = '$option' ORDER BY option_id DESC" );
        $wpdb->suppress_errors($suppress);

        if (!empty($options) && isset($options[0])) {
            $searchResult = $options[0];
            $value = $searchResult->option_value;
            $value = maybe_unserialize($value);
        }

        return $value;
    }

    public static function getAjaxResponse($data = array(), $error = "") {
        $result = array("success" => 0, "error" => $error, "message" => "", "data" => $data);
        return $result;
    }
}
