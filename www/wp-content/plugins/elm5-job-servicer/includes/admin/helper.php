<?php
class E5JSMH { 
    public static function log($obj, $file = "", $prefixLogWithDate = true) {
        if (empty($file)) {
            $file = "job-".(date("Ymd")).".log";
        }
        $upload_dir = wp_upload_dir();
        $dir = $upload_dir['basedir'] . '/elm5-job-servicer';
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
        
        $path = $dir . '/'. $file;
        $date = "";
        if ($prefixLogWithDate) {
            $date = "[" . date("Y-m-d H:i:s"). "] ";
        }        
        if (is_array($obj) || is_object($obj)) {
            $log = json_encode($obj, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }
        else {
            $log = $obj;
        }
        file_put_contents($path, $date.$log."\r\n", FILE_APPEND);
    }  
    
    public static function logNoDate($obj, $file = "") {
        self::log($obj, $file, false);
    }  
    public static function initlogFile($obj, $file = "") {
        if (empty($file)) {
            $file = "jobs-".(date("Ymd")).".log";
        }
        $upload_dir = wp_upload_dir();
        $dir = $upload_dir['basedir'] . '/elm5-job-servicer';
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }        
        $path = $dir . '/'. $file;        
        if (!file_exists($path)) {
            self::log($obj, $file, false);
        }
    }  
    
    public static function ceilTime($time, $seconds = 300) {
        return ceil($time/$seconds)*$seconds;
    }
    
    public static function sanitizeTitleToId($title) {
        $sTitle = preg_replace('/\W/', '', strtolower($title));
        return $sTitle;
    }
    
    public static function secondsToHumanTime($seconds) {
        $time = array();
        if (!empty($seconds)) {
            //$seconds = intval($seconds);
            $dtF = new DateTime('@0');
            $dtT = new DateTime("@$seconds");
            $diff = $dtF->diff($dtT);
            $days = $diff->format('%a');
            $hours = $diff->format('%h');
            $mins = $diff->format('%i');
            $snds = $diff->format('%s');
            
            if (!empty($days)) {
                $time[] = "$days days";
            }
            if (!empty($hours)) {
                $time[] = "$hours hours";
            }
            if (!empty($mins)) {
                $time[] = "$mins minutes";
            }
            if (!empty($snds)) {
                $time[] = "$snds seconds";
            }            
        }
        
        $timeString = implode(" ", $time);
        if (empty($timeString)) {
            $timeString = '0';
        }
        return $timeString;
    }    
    

}
