<?php

/* Add AJAX actions */

function E5JSM_remove_post() {
    @set_time_limit( 600 );
    @ini_set( 'memory_limit', -1);
    $postID = $_POST['id'];
    $gID = $_POST['gid'];
    $result = ELM5JSM::getAjaxResponse( array("ids" => $postID, "gid" => $gID), "Failed to trash post $postID");
//    echo json_encode($result);
//	wp_die();
    
    if (!empty($postID)) {
        wp_delete_post($postID, false);
        ELM5JSM::removeFromRecordedGSIDs($postID, $gID);
    }
    $result['success'] = '1';
    $result['error'] = '';
    echo json_encode($result);
	wp_die();
}

function E5JSM_scrap_post() {
    @set_time_limit( 600 );
    @ini_set( 'memory_limit', -1);
    $postID = $_POST['ids'];
    $gID = $_POST['gid'];
    $result = ELM5JSM::getAjaxResponse( array("ids" => $postID, "gid" => $gID), "Failed to delete post $postID");
//    echo json_encode($result);
//	wp_die();

    if (!empty($postID)) {
        wp_delete_post($postID, true);
        ELM5JSM::removeFromRecordedGSIDs($postID, $gID);
    }
    $result['success'] = '1';
    $result['error'] = '';
    echo json_encode($result);
	wp_die();
}
function E5JSM_enlist() {
    @set_time_limit( 600 );
    @ini_set( 'memory_limit', -1);
    $postID = $_POST['ids'];
    $gID = $_POST['gid'];
    $result = ELM5JSM::getAjaxResponse( array("ids" => $postID, "gid" => $gID), "Failed to include post $postID");
//    echo json_encode($result);
//	wp_die();

    ELM5JSM::addToRecordedGSIDs($postID, $gID);
    $result['success'] = '1';
    $result['error'] = '';
    echo json_encode($result);
	wp_die();
}


add_action( 'wp_ajax_e5jsm_enlist', 'E5JSM_enlist' );
add_action( 'wp_ajax_e5jsm_remove_post', 'E5JSM_remove_post' );
add_action( 'wp_ajax_e5jsm_scrap_post', 'E5JSM_scrap_post' );