<pre style="background:#eee;">
<?php
/*  TODO
 *  1. Dynamically get Url for Options/Company/Location Google Sheets (from some setting in admin)
 *  2. Dynamically get upload path to load image from (from some setting in admin) - WP uploads dir if nothing is set
 *  3. Dynamically get Social Networking site's API credentials from soem settings in admin
 *  4. Need a admin UI way to reset or remove the options created by this code like job_gs_ids etc.
 * 
 */

    error_reporting(E_ALL);
    ini_set('display_errors','On');
    logit("[Google2WP Jobs Service] Starting Update of WP Jobs from Google Docs", 0);
    
    $startTime = date("Y-m-d H:i:s");
    $startTimeStamp = strtotime($startTime);
//    logit("************** Start Execution ..." . $startTime . "*************\r\n\r\n");


/** That's all, stop editing from here **/

if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;

$posts = getPostWithMeta();
$recordedGSIDs = getRecordedGSIDs();




$endTime = date("Y-m-d H:i:s");
$endTimeStamp = strtotime($endTime);
$timegap = $endTimeStamp - $startTimeStamp;
logit( "\r\n****************** End Execution ****************************" . $endTime . "\r\n");
logit( "\r\nTotal time taken: " . ($timegap) . " seconds.\r\n");
error_log("[Google2WP Jobs] Ending Update of WP Jobs from Google Docs", 0);


die();

?>
<?php

function getPostWithMeta($meta = '') {
    global $wpdb;
    $data = array();
    if (empty($meta)) {
        $meta = 'googlesheet_openorders_id';
    }
    $sql = "SELECT p.post_title, p.post_status, p.post_modified, pm.*
        FROM `wp_postmeta` pm
        LEFT JOIN wp_posts p ON p.id=pm.post_id
        WHERE `meta_key` LIKE '%$meta%'
        order by meta_value asc, post_id asc, post_modified asc";

    $records = $wpdb->get_results($sql);
    if (!empty($records)) {
        foreach($records as $item) {
            $val = $item->meta_value;
            $postId = $item->post_id;
            if (!isset($data[$val])) {
                $data[$val] = array();
            }
            $data[$val][$postId] = $item;
        }
    }
    return $data;
}


function update_post_metas($ID, $metas) {
    foreach($metas as $key=>$value) {
        update_post_meta($ID, $key, $value);
    }
}
function update_post_terms($ID, $taxTerms) {
    foreach($taxTerms as $taxname => $terms) {
        try {
            wp_set_object_terms($ID, $terms, $taxname);
        } catch (Exception $ex) {
        }
    }
}

function get_my_option($option, $default = "") {
    global $wpdb;
    $value = $default;
    $suppress = $wpdb->suppress_errors();
    $options = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name = '$option' ORDER BY option_id DESC" );
    $wpdb->suppress_errors($suppress);
    
    if (!empty($options) && isset($options[0])) {
        $searchResult = $options[0];
        $value = $searchResult->option_value;
        $value = maybe_unserialize($value);        
    }
    
    return $value;    
}
function getLastDataEntryTimeStamp() {
    return get_my_option('last_job_updated_googlesheet', '');
}
function getLastUpdatedTimeStamp() {
    return get_my_option('last_job_updated_wp', '');
}
function getRecordedGSIDs() {
    return get_my_option('job_gs_ids', array());
}
function setLastDataEntryTimeStamp($val) {
    update_option('last_job_updated_googlesheet', $val);
}
function setLastUpdatedTimeStamp($val) {
    update_option('last_job_updated_wp', $val);
}
function setRecordedGSIDs($val) {
    update_option('job_gs_ids', $val);
}
function trashPosts($IDs) {
    foreach($IDs as $ID) {
        logit("Trash Post: '$ID'\r\n");
        if (!empty($ID)) {
            wp_trash_post( $ID  ); 
        }        
    }    
}

function processOpenOrders($map, $orders, $companies = array(), $locations = array()) {
    $last_dataentry = intval(getLastDataEntryTimeStamp());
    $last_updated = intval(getLastUpdatedTimeStamp());
    $nowTimestamp = intval(date('U'));
    $lastest_dataentry = $last_dataentry;
    
    
    $recordedGSIDs = getRecordedGSIDs();
    logit("JOB GS IDS:");
    logit(var_export($recordedGSIDs, true));
    logit("\r\n############################\r\n");
    
    $newGSIDs = array();
    $duplicateGSIDs = array();
    
    $orderCount = 0;
    $rowCount = 1;
    foreach($orders as $order) {
        $rowCount++;
        $GSID = @$order['ID'];
        $existingPostId = $recordedGSIDs[$GSID];
        if (empty($GSID)) {
            continue;
        }
        
        if (isset($recordedGSIDs[$GSID])) {
            unset($recordedGSIDs[$GSID]);
        }
        if (isset($newGSIDs[$GSID])) {
            logit("!!!! [ ERROR ] Duplicate Google ID [$GSID] found in Spreadsheet ROW-$rowCount. Not posting!\r\n");
            if (!isset($duplicateGSIDs[$GSID])) {
                $duplicateGSIDs[$GSID] = array();
            }
            $duplicateGSIDs[$GSID][] = $rowCount;            
            continue;
        }

        $gsEntryDatetime = @$order['Timestamp'];
        logit("[$GSID] GS timestamp [$gsEntryDatetime] \r\n");
        $gsSheetDate = $gsEntryDatetime;
        if (empty($gsEntryDatetime)) {
            $gsSheetDate = date("Y-m-d H:i:s");
        }        
        $gsSheetUpdateTimestamp = strtotime($gsSheetDate);
        logit((date("Y-m-d H:i:s", $last_dataentry) . ' >= ' . date("Y-m-d H:i:s", $gsSheetUpdateTimestamp) ."\r\n"));
		
		$isPostNotUpdated = !empty($gsEntryDatetime) && $last_dataentry >= $gsSheetUpdateTimestamp;
        if ($isPostNotUpdated) {
            if (!empty($existingPostId)) {
				logit("Skipping as no update found ... $GSID - \r\n");
                $newGSIDs[$GSID] = $existingPostId;
				continue;
            }
            else {
                logit("Google POST [$GSID] not found in WP Data. Will be added as new JOB.\r\n");
            }           
        }
        
        $lastest_dataentry = max($lastest_dataentry, $gsSheetUpdateTimestamp);
               
        $post_info = processOpenOrderItem($order, $map, $companies, $locations);
        $orderCount++;
        logit("[$orderCount]----------------------------------------</br>\r\n");
        
        $options = $post_info['options'];
        $publish_ad = $options['publish_ad'];
        $force_publish_ad = $options['force_publish_ad'];

        $ID = $existingPostId;//getPostByGSID($GSID);
        $neverPosted = empty($ID); 

        logit("Existing PostID for Google Entry [$GSID] is [$ID] \r\n");
        
        if (!empty($ID)) {
            $post = get_post($ID);
            $eID = $post->ID;
            if (empty($eID)) {
                logit("Post $ID not found! \r\n");
                $ID = $eID;
            }
        }        
        
        $newID = updateJob($post_info, $ID);
        
        $newGSIDs[$GSID] = $newID;

        if ($force_publish_ad || $publish_ad) {
            build_ad($post_info, $newID, $force_publish_ad);
        }

        logit("----------------------$GSID = $newID ---------------------------</br>\r\n");
        if ($orderCount >= 75) {
            break;
        }        
    }
    
    logit("TRASH IDS:");
    logit(var_export($recordedGSIDs, true));
    trashPosts($recordedGSIDs);
    logit("\r\n############################\r\n");
    
    setLastDataEntryTimeStamp($lastest_dataentry);
    setLastUpdatedTimeStamp($nowTimestamp);
    logit("ALl recorded IDS: ");
    logit(var_export($newGSIDs, true));
    logit("\r\n");
    setRecordedGSIDs($newGSIDs);
    
    if (!empty($duplicateGSIDs)) {
        mailDuplicateGSIDWarning($duplicateGSIDs);
    }
}

function updateJob($data, $ID) {
    extract($data);//meta, tax, actions, post_param, options, , 
    $status = $options['publish_job'] == 1 ? "publish" : "draft";
    $post_data = array(
        "post_status" => $status, //"publish", //draft
    );					
    $post_data = array_merge($post_data, $post_param);
    
    if (!empty($ID)) {
        $post = get_post($ID);
        $eID = $post->ID;
        if (empty($eID)) {
            logit("Post $ID not found! \r\n");    
            $ID = $eID;
        }
    }
    
    if (empty($ID)) {
        logit("Creating new post --- ");    
        $nID = wp_insert_post( $post_data );
        logit("Created new post $nID\r\n");    
    }
    else {
        logit("Updating Post ID: [$ID] -- ");    
        $post_data['ID'] = $ID;
        $nID = wp_update_post( $post_data );
        logit("Updated Post ID: [$nID]\r\n");    
    }    
    
    update_post_metas($nID, $meta);
    update_post_terms($nID, $tax);
    
    $data['post_id'] = $nID;    
    runActions($actions, $data);
    
    return $nID;
    
}

function runActions($actions, $data) {       
    foreach($actions as $actionKey => $actionItem) {
        $action = $actionItem['action'];
        if (function_exists($action)) {
            $actionItem['post_id'] = $data['post_id'];
            $value = $action($actionItem);
       }     
    }    
}


function build_ad($data, $ID, $isForce = false) {
    $isAdBuilt = get_post_meta($ID, 'ad_builder', true);
    $isLinkedinBuilt = get_post_meta($ID, 'ad_builder_linkedin', true);
    //$adPostedForPosts = get_my_option('posted_job_ads', '');
    //$neverPosted = $isAdBuilt && (!empty($adPostedForPosts) && !empty($adPostedForPosts['$ID']));
//    logit("Is Ad built? ");
//    logit(var_export($isAdBuilt, true));
//    logit("\r\n");
    
    if ($isForce || empty($isAdBuilt)) {
        require_once 'generateimage.php';
        create_ads($ID);
        update_post_meta($ID, 'ad_builder', 1);
    }
    elseif (empty($isLinkedinBuilt)) {
        require_once 'generateimage.php';
        create_ads($ID, true);
    }
}


function processOpenOrderItem($order, $mapList, $companies = array(), $locations = array()) {    
    $data = array(
        "options" => array(),
        "post_param" => array("post_type" => 'job_listing', "post_author" => 0,),
        "tax" => array(),
        "meta" => array(),
        "actions" => array(),
    );
    $mapValues = array();
    
    $company = array();
    $location = array();
    
    $company_id = @$order['Company ID'];
    $location_id = @$order['Location ID'];
    
    if (isset($companies[$company_id])) {
        $company = $companies[$company_id];
    }
    if (isset($locations[$location_id])) {
        $location = $locations[$location_id];
    }    
 
    //$data['rawCompany'] = $company;
    //$data['rawLocation'] = $location;
    //$data['rawOrder'] = $order;
    
    foreach($mapList as $mapKey => $map) {

        $sourceSheet = @$map['source'];
        if (empty($sourceSheet)) {
            $sourceSheet = "order";
        }
        $source = $$sourceSheet;  // $$ is a must here to work as a pointer
        $value = @$source[$mapKey]; 
        
        $type = @$map['type'];
        if (empty($type)) {
            $type = "meta";
        }
        
        $key = @$map['key'];
        $tax = @$map['tax'];
        $post_param = @$map['post_param'];
        $option = @$map['option'];
        $action = @$map['action'];
            
        $sanitizer = @$map['sanitizer'];
        $sanitizers = explode(',', $sanitizer); 
        $value = applySanitizers($value, $sanitizers);
        
        $mapValues[$mapKey] = $value;
        
        if (!empty($option)) {                        
            $data['options'][$option] = $value;
        }
        
        if (!empty($post_param)) {                        
            $data['post_param'][$post_param] = $value;
        }
        
        if (!empty($key)) {                        
            $data['meta'][$key] = $value;
        }         
        
        if (!empty($tax)) {     
            $actionTerm = @$map['actionTerm'];

            if (!$actionTerm) {
                $booleanTerm = @$map['booleanTerm'];
                $term = @$map['term'];
                if (!isset($data['tax'][$tax])) {
                    $data['tax'][$tax] = array();
                }

                if (!empty($value)) {
                    if (is_array($value)) {
                        $data['tax'][$tax] = array_merge($data['tax'][$tax], $value);
                    }
                    else {
                        $data['tax'][$tax][] = $value;
                    }
                }                            
            }
        }        
        if (!empty($action)) {
            $data['actions'][] = array(
                "action" => $action,
                "map" => $map,
                "value" => $value,
                "values" => $mapValues,
                "meta" => $key,
                "post_param" => $post_param,
                "option" => $option,
                "tax" => $tax,
                "field" => $mapKey,
            );
        }           

    }
       
    foreach($data['actions'] as $actionKey => $actionItem) {
        $action = $actionItem['action'];
        if (function_exists($action)) {
            $map = $actionItem['map'];
            $value = $action($actionItem, false);
            $sanitizer = @$map['postactionsanitizer'];
            $sanitizers = explode(',', $sanitizer); 
            $value = applySanitizers($value, $sanitizers);            
            
            $data['actions'][$actionKey]['value'] = $value;
            
            $tax = $actionItem['tax'];
            $booleanTerm = @$map['booleanTerm'];
            $term = @$map['term'];
            
            if (!empty($tax)) {     
                if (!isset($data['tax'][$tax] )) {
                    $data['tax'][$tax] = array();
                }

                if (!empty($value)) {
                    if (is_array($value)) {
                        $data['tax'][$tax] = array_merge($data['tax'][$tax], $value);
                    }
                    else {
                        $data['tax'][$tax][] = $value;
                    }
                }              
            } 
            
            $key = $actionItem['meta'];
            if (!empty($key)) {                        
                $data['meta'][$key] = $value;
            }            
            $post_param = $actionItem['post_param'];            
            if (!empty($post_param)) {                        
                $data['post_param'][$post_param] = $value;
            }              
            $option = $actionItem['option'];            
            if (!empty($option)) {                        
                $data['options'][$option] = $value;
            }              
            $metaOverwrite = $actionItem['metaoverwrites'];
            if (!empty($metaOverwrite)) {
                foreach($metaOverwrite as $metakey => $metavalue) {
                    $data['meta'][$metakey] = $metavalue;
                }
                
            }             
            
        }
    }     
    
    return $data;
    
}


/* sanitizers */
function currency_clean($val = "") {
	$val = str_replace("$","",trim($val));
	$val = str_replace(",","",$val);
	$val = trim($val);
    return $val;
}
function splitcsv($value) {    
    $values =  preg_split('/\s{0,},\s{0,}/', trim($value));
    return $values;
}
function randomCSV($value) {    
    $values =  splitcsv($value);    
    $pos = mt_rand(0, count($values) -1);
    $finalValue = $values[$pos];
    return $finalValue;
}
function setbool($value) {
    $value = strtolower(trim($value));
    $ret = 1;
    if ($value == 'no'  || 
        $value == 'n'   ||
        $value === "0"  ||
        is_nan(floatval($value))  ||
        is_null($value) ||
        empty($value) ||
        !isset($value) ||
        $value === FALSE        
    ) {
        $ret = 0;
    }
    
    return $ret;
}

function formatDate_Ymd($val) {
	
	if ($val !== '') {
		if (strtolower($val) != 'asap') {
			$date = strtotime($val);
			if ($date !== FALSE) {
				$val = date("Y-m-d", $date);
			}
		}
	}	
	return $val;	
}

function shift_map($vals) {
    global $shiftTermsByName;
    $map = array(
        "d-10" => "Days (10 Hours)",
        "d-12" => "Days (12 Hours)",
        "d" => "Days (8 Hours)", 
        "d-8" => "Days (8 Hours)", 
        "n-10" => "Night (10 Hours)",
        "n-12" => "Night (12 Hours)",
        "n" => "Night (8 Hours)", 
        "n-8" => "Night (8 Hours)", 
        "s-10" => "Swing (10 Hours)",
        "s-12" => "Swing (12 Hours)",
        "s" => "Swing (8 Hours)",
        "s-8" => "Swing (8 Hours)",
        "r-10" => "Rotating (10 Hours)",
        "r-12" => "Rotating (12 Hours)",
        "r" => "Rotating (8 Hours)",
        "r-8" => "Rotating (8 Hours)",
    );
    
    $ids = array();
    foreach($vals as $val) {
        if (isset($map[strtolower($val)])) {
            $namelcase = strtolower($map[strtolower($val)]);
            $id = @$shiftTermsByName[$namelcase];
            if (!empty($id)) {
                $ids[] = $id;
            }
        }        
    }
    return $ids;
}

function amenities_travel_map($val) {
    $termName = "Travel";
    return amenities_map($val, $termName);
}
function amenities_mm_map($val) {
    $termName = "Meals &amp; Incidentals";
    return amenities_map($val, $termName);
}
function amenities_housing_map($val) {
    $termName = "Housing Stipend";
    return amenities_map($val, $termName);
}
function amenities_map($val, $termName) {
    global $amenitiesTermsByName;
    $id = "";
    if ($val != 0) {
        $namelcase = strtolower($termName);
        $id = @$amenitiesTermsByName[$namelcase];
    }
    return $id;    
}

function specialty_map($vals) {
    global $specialtyTermsByName;
    $taxonomy = "job_listing_category";
    $ids = array();
    foreach($vals as $val) {
        if (!empty($val)) {            
            $val = htmlentities(trim($val));
            $lcaseval = strtolower($val);
            $id = $specialtyTermsByName[$lcaseval];
            if (empty($id)) {
                try {
                    $term  = wp_insert_term($val, $taxonomy);
                    $id = $term['term_id'];                
                    $specialtyTermsByName[$lcaseval] = $id;                    
                } catch (Exception $ex) {

                }
            }
            if (!empty($id)) {
                $ids[] = $id;
            }            
        }        
    }
    return $ids;
}

function appendUploadUrl($val) {
    
    if (!empty($val) && stripos($val, "://") === FALSE) {
        $upload_dir = wp_upload_dir(null, false);
        $val = $upload_dir['baseurl'].'/'.$val;
    }
    return $val;
}
function appendUploadPath($val) {
    if (!empty($val) && stripos($val, '/') !== 0 && stripos($val, "://") === FALSE) {
        $upload_dir = wp_upload_dir(null, false);
        $val = $upload_dir['basedir'].'/'.$val;
    }
    return $val;
}

/* actions */
function joinFields($data, $afterPostCreation = true) {
    $finalValue = "";
    if ($afterPostCreation === FALSE) {
        extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $fields = @$map['fields'];
        $delim = @$map['param'];
        $fieldKeys = explode(',', $fields);
        $valueArray = array();
        foreach($fieldKeys as $key) {
            $value = trim(@$values[$key]);
            if (!empty($value)) {
                $valueArray[] = $value;
            }            
        }
        $finalValue = implode($delim, $valueArray);
        return $finalValue;       
    }
    else {
        return $data['value'];
    }    
}


function setASAPDeadline(&$data, $afterPostCreation = true) {
    $value = 0;
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        $deadline = $values['Orientation'];
        if (strtolower(trim($deadline)) == 'asap') {
            $value = 1;            
            $newOrienation = date("Y-m-d", strtotime("+1 day"));
            $data['values']['Orientation'] = $newOrienation;
            if (!isset($data['metaoverwrites'])) {
                $data['metaoverwrites'] = array();
            }
            $data['metaoverwrites']['_application_deadline'] = $newOrienation;
			$data['metaoverwrites']['job_deadline'] = $newOrienation;
			$data['metaoverwrites']['job_asap'] = "ASAP";
        }
        
    }
    return $value;
}
function setLocationImageUrl($data, $afterPostCreation = true) {
    $value = @$data['value'];
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        $location_image_path = $values['Image(s)'];
        $value = $location_image_path;
        
        if (!empty($value)) {
            $value = appendUploadUrl($value);
        }        
    }
    return $value;
}
function setJobImage($data, $afterPostCreation = true) {
    $value = @$data['value'];
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        $location_image_url = appendUploadUrl($values['Image(s)']);
        $logoUrl = $values['Logo URL'];
        $value = $location_image_url;        
        if (empty($value)) {
            $value = $logoUrl;
        }        
    }
    return $value;
}
function setCompanyLogo($data, $afterPostCreation = true) {
    $value = @$data['value'];
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        if (empty($value)) {
            if(!empty($values['Image(s)'])) {
                $value = appendUploadUrl($values['Image(s)']);
            }
        }        
    }
    return $value;
}

function detectRegions($data, $afterPostCreation = true) {
    global $stateRegionMap;
    $ret = null;
    if ($afterPostCreation === FALSE) {    
        extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $fields = @$map['fields'];
        $state = @$values[$fields];
        
        if (!empty($state)) {
            $state = strtolower(trim($state));
            $ret = $stateRegionMap[$state];
        }
        
        return $ret;
    }
    else {
        return $data['value'];
    }    
}

function detectCompactState($data, $afterPostCreation = true) {    
    global $compactStatesTermId;
    $ret = 0;
    if ($afterPostCreation === FALSE) {    
        extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $termIds = detectRegions($data, false);
        if (!empty($termIds) && !empty($compactStatesTermId)) {
            if (in_array($compactStatesTermId, $termIds)) {
                $ret = 1;
            }            
        }
        return $ret;
    }
    else {
        return $data['value'];
    }    
}


/* Helper functions */
function applySanitizers($val, $sanitizers = array()) {
    foreach ($sanitizers as $sanitizer) {
        if (function_exists($sanitizer)) {
            $val = $sanitizer($val);
        }
    }
    return $val;
}
function getCompactStateParentId($raw) {
    $ret = null;
    foreach($raw as $term) {
        $slug = $term->slug;
        $id = $term->term_id;
        if ($slug == 'multi-state-license') {
            $ret = $id;
            break;
        }
    }    
    return $ret;    
}
/**
 * 
 * @param Array $raw Array of WP_Term object
 * @param Boolean $useStateName true/false (default false) - to set state names (lowercase) as the key of the resultant array
 * @return Array Associative array with lowercase state abbreviation (stored in description field of the term) as the key 
 *                      or State Names in lowercase if $useStateName is set to true.
 *                      Term id is set as value of the array element.
 *      
 */
function orderRegionTermsByState($raw, $useStateName = false) {
    $ret = array();
    $parents = array();
    foreach($raw as $term) {
        $id = $term->term_id;
        $name = $term->name;
        $description = strtolower($term->description);
        $lcasename = strtolower($name);
        $slug = $term->slug;
        $parentId = $term->parent;
        if ($parentId == 0) {
            $parents[$parentId] = array("name" => $name, "id" => $id, "slug" => $slug, "parent" => $parentId);
        }
        else {
            if (!isset($ret[$description])) {
                $ret[$description] = array();                
            }
            $ret[$description][] = $id;
            if (!in_array($parentId, $ret[$description])) {
                $ret[$description][] = $parentId;
            }
        }        
    }    
    return $ret;
    
}
function getTaxonomyTerms($tax, $fields = "") {
    $args = array(
        'hide_empty' => false,
        'hierarchical' => true,
        'orderby' => 'term_group',
    );
    
    if (!empty($fields)) {
        $args['fields'] = $fields;
    }
    
    $terms = get_terms($tax, $args);
    return $terms;
}

/**
 * 
 * @param type $terms   => array of WP_Term objects
 * @param type $byWhat  => string. value can be 'name', 'id', 'slug',  'name-lowercase' (default)
 * @param type $valueAs => string. Value can be:
 *                                   'all' (resulting in an associative array),
 *                                   'id' (resulting in an associative array),
 *                                   'name' (resulting in an associative array),
 *                                   'name-lowercase',
 *                                   'slug',
 *                                   'description',
 *                                   'parent' (for parent id),
 * 
 * @return type
 */
function listTermsBy($terms, $byWhat = 'name-lowercase', $valueAs = 'id') {
    $ret = array();   
    foreach($terms as $term) {
        $values = array();
        $values['id'] = $term->term_id;
        $name = $values['name'] = $term->name;
        $values['name-lowercase'] = strtolower($name);
        $values['slug'] = $term->slug;
        $values['parent'] = $term->parent;
        $values['description'] = $term->description;
        
        $byWhat = strtolower($byWhat);
        if (!isset($values[$byWhat])) {
            $byWhat = 'name-lowercase';
        }
        $valueAs = strtolower($valueAs);
        if ($valueAs != 'all' && !isset($values[$valueAs])) {
            $valueAs = 'id';
        }
        if ($valueAs == 'all') {
            $ret[$values[$byWhat]] = $values;
        }
        else {
            $ret[$values[$byWhat]] = $values[$valueAs];
        }        
    }    
    return $ret;    
}
    
// Function to convert CSV into associative array
function csvToArrayFilter($file, $delimiter = ',', $matchField = "", $matchValue = "") { 
    $keys = array();
    $data = array();
    $matchIndex = FALSE;
    if (($handle = fopen($file, 'r')) !== FALSE) { 
        $i = 0;
        while (($lineArray = fgetcsv($handle, 0, $delimiter, '"')) !== FALSE) { 
            if ($i == 0) {
                $keys = $lineArray;
                if (!empty($matchField)) {
                    $matchIndex = array_search($matchField, $keys);
                }
                
            }
            else {
                if (!empty($matchField)) {
                    if ($matchIndex !== FALSE) {
                        if ($lineArray[$matchIndex] === $matchValue) {
                            $data[] = array_combine($keys, $lineArray);
                        }
                    }                    
                }
                else {
                    $data[] = array_combine($keys, $lineArray);
                }
                
            }
            $i++; 
        }    
        fclose($handle);     
    } 

  return $data; 
} 
function csvToArray($file, $delimiter = ',', $keyField = "") { 
    $keys = array();
    $data = array();
    $matchIndex = FALSE;
	$handle = fopen($file, 'r');
    if ($handle !== FALSE) { 
        $i = 0;
        while (($lineArray = fgetcsv($handle, 0, $delimiter, '"')) !== FALSE) { 
            if ($i == 0) {
                $keys = $lineArray;
                if (!empty($keyField)) {
                    $matchIndex = array_search($keyField, $keys);
                }			
            }
            else {
                if ($matchIndex !== FALSE) {
                    $data["".$lineArray[$matchIndex]] = array_combine($keys, $lineArray);
                }                    
                else {
                    $data[] = array_combine($keys, $lineArray);
                }
                
            }
            $i++; 
        }    
        fclose($handle);     
    } 
	else {
		return false;
	}

  return $data; 
} 

function getPostByGSID($GSID, $returnPostID = true) {
    $ID = null;
    $job = new WP_Query( array( 'post_type' => 'job_listing', 
                        'meta_key' => 'googlesheet_openorders_id', 
                        'meta_value' => $GSID) 
        );
    $posts = $job->posts;
    $post = null;
    if (!empty($posts)) {
        $post = $posts[0];
        $ID = $post->ID;        
    }
    logit("Detected POST ID: $ID (num posts: {count($posts)})... = " .count($posts));
    if ($returnPostID) {
        return $ID;
    }
    return $post;    
}

function setFeaturedImage($data, $afterPostCreation = true) {
    if ($afterPostCreation) {    
        //extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $value = @$data['value'];
        $ID = @$data['post_id'];
        
        if (!empty($ID) && !empty($value)) {
            setPostLocationImage($ID, $value);
            setPostFeaturedImage($ID, $value);
        }
        return $value;
    }
    else {
        return $data['value'];
    }
    
}

function setPostLocationImage($post_id, $image) {
    $upload_dir = wp_upload_dir();
    $file = $upload_dir['basedir'] . '/' . $image;
    $url = $upload_dir['baseurl'] . '/' . $image;
    update_post_meta($post_id, 'location_image_path', $file);
    update_post_meta($post_id, 'location_image_url', $url);
}

function setPostFeaturedImage($post_id, $image) {

    $prevAttachmentId = get_post_meta($post_id, 'featured_image_id', true);
    $prevAttachmentName = get_post_meta($post_id, 'featured_image_name', true);
    
    if ($image == $prevAttachmentName) {
        return $prevAttachmentId;
    }
    
    wp_delete_attachment($prevAttachmentId);
    if ($prevAttachmentId && $image == "") {
        update_post_meta($post_id, 'featured_image_id', null);
        update_post_meta($post_id, 'featured_image_name', null);
        return null;
    }
    
    $upload_dir = wp_upload_dir();

    $filename = basename($image);
    if ($filename == "no-image-white-original.png") { 
      return null;//ignore white
    }
//    if(wp_mkdir_p($upload_dir['path']))
//        $file = $upload_dir['path'] . '/' . $filename;
//    else
    $file = $upload_dir['basedir'] . '/' . $image;

    $image_data = file_get_contents($file);
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit',
        'guid' => $image
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    update_post_meta($post_id, 'featured_image_id', $attach_id);
    update_post_meta($post_id, 'featured_image_name', $file);
    
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );    
	
	wp_update_attachment_metadata( $attach_id, $attach_data );
	
	
    return $attach_id; 
  }  

function logit($obj, $isEcho = true, $file = "") {
    if (empty($file)) {
        $file = "job.update.".date("Ymd").".log";
    }
    $path = $file;
    $date = "[" . date("Y-m-d H:i:s"). "] ";
    if (is_array($obj) || is_object($obj)) {
        $log = json_encode($obj);
    }
    else {
        $log = $obj;
    }
    file_put_contents($path, $date.$log, FILE_APPEND);
    if ($isEcho) {
        echo $log;
    }    
} 

function mailDuplicateGSIDWarning($GSIDs) {
    $site = get_my_option("blogname");
    $to   = "recruiting@blueforcestaffing.com,scassady@huffmaster.com";//get_my_option("admin_email");
    $from = "social@blueforcestaffing.com";//get_my_option("admin_email");
    $subject = "Problem with the Open Orders Spreadsheet";  
    $headers = array();
    $headers[] = "From: ". $from;
    
    $count = count($GSIDs);
    if ($count <= 0) {
        return;
    }
    if ($count > 1) {
        $pref1Tmpl = "some";
        $sufx1Tmpl = "s";
        $numTempl = "s are";
        $pref2Tmpl = "each";
                
    }
    else {
        $pref1Tmpl = "a";
        $sufx1Tmpl = "";
        $numTempl = " is";
        $pref2Tmpl = "the";        
    }
    
    
    $IdList = "";
    foreach($GSIDs as $ID => $rows) {
        $rowsCount = count($rows);
        if ($rowsCount > 0) {
            $IdList .= "\r\n\t{$ID} in row" . ($rowsCount > 1 ? 's ' : ' '). implode(", ", $rows);
        }        
    }
    
    
    $message =  "Attention BlueForceStaffing.com team,

Someone has entered $pref1Tmpl duplicate unique identifier number{$sufx1Tmpl} in column A of your Open Orders Google Sheet. This will cause problems with the job listing. Please correct the problem by assigning a unique ID number to {$pref2Tmpl} job listing.

The duplicated ID number{$numTempl}: 
$IdList


Thank you for your attention in this matter.

Sincerely,

The BlueForceStaffing.com WordPress Script";    
    wp_mail($to, $subject, $message, $headers);     
}