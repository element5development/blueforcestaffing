<?php

/**
 * This is just for illustrative purposes. Ideally should be a user object and based on a real database
 */
class TokenDB {
    static $file = 'token.storage';
    public static function getToken(){
        return @file_get_contents(TokenDB::$file);
    }
    public static function setToken($t){
        file_put_contents(TokenDB::$file,$t);
    }
    public static function resetToken(){
        file_put_contents(TokenDB::$file,'');
    }
}

include '../simplelinkedin.class.php';

//Set up the API
$ln = new SimpleLinkedIn('75o4crlvhhsgg2', 'pvyuR8XXBVEV0vjh');

//Set the current consumer token as the one stored in our DB.
$ln->setTokenData(TokenDB::getToken());

if($ln->authorize()){    
    try {
        //Do some OAuth requests...
        $user = $ln->fetch('GET', '/v1/people/~:(firstName,lastName)');
        
        print "Hello $user->firstName $user->lastName.";
    
        //Update stored token.
        $tokenData = $ln->getTokenData();
		 print_r ($ln->fetch('POST','/v1/people/~/shares',
        array(
            'comment' => 'Hello Linkedin',
            'content' => array(
                'title' => 'SimpleLinkedIn (SLinkedin)',
                'description' => 'Open source OAuth2 Implementation for PHP and linkedin.',
                'submittedUrl' => 'https://github.com/EJTH/SLinkedIn/'
            ),
            'visibility' => array('code' => 'anyone' )
        )
    ));
		
       // TokenDB::setToken($tokenData['access_token']);
        
    } catch(SimpleLinkedInException $e){
        
        //If token was expired or invalid, we reset and reauthorize.
        if($e->getLastResponse()->status == 401){
            //reset the stored token, so we can go through the authorization process 
            //again at page refresh
            TokenDB::resetToken();
            
            //Reauthorize..
            $ln->authorize();
            exit;
        } else throw $e;
    }
}
//$ln->addScope('w_share');
//Authorize.
if($ln->authorize()){
    //Make an example post, and print the result with print_r
    print_r ($ln->fetch('POST','/v1/people/~/shares',
        array(
            'comment' => 'Hello Linkedin',
            'content' => array(
                'title' => 'SimpleLinkedIn (SLinkedin)',
                'description' => 'Open source OAuth2 Implementation for PHP and linkedin.',
                'submittedUrl' => 'https://github.com/EJTH/SLinkedIn/'
            ),
            'visibility' => array('code' => 'anyone' )
        )
    ));
}
?>