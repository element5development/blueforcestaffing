<pre style="background:#eee;">
<?php
/*  TODO
 *  1. Dynamically get Url for Options/Company/Location Google Sheets (from some setting in admin)
 *  2. Dynamically get upload path to load image from (from some setting in admin) - WP uploads dir if nothing is set
 *  3. Dynamically get Social Networking site's API credentials from soem settings in admin
 *  4. Need a admin UI way to reset or remove the options created by this code like job_gs_ids etc.
 * 
 */

    error_reporting(E_ALL);
    ini_set('display_errors','On');
    
    /** That's all, stop editing from here **/

if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;
$last_dataentry = intval(getLastDataEntryTimeStamp());
$last_updated = intval(getLastUpdatedTimeStamp());
$nowTimestamp = intval(date('U'));
$lastest_dataentry = $last_dataentry;


$recordedGSIDs = getRecordedGSIDs();
logit("JOB GS IDS:");
logit(var_export($recordedGSIDs, true));

$recordedGSIDs['O1100314'] = 4720;
$recordedGSIDs['O1100315'] = 4721;

//setRecordedGSIDs($recordedGSIDs);


//$post = getPostByGSID("O1100314");

function logit($obj, $isEcho = true, $file = "") {
    if (empty($file)) {
        $file = "job.update.".date("Ymd").".log";
    }
    $path = $file;
    $date = "[" . date("Y-m-d H:i:s"). "] ";
    if (is_array($obj) || is_object($obj)) {
        $log = json_encode($obj);
    }
    else {
        $log = $obj;
    }
//    file_put_contents($path, $date.$log, FILE_APPEND);
    if ($isEcho) {
        echo $log;
    }    
} 

function getPostByGSID($GSID, $returnPostID = true) {
    $ID = null;
    $job = new WP_Query( array( 'post_type' => 'job_listing', 
                        'meta_key' => 'googlesheet_openorders_id', 
                        'meta_value' => $GSID) 
        );
    $posts = $job->posts;
    foreach($posts as $post) {
        logit("POST for GS ID $GSID : " . $post->ID . "\r\n");
    }
    $post = null;
    if (!empty($posts)) {
        $post = $posts[0];
        $ID = $post->ID;        
    }
    logit("Detected POST ID: $ID (num posts: {count($posts)})... = " .count($posts));
    if ($returnPostID) {
        return $ID;
    }
    return $post;    
}
function get_my_option($option, $default = "") {
    global $wpdb;
    $value = $default;
    $suppress = $wpdb->suppress_errors();
    $options = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name = '$option' ORDER BY option_id DESC" );
    $wpdb->suppress_errors($suppress);
    
    if (!empty($options) && isset($options[0])) {
        $searchResult = $options[0];
        $value = $searchResult->option_value;
        $value = maybe_unserialize($value);        
    }
    
    return $value;    
}
function getLastDataEntryTimeStamp() {
    return get_my_option('last_job_updated_googlesheet', '');
}
function getLastUpdatedTimeStamp() {
    return get_my_option('last_job_updated_wp', '');
}
function getRecordedGSIDs() {
    return get_my_option('job_gs_ids', array());
}
function setRecordedGSIDs($val) {
    update_option('job_gs_ids', $val);
}