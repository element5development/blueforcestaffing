<?php
$path  = ''; // It should be end with a trailing slash    
/** That's all, stop editing from here **/

if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
require_once 'facebook/lib/config.php';
require_once 'linkedin/lib/config.php';
require_once 'facebook/lib/Facebook/autoload.php';   

global $wpdb;

include('constants.php');
require_once( WP_LOAD_PATH . 'webServices/twitter_lib/TwitterAPIExchange.php');

//crop string
function leng_str ($job_title,$font_size,$font,$tp,$width_text,$strs) {
    $b = 0;$ret = "";
    $arr = explode(' ', strtoupper($job_title));
    foreach($arr as $word) {
        $tmp_string = $ret.' '.$word;

        $textbox = imagettfbbox($font_size, 0, dirname(__FILE__).'/'.$font, $tmp_string);

        if($textbox[2] > $width_text) {
            $ret.=($ret==""?"":"\n").$word;
            $b++;
        } else $ret.=($ret==""?"":" ").$word;
    }
    if($b > $strs) {
        if($font_size>22 && $strs>0) {
            $font_size = $font_size - 2;
            $dat = leng_str ($job_title,$font_size, $font,$tp,$width_text,$strs);
            $font_size = $dat[0];
            $tp =  $dat[1] - 2;
            $ret = $dat[2];   
        } else if($font_size>18 && $strs<1) {
            $font_size = $font_size - 2;
            $dat = leng_str ($job_title,$font_size, $font,$tp,$width_text,$strs);
            $font_size = $dat[0];
            $tp =  $dat[1] - 4;
            $ret = $dat[2];            
        } else {
            $len = strlen($ret);
            $pos = strpos($ret,"\n");
            $ret = substr($ret,0,-$len+$pos*2);  
            $len = strlen($ret);
            if($pos*2 - $len > 3) {
                $ret = $ret.'...';
            } else {
                $ret[$len-1] = '.';$ret[$len-2] = '.';$ret[$len-3] = '.'; 
            }
            
        }
    };


     return array(
      0 => $font_size,
      1 => $tp,
      2 => $ret
     );
};    


//begin function templates
function four_template($doctor,$num_template,$fb_image, $post_id,$soz) {
    $getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	
    $weekly = $job_meta_data['_job_weekly_gross'][0];	
    $one = $weekly[0];
    $weekly = substr_replace($weekly,',',0,1);
    $weekly = $one.$weekly;
    $hourly  = $job_meta_data['_hours'][0];	
    $date = $job_meta_data['_application_deadline'][0];
    
    $date = new DateTime($date);
    if ($num_template == 4) {
        $date = $date->Format("M d , Y");
    } else $date = $date->Format("m / j / Y");
    
    if(empty($date)) {
        $date = date("m / j / Y"); 
    };
    

    
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
    
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
    if ( !empty( $terms ) ){			
        $term = array_shift( $terms );
        $job_category = $term->name;
    }
    
    
    

    $x = 1200;
    $y = 675;
    $background = imagecreatetruecolor($x, $y);
    $white = imagecolorallocate($background, 255, 255, 255);
    imagefilledrectangle($background, 0, 0, $x, $y, $white);
    
    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];
    
    if (empty($location_image_url)) {
        //$location_image_url = $company_logo;
        $location_image_url = FAILSAFE_BANNER_URL;
    }
    
        if ($num_template == 1) {	   

        $firstUrl = $location_image_url;	
        //$firstUrl = SITEURL.'webServices/facebook_images/icons/banner.jpg';		
        $thirdUrl = SITEURL.'webServices/facebook_images/icons/local.png';	
        $fourthUrl = SITEURL.'webServices/facebook_images/icons/user2.png';	
        $fifthUrl = SITEURL.'webServices/facebook_images/icons/date.png';	
        $butt = SITEURL.'webServices/facebook_images/icons/apply.png';
        $outputImage = $background;

            try {
        $first = imagecreatefromjpeg($firstUrl);
            } catch (Exception $ex) {
                $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
            }        
        $third = imagecreatefrompng($thirdUrl);
        $fourth = imagecreatefrompng($fourthUrl);
        $fifth = imagecreatefrompng($fifthUrl);
        $butt = imagecreatefrompng($butt);

        $dynamicImageWindowX = 0;
        $dynamicImageWindowY = 0;
        $dynamicImageWindowW = 1200; 
        $dynamicImageWindowH = 310;
        $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
        imagecopyresampled($outputImage, $first, 
                $dynamicImageWindowX, $dynamicImageWindowY,
                $resampleData['positionX'], $resampleData['positionY'], 
                $dynamicImageWindowW, $dynamicImageWindowH, 
                $resampleData['width'], $resampleData['height']);

        //background 60%
        $x = 1200;
        $y = 310;
        $backgr = imagecreatetruecolor($x, $y);
        $obl = imagecolorallocate($backgr, 31, 52, 99);
        imagefilledrectangle($backgr, 0, 0, $x, $y, $obl);
        
        imagecopymerge($outputImage,$backgr,0,0,0,0, 1200, 310,60);            
            
        $logo =  SITEURL.'webServices/facebook_images/icons/logo_1.png';
        $logo = imagecreatefrompng($logo);
        imagecopyresampled ($outputImage, $logo, 500, 50, 0, 0, imagesx($logo), imagesy($logo), imagesx($logo), imagesy($logo));

        //draw icons and button
        imagecopyresampled ($outputImage, $third, 520, 450, 0, 0, imagesx($third), imagesy($third), imagesx($third), imagesy($third));
        imagecopyresampled ($outputImage, $fourth, 520, 520, 0, 0, imagesx($fourth), imagesy($fourth), imagesx($fourth), imagesy($fourth));
        imagecopyresampled ($outputImage, $fifth, 520, 590, 0, 0, imagesx($fifth), imagesy($fifth), imagesx($fifth), imagesy($fifth));
        imagecopyresampled ($outputImage, $butt, 100, 560, 0, 0, imagesx($butt), imagesy($butt), imagesx($butt), imagesy($butt));

        //draw text
        $color_text = imagecolorallocate($outputImage, 31, 52, 99); 
            
        $dat = leng_str($job_title,36,"fonts/ProximaNova-Light.ttf",560,560,1);
        imagettftext ($outputImage, $dat[0], 0, 600, 560-$dat[0]/2-4, $color_text, "fonts/ProximaNova-Light.ttf", $dat[2]);         
            
        imagettftext ($outputImage, $dat[0]+14, 0, 520, 400, $color_text, "fonts/ProximaNova-Bold.ttf", '$'.strtoupper($weekly).' '.strtoupper($hourly).' Hrs/Wk');
        imagettftext ($outputImage, $dat[0], 0, 600, 485, $color_text, "fonts/ProximaNova-Light.ttf", strtoupper($lcation_city).', '.strtoupper($lcation_state));
        imagettftext ($outputImage, $dat[0], 0, 600, 622, $color_text, "fonts/ProximaNova-Light.ttf", strtoupper($date));
        imagettftext ($outputImage, 34, 0, 520, 196, $white, "fonts/ProximaNova-Regular.ttf", 'Now Hiring Registered Nurses');
            
        $dat = leng_str($job_category,34,"fonts/ProximaNova-Bold.ttf",242,650,1);
        imagettftext ($outputImage, $dat[0], 0, 520, $dat[1], $white, "fonts/ProximaNova-Bold.ttf", $dat[2]);


        //add foto doctor
        $doctor = imagecreatefrompng($doctor);
        imagecopyresampled ($outputImage, $doctor, 20, 50, 0, 0, imagesx($doctor), imagesy($doctor), imagesx($doctor), imagesy($doctor));


        } else if ($num_template == 2) {
            
        $firstUrl = $location_image_url;	
        //$firstUrl = SITEURL.'webServices/facebook_images/icons/banner.jpg';	
        $thirdUrl = SITEURL.'webServices/facebook_images/icons/local2.png';	
        $fourthUrl = SITEURL.'webServices/facebook_images/icons/user3.png';	
        $fifthUrl = SITEURL.'webServices/facebook_images/icons/date2.png';	
        $butt = SITEURL.'webServices/facebook_images/icons/apply2.png';
        $outputImage = $background;

            try {
        $first = imagecreatefromjpeg($firstUrl);
            } catch (Exception $ex) {
                $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
            }        
        $third = imagecreatefrompng($thirdUrl);
        $fourth = imagecreatefrompng($fourthUrl);
        $fifth = imagecreatefrompng($fifthUrl);
        $butt = imagecreatefrompng($butt);

        $dynamicImageWindowX = 0;
        $dynamicImageWindowY = 0;
        $dynamicImageWindowW = 720; 
        $dynamicImageWindowH = 675;
        $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
        imagecopyresampled($outputImage, $first, 
                $dynamicImageWindowX, $dynamicImageWindowY,
                $resampleData['positionX'], $resampleData['positionY'], 
                $dynamicImageWindowW, $dynamicImageWindowH, 
                $resampleData['width'], $resampleData['height']);

        //background 60%
        $x = 720;
        $y = 675;
        $backgr = imagecreatetruecolor($x, $y);
        $obl = imagecolorallocate($backgr, 31, 52, 99);
        imagefilledrectangle($backgr, 0, 0, $x, $y, $obl);
        
        imagecopymerge($outputImage,$backgr,0,0,0,0, 720, 675,60);
            
        $logo =  SITEURL.'webServices/facebook_images/icons/logo_1.png';
        $logo = imagecreatefrompng($logo);
        //imagecopyresampled ($outputImage, $logo, 500, 50, 0, 0, imagesx($logo), imagesy($logo), imagesx($logo), imagesy($logo));

        //draw icons and button
        imagecopyresampled ($outputImage, $third, 50, 240, 0, 0, imagesx($third), imagesy($third), imagesx($third), imagesy($third));
        imagecopyresampled ($outputImage, $fourth, 50, 310, 0, 0, imagesx($fourth), imagesy($fourth), imagesx($fourth), imagesy($fourth));
        imagecopyresampled ($outputImage, $fifth, 50, 380, 0, 0, imagesx($fifth), imagesy($fifth), imagesx($fifth), imagesy($fifth));
        imagecopyresampled ($outputImage, $butt, 220, 560, 0, 0, imagesx($butt), imagesy($butt), imagesx($butt), imagesy($butt));

        //draw text
        $dat = leng_str(strtoupper($job_title),36,"fonts/ProximaNova-Light.ttf",350,550,1);
        imagettftext ($outputImage, $dat[0], 0, 130, 350-$dat[0]/2-4, $white, "fonts/ProximaNova-Light.ttf", $dat[2]);            
            
        imagettftext ($outputImage, $dat[0]+14, 0, 50, 500, $white, "fonts/ProximaNova-Bold.ttf", '$'.strtoupper($weekly).' '.strtoupper($hourly).' Hrs/Wk');
        imagettftext ($outputImage, $dat[0], 0, 130, 275, $white, "fonts/ProximaNova-Light.ttf", strtoupper($lcation_city).', '.strtoupper($lcation_state));
        imagettftext ($outputImage, $dat[0], 0, 130, 410, $white, "fonts/ProximaNova-Light.ttf", strtoupper($date));
        imagettftext ($outputImage, 36, 0, 50, 86, $white, "fonts/ProximaNova-Regular.ttf", 'Now Hiring Registered Nurses');
            
        $dat = leng_str($job_category,36,"fonts/ProximaNova-Bold.ttf",146,650,1);
        imagettftext ($outputImage, $dat[0], 0, 50, $dat[1], $white, "fonts/ProximaNova-Bold.ttf", $dat[2]);   


        //add foto doctor
        $doctor = imagecreatefrompng($doctor);
        imagecopyresampled ($outputImage, $doctor, 720, 0, 0, 0, imagesx($doctor), imagesy($doctor), imagesx($doctor), imagesy($doctor));      

        } else if ($num_template == 3) {
            
        $firstUrl = $location_image_url;	
        //$firstUrl = SITEURL.'webServices/facebook_images/icons/banner.jpg';	
        $thirdUrl = SITEURL.'webServices/facebook_images/icons/local2.png';	
        $fourthUrl = SITEURL.'webServices/facebook_images/icons/user3.png';	
        $fifthUrl = SITEURL.'webServices/facebook_images/icons/date2.png';	
        $butt = SITEURL.'webServices/facebook_images/icons/apply2.png';
        $outputImage = $background;

            try {
        $first = imagecreatefromjpeg($firstUrl);
            } catch (Exception $ex) {
                $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
            }        
        $third = imagecreatefrompng($thirdUrl);
        $fourth = imagecreatefrompng($fourthUrl);
        $fifth = imagecreatefrompng($fifthUrl);
        $butt = imagecreatefrompng($butt);

        $dynamicImageWindowX = 0;
        $dynamicImageWindowY = 0;
        $dynamicImageWindowW = 1200; 
        $dynamicImageWindowH = 675;
        $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
        imagecopyresampled($outputImage, $first, 
                $dynamicImageWindowX, $dynamicImageWindowY,
                $resampleData['positionX'], $resampleData['positionY'], 
                $dynamicImageWindowW, $dynamicImageWindowH, 
                $resampleData['width'], $resampleData['height']);

        //background 60%
        $x = 1200;
        $y = 675;
        $backgr = imagecreatetruecolor($x, $y);
        $obl = imagecolorallocate($backgr, 31, 52, 99);
        imagefilledrectangle($backgr, 0, 0, $x, $y, $obl);
        
        imagecopymerge($outputImage,$backgr,0,0,0,0, 1200, 675,60);            
            
        $logo =  SITEURL.'webServices/facebook_images/icons/logo_1.png';
        $logo = imagecreatefrompng($logo);
        //imagecopyresampled ($outputImage, $logo, 500, 50, 0, 0, imagesx($logo), imagesy($logo), imagesx($logo), imagesy($logo));

        //draw icons and button
        imagecopyresampled ($outputImage, $third, 70, 330, 0, 0, imagesx($third), imagesy($third), imagesx($third), imagesy($third));
        imagecopyresampled ($outputImage, $fourth, 70, 400, 0, 0, imagesx($fourth), imagesy($fourth), imagesx($fourth), imagesy($fourth));
        imagecopyresampled ($outputImage, $fifth, 70, 470, 0, 0, imagesx($fifth), imagesy($fifth), imagesx($fifth), imagesy($fifth));
        imagecopyresampled ($outputImage, $butt, 70, 540, 0, 0, imagesx($butt), imagesy($butt), imagesx($butt), imagesy($butt));

        //draw text
        $dat = leng_str(strtoupper($job_title),36,"fonts/ProximaNova-Light.ttf",440,560,1);
        imagettftext ($outputImage, $dat[0], 0, 150, 440-$dat[0]/2-6, $white, "fonts/ProximaNova-Light.ttf",$dat[2]);            
            
        imagettftext ($outputImage, $dat[0]+14, 0, 70, 290, $white, "fonts/ProximaNova-Bold.ttf", '$'.strtoupper($weekly).' '.strtoupper($hourly).' Hrs/Wk');
        imagettftext ($outputImage, $dat[0], 0, 150, 370, $white, "fonts/ProximaNova-Light.ttf", strtoupper($lcation_city).', '.strtoupper($lcation_state));
        imagettftext ($outputImage, $dat[0], 0, 150, 500, $white, "fonts/ProximaNova-Light.ttf", strtoupper($date));
        imagettftext ($outputImage, 36, 0, 70, 106, $white, "fonts/ProximaNova-Regular.ttf", 'Now Hiring Registered Nurses');
            
        $dat = leng_str($job_category,36,"fonts/ProximaNova-Bold.ttf",165,650,1);
        imagettftext ($outputImage, $dat[0], 0, 70, $dat[1], $white, "fonts/ProximaNova-Bold.ttf", $dat[2]);  


        //add foto doctor
        $doctor = imagecreatefrompng($doctor);
        imagecopyresampled ($outputImage, $doctor, 660, 18, 0, 0, imagesx($doctor), imagesy($doctor), imagesx($doctor), imagesy($doctor));
            
        //lines
        $x = 1200;
        $y = 675;
        $line = imagecreatetruecolor($x, $y);
        $obl2 = imagecolorallocate($line, 255, 255, 255);
        imagefilledrectangle($line, 0, 0, $x, $y, $obl2);
        imagecopymerge($outputImage,$line,40,40,0,0, 920, 3,100);
        imagecopymerge($outputImage,$line,40,630,0,0, 920, 3,100);
        imagecopymerge($outputImage,$line,40,40,0,0, 3, 590,100);

        } else {

        $firstUrl = $location_image_url;	
        //$firstUrl = SITEURL.'webServices/facebook_images/icons/banner.jpg';	
        $thirdUrl = SITEURL.'webServices/facebook_images/icons/local2.png';	
        $fourthUrl = SITEURL.'webServices/facebook_images/icons/user3.png';	
        $fifthUrl = SITEURL.'webServices/facebook_images/icons/date2.png';	
        $sixthUrl = SITEURL.'webServices/facebook_images/icons/dollar.png';	
        $butt = SITEURL.'webServices/facebook_images/icons/apply3.png';
        $outputImage = $background;

            try {
        $first = imagecreatefromjpeg($firstUrl);
            } catch (Exception $ex) {
                $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
            }        
        $third = imagecreatefrompng($thirdUrl);
        $fourth = imagecreatefrompng($fourthUrl);
        $fifth = imagecreatefrompng($fifthUrl);
        $sixth = imagecreatefrompng($sixthUrl);
        $butt = imagecreatefrompng($butt);

        $dynamicImageWindowX = 0;
        $dynamicImageWindowY = 0;
        $dynamicImageWindowW = 1200; 
        $dynamicImageWindowH = 675;
        $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
        imagecopyresampled($outputImage, $first, 
                $dynamicImageWindowX, $dynamicImageWindowY,
                $resampleData['positionX'], $resampleData['positionY'], 
                $dynamicImageWindowW, $dynamicImageWindowH, 
                $resampleData['width'], $resampleData['height']);

        //background 60%
        $x = 1200;
        $y = 675;
        $backgr = imagecreatetruecolor($x, $y);
        $obl = imagecolorallocate($backgr, 0, 0, 0);
        imagefilledrectangle($backgr, 0, 0, $x, $y, $obl);
        
        imagecopymerge($outputImage,$backgr,70,95,0,0, 1060, 480,60);            
            
        $logo =  SITEURL.'webServices/facebook_images/icons/logo_1.png';
        $logo = imagecreatefrompng($logo);
        imagecopyresampled ($outputImage, $logo, 110, 140, 0, 0, imagesx($logo), imagesy($logo), imagesx($logo), imagesy($logo));

        //draw icons and button
        imagecopyresampled ($outputImage, $third, 800, 160, 0, 0, imagesx($third), imagesy($third), imagesx($third), imagesy($third));
        imagecopyresampled ($outputImage, $fourth, 800, 250, 0, 0, imagesx($fourth), imagesy($fourth), imagesx($fourth), imagesy($fourth));
        imagecopyresampled ($outputImage, $fifth, 800, 340, 0, 0, imagesx($fifth), imagesy($fifth), imagesx($fifth), imagesy($fifth));
        imagecopyresampled ($outputImage, $sixth, 800, 420, 0, 0, imagesx($sixth), imagesy($sixth), imagesx($sixth), imagesy($sixth));

        //draw text
            
        
        //one
        $font = "fonts/Roboto-Bold.ttf";
        $width_text = 260;
        $font_size = 21;          
        $dat = leng_str($job_title,$font_size,$font,280,260,0);
        imagettftext ($outputImage, $dat[0], 0, 880, 280-$dat[0]/2-4, $white, $font, $dat[2]);
            
        //two
        $arr = explode(' ', '$'.strtoupper($weekly).' '.strtoupper($hourly).' Hrs/Wk');   
        foreach($arr as $word) {
            $tmp_string = $ret.' '.$word;

            $textbox = imagettfbbox($font_size, 0, $font, $tmp_string);

            if($textbox[2] > $width_text)
                $ret.=($ret==""?"":"\n").$word;
            else
                $ret.=($ret==""?"":" ").$word;
        }    
        imagettftext ($outputImage, $dat[0], 0, 880, 455, $white, $font, $ret);
            
        //free 
        $arr = explode(' ', strtoupper($lcation_city).', '.strtoupper($lcation_state)); 
        $ret = "";
        $b = 0;
        foreach($arr as $word) {
            $tmp_string = $ret.' '.$word;

            $textbox = imagettfbbox($font_size, 0, $font, $tmp_string);

            if($textbox[2] > $width_text) {
                $ret.=($ret==""?"":"\n").$word;
                $b++;
            } else $ret.=($ret==""?"":" ").$word;
        }
        if($b==1) {
            $tp = 180;
        } else $tp = 195;
        imagettftext ($outputImage, $dat[0], 0, 880, $tp, $white, $font, $ret);
            
        //four
        $arr = explode(' ', strtoupper($date)); 
        $ret = "";  
        $b = 0;
        foreach($arr as $word) {
            $tmp_string = $ret.' '.$word;

            $textbox = imagettfbbox($font_size, 0, $font, $tmp_string);

            if($textbox[2] > $width_text) {
                $b++;
                $ret.=($ret==""?"":"\n").$word;
            } else $ret.=($ret==""?"":" ").$word;
        }
        if($b==1) {
            $tp = 355;
        } else $tp = 370;            
        imagettftext ($outputImage, $dat[0], 0, 880, $tp, $white, $font, $ret);
        
        imagettftext ($outputImage, 35, 0, 110, 300, $white, "fonts/Roboto-Regular.ttf", 'Now Hiring Registered Nurses');
            
        $dat = leng_str($job_category,39,'fonts/Roboto-Bold.ttf',386,620,1);
        imagettftext ($outputImage, $dat[0], 0, 110, $dat[1], $white, 'fonts/Roboto-Bold.ttf', $dat[2]);          

 

        //lines
        $x = 1200;
        $y = 675;
        $line = imagecreatetruecolor($x, $y);
        $obl2 = imagecolorallocate($line, 255, 255, 255);
        imagefilledrectangle($line, 0, 0, $x, $y, $obl2);
        imagecopymerge($outputImage,$line,80,80,0,0, 1060, 3,100);
        imagecopymerge($outputImage,$line,80,560,0,0, 1060, 3,100);
        imagecopymerge($outputImage,$line,80,80,0,0, 3, 480,100);  
        imagecopymerge($outputImage,$line,1140,80,0,0, 3, 480,100);  
        imagecopymerge($outputImage,$line,750,120,0,0, 3, 410,100);  
            
        imagecopyresampled ($outputImage, $butt, 860, 530, 0, 0, imagesx($butt), imagesy($butt), imagesx($butt), imagesy($butt));
            
        };
    if($soz == 'fb') {
        $fb_image = 'facebook_images/'.$fb_image;
    } else if ($soz == 'tw') {
        $fb_image = 'twitter_images/'.$fb_image;
    } else if ($soz == 'pn') {
        $fb_image = 'pinterest_images/'.$fb_image;
    } else if ($soz == 'ln') {
        $fb_image = 'Linkedin_images/'.$fb_image;
    } else if ($soz == 'in') {
        $fb_image = 'instagram_images/'.$fb_image;
    };
    

    imagejpeg($outputImage,$fb_image);  

    imagedestroy($outputImage);
};
//end function templates


if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
	$pic_url= 'wp-content/uploads/2016/06/default-location-pic.jpg';
else 
	$pic_url= 'wp-content/uploads/2016/05/default-location-pic.jpg';



if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
    define("FAILSAFE_BANNER_URL", SITEURL . $pic_url);
}
else {
    // LIVE
    define("FAILSAFE_BANNER_URL", SITEURL . 'wp-content/uploads/2016/05/default-location-pic.jpg');
}


/*if(isset($_REQUEST['test_fb']) && $_REQUEST['test_fb'] != 0) {

    $post_id = $_REQUEST['test_fb'];


    $list = true;
    $global_limit = get_option('_posting_limit_', 0);
    $today_count = get_option('__ad_posting_count',0);

    add_option('facebook_access_token', 'EAAOfBWf55UcBAOqE4jQTFuZAHdxkE117qIO1rQUoPQSu6lbvYsSmlfkbygrUgMRnWsjrCJWOaJPWTmHc4ZBYdbMA');

    if($global_limit > 0){

		$date_posting = get_option('__ad_posting_date', date('Y-m-d', strtotime("1 day ago")));
		$date1 = date_create($date_posting); // format of yyyy-mm-dd
	    $date2 = date_create(date('Y-m-d')); // format of yyyy-mm-dd
	    $dateDiff = date_diff($date2, $date1);

	    if($dateDiff->d != 0 ) {
	    	 add_option('__ad_posting_count',0);
	    } else {
	    	if($global_limit > 0){
	    		if( $global_limit <= $today_count)
	    			$list = false;
	    	}
	    }
    }

    if( $list ) {

    	$joblink = get_post_permalink($post_id); 
	    $post = get_post($post_id);
	    $job_title = get_post_meta($post_id, 'ad_descriptions', true);
	    if($job_title == ''){
	    	$job_title = $post->post_title;
	    }

        create_ad_facebook($post, $post_id, $joblink, $job_title);

	    add_option('__ad_posting_count',$today_count+1);
	    add_option('__ad_posting_date', date('Y-m-d'));
    }

    die;

}*/

if(isset($_REQUEST['test_twitter']) && $_REQUEST['test_twitter'] != 0) {

    $post_id = $_REQUEST['test_twitter'];


	$joblink = get_post_permalink($post_id); 
    $post = get_post($post_id);
    $job_title = get_post_meta($post_id, 'ad_descriptions', true);
    if($job_title == ''){
    	$job_title = $post->post_title;
    }

    create_ad_linkedin($post, $post_id, $joblink, $job_title);
	print_r(get_post_meta($post_id, 'ad_builder_linkedin_data'));
die;

}




function fb_image_create($fb_image, $post_id,$soz){ 

    $char_limit_title = 32;
    $char_limit_location = 46;
    $char_limit_speciality = 46;
    
    four_template($GLOBALS['doctor'],$GLOBALS['num_template'],$fb_image, $post_id,$soz);


 }

    

function twitter_image_create($twitt_image, $post_id,$soz){ 
 
    $char_limit_title = 32;
    $char_limit_location = 46;
    $char_limit_speciality = 46;
    
	four_template($GLOBALS['doctor'],$GLOBALS['num_template'],$twitt_image, $post_id,$soz);
	
 }
function pineterest_image_create($pint_image, $post_id,$soz){ 
     
    $char_limit_title = 24;
    $char_limit_location = 29;
    $char_limit_speciality = 29;
    
	four_template($GLOBALS['doctor'],$GLOBALS['num_template'],$pint_image, $post_id,$soz);
	
}
function linkedin_image_create($linked_image, $post_id,$soz){
        
    four_template($GLOBALS['doctor'],$GLOBALS['num_template'],$linked_image, $post_id,$soz);

}
function linkedin_image_create_old($linked_image, $post_id){
        
    $char_limit_title = 48;
    $char_limit_location = 48;
    $char_limit_speciality = 48;
    
	$getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	  
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
	
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			$job_category = $term->name;
		}

	
	$x = 646;
	$y = 220;
	$background = imagecreatetruecolor($x, $y);
	$white = imagecolorallocate($background, 30, 52, 101);
	imagefilledrectangle($background, 0, 0, $x, $y, $white);

    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];	   
//    if (empty($location_image_path)) {
//        $location_image_path = $company_logo;
//    }
//    else {
//        $location_image_path = appendUploadPath($location_image_path);
//    }
    if (empty($location_image_url)) {
//        $location_image_url = $company_logo;
        $location_image_url = FAILSAFE_BANNER_URL;
    }

	$firstUrl = $location_image_url;    
//	$firstUrl = SITEURL.'webServices/Linkedin_images/icons/banner.jpg';	
	$secondUrl = SITEURL.'webServices/Linkedin_images/icons/white-im.jpg';	
	$thirdUrl = SITEURL.'webServices/Linkedin_images/icons/location.png';	
	$fourthUrl = SITEURL.'webServices/Linkedin_images/icons/icu-user.png';	
	//$fifthUrl = SITEURL.'webServices/Linkedin_images/icons/date.png';
	
	$outputImage = $background;
	
        try {
	$first = imagecreatefromjpeg($firstUrl);
        } catch (Exception $ex) {
            $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
        }
	$second = imagecreatefromjpeg($secondUrl);
	$third = imagecreatefrompng($thirdUrl);
	$fourth = imagecreatefrompng($fourthUrl);
	//$fifth = imagecreatefrompng($fifthUrl);

    $dynamicImageWindowX = 0;
    $dynamicImageWindowY = 0;
    $dynamicImageWindowW = 505;
    $dynamicImageWindowH = 145;
    $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
    imagecopyresampled($outputImage, $first, 
            $dynamicImageWindowX, $dynamicImageWindowY,
            $resampleData['positionX'], $resampleData['positionY'], 
            $dynamicImageWindowW, $dynamicImageWindowH, 
            $resampleData['width'], $resampleData['height']);    
//    imagecopymerge($outputImage,$first,0,0,0,0, 504, 165,100);
    imagecopymerge($outputImage,$second,505,0,0,0, 142, 220,100);
	
    $leftLine = 10;
    $textTopPad = 14;
    $textLeftPad = 20;
    
	$font = 'fonts/Lato-Bold.ttf';
	$grey = imagecolorallocate($outputImage, 255, 255, 255);
	$font_size = 14;
	$topline = 154;
    $job_title = substr($job_title, 0, $char_limit_title);
	imagettftext($outputImage, $font_size, 0, $leftLine, $topline + $textTopPad, $grey, $font, $job_title);
	
	
	$font = 'fonts/Lato-Regular.ttf';
	$font_size = 10;
	$topline = 177;
	$topline2 = 199;  
		 
	imagecopymerge($outputImage,$third,$leftLine + 1,$topline,0,0, 13, 19,100);
        $location = substr($location, 0, $char_limit_location);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline + $textTopPad, $grey, $font, $location);
	
        $category = substr ($job_category, 0, $char_limit_speciality);
	imagecopymerge($outputImage,$fourth,$leftLine,$topline2,0,0, 15, 18,100);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline2 + $textTopPad, $grey, $font, $category);
	
	//imagecopymerge($outputImage,$fifth,213,$topline,0,0, 22, 20,100);
	
    /*
    $datefield = $expirydate;
    $timestamp = strtotime($datefield);
    if ($timestamp === FALSE) {
        $datefield = substr($datefield, 0, 10);
    }
    else {
        $datefield = date("m/d/y", $timestamp);
    }  
	imagettftext($outputImage, $font_size, 0, 235, $topline_text, $grey, $font, $datefield); 
    
    */
    
    $linked_image = 'Linkedin_images/'.$linked_image;
    imagejpeg($outputImage,$linked_image);  
    imagedestroy($outputImage);

}
function instagram_image_create($instagram_image_filename, $post_id,$soz){
        
    $char_limit_title = 19;
    $char_limit_location = 24;
    $char_limit_speciality = 24;
    
	four_template($GLOBALS['doctor'],$GLOBALS['num_template'],$instagram_image_filename, $post_id,$soz);

}

function create_ad_facebook_old($post, $post_id, $joblink, $job_title) {
    //Share image on facebook
    $fb_image = time().'_fbimage.jpg';	

    fb_image_create($fb_image, $post_id,'fb');  //Create image for facebook call facebook function images

    //$fb_setting = $wpdb->prefix . 'facebook_setting';
    //$get_Settings =   $wpdb->get_results("select * from $fb_setting");
    //$get_Settings = $get_Settings[0];
    //$page_access_token = $get_Settings->accessToken;
    //$page_id = $get_Settings->pageId;
    
 	if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
        $page_access_token = 'EAAOfBWf55UcBAOqE4jQTFuZAHdxkE117qIO1rQUoPQSu6lbvYsSmlfkbygrUgMRnWsjrCJWOaJPWTmHc4ZBYdbMA';
        $page_id = '100011340128567';        
        $fb_app_id = "1019270498215239";
        
    }
    else {
        // LIVE
        // OLD $page_access_token = 'EAACEdEose0cBAGRNXws4HAbgS4KPXgnrhfaZBhlrxVPS921UlSNLcklAXJ4N56ZByNRj6IolZBv1uWeK6CNooDVGolQ8lwJV6NDfl6qmCojlmEyyTInixKZBOGeKVaxlPiMM16kI35hY8zYSP0QKVxL28zqpKjFNcwKtq01WGQZDZD';
        
        $fb_app_id = "735497823219616";
        $fb_app_secret = "285a509b3fe56efd38aba3e0f7c6d69b";
        $fb_client_token = "9feb8979f1c85333f10ff29e203b2042";
        
        $page_access_token = 'EAACEdEose0cBAOCpwoa6rWZAQsFRijhsiLA1p4xLdMm15TUvnb2XwOdUJcCl3v3pFNaGxMjgTsuqvkmJFhOZCmL8zZCLz1sdMaXsTn8oeDkZBzAlxaqrzNUZCj4KFYVN7qhTz1292LBNYVexDoZBe8JRyUVHfUjJqeCqmmYOdlRQZDZD';            
        $page_id = '933826073362902';        
    }

    $picture = SITEURL.'webServices/facebook_images/'.$fb_image;	
    $data['picture'] = $picture;
    $data['link'] = $joblink;
    $data['message'] = $job_title;
    $data['caption'] = $job_title;
    $data['access_token'] = $page_access_token;	
    $post_url = 'https://graph.facebook.com/'.$page_id.'/feed';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    curl_close($ch);
    //print_r($return);	
    update_post_meta($post_id, 'ad_builder_facebook_data', $return);        
    $returnval = json_decode($return);
    if(isset($returnval->id)){
       logit('Successfully posted on your facebook page');	
       update_post_meta($post_id, 'ad_builder_facebook', 1  );        
    }
    else{
      logit($returnval->error->message); 
    }
    ?>
        <h2> Ad for Facebook</h2>
        <br/>
        <img src="<?php echo SITEURL.'webServices/facebook_images/'.$fb_image; ?>" />
        <br/>
    <?php
    //end Share image on facebook    
}
function get_fb_page_access_token() {  
    global $fb_args, $fb_page_id;
    $fb_args['app_id'] = '1019270498215239';
    $fb_args["app_secret"] = '8a52acef13c94199fb49243b874bdf89';
    $fb_args["default_graph_version"] = 'v2.10';
    
    $ret = array("status" => false, "page_token" => "");

    if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
	    update_option('facebook_access_token', 'EAAOfBWf55UcBAAKWbnw76pj9Fu66BXxBj7Vs4m1j3j2mfuRrRE0bjNBe2Dq60QZBeYJhezDr1teKgN6Hj9L79VJO8iNyGGdzWJ1nnCCWANAgwMuDLN74eC5GqwuZCsEfIJUHwXoehMline7ZAihKyv8fbWJnOQZD');

	}
    
    $app_token = get_my_option("facebook_access_token");

    $fb = new Facebook\Facebook($fb_args);
    $page_access_request_data = array(
        "fields" => "access_token", 
        'access_token' => $app_token	    
        );
    $page_access_request = $fb->request('GET', '/1759376624354902', $page_access_request_data);
    try {
        $response = $fb->getClient()->sendRequest($page_access_request);
    } 
    catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        $ret['error'] = 'Error getting AppToken - Graph returned an error: ' . 
                $e->getMessage();
    } 
    catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        $ret['error'] = 'Error getting AppToken - Facebook SDK returned an error: ' . 
                $e->getMessage();
    }    
    $response_body = $response->getDecodedBody();
    $page_access_token = $response_body['access_token'];
    
    $ret['status'] = true;
    $ret['page_token'] = $page_access_token;
        
    return $ret;
}
function create_ad_facebook($post, $post_id, $joblink, $job_title) {
    global $fb_args, $fb_page_id;
    $fb_args['app_id'] = '1019270498215239';
    $fb_args["app_secret"] = '8a52acef13c94199fb49243b874bdf89';
    $fb_args["default_graph_version"] = 'v2.10';
    
    //Share image on facebook
    $isSuccess = true;
    $fb_image = time().'_fbimage.jpg';	
    fb_image_create($fb_image, $post_id,'fb');  //Create image for facebook call facebook function images
    $picture = SITEURL.'webServices/facebook_images/'.$fb_image;	
    
    // uncomment the code when testing done
    $fb_page_access_token_data = get_fb_page_access_token();
    $fb_page_access_token = @$fb_page_access_token_data['page_token'];

    
    if (empty($fb_page_access_token)) {
        $return = @$fb_page_access_token_data['error'];
    }
    else {
        $data['source'] = $picture;
        $data['link'] = $joblink;
        $data['message'] = $job_title;
        $data['caption'] = $job_title;
        $data['access_token'] = $fb_page_access_token;	

        $fb = new Facebook\Facebook($fb_args);
                $request = $fb->request('POST', '/1759376624354902/feed', $data);

        $return = "Unknown error while posting to Fabebook!";
        try {
            $response = $fb->getClient()->sendRequest($request);
            $status_code = $response->getHttpStatusCode();
            $isSuccess = $status_code == 200;
            if ($isSuccess) {
                $response_body = $response->getDecodedBody();
                $fb_post_id = @$response_body['id'];    
                $ids = explode("_", $fb_post_id);
                $return = "https://www.facebook.com/permalink.php?story_fbid=" . 
                        @$ids[1] . "&id=" . @$ids[0];
            }
        } 
        catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $return = 'Graph returned an error: ' . $e->getMessage();
        } 
        catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $return = 'Facebook SDK returned an error: ' . $e->getMessage();
        }
        
    }
    update_post_meta($post_id, 'ad_builder_facebook_data', $return);        
    if($isSuccess){
       update_post_meta($post_id, 'ad_builder_facebook', 1);        
        ?>
            <h2> Ad for Facebook</h2>
            <br/>
            <img src="<?php echo SITEURL.'webServices/facebook_images/'.$fb_image; ?>" />
            <br/>
        <?php
    }
    
    //logit($return); 
    print_r($return);
    //end Share image on facebook    
}
function create_ad_twitter($post, $post_id, $joblink, $job_title) {

    //Share image on twitter
    $twitt_image = time().'_twimage.jpg';	
    twitter_image_create($twitt_image, $post_id,'tw');  //Create image for twitter, call twitter function images
    if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
        /*$settings = array(
             'oauth_access_token' => "3235632702-VzBfXHdsgd0qK7KrOFu1AS64kJHTkBoaT7wyn85",
             'oauth_access_token_secret' => "Wnsgl63BktOloXEDw4YksqMAFG0misLWbJbt6hYMaX6PG",
             'consumer_key' => "txV6qshRqg0afrCdpcTSU25Rx",
             'consumer_secret' => "HWbw4zcStukRhOEw0vztz3KtCpHF4BoHNRXDWUvxn2fV0idRyJ"
         );	  */

         $settings = array(
             'oauth_access_token' => "4898148733-AxvuMsHDMFP9qX1j6EQNlKvpQPPANBnFMMvQBBP",
             'oauth_access_token_secret' => "vtQYx340S8yKuJom4DvtocPoZEceUAPHL4YLpoNVhKysN",
             'consumer_key' => "0noYVrvzqYPvhuhDxl5wfOlfl",
             'consumer_secret' => "WCu4Tnt9fgvezsegH834iNGX567waM4MxPh9fzV9924xGLNRHS"
         );	        
    }
    else {
         $settings = array(
             'oauth_access_token' => "4898148733-AxvuMsHDMFP9qX1j6EQNlKvpQPPANBnFMMvQBBP",
             'oauth_access_token_secret' => "vtQYx340S8yKuJom4DvtocPoZEceUAPHL4YLpoNVhKysN",
             'consumer_key' => "0noYVrvzqYPvhuhDxl5wfOlfl",
             'consumer_secret' => "WCu4Tnt9fgvezsegH834iNGX567waM4MxPh9fzV9924xGLNRHS"
         );	         
    }

    $twitterpicture = SITEURL.'webServices/twitter_images/'.$twitt_image;	
    $ad_image = $twitterpicture;
    $job_link = $joblink;
    $status = $job_title.' '.$joblink;


    // To upload image in twitter
    $file = file_get_contents($ad_image);
    $data = base64_encode($file);
    $url = 'https://upload.twitter.com/1.1/media/upload.json';
    $requestMethod = 'POST';
    $postfields = array(
         'media_data' => $data
    );

    /** Perform a POST request and echo the response **/
    $twitter = new TwitterAPIExchange($settings);
    $getmedia = $twitter->buildOauth($url, $requestMethod)
                 ->setPostfields($postfields)
                 ->performRequest();
    $getmedia = json_decode($getmedia);
    $media_id = $getmedia->media_id;  // fetch media id


    $url = 'https://api.twitter.com/1.1/statuses/update.json';
    $requestMethod = 'POST';
    $postfields = array(
         'status' => $status,
         'media_ids' => $media_id,
    );

    //$twitter = new TwitterAPIExchange($settings);
    $successmessage = $twitter->buildOauth($url, $requestMethod)
        ->setPostfields($postfields)
        ->performRequest();
    $successmessage = json_decode($successmessage);	
    if(!empty($successmessage->created_at)){
      $successmsg = 1; 
    }
    if($successmsg == '1'){
    //$wpdb->query("update $getadjobtable set status = 0 where job_id = '$job_id'");	
    }
    //end Share image on twitter    
    ?>
        <h2> Ad for Twitter</h2>
        <br/>
        <img src="<?php echo SITEURL.'webServices/twitter_images/'.$twitt_image;?>" />
        <br/>
    <?php    
    
}
function create_ad_pinterest($post, $post_id, $joblink, $job_title) {
    // share image on pinterest
    $pint_image = time().'_pint.jpg';
    pineterest_image_create($pint_image, $post_id,'pn');	
    $pineterest_picture = SITEURL.'webServices/pinterest_images/'.$pint_image;	

    if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
        // Staging
        $access_token = 'AQeo6vbKK9nAffqU_KpdqnoJRMELFFOoqFjQldFDIm-6_cAvwwAAAAA';
        $board_id = '148759662630761067';

    }
    else {
        // LIVE
        $access_token = 'ASMRHKn9qkeatZsdiMZhD5VYuWTwFFQShtJJrDJDI028F4Ar3wAAAAA';
        $board_id = '317926123634104943';        
    }
    
    $file = file_get_contents($pineterest_picture);
    $picture = base64_encode($file);
    
    $data['image_base64'] = $picture;
    $data['link'] = $joblink;
    $data['note'] = $job_title;
    $data['board'] = $board_id;
    
    //$post_url = "https://api.pinterest.com/v1/pins/?access_token=$access_token&fields=id%2Clink%2Cnote%2Curl%2Cboard%2Cattribution%2Ccolor%2Ccounts%2Ccreated_at%2Ccreator%2Cmedia%2Cmetadata%2Coriginal_link";
    $post_url = "https://api.pinterest.com/v1/pins/?access_token=$access_token&fields=id,link,note,url,board,attribution,color,counts,created_at,creator,media,metadata,original_link";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    curl_close($ch);
    //print_r($return);	
    $returnval = json_decode($return);
    if(isset($returnval->data) && isset($returnval->data->id)){
       logit('Successfully posted on your pinterest board');	
    }
    else{
      logit($returnval->message); 
    }    
    // end share image on pinterest
    ?>
        <h2> Ad for Pinterest</h2>
        <br/>
        <img src="<?php echo SITEURL.'webServices/pinterest_images/'.$pint_image;?>" />
        <br/>
    <?php
    
}
function create_ad_linkedin($post, $post_id, $joblink, $job_title) {
    // share image on Linkedin
    global $linkedInCompanyId;
 /*
<Job Title>
<Job Excerpt>
<Job Location> <Job Speciality>
<Link to ad>
  * 
  */
    $job_meta_data = get_post_meta($post_id);	
    $ad_title = $job_meta_data['ad_title'][0];	  
    $job_excerpt = $job_meta_data['_excerpt'][0];	  
    $job_description = $job_meta_data['_job_description'][0];	  
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state = $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
    $location =  $lcation_city.', '.$lcation_state;
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
    if ( !empty( $terms ) ){			
        $term = array_shift( $terms );
        $job_category = $term->name;
    }
    
    $excerpt = trim($job_excerpt);
    if (empty($excerpt)) {
        $excerpt = substr($job_description, 0, 50);
    }
    
    $description = $job_category . ' - ' . $location;// . ' ' . $joblink;
    
    $linked_image = time().'_linkedin.jpg';
    linkedin_image_create($linked_image, $post_id,'ln');	
    $linkedin_picture = SITEURL.'webServices/Linkedin_images/'.$linked_image;	
    $ad_image = $linkedin_picture;

    
    
    $token = get_my_option("linkedin_access_token");

    //$post_url = "https://api.linkedin.com/v1/people/~/shares?format=json";
    $post_url = "https://api.linkedin.com/v1/companies/$linkedInCompanyId/shares?format=json";
    
    $data = array(
        "comment" => "",
        "visibility" => array("code" => "anyone"),
        "content" => array(
          "title" => $ad_title,
          "description" => $description,
          "submitted-url" => $joblink,  
          "submitted-image-url" => $ad_image
        ),        
    );
    $header = array(
        "Authorization: Bearer $token",
        "Content-Type: application/json", 
        "x-li-format: json",
    );
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    update_post_meta($post_id, 'ad_builder_linkedin_data', $return);        
    curl_close($ch);
    print_r($return);	
    $returnval = json_decode($return);
    if (isset($returnval->updateKey)) {
        logit('Successfully posted on Linkedin <br/>');	
        logit($returnval->updateUrl);
        update_post_meta($post_id, 'ad_builder_linkedin', 1);        
    }
    else {
        logit($returnval->message); 
    }
        // End share image on Linkedin
    ?>
        <h2> Ad for LinkedIn</h2>
        <br/>
        <img src="<?php echo SITEURL.'webServices/Linkedin_images/'.$linked_image;?>" />
        <br/>
    <?php    
}
function create_ad_instagram($post, $post_id, $joblink, $job_title) {
    // share image on Instagram
    $instagram_image = time().'_instagram.jpg';
    instagram_image_create($instagram_image, $post_id,'in');	
    $instagram_picture = SITEURL.'webServices/instagram_images/'.$instagram_image;	
    $ad_image = $instagram_picture;
    $job_link = $joblink;
    $status = $job_title.' '.$joblink;   
    
    // End share image on Instagram    
    ?>
        <h2> Ad for Instagram</h2><br/>
        <br/>
        <img src="<?php echo SITEURL.'webServices/instagram_images/'.$instagram_image;?>" />
        <br/>
    <?php    
}
function create_ads($post_id, $linkedinOnly = false) {
    global $wpdb;

	$joblink = get_post_permalink($post_id); 
    $post = get_post($post_id);
    $job_title = get_post_meta($post_id, 'ad_descriptions', true);
    if($job_title == ''){
    	$job_title = $post->post_title;
    }

    if (!$linkedinOnly) {
        // Disabling facebook post as its not posting correctly [19 July 2017]
        create_ad_facebook($post, $post_id, $joblink, $job_title);
    }
    if (!$linkedinOnly) {
        create_ad_twitter($post, $post_id, $joblink, $job_title);
    }
    if (!$linkedinOnly) {
        create_ad_pinterest($post, $post_id, $joblink, $job_title);
    }
    if (!$linkedinOnly) {
        create_ad_instagram($post, $post_id, $joblink, $job_title);
    }
    
    create_ad_linkedin($post, $post_id, $joblink, $job_title);

}

if (!function_exists('appendUploadPath')) {
    function appendUploadPath($val) {
        if (!empty($val) && stripos($val, '/') !== 0 && stripos($val, "://") === FALSE) {
            $upload_dir = wp_upload_dir(null, false);
            $val = $upload_dir['basedir'].'/'.$val;
        }
        return $val;
    }
}

function imagefit($image, $mW, $mH, $position = 'center') {
    $iW = imagesx($image);
    $iH = imagesy($image);    
    $iR = $iW/$iH;
    
    $nW = $iW;
    $nH = $iH;
    
    $mR = $mW/$mH;
    
    if ($iR > $mR) {
        $nW = $iH * $mR;
    }
    else {
        $nH = $iW / $mR;
    }
    
    $dW = $iW - $nW;
    $pX = 0;
    if ($dW > 0) {
        $pX = $dW/2;
    }
    $dH = $iH - $nH;
    $pY = 0;
    if ($dH > 0) {
        $pY = $dH/2;
    }
    return array(
        "positionX" => $pX,
        "positionY" => $pY,
        "width" => $nW,
        "height" => $nH,
    );   
}