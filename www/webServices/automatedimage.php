<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<?php

$path  = ''; 
if ( !defined('WP_LOAD_PATH') ) {
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;

include('constants.php');
function image_resize1($src, $dst, $width, $height, $crop=0){
  if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";
  $type = strtolower(substr(strrchr($src,"."),1));
  if($type == 'jpeg') $type = 'jpg';
  switch($type){
    case 'bmp': $img = imagecreatefromwbmp($src); break;
    case 'gif': $img = imagecreatefromgif($src); break;
    case 'jpg': $img = imagecreatefromjpeg($src); break;
    case 'png': $img = imagecreatefrompng($src); break;
    default : return "Unsupported picture type!";
  }

  if($crop){
    if($w < $width or $h < $height) return "Picture is too small!";
    $ratio = max($width/$w, $height/$h);
    $h = $height / $ratio;
    $x = ($w - $width / $ratio) / 2;
    $w = $width / $ratio;
  }
  else{
    if($w < $width and $h < $height) return "Picture is too small!";
    $ratio = min($width/$w, $height/$h);
    $width = $w * $ratio;
    $height = $h * $ratio;
    $x = 0;
  }

  $new = imagecreatetruecolor($width, $height);

  // preserve transparency
  if($type == "gif" or $type == "png"){
    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
    imagealphablending($new, false);
    imagesavealpha($new, true);
  }

  imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

  switch($type){
    case 'bmp': imagewbmp($new, $dst); break;
    case 'gif': imagegif($new, $dst); break;
    case 'jpg': imagejpeg($new, $dst); break;
    case 'png': imagepng($new, $dst); break;
  }
  return true;
}

$getadjobtable = $wpdb->prefix.'job_ad';

$getadjobs = $wpdb->get_results("select * from $getadjobtable where status = 1");

foreach($getadjobs as $jobaddd){
	$inc_id = $jobaddd->id;
 $post_id = $jobaddd->job_id;	
 
 //$post_id = 7325;
$getposts = get_post($post_id);
//print_r($getposts);

$post_thumbnail_id = get_post_thumbnail_id( $post_id ); 
$job_meta_data = get_post_meta($post_id);	


      $lcation_city = $job_meta_data['geolocation_city'][0];	
      $lcation_state= $job_meta_data['geolocation_state_long'][0];	
      $lcation_country = $job_meta_data['geolocation_country_short'][0];
      $expirydate = $job_meta_data['Orientation'][0];	
	 
	 $job_category ='';
	 $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			 $job_category .= $term->slug.' ,';
		}
	 $job_category = rtrim($job_category , ',');
	 
	 
	 
$imgurl=  SITEURL.'wp-content/plugins/facebook-ad-manager/add_images/1462438918_world.jpg';
$imgpath = SITEURL.'wp-content/plugins/facebook-ad-manager/add_images/';
$size = getimagesize($imgurl);
$img_width = $size[0];
$img_height = $size[1]; 

if($img_height > 264){
	$canvas_height= $img_height + 68;
}
else{
$canvas_height = $img_height + 68;	
}
$total_width = 	$img_width + 161;

?>
<canvas id="myCanvas_<?php echo $inc_id; ?>" width="<?php echo $total_width;?>" height="<?php echo $canvas_height; ?>" style="border:1px solid #d3d3d3;">
    </canvas>
    <br/>
   

 <script>	
	   var canvas_<?php echo $inc_id; ?> = document.getElementById('myCanvas_<?php echo $inc_id; ?>');
       var context_<?php echo $inc_id; ?> = canvas_<?php echo $inc_id; ?>.getContext('2d');
       var imageObj_<?php echo $inc_id; ?> = new Image();
       var imageObj1_<?php echo $inc_id; ?> = new Image();
	   var imageObl_<?php echo $inc_id; ?> = new Image();
       var imageObju_<?php echo $inc_id; ?> = new Image();
	   var imageObd_<?php echo $inc_id; ?> = new Image();
      
	   context_<?php echo $inc_id; ?>.fillStyle = '#1E3465';
       context_<?php echo $inc_id; ?>.fillRect(0, 0, <?php echo $total_width;?>, <?php echo $canvas_height; ?>);
       imageObj_<?php echo $inc_id; ?>.onload = function() {
            context_<?php echo $inc_id; ?>.drawImage(imageObj_<?php echo $inc_id; ?>, 0, 0 ,<?php echo $img_width;?>, <?php echo $img_height;?>);
		   context_<?php echo $inc_id; ?>.drawImage(imageObj1_<?php echo $inc_id; ?>, <?php echo $img_width;?>, 0 ,161, 264);
		  
		   context_<?php echo $inc_id; ?>.font = '18px Open Sans';
		   context_<?php echo $inc_id; ?>.fillStyle = "#fff";
		   //context.textAlign = 'center';
		   var title= '<?php echo $job_title;?>';
		   var titleheight = '<?php echo $img_height + 24;?>';
		   
		  context_<?php echo $inc_id; ?>.drawImage(imageObj1_<?php echo $inc_id; ?>, <?php echo $img_width;?>, 0 ,161, 264);
		  
		  context_<?php echo $inc_id; ?>.fillText(title, 10, titleheight);
		  context_<?php echo $inc_id; ?>.font = '14px Open Sans';
		 // context.drawImage(imageObjl, 15, 250 ,10, 17);
		  var statetext =  '<?php echo $lcation_state.' - '.$lcation_country;?>';
		   var lastline_imgheight = '<?php echo $img_height + 40;?>';
		  var lastline = '<?php echo $img_height + 54;?>';
		  // for location
		  context_<?php echo $inc_id; ?>.drawImage(imageObl_<?php echo $inc_id; ?>, 10, lastline_imgheight ,10, 17);
		  context_<?php echo $inc_id; ?>.fillText(statetext, 27, lastline);
		    // for user
		   context_<?php echo $inc_id; ?>.drawImage(imageObju_<?php echo $inc_id; ?>, 140, lastline_imgheight ,10, 17);
		   var caategory = '<?php echo  $job_category; ?>';
		  context_<?php echo $inc_id; ?>.fillText(caategory, 157, lastline);
		  
		  	    // for date
		  var expirydate = '<?php echo  $expirydate;?>';
		  context_<?php echo $inc_id; ?>.drawImage(imageObd_<?php echo $inc_id; ?>, 255, lastline_imgheight ,10, 17);
		  context_<?php echo $inc_id; ?>.fillText(expirydate, 272, lastline);
       };
	   
      imageObj_<?php echo $inc_id; ?>.src = '<?php echo $imgurl;?>';
	  imageObj1_<?php echo $inc_id; ?>.src = '<?php echo $imgpath.'icons/right-side.png' ;?>';
	  imageObl_<?php echo $inc_id; ?>.src = '<?php echo $imgpath.'icons/1ocation.png' ;?>';
	  imageObju_<?php echo $inc_id; ?>.src = '<?php echo $imgpath.'icons/user.png';?>';
	  imageObd_<?php echo $inc_id; ?>.src = '<?php echo $imgpath.'icons/date.png';?>';  
	  
	  
		setTimeout(function(){
		var canvasurl<?php echo $inc_id; ?> = canvas_<?php echo $inc_id; ?>.toDataURL("image/png");	
		var post_id<?php echo $inc_id; ?> = '<?php echo $post_id; ?>';
	    var ajax = new XMLHttpRequest();
		ajax.open("POST",'saveimage.php?postid='+post_id<?php echo $inc_id; ?>,false);
			ajax.onreadystatechange = function() {
				console.log(ajax.responseText);
			}
			ajax.setRequestHeader('Content-Type', 'application/upload');
			ajax.send("imgData="+canvasurl<?php echo $inc_id; ?>);
	
		
		 }, 3000);



	</script>
<?php } 

//die('fg');

/*$post_id = 7325;
$getposts = get_post($post_id);
//print_r($getposts);

$post_thumbnail_id = get_post_thumbnail_id( $post_id ); 
$job_meta_data = get_post_meta($post_id);	


      $lcation_city = $job_meta_data['geolocation_city'][0];	
      $lcation_state= $job_meta_data['geolocation_state_long'][0];	
      $lcation_country = $job_meta_data['geolocation_country_short'][0];
      $expirydate = $job_meta_data['Orientation'][0];	
	 
	 $job_category ='';
	 $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			 $job_category .= $term->slug.' ,';
		}
	 $job_category = rtrim($job_category , ',');
	 
	 
	 
$imgurl=  SITEURL.'wp-content/plugins/facebook-ad-manager/add_images/1462438918_world.jpg';
$imgpath = SITEURL.'wp-content/plugins/facebook-ad-manager/add_images/';
$size = getimagesize($imgurl);
$img_width = $size[0];
$img_height = $size[1]; 

if($img_height > 264){
	$canvas_height= $img_height + 68;
}
else{
$canvas_height = $img_height + 68;	
}
$total_width = 	$img_width + 161;

?>
<canvas id="myCanvas" width="<?php echo $total_width;?>" height="<?php echo $canvas_height; ?>" style="border:1px solid #d3d3d3;">
    </canvas>
    <br/>
     <h3>Image for Instagram</h3> <br/>
      <canvas id="myCanvas_insta" width="<?php echo $total_width;?>" height="<?php echo $total_width; ?>" style="border:1px solid #d3d3d3;">
    </canvas>

 <script>	
	   var canvas = document.getElementById('myCanvas');
       var context = canvas.getContext('2d');
       var imageObj = new Image();
       var imageObj1 = new Image();
	   var imageObl = new Image();
       var imageObju = new Image();
	   var imageObd = new Image();
      
	   context.fillStyle = '#1E3465';
       context.fillRect(0, 0, <?php echo $total_width;?>, <?php echo $canvas_height; ?>);
       imageObj.onload = function() {
            context.drawImage(imageObj, 0, 0 ,<?php echo $img_width;?>, <?php echo $img_height;?>);
		   context.drawImage(imageObj1, <?php echo $img_width;?>, 0 ,161, 264);
		  
		   context.font = '18px Open Sans';
		   context.fillStyle = "#fff";
		   //context.textAlign = 'center';
		   var title= '<?php echo $job_title;?>';
		   var titleheight = '<?php echo $img_height + 24;?>';
		   
		  context.drawImage(imageObj1, <?php echo $img_width;?>, 0 ,161, 264);
		  
		  context.fillText(title, 10, titleheight);
		  context.font = '14px Open Sans';
		 // context.drawImage(imageObjl, 15, 250 ,10, 17);
		  var statetext =  '<?php echo $lcation_state.' - '.$lcation_country;?>';
		   var lastline_imgheight = '<?php echo $img_height + 40;?>';
		  var lastline = '<?php echo $img_height + 54;?>';
		  // for location
		  context.drawImage(imageObl, 10, lastline_imgheight ,10, 17);
		  context.fillText(statetext, 27, lastline);
		    // for user
		   context.drawImage(imageObju, 140, lastline_imgheight ,10, 17);
		   var caategory = '<?php echo  $job_category; ?>';
		  context.fillText(caategory, 157, lastline);
		  
		  	    // for date
		  var expirydate = '<?php echo  $expirydate;?>';
		  context.drawImage(imageObd, 255, lastline_imgheight ,10, 17);
		  context.fillText(expirydate, 272, lastline);
       };
	   
      imageObj.src = '<?php echo $imgurl;?>';
	  imageObj1.src = '<?php echo $imgpath.'icons/right-side.png' ;?>';
	  imageObl.src = '<?php echo $imgpath.'icons/1ocation.png' ;?>';
	  imageObju.src = '<?php echo $imgpath.'icons/user.png';?>';
	  imageObd.src = '<?php echo $imgpath.'icons/date.png';?>';  
	  
	  
		setTimeout(function(){
		var canvasurl= canvas.toDataURL("image/png");
		var canvasinstaurl= canvas_insta.toDataURL("image/png");	
		var post_id = '<?php echo $post_id; ?>';
	    var ajax = new XMLHttpRequest();
		ajax.open("POST",'saveimage.php?postid='+post_id,false);
			ajax.onreadystatechange = function() {
				console.log(ajax.responseText);
			}
			ajax.setRequestHeader('Content-Type', 'application/upload');
			ajax.send("imgData="+canvasurl);
	
		
		 }, 3000);


	</script>*/