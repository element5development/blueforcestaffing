<?php
$path  = ''; // It should be end with a trailing slash    

/** That's all, stop editing from here **/

if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;

include('constants.php');
require_once( WP_LOAD_PATH . 'webServices/twitter_lib/TwitterAPIExchange.php');

define("FAILSAFE_BANNER_URL", SITEURL . 'wp-content/uploads/2016/05/default-location-pic.jpg');

function fb_image_create($fb_image, $post_id){ 

    $char_limit_title = 32;
    $char_limit_location = 46;
    $char_limit_speciality = 46;
    
	$getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
	
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			$job_category = $term->name;
		}



	$x = 511;
	$y = 264;
	$background = imagecreatetruecolor($x, $y);
	$white = imagecolorallocate($background, 30, 52, 101);
	imagefilledrectangle($background, 0, 0, $x, $y, $white);
	
    
    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];	   
//    if (empty($location_image_path)) {
//        $location_image_path = $company_logo;
//    }
//    else {
//        $location_image_path = appendUploadPath($location_image_path);
//    }
    if (empty($location_image_url)) {
        $location_image_url = $company_logo;
    }

	$firstUrl = $location_image_url;	
	//$firstUrl = SITEURL.'webServices/facebook_images/icons/banner.jpg';	
	$secondUrl = SITEURL.'webServices/facebook_images/icons/right-side.png';	
	$thirdUrl = SITEURL.'webServices/facebook_images/icons/facebook-location.png';	
	$fourthUrl = SITEURL.'webServices/facebook_images/icons/facebook-icu.png';	
	//$fifthUrl = SITEURL.'webServices/facebook_images/icons/date-picker.png';	
	$outputImage = $background;
	
        try {
	$first = imagecreatefromjpeg($firstUrl);
        } catch (Exception $ex) {
            $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
        }        
	$second = imagecreatefrompng($secondUrl);
	$third = imagecreatefrompng($thirdUrl);
	$fourth = imagecreatefrompng($fourthUrl);
	//$fifth = imagecreatefrompng($fifthUrl);	
	
    $dynamicImageWindowX = 0;
    $dynamicImageWindowY = 0;
    $dynamicImageWindowW = 351; 
    $dynamicImageWindowH = 169;
    $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
    imagecopyresampled($outputImage, $first, 
            $dynamicImageWindowX, $dynamicImageWindowY,
            $resampleData['positionX'], $resampleData['positionY'], 
            $dynamicImageWindowW, $dynamicImageWindowH, 
            $resampleData['width'], $resampleData['height']);

    
	//imagecopymerge($outputImage,$first,0,0,0,0, 350, 196,100);
	imagecopymerge($outputImage,$second,351,0,0,0, 161, 264,100);
    
    $leftLine = 10;
    $textTopPad = 15;
    $textLeftPad = 20;
//	$text = 'North Carolina - ICU';
	$font = 'fonts/Lato-Bold.ttf';
	$font_size = 14;
	$topline = 180;

	
	$grey = imagecolorallocate($outputImage, 255, 255, 255);
        $job_title = substr($job_title, 0, $char_limit_title);
	imagettftext($outputImage, $font_size, 0, $leftLine, $topline + $textTopPad, $grey, $font, $job_title);
	
	
	$font = 'fonts/Lato-Regular.ttf';
	$font_size = 12;
	$topline = 208;
	$topline2 = 235;

	imagecopymerge($outputImage,$third,$leftLine+1,$topline,0,0, 14, 20,100);	
    $location = substr($location, 0, $char_limit_location);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline + $textTopPad, $grey, $font, $location);
	
	
	imagecopymerge($outputImage,$fourth,$leftLine,$topline2,0,0, 16, 20,100);
	$category = substr ($job_category, 0, $char_limit_speciality);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline2 + $textTopPad, $grey, $font, $category);
	
	//imagecopymerge($outputImage,$fifth,205,$topline,0,0, 23, 21,100);
    
    /*
	$datefield = $expirydate;
    $timestamp = strtotime($datefield);
    if ($timestamp === FALSE) {
        $datefield = substr($datefield, 0, 10);
    }
    else {
        $datefield = date("m/d/y", $timestamp);
    }
	imagettftext($outputImage, $font_size, 0, 231, $topline2, $grey, $font, $datefield);
    */
    
	$fb_image = 'facebook_images/'.$fb_image;
	imagejpeg($outputImage,$fb_image);  
	
	imagedestroy($outputImage);

 }
function twitter_image_create($twitt_image, $post_id){ 
 
    $char_limit_title = 32;
    $char_limit_location = 46;
    $char_limit_speciality = 46;
    
	$getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	    
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
	
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			$job_category = $term->name;
		}
	
	$x = 880;
	$y = 440;
	$background = imagecreatetruecolor($x, $y);
	$white = imagecolorallocate($background, 30, 52, 101);
	imagefilledrectangle($background, 0, 0, $x, $y, $white);
	
    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];	   
//    if (empty($location_image_path)) {
//        $location_image_path = $company_logo;
//    }
//    else {
//        $location_image_path = appendUploadPath($location_image_path);
//    }
    if (empty($location_image_url)) {
        $location_image_url = $company_logo;
    }

	$firstUrl = $location_image_url;	
//	$firstUrl = SITEURL.'webServices/twitter_images/icons/twitter-banner.jpg';	
	$secondUrl = SITEURL.'webServices/twitter_images/icons/white-bg.png';	
	$thirdUrl = SITEURL.'webServices/twitter_images/icons/twitter-location.png';	
	$fourthUrl = SITEURL.'webServices/twitter_images/icons/twitter-icu.png';	
	//$fifthUrl = SITEURL.'webServices/twitter_images/icons/twitter-date.png'; 
	
	$outputImage = $background;
	
        try {
	$first = imagecreatefromjpeg($firstUrl);
        } catch (Exception $ex) {
            $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
        }
	$second = imagecreatefrompng($secondUrl);
	$third = imagecreatefrompng($thirdUrl);
	$fourth = imagecreatefrompng($fourthUrl);
	//$fifth = imagecreatefrompng($fifthUrl);	
	
    $dynamicImageWindowX = 0;
    $dynamicImageWindowY = 0;
    $dynamicImageWindowW = 606;
    $dynamicImageWindowH = 300;
    $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
    imagecopyresampled($outputImage, $first, 
            $dynamicImageWindowX, $dynamicImageWindowY,
            $resampleData['positionX'], $resampleData['positionY'], 
            $dynamicImageWindowW, $dynamicImageWindowH, 
            $resampleData['width'], $resampleData['height']);
    
//	imagecopymerge($outputImage,$first,0,0,0,0, 605, 328,100);
	imagecopymerge($outputImage,$second,606,0,0,0, 275, 440,100);
    
    $leftLine = 15;
    $textTopPad = 25;
    $textLeftPad = 35;
    
	$font = 'fonts/Lato-Bold.ttf';
	$grey = imagecolorallocate($outputImage, 255, 255, 255);
	$font_size = 26;
	$topline = 319;
    $job_title = substr($job_title, 0, $char_limit_title);
	imagettftext($outputImage, $font_size, 0, $leftLine, $topline + $textTopPad, $grey, $font, $job_title);
	
	
	$font = 'fonts/Lato-Regular.ttf';
	$font_size = 16;
	$topline = 358;
	$topline2 = 398;  
		 
	imagecopymerge($outputImage,$third,$leftLine + 2,$topline,0,0, 23, 33,100);
    $location = substr($location, 0, $char_limit_location);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline + $textTopPad, $grey, $font, $location);
	
	$category = substr ($job_category, 0, $char_limit_speciality);
	imagecopymerge($outputImage,$fourth,$leftLine,$topline2,0,0, 27, 32,100);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline2 + $textTopPad, $grey, $font, $job_category);
	
	//imagecopymerge($outputImage,$fifth,393,$topline,0,0, 38, 34,100);
	
    /*
	$datefield = $expirydate;
    $timestamp = strtotime($datefield);
    if ($timestamp === FALSE) {
        $datefield = substr($datefield, 0, 10);
    }
    else {
        $datefield = date("m/d/y", $timestamp);
    }    
	imagettftext($outputImage, $font_size, 0, 436, $topline_text, $grey, $font, $datefield); 
    */
    
	$twitt_image = 'twitter_images/'.$twitt_image;
	imagejpeg($outputImage,$twitt_image);  
	
	imagedestroy($outputImage);
	
	 }
function pineterest_image_create($pint_image, $post_id){ 
     
    $char_limit_title = 24;
    $char_limit_location = 29;
    $char_limit_speciality = 29;
    
	$getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	      
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
	
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			$job_category = $term->name;
		}


	$x = 236;
	$y = 600;
	$background = imagecreatetruecolor($x, $y);
	$white = imagecolorallocate($background, 30, 52, 101);
	imagefilledrectangle($background, 0, 0, $x, $y, $white);
	
    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];	   
//    if (empty($location_image_path)) {
//        $location_image_path = $company_logo;
//    }
//    else {
//        $location_image_path = appendUploadPath($location_image_path);
//    }
    if (empty($location_image_url)) {
        $location_image_url = $company_logo;
    }

	$firstUrl = $location_image_url;    
//	$firstUrl =  SITEURL.'webServices/pinterest_images/icons/pintrest-banner.jpg';	
	$secondUrl = SITEURL.'webServices/pinterest_images/icons/white-area.png';	
	$thirdUrl = SITEURL.'webServices/pinterest_images/icons/location.png';	
	$fourthUrl = SITEURL.'webServices/pinterest_images/icons/icu-user.png';	
	//$fifthUrl = SITEURL.'webServices/pinterest_images/icons/date.png'; 
	
	$outputImage = $background;
	
        try {
	$first = imagecreatefromjpeg($firstUrl);
        } catch (Exception $ex) {
            $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
        }
	$second = imagecreatefrompng($secondUrl);
	$third = imagecreatefrompng($thirdUrl);
	$fourth = imagecreatefrompng($fourthUrl);
	//$fifth = imagecreatefrompng($fifthUrl);	
	
    $dynamicImageWindowX = 0;
    $dynamicImageWindowY = 0;
    $dynamicImageWindowW = 237;
    $dynamicImageWindowH = 135;
    $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
    imagecopyresampled($outputImage, $first, 
            $dynamicImageWindowX, $dynamicImageWindowY,
            $resampleData['positionX'], $resampleData['positionY'], 
            $dynamicImageWindowW, $dynamicImageWindowH, 
            $resampleData['width'], $resampleData['height']);    
	//imagecopymerge($outputImage,$first,0,0,0,0, 236, 135,100);

    $leftLine = 10;
    $textTopPad = 14;
    $textLeftPad = 20;
    
	$font = 'fonts/Lato-Bold.ttf';
	$grey = imagecolorallocate($outputImage, 255, 255, 255);
	$font_size = 14;
	$topline = 160;
    $job_title = substr($job_title, 0, $char_limit_title);
	imagettftext($outputImage, $font_size, 0, $leftLine, $topline, $grey, $font, $job_title);	
	
	$font = 'fonts/Lato-Regular.ttf';
	$font_size = 10;
	$topline = 173;
	$topline2 = 198;  
		 
	imagecopymerge($outputImage,$third,$leftLine + 1,$topline,0,0, 13, 19,100);
    $location = substr($location, 0, $char_limit_location);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline + $textTopPad, $grey, $font, $location);	
	
    $category = substr ($job_category, 0, $char_limit_speciality);
	imagecopymerge($outputImage,$fourth,$leftLine, $topline2,0,0, 15, 18,100);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline2 + $textTopPad, $grey, $font, $category);
	
	$topline = 195;
	$topline_text = 212; 	
	//imagecopymerge($outputImage,$fifth,60,$topline,0,0, 22, 20,100);
    
    /*
	$datefield = $expirydate;
    $timestamp = strtotime($datefield);
    if ($timestamp === FALSE) {
        $datefield = substr($datefield, 0, 10);
    }
    else {
        $datefield = date("m/d/y", $timestamp);
    }  
	imagettftext($outputImage, $font_size, 0, 86, $topline_text, $grey, $font, $datefield); 	
    */
    
	// right side image
	imagecopymerge($outputImage,$second,0,225,0,0, 236, 375,100);
	
	$pint_image = 'pinterest_images/'.$pint_image;
	 imagejpeg($outputImage,$pint_image);  	
	 imagedestroy($outputImage);
	
}
function linkedin_image_create($linked_image, $post_id){
        
    $char_limit_title = 48;
    $char_limit_location = 48;
    $char_limit_speciality = 48;
    
	$getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	  
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
	
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			$job_category = $term->name;
		}

	
	$x = 646;
	$y = 220;
	$background = imagecreatetruecolor($x, $y);
	$white = imagecolorallocate($background, 30, 52, 101);
	imagefilledrectangle($background, 0, 0, $x, $y, $white);

    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];	   
//    if (empty($location_image_path)) {
//        $location_image_path = $company_logo;
//    }
//    else {
//        $location_image_path = appendUploadPath($location_image_path);
//    }
    if (empty($location_image_url)) {
        $location_image_url = $company_logo;
    }

	$firstUrl = $location_image_url;    
//	$firstUrl = SITEURL.'webServices/Linkedin_images/icons/banner.jpg';	
	$secondUrl = SITEURL.'webServices/Linkedin_images/icons/white-im.jpg';	
	$thirdUrl = SITEURL.'webServices/Linkedin_images/icons/location.png';	
	$fourthUrl = SITEURL.'webServices/Linkedin_images/icons/icu-user.png';	
	//$fifthUrl = SITEURL.'webServices/Linkedin_images/icons/date.png';
	
	$outputImage = $background;
	
        try {
	$first = imagecreatefromjpeg($firstUrl);
        } catch (Exception $ex) {
            $first = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
        }
	$second = imagecreatefromjpeg($secondUrl);
	$third = imagecreatefrompng($thirdUrl);
	$fourth = imagecreatefrompng($fourthUrl);
	//$fifth = imagecreatefrompng($fifthUrl);

    $dynamicImageWindowX = 0;
    $dynamicImageWindowY = 0;
    $dynamicImageWindowW = 505;
    $dynamicImageWindowH = 145;
    $resampleData = imagefit($first, $dynamicImageWindowW, $dynamicImageWindowH);
    imagecopyresampled($outputImage, $first, 
            $dynamicImageWindowX, $dynamicImageWindowY,
            $resampleData['positionX'], $resampleData['positionY'], 
            $dynamicImageWindowW, $dynamicImageWindowH, 
            $resampleData['width'], $resampleData['height']);    
//    imagecopymerge($outputImage,$first,0,0,0,0, 504, 165,100);
    imagecopymerge($outputImage,$second,505,0,0,0, 142, 220,100);
	
    $leftLine = 10;
    $textTopPad = 14;
    $textLeftPad = 20;
    
	$font = 'fonts/Lato-Bold.ttf';
	$grey = imagecolorallocate($outputImage, 255, 255, 255);
	$font_size = 14;
	$topline = 154;
    $job_title = substr($job_title, 0, $char_limit_title);
	imagettftext($outputImage, $font_size, 0, $leftLine, $topline + $textTopPad, $grey, $font, $job_title);
	
	
	$font = 'fonts/Lato-Regular.ttf';
	$font_size = 10;
	$topline = 177;
	$topline2 = 199;  
		 
	imagecopymerge($outputImage,$third,$leftLine + 1,$topline,0,0, 13, 19,100);
        $location = substr($location, 0, $char_limit_location);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline + $textTopPad, $grey, $font, $location);
	
        $category = substr ($job_category, 0, $char_limit_speciality);
	imagecopymerge($outputImage,$fourth,$leftLine,$topline2,0,0, 15, 18,100);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline2 + $textTopPad, $grey, $font, $category);
	
	//imagecopymerge($outputImage,$fifth,213,$topline,0,0, 22, 20,100);
	
    /*
    $datefield = $expirydate;
    $timestamp = strtotime($datefield);
    if ($timestamp === FALSE) {
        $datefield = substr($datefield, 0, 10);
    }
    else {
        $datefield = date("m/d/y", $timestamp);
    }  
	imagettftext($outputImage, $font_size, 0, 235, $topline_text, $grey, $font, $datefield); 
    
    */
    
    $linked_image = 'Linkedin_images/'.$linked_image;
    imagejpeg($outputImage,$linked_image);  
    imagedestroy($outputImage);

}
function instagram_image_create($instagram_image_filename, $post_id){

    $char_limit_title = 19;
    $char_limit_location = 24;
    $char_limit_speciality = 24;
    
    $getposts = get_post($post_id);
    $job_meta_data = get_post_meta($post_id);	
//    $job_title = $getposts->post_title;
    $job_title = $job_meta_data['ad_title'][0];	  
    $lcation_city = $job_meta_data['_city'][0];	
    $lcation_state= $job_meta_data['_state'][0];	   
	
    //$expirydate = $job_meta_data['_job_deadline'][0];	
	if(strlen($lcation_city) <= 20){
      $location =  $lcation_city.', '.$lcation_state;
	}
	else{
	 $location =  $lcation_city;	
	}
	
  
    $job_category ='';
    $terms = get_the_terms( $post_id, 'job_listing_category' );
		if ( !empty( $terms ) ){			
			$term = array_shift( $terms );
			$job_category = $term->name;
		}

	
	$x = 640;
	$y = 640;
	$background = imagecreatetruecolor($x, $y);
	$white = imagecolorallocate($background, 30, 52, 101);
	imagefilledrectangle($background, 0, 0, $x, $y, $white);

    $company_logo = $job_meta_data['company_logo'][0];	   
    $location_image_path = $job_meta_data['location_image_path'][0];	   
    $location_image_url = $job_meta_data['location_image_url'][0];	   
//    if (empty($location_image_path)) {
//        $location_image_path = $company_logo;
//    }
//    else {
//        $location_image_path = appendUploadPath($location_image_path);
//    }
    if (empty($location_image_url)) {
        $location_image_url = $company_logo;
    }

	$bannerUrl = $location_image_url;    
	$bannerApplyNowUrl = SITEURL.'webServices/instagram_images/icons/banner-applynow.jpg';	
	$locationIconUrl = SITEURL.'webServices/instagram_images/icons/icon-location.png';	
	$userIconUrl = SITEURL.'webServices/instagram_images/icons/icon-user.png';	
	
	$outputImage = $background;
        
        try {
            $banner = imagecreatefromjpeg($bannerUrl);            
        } catch (Exception $ex) {
            $banner = imagecreatefromjpeg(FAILSAFE_BANNER_URL);            
        }
	$bannerApplyNow = imagecreatefromjpeg($bannerApplyNowUrl);
	$locationIcon = imagecreatefrompng($locationIconUrl);
	$userIcon = imagecreatefrompng($userIconUrl);

    $dynamicImageWindowX = 0;
    $dynamicImageWindowY = 0;
    $dynamicImageWindowW = 351;
    $dynamicImageWindowH = 640;
    $resampleData = imagefit($banner, $dynamicImageWindowW, $dynamicImageWindowH);
    imagecopyresampled($outputImage, $banner, 
            $dynamicImageWindowX, $dynamicImageWindowY,
            $resampleData['positionX'], $resampleData['positionY'], 
            $dynamicImageWindowW, $dynamicImageWindowH, 
            $resampleData['width'], $resampleData['height']);    
//    imagecopymerge($outputImage,$first,0,0,0,0, 504, 165,100);
    imagecopymerge($outputImage,$bannerApplyNow,351,0,0,0, 289, 466,100);
	
    $leftLine = 365;
    $textTopPad = 16;
    $textLeftPad = 25;
    
	$font = 'fonts/Lato-Bold.ttf';
	$grey = imagecolorallocate($outputImage, 255, 255, 255);
	$font_size = 22;
	$topline = 505;
    $job_title = substr($job_title, 0, $char_limit_title);
	imagettftext($outputImage, $font_size, 0, $leftLine, $topline + $textTopPad, $grey, $font, $job_title);
	
	
	$font = 'fonts/Lato-Regular.ttf';
	$font_size = 14;
	$topline = 552;
	$topline2 = 590;  
		 
	imagecopymerge($outputImage,$locationIcon,$leftLine + 1, $topline,0,0, 14, 20, 100);
    $location = substr($location, 0, $char_limit_speciality);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline + $textTopPad, $grey, $font, $location);
	
    $category = substr ($job_category, 0, $char_limit_speciality);
	imagecopymerge($outputImage,$userIcon,$leftLine,$topline2,0,0, 16, 20,100);
	imagettftext($outputImage, $font_size, 0, $leftLine + $textLeftPad, $topline2 + $textTopPad, $grey, $font, $category);
	
	//imagecopymerge($outputImage,$fifth,213,$topline,0,0, 22, 20,100);
	
    /*
    $datefield = $expirydate;
    $timestamp = strtotime($datefield);
    if ($timestamp === FALSE) {
        $datefield = substr($datefield, 0, 10);
    }
    else {
        $datefield = date("m/d/y", $timestamp);
    }  
	imagettftext($outputImage, $font_size, 0, 235, $topline_text, $grey, $font, $datefield); 
    
    */
    
    $instagram_image_filename = 'instagram_images/'.$instagram_image_filename;
    imagejpeg($outputImage,$instagram_image_filename);  
    imagedestroy($outputImage);

}

function create_ads($post_id) {
    global $wpdb;
    $joblink = get_post_permalink($post_id); 
    $getposts = get_post($post_id);
    $job_title = $getposts->post_title;

    //Share image on facebook
    $fb_image = time().'_fbimage.jpg';	

    fb_image_create($fb_image, $post_id);  //Create image for facebook call facebook function images

    //$fb_setting = $wpdb->prefix . 'facebook_setting';
    //$get_Settings =   $wpdb->get_results("select * from $fb_setting");
    //$get_Settings = $get_Settings[0];
    //$page_access_token = $get_Settings->accessToken;
    //$page_id = $get_Settings->pageId;
if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
    $page_access_token = 'CAADoq74Amp4BACzktSCrArS8FoGQLWC8ZAAZBD3u1vwU3dfZC4L1ZBhS6cmEPo9THNfOYBk3ZA7lR7hio4f0AhWMUyVCUAgJkmacruBCH9rwgsbZCgBXjAuzjT4Q68yahTQZAWavGZAMCcanTAYVZAmf1FG6d5uOei1MpdIJdy0SGzO2zRfmIrwCj75J6fvSREkQ22SZAzo8ZBnJQZDZD';
    $page_id = '214900892222530';
    }
    else {
        // LIVE
        $page_access_token = 'EAACEdEose0cBAGRNXws4HAbgS4KPXgnrhfaZBhlrxVPS921UlSNLcklAXJ4N56ZByNRj6IolZBv1uWeK6CNooDVGolQ8lwJV6NDfl6qmCojlmEyyTInixKZBOGeKVaxlPiMM16kI35hY8zYSP0QKVxL28zqpKjFNcwKtq01WGQZDZD';
        $page_id = '933826073362902';        
    }

    $picture = SITEURL.'webServices/facebook_images/'.$fb_image;	
    $data['picture'] = $picture;
    $data['link'] = $joblink;
    $data['message'] = $job_title;
    $data['caption'] = $job_title;
    $data['access_token'] = $page_access_token;	
    $post_url = 'https://graph.facebook.com/'.$page_id.'/feed';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    curl_close($ch);
    //print_r($return);	
    $returnval = json_decode($return);
    if(isset($returnval->id)){
       echo 'Successfully posted on your facebook page';	
    }
    else{
      echo $returnval->error->message; 
    }

    //end Share image on facebook



    //Share image on twitter
    $twitt_image = time().'_twimage.jpg';	
    twitter_image_create($twitt_image, $post_id);  //Create image for twitter, call twitter function images
    if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
        $settings = array(
             'oauth_access_token' => "3235632702-VzBfXHdsgd0qK7KrOFu1AS64kJHTkBoaT7wyn85",
             'oauth_access_token_secret' => "Wnsgl63BktOloXEDw4YksqMAFG0misLWbJbt6hYMaX6PG",
             'consumer_key' => "txV6qshRqg0afrCdpcTSU25Rx",
             'consumer_secret' => "HWbw4zcStukRhOEw0vztz3KtCpHF4BoHNRXDWUvxn2fV0idRyJ"
         );	
    }
    else {
        $settings = array(
             'oauth_access_token' => "733723131139915776-JiNaa1n37CXZiWTJPMe81BlCFH3atpP",
             'oauth_access_token_secret' => "9F4V83IWuXfEaFoAxCGF9v4HJgZ72lVFQlDRgZd1WSWsj",
             'consumer_key' => "DsGtNk18gs4Yrvddu3CoV7U0A",
             'consumer_secret' => "xSKUadXrNrnutU8fRfpu3GSK2KJJySFQLksAf5YmkzqYYk0vAr"
         );	        
    }

    $twitterpicture = SITEURL.'webServices/twitter_images/'.$twitt_image;	
    $ad_image = $twitterpicture;
    $job_link = $joblink;
    $status = $job_title.' To more view link: '.$joblink;


    // To upload image in twitter
    $file = file_get_contents($ad_image);
    $data = base64_encode($file);
    $url = 'https://upload.twitter.com/1.1/media/upload.json';
    $requestMethod = 'POST';
    $postfields = array(
         'media_data' => $data
    );

    /** Perform a POST request and echo the response **/
    $twitter = new TwitterAPIExchange($settings);
    $getmedia = $twitter->buildOauth($url, $requestMethod)
                 ->setPostfields($postfields)
                 ->performRequest();
    $getmedia = json_decode($getmedia);
    $media_id = $getmedia->media_id;  // fetch media id


    $url = 'https://api.twitter.com/1.1/statuses/update.json';
    $requestMethod = 'POST';
    $postfields = array(
         'status' => $status,
         'media_ids' => $media_id,
    );

    $twitter = new TwitterAPIExchange($settings);
    $successmessage = $twitter->buildOauth($url, $requestMethod)
        ->setPostfields($postfields)
        ->performRequest();
    $successmessage = json_decode($successmessage);	
    if(!empty($successmessage->created_at)){
      $successmsg = 1; 
    }
    if($successmsg == '1'){
    //$wpdb->query("update $getadjobtable set status = 0 where job_id = '$job_id'");	
    }
    //end Share image on twitter




    // share image on pinterest
    $pint_image = time().'_pint.jpg';
    pineterest_image_create($pint_image, $post_id);	

    $pineterest_rpicture = SITEURL.'webServices/pinterest_images/'.$pint_image;	
    $ad_image = $pineterest_rpicture;
    $job_link = $joblink;
    $status = $job_title.' To more view link: '.$joblink;    
    
    
    
    // end share image on pinterest



    // share image on Linkedin

    $linked_image = time().'_linkedin.jpg';
    linkedin_image_create($linked_image, $post_id);	
    $linkedin_picture = SITEURL.'webServices/Linkedin_images/'.$linked_image;	
    $ad_image = $linkedin_picture;
    $job_link = $joblink;
    $status = $job_title.' To more view link: '.$joblink;      
    
    
    
    // End share image on Linkedin
    
    // share image on Instagram
    $instagram_image = time().'_instagram.jpg';
    instagram_image_create($instagram_image, $post_id);	
    $instagram_picture = SITEURL.'webServices/instagram_images/'.$instagram_image;	
    $ad_image = $instagram_picture;
    $job_link = $joblink;
    $status = $job_title.' To more view link: '.$joblink;   
    
    
    
    // End share image on Instagram
        ?>

    <h2> Ad for Facebook</h2>
    <br/>
    <img src="<?php echo SITEURL.'webServices/facebook_images/'.$fb_image; ?>" />
    <br/>
    <h2> Ad for Twitter</h2>
    <br/>
    <img src="<?php echo SITEURL.'webServices/twitter_images/'.$twitt_image;?>" />
    <br/>
    <h2> Ad for Pinterest</h2>
    <br/>
    <img src="<?php echo SITEURL.'webServices/pinterest_images/'.$pint_image;?>" />
    <br/>
    <h2> Ad for LinkedIn</h2>
    <br/>
    <img src="<?php echo SITEURL.'webServices/Linkedin_images/'.$linked_image;?>" />

    <h2> Ad for Instagram</h2><br/>
    <img src="<?php echo SITEURL.'webServices/instagram_images/'.$instagram_image;?>" />

    <?php

}

if (!function_exists('appendUploadPath')) {
    function appendUploadPath($val) {
        if (!empty($val) && stripos($val, '/') !== 0 && stripos($val, "://") === FALSE) {
            $upload_dir = wp_upload_dir(null, false);
            $val = $upload_dir['basedir'].'/'.$val;
        }
        return $val;
    }
}

function imagefit($image, $mW, $mH, $position = 'center') {
    $iW = imagesx($image);
    $iH = imagesy($image);    
    $iR = $iW/$iH;
    
    $nW = $iW;
    $nH = $iH;
    
    $mR = $mW/$mH;
    
    if ($iR > $mR) {
        $nW = $iH * $mR;
    }
    else {
        $nH = $iW / $mR;
    }
    
    $dW = $iW - $nW;
    $pX = 0;
    if ($dW > 0) {
        $pX = $dW/2;
    }
    $dH = $iH - $nH;
    $pY = 0;
    if ($dH > 0) {
        $pY = $dH/2;
    }
    return array(
        "positionX" => $pX,
        "positionY" => $pY,
        "width" => $nW,
        "height" => $nH,
    );   
}