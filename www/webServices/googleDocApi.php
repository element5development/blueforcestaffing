<?php

$path  = ''; // It should be end with a trailing slash    

/** That's all, stop editing from here **/

if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;

 function processImage($post_id, $image) {
    $upload_dir = wp_upload_dir();

    $filename = basename($image);
    if ($filename == "no-image-white-original.png") { 
      return null;//ignore white
    }
    if(wp_mkdir_p($upload_dir['path']))
        $file = $upload_dir['path'] . '/' . $filename;
    else
        $file = $upload_dir['basedir'] . '/' . $filename;

    $image_data = file_get_contents($image);
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit',
        'guid' => $image
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );    
	
	wp_update_attachment_metadata( $attach_id, $attach_data );
	
	
    return $attach_id; 
  }  
  

//print_r(get_post_meta(3726));
//die('sdffg');

header('Content-type: application/json');

$feed = 'https://docs.google.com/spreadsheets/d/1gO1rfe5otYTCJM0uqlPv3-wFZBDYxbKTQwKXAtRydo4/pub?gid=283842081&single=true&output=csv'; 

$keys = array();
$newArray = array();

// Function to convert CSV into associative array
function csvToArray($file, $delimiter) { 
  if (($handle = fopen($file, 'r')) !== FALSE) { 
    $i = 0;
	
    while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
      for ($j = 0; $j < count($lineArray); $j++) {		 
        $arr[$i][$j] = $lineArray[$j]; 
      } 
      $i++; 
    } 
    fclose($handle); 
  } 

  return $arr; 
} 




$data = csvToArray($feed, ',');
// Set number of elements (minus 1 because we shift off the first row)
$count = count($data) - 1;
//Use first row for names  
$labels = array_shift($data);
foreach ($labels as $label) {
  $keys[] = $label;
}
 
// Add Ids, just in case we want them later
$keys[] = 'id'; 
for ($i = 0; $i < $count; $i++) {
  $data[$i][] = $i;
} 
// Bring it all together
for ($j = 0; $j < $count; $j++) {
  $d = array_combine($keys, $data[$j]);
  $newArray[$j] = $d;
}
    
$increment = 1;
foreach($newArray as $dataArray){
//if($increment <= 1 ) { 
		echo date('m/d/Y');
	print_r($dataArray);
	$companyName = $dataArray['Client']; // company name
	$Facility = $dataArray['Facility']; //location
	$City = $dataArray['City']; //geolocation_city cusatom filed c
	$State = $dataArray['State']; //geolocation_state_short filed D
	$Specialty = $dataArray['Specialty']; //custom filed E
	$Orientation = $dataArray['Orientation']; // custom filed
	$Shift = $dataArray['Shift']; //// custom filed
	$Guarantee = $dataArray['Guarantee']; // guramtee I custom field
	$ContractLength = $dataArray['Contract Length']; // comumn j contract lenght custom field
	$PPC = $dataArray['PPC'];// custom filed
	$HourlyMax = $dataArray['Hourly Max']; //rate per hour_max
	$HourlyMax = str_replace("$","",$HourlyMax);
	$HourlyMax = trim($HourlyMax);
	$WeeklyGross = $dataArray['Weekly Gross']; // custom filed
	$WeeklyGross = str_replace("$","",$WeeklyGross);
	$WeeklyGross = trim($WeeklyGross);
	$TravelStipend = $dataArray['Travel Stipend']; // custom filed
	$TravelStipend = str_replace("$","",$TravelStipend);
	$TravelStipend = trim($TravelStipend);
	$Notes = $dataArray['Notes'];
	$Bonus = $dataArray['Bonus']; // bonus p custom field
	$CompactState = $dataArray['Compact State?']; // Column R custom field
	$HousingStipend = $dataArray['Housing Stipend']; // housing sdtpend column ???U 
	$HousingStipend = str_replace("$","",$HousingStipend);
	$HousingStipend = trim($HousingStipend);
	$jobId = $dataArray['id'];
	
	//$job_title = $companyName. ' job';
	
	$job_title = $dataArray['Job_title'];
	if(empty($job_title)){
		$job_title = $companyName. ' job';
	}
	$job_description = $dataArray['job_description'];
	$job_region = $dataArray['job_region'];
	$job_updated = $dataArray['job_updated'];
	$zipcode = $dataArray['zipcode'];
	$company_website = $dataArray['company_website'];
	$facebook_page = $dataArray['facebook_page'];
	$twitter_page = $dataArray['twitter_page'];
	$featured_image = $dataArray['featured_image'];
	$travel_image = $dataArray['travel_image'];
	$job_type = $dataArray['job_type'];
	$job_typesss= explode(',',$dataArray['job_type'] );	
	
	
	
   $post = array(
				   "post_type" => 'job_listing',
				   "post_title" => $job_title,
				   "post_content" => $job_description,
				   "post_status" =>"publish",
				   "post_author" => 0,				
			   );					
   $job_id = wp_insert_post( $post );	
  //for category
   $term = term_exists($Specialty, 'job_listing_category');
 //  print_r($term);
   if ($term !== 0 && $term !== null) {
	  $termid = $term['term_id']; 	  
	  wp_set_object_terms( $job_id, $Specialty, 'job_listing_category');       
	}
 else{
	 $speciality_slug = strtolower($Specialty);
     $speciality_slug =str_replace(' ', '-', $speciality_slug);
	$termss = wp_insert_term(
	  $Specialty, 
	  'job_listing_category', 
	  array(
		'description'=> '',
		'slug' => $speciality_slug,
		'parent'=> 0
	   )
	 ); 
	 
	 wp_set_object_terms( $job_id, $speciality_slug, 'job_listing_category', true);     
 }
 
 if(count($job_types) > 1) {
 foreach($job_types as $jtype){
	 	echo $jtype = trim($jtype);
		$term = get_term_by('slug', $jtype, 'job_listing_type');	
		print_r($term);
		 if ($term !== 0 && $term !== null) {
	        echo  $termid = $term->term_id; 		   
	   			wp_set_object_terms( $job_id, $termid, 'job_listing_type', true);       
	    }
	}
 }
 else{
echo 'else';
	echo $job_type = trim($dataArray['job_type']);
	 //$term = term_exists($job_type, 'job_listing_type');
	 $term = get_term_by('slug', $job_type, 'job_listing_type');	
	 print_r( $term);	
		 if ($term !== 0 && $term !== null) {
	       echo  $termid = $term->term_id; 		          
			wp_set_object_terms( $job_id, $termid, 'job_listing_type');          
	    } 
		
	 
 }
  
    update_post_meta($job_id, '_company_name', $companyName);
  
    update_post_meta($job_id, '_job_location', $Facility);
   
    update_post_meta($job_id, '_rate_max', $HourlyMax);
	update_post_meta($job_id, 'Orientation', $Orientation);
	update_post_meta($job_id, 'geolocation_city', $City);
	update_post_meta($job_id, 'geolocation_state_long', $State);
	
	  update_post_meta($job_id, 'Shift', $Shift);
	update_post_meta($job_id, 'Guarantee', $Guarantee);
	update_post_meta($job_id, 'ContractLength', $ContractLength);
	
	 update_post_meta($job_id, 'PPC', $PPC);
	update_post_meta($job_id, 'WeeklyGross', $WeeklyGross);
	update_post_meta($job_id, 'TravelStipend', $TravelStipend);
	
	update_post_meta($job_id, 'Notes', $Notes);
	update_post_meta($job_id, 'Bonus', $Bonus);
	update_post_meta($job_id, 'CompactState', $CompactState);
	
	update_post_meta($job_id, 'HousingStipend', $HousingStipend);
	update_post_meta($job_id, 'jobId', $jobId);
	
	update_post_meta($job_id, 'geolocation_country_short', 'US');
	update_post_meta($job_id, 'geolocation_country_long', 'United States');
	update_post_meta($job_id, 'Specialty', $Specialty);
	
	
	update_post_meta($job_id, '_company_website', $company_website);
	update_post_meta($job_id, '_company_twitter', $twitter_page);
	update_post_meta($job_id, 'facebook_link', $facebook_page);
	
	update_post_meta($job_id, 'job_region', $job_region);
	update_post_meta($job_id, 'job_zipcode', $zipcode);
	update_post_meta($job_id, 'travel_image', $travel_image);
	
	
		//print_r(get_post_meta($job_id));
		
       echo $attachment_id = processImage($job_id,$featured_image);
			 if(!empty($attachment_id)){
				         set_post_thumbnail( $job_id, $attachment_id ); 
	          }
	
	//}
	$increment = $increment +1;
//die('df');
}
//echo json_encode($newArray);
?>