<pre>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');

require_once 'lib/loadWP.php';
require_once 'lib/config.php';

$error = @$_GET['error'];
$error_description = @$_GET['error_description'];

$code = @$_GET['code'];
$state = @$_GET['state'];

if (!empty($error)) {
    die("Error: $error_description");
}
if (!empty($code) && $state == $stateToMatch) {
    $data = array(
        "grant_type" => "authorization_code",
        "code" => $code,
        "client_id" => $client_id,
        "client_secret" => $client_secret,
        "redirect_uri" => $redirect_uri,
    );
    
    $query = (http_build_query ($data));
//    echo "\r\n";
//    var_dump($query);	
//    echo "\r\n";
    $post_url = "https://www.linkedin.com/oauth/v2/accessToken?$query";
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    curl_close($ch);
    
    $returnval = json_decode($return);    
//    echo "\r\n";
//    var_dump($return);	
//    echo "\r\n";
//    var_dump($returnval);	
//    echo "\r\n";
    
    if (isset($returnval->error)) {
        die ($returnval->error_description);
    }
    elseif (isset($returnval->access_token)) {
        
        $access_token = $returnval->access_token;
        $access_life_remains = 0;
        if (isset($returnval->expires_in)) {
            $access_life_remains = $returnval->expires_in;
        }

        $next_renewal_timestamp = strtotime($access_life_remains ." seconds");
        $next_renewal = date("Y-m-d", $next_renewal_timestamp);
        
        $next_alert_timestamp = $next_renewal_timestamp - (24*60*60*5);
        $next_alert = date("Y-m-d", $next_alert_timestamp);
        
        $now_timestamp = strtotime("now");
        $now = date("Y-m-d H:i:s", $now_timestamp);
        
        update_option("linkedin_access_token", $access_token);
        update_option("linkedin_token_refreshed_date", $now);
        update_option("linkedin_token_refreshed_timestamp", $now_timestamp);
        
        update_option("linkedin_token_refresh_alert_date", $next_alert);
        update_option("linkedin_token_refresh_alert_timestamp", $next_alert_timestamp);
        update_option("linkedin_token_refresh_end_date", $next_renewal);
        update_option("linkedin_token_refresh_end_timestamp", $next_renewal_timestamp);
        
//        echo "\r\n access_token:";
//        var_dump($access_token);	
//        echo "\r\n";
//        echo "\r\n next_alert:";
//        var_dump($next_alert);	
//        echo "\r\n";
//        echo "\r\n next_renewal:";
//        var_dump($next_renewal);	
//        echo "\r\n";
        
        die ("Access Token saved for 60 days.");
        //die ("Access Token saved for 60 days: $access_token");
    }
    else {
        die ("Error! No access Token.");
    }
    
    
}

?>
</pre>