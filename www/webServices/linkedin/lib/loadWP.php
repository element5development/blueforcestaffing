<?php

//error_reporting(E_ALL);
//ini_set('display_errors','On');

$stateToMatch = "574e00ee77b94";

    
$path  = '../../'; // It should be end with a trailing slash
if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}


// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;


function get_my_option($option, $default = "") {
    global $wpdb;
    $value = $default;
    $suppress = $wpdb->suppress_errors();
    $options = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name = '$option' ORDER BY option_id DESC" );
    $wpdb->suppress_errors($suppress);
    
    if (!empty($options) && isset($options[0])) {
        $searchResult = $options[0];
        $value = $searchResult->option_value;
        $value = maybe_unserialize($value);        
    }
    
    return $value;    
}
