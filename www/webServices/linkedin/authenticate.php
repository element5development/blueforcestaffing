<?php

require_once 'lib/loadWP.php';
require_once 'lib/config.php';

$response_type = "code";
$state = $stateToMatch;//uniqid();

$authUrl = "https://www.linkedin.com/oauth/v2/authorization?response_type=$response_type&client_id=$client_id&redirect_uri=$redirect_uri&state=$state";

header("Location: $authUrl");
die();