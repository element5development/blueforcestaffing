<?php

if (!function_exists('job_orientation_daily_date_update_for_asap')) {
    function job_orientation_daily_date_update_for_asap() {
    //    global $wpdb;
        $ID = null;
        $job = new WP_Query( array( 'post_type' => 'job_listing', 
                            'meta_key' => '_orientation_date_asap', 
                            'meta_value' => 1) 
            );
        $posts = $job->posts;
        $post = null;
        if (!empty($posts)) {
            foreach($posts as $post) {
                $ID = $post->ID;        
                $nextDay = date("Y-m-d", strtotime("+1 day"));
                update_post_meta($ID, "_application_deadline", $nextDay);
            }
        }
    }    
}

if (!function_exists('get_my_option')) {
    function get_my_option($option, $default = "") {
        global $wpdb;
        $value = $default;
        $suppress = $wpdb->suppress_errors();
        $options = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name = '$option' ORDER BY option_id DESC" );
        $wpdb->suppress_errors($suppress);

        if (!empty($options) && isset($options[0])) {
            $searchResult = $options[0];
            $value = $searchResult->option_value;
            $value = maybe_unserialize($value);        
        }

        return $value;    
    }    
}
function wpws_requires_linkedin_token_refresh_email_urgent() {    
    $end_on = get_my_option("linkedin_token_refresh_end_date");
    $end_on_timestamp = intval(get_my_option("linkedin_token_refresh_end_timestamp"));    
    $now = strtotime("now");    
    return $now >= $end_on_timestamp;
}

function wpws_requires_linkedin_token_refresh_email() {
    $last_refreshed_on = get_my_option("linkedin_token_refreshed_date");
    $last_refreshed_on_timestamp = intval(get_my_option("linkedin_token_refreshed_timestamp"));

    $refresh_alert_on = get_my_option("linkedin_token_refresh_alert_date");
    $refresh_alert_on_timestamp = intval(get_my_option("linkedin_token_refresh_alert_timestamp"));
    
    $now = strtotime("now");
    
    return empty($last_refreshed_on) || $now > $refresh_alert_on_timestamp;
            
}
function wpws_check_linkedin_token_validity() {
    $token = get_my_option("linkedin_access_token");       

    if (empty($token)) {
        wpws_send_linkedin_no_token_email();
    }
    else {
        if (wpws_requires_linkedin_token_refresh_email_urgent()) {
            wpws_send_linkedin_token_refresh_email_urgent();
        }
        elseif (wpws_requires_linkedin_token_refresh_email()) {
            wpws_send_linkedin_token_refresh_email();
        }
    }
}

function wpws_send_linkedin_token_email($message, $subject = "Action Required: Authenticate Linkedin to post automatically from your website") {
    $site = get_my_option("blogname");
    $to   = get_my_option("admin_email");//"e5devmail@gmail.com";//
    $from = get_my_option("admin_email");
    $siteurl = get_my_option("siteurl");   
    $authUrl = $siteurl . '/webServices/linkedin/authenticate.php';    
    $message .=  "\r\n Open $siteurl/wp-admin/ in a Web Browser and log in. 
Next, in another tab of the same browser, log into the Scott Cassidy LinkedIn account and navigate to it's Business Profile.
Lastly, in a 3rd tab of the same browser, follow the link below: \r\n $authUrl";
	if (!function_exists('is_wpe_snapshot') || !is_wpe_snapshot()) {
		wp_mail($to, $subject, $message);    
	}    
}

function wpws_send_linkedin_no_token_email() {
    $content = "You need to authorize and allow your website to post Ads in your LinkedIn page.
";
    wpws_send_linkedin_token_email($content);
}
function wpws_send_linkedin_token_refresh_email_urgent() {
    $end_on = get_my_option("linkedin_token_refresh_end_date");
    $end_on_timestamp = intval(get_my_option("linkedin_token_refresh_end_timestamp"));    
    $end_date = date("m/d/Y", $end_on_timestamp);    
    $content = "Your website will no longer be able to post in LinkedIn page automatically. Token has expired on $end_date.
";
    $subject = "[URGENT: Action Required] LinkedIn Authorization Token has expired";
    wpws_send_linkedin_token_email($content, $subject);
}
function wpws_send_linkedin_token_refresh_email() {
    $end_on = get_my_option("linkedin_token_refresh_end_date");
    $end_on_timestamp = intval(get_my_option("linkedin_token_refresh_end_timestamp"));    
    $end_date = date("m/d/Y", $end_on_timestamp);
    
    $content = "Your Websites's Authorization token for LinkedIn is about to expire on $end_date. 
Your website will no longer be able to post Ads in LinkedIn page automatically after the token expires.
";
    $subject = "[Action Required] LinkedIn Authorization Token is about to expire";
    wpws_send_linkedin_token_email($content, $subject);    
}

    // Must run at 9 AM EST = 1 PM GMT (what is shown in admin Cronjob scheduler page) = 6:30 PM IST
add_action('webservice_hook_daily', 'wpws_check_linkedin_token_validity');

function wpws_requires_facebook_token_refresh_email_urgent() {    
    $end_on = get_my_option("facebook_token_refresh_end_date");
    $end_on_timestamp = intval(get_my_option("facebook_token_refresh_end_timestamp"));    
    $now = strtotime("now");    
    return $now >= $end_on_timestamp;
}

function wpws_requires_facebook_token_refresh_email() {
    $last_refreshed_on = get_my_option("facebook_token_refreshed_date");
    $last_refreshed_on_timestamp = intval(get_my_option("facebook_token_refreshed_timestamp"));

    $refresh_alert_on = get_my_option("facebook_token_refresh_alert_date");
    $refresh_alert_on_timestamp = intval(get_my_option("facebook_token_refresh_alert_timestamp"));
    
    $now = strtotime("now");
    
    return empty($last_refreshed_on) || $now > $refresh_alert_on_timestamp;
            
}
function wpws_check_facebook_token_validity() {
    $token = get_my_option("facebook_access_token");       

    if (empty($token)) {
        wpws_send_facebook_no_token_email();
    }
    else {
        if (wpws_requires_facebook_token_refresh_email_urgent()) {
            wpws_send_facebook_token_refresh_email_urgent();
        }
        elseif (wpws_requires_facebook_token_refresh_email()) {
            wpws_send_facebook_token_refresh_email();
        }
    }
}

function wpws_send_facebook_token_email($message, $subject = "Action Required: Authenticate Facebook to post automatically from your website") {
    $site = get_my_option("blogname");
    $to   = get_my_option("admin_email");//"e5devmail@gmail.com";//
    $from = get_my_option("admin_email");
    $siteurl = get_my_option("siteurl");   
    $authUrl = $siteurl . '/webServices/facebook/authenticate.php';    
    $message .=  "\r\n Open $siteurl/wp-admin/ in a Web Browser and log in. 
Next, in another tab of the same browser, log into the https://www.facebook.com/blueforcehealth/ account. 
Lastly, in a 3rd tab of the same browser, follow the link below:  \r\n $authUrl";
	
	if (!function_exists('is_wpe_snapshot') || !is_wpe_snapshot()) {
		wp_mail($to, $subject, $message);    
	}
    
}

function wpws_send_facebook_no_token_email() {
    $content = "You need to authorize and allow your website to post Ads in your Facebook page.
";
    wpws_send_facebook_token_email($content);
}
function wpws_send_facebook_token_refresh_email_urgent() {
    $end_on = get_my_option("facebook_token_refresh_end_date");
    $end_on_timestamp = intval(get_my_option("facebook_token_refresh_end_timestamp"));    
    $end_date = date("m/d/Y", $end_on_timestamp);    
    $content = "Your website will no longer be able to post in Facebook page automatically. Token has expired on $end_date.
";
    $subject = "[URGENT: Action Required] Facebook Authorization Token has expired";
    wpws_send_facebook_token_email($content, $subject);
}
function wpws_send_facebook_token_refresh_email() {
    $end_on = get_my_option("facebook_token_refresh_end_date");
    $end_on_timestamp = intval(get_my_option("facebook_token_refresh_end_timestamp"));    
    $end_date = date("m/d/Y", $end_on_timestamp);
    
    $content = "Your Websites's Authorization token for Facebook is about to expire on $end_date. 
Your website will no longer be able to post Ads in Facebook page automatically after the token expires.
";
    $subject = "[Action Required] Facebook Authorization Token is about to expire";
    wpws_send_facebook_token_email($content, $subject);    
}

add_action('webservice_hook_daily', 'wpws_check_facebook_token_validity');


add_action('admin_menu', 'add_global_custom_options');
function add_global_custom_options()
{
    add_options_page('Global Options', 'Global Options', 'manage_options', 'global_custom_options','global_custom_options');
}


function global_custom_options()
{
?>
    <div class="wrap">
        <h2>Global Options</h2>
        <form method="post" action="options.php">
            <?php wp_nonce_field('update-options') ?>
            <p><strong>Max Number of Social Media Ads to Post Per Day:</strong> <small>(0 for no limit)</small><br />
                <input type="text" name="_posting_limit_" size="45" value="<?php echo get_option('_posting_limit_', 0); ?>" />
            </p>

            <p><strong>New Location alert Emails:</strong> <small>add comma seperated email ids</small><br />
                <input type="text" name="_location_alert_emails_" size="45" value="<?php echo get_option('_location_alert_emails_', get_option('admin_email')); ?>" />
            </p>
            <p><input type="submit" name="Submit" value="Store Options" /></p>
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="_posting_limit_,_location_alert_emails_" />
        </form>
    </div>
<?php
}
?>

