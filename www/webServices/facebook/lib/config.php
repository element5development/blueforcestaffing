<?php
global $fb_args, $fb_page_id;
if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
    // Staging
    $fb_args = array(
        'app_id' => '1170410469696906',
        'app_secret' => 'ad3a6518ee98b6ffde908c81855eefbc',
        'default_graph_version' => 'v2.5',
    );
    $fb_client_token = "19e72b4c71d4710e957413493086a8dd";
    $fb_redirect_uri = "http://e5blueforce.staging.wpengine.com/webServices/facebook/authredirect.php";
    $fb_page_id = "1610808949230248";
}
else {
    // LIVE 
    $fb_args = array(
        'app_id' => '735497823219616',
        'app_secret' => '285a509b3fe56efd38aba3e0f7c6d69b',
        'default_graph_version' => 'v2.5',
    );
    $fb_client_token = "9feb8979f1c85333f10ff29e203b2042";
    $fb_redirect_uri = "http://www.blueforcestaffing.com/webServices/facebook/authredirect.php";
    $fb_page_id = "933826073362902";
}

