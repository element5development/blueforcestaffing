<pre>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');
session_start();

require_once 'lib/loadWP.php';
require_once 'lib/config.php';
require_once 'lib/Facebook/autoload.php';



$fb = new Facebook\Facebook($fb_args);
$helper = $fb->getRedirectLoginHelper();
if (isset($_GET['state'])) {
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
}
try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (isset($accessToken)) {
    // Logged in!
    $_SESSION['facebook_access_token'] = (string) $accessToken;
    // Now you can redirect to another page and use the
    // access token from $_SESSION['facebook_access_token']
  
    // OAuth 2.0 client handler
    $oAuth2Client = $fb->getOAuth2Client();

    // Exchanges a short-lived access token for a long-lived one
    $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);  
//    
//    echo "\r\n";
//    var_dump($accessToken);	
//    echo "\r\n";    
//    var_dump($longLivedAccessToken);
    
    if (isset($longLivedAccessToken)) {
        
        $access_token = $longLivedAccessToken->getValue();
        $next_renewal_datetime = $longLivedAccessToken->getExpiresAt();
    
        if(($next_renewal_datetime instanceof DateTime)){
            $next_renewal = $next_renewal_datetime->format('Y-m-d H:i:s');     
        }
        else{
            $next_renewal = date('Y-m-d H:i:s');
        }
        
        $next_renewal_timestamp = strtotime($next_renewal);
        $next_alert_timestamp = strtotime("-5 days", $next_renewal_timestamp);
        $next_alert = date("Y-m-d", $next_alert_timestamp);
        $now_timestamp = strtotime("now");
        $now = date("Y-m-d H:i:s", $now_timestamp);
        
        update_option("facebook_access_token", $access_token);
        update_option("facebook_token_refreshed_date", $now);
        update_option("facebook_token_refreshed_timestamp", $now_timestamp);
        
        update_option("facebook_token_refresh_alert_date", $next_alert);
        update_option("facebook_token_refresh_alert_timestamp", $next_alert_timestamp);
        update_option("facebook_token_refresh_end_date", $next_renewal);
        update_option("facebook_token_refresh_end_timestamp", $next_renewal_timestamp);
        
//        echo "\r\n access_token:";
//        var_dump($access_token);	
//        echo "\r\n";
//        echo "\r\n next_alert:";
//        var_dump($next_alert);	
//        echo "\r\n";
//        echo "\r\n next_renewal:";
//        var_dump($next_renewal);	
//        echo "\r\n";
        
        die ("Access Token saved for 60 days.");
        //die ("Access Token saved for 60 days: $access_token");
    }
    else {
        die ("Error! No access Token.");
    }    
  
}

?>
</pre>