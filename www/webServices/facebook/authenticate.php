<?php
die(); // Have set a non expiring token - hence no more re-auth is required
session_start();
require_once 'lib/loadWP.php';
require_once 'lib/config.php';
require_once 'lib/Facebook/autoload.php'; 


$fb = new Facebook\Facebook($fb_args);
$helper = $fb->getRedirectLoginHelper();


$scope = array('publish_pages','manage_pages');
$permissions = $scope;//['email', 'user_likes']; // optional

$loginUrl = $helper->getLoginUrl($fb_redirect_uri, $permissions);

header("Location: $loginUrl");
die();