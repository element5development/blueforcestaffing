<pre style="background:#eee;">
<?php
/*  TODO
 *  1. Dynamically get Url for Options/Company/Location Google Sheets (from some setting in admin)
 *  2. Dynamically get upload path to load image from (from some setting in admin) - WP uploads dir if nothing is set
 *  3. Dynamically get Social Networking site's API credentials from soem settings in admin
 *  4. Need a admin UI way to reset or remove the options created by this code like job_gs_ids etc.
 * 
 */
    error_reporting(E_ALL);
    ini_set('display_errors','On');
    set_time_limit(0);
    error_log("[Google2WP Jobs] Starting Update of WP Jobs from Google Docs", 0);
    
    $startTime = date("Y-m-d H:i:s");
    $startTimeStamp = strtotime($startTime);
    logit("************** Start Execution ..." . $startTime . "*************\r\n\r\n");

$path  = '../'; // It should be end with a trailing slash  

// Maps Google Spreadsheet column with job post meta key/taxonomy terms or actions
$metaMap = array(    
    "PPC" => array("key" => "_job_ppc", "sanitizer" => "setbool",   "type" => "option", "option" => "publish_job"),
    "Ad Builder" => array("key" => "", "sanitizer" => "setbool",  "type" => "option", "option" => "publish_ad"),    
    "Force Ad Rebuild" => array("key" => "", "sanitizer" => "setbool",  "type" => "option", "option" => "force_publish_ad"),    
    "Timestamp" => array("sanitizer" => "trim", "type" => "option", "option" => "timestamp"),    
    
    "ID" => array("key" => "googlesheet_openorders_id", "sanitizer" => "trim"),
    "Job Title" => array("key" => "_job_title", "sanitizer" => "trim", "post_param" => "post_title"),
    "Ad Title" => array("key" => "ad_title", "sanitizer" => "trim"),
    "Ad Descriptions" => array("key" => "ad_descriptions", "sanitizer" => "trim"),
    "Job Description" => array("key" => "_job_description", "sanitizer" => "trim", "post_param" => "post_content"),
    "Requirements" => array("key" => "_job_requirements", "sanitizer" => "trim"),
    "Spotlight" => array("key" => "_featured", "sanitizer" => "setbool"),
    "Job Excerpt" => array("key" => "_excerpt", "sanitizer" => "trim"),
    //"Orientation" => array("key" => "_job_deadline", "sanitizer" => "trim,formatDate_Ymd"),
    "Orientation" => array("key" => "_application_deadline", "sanitizer" => "trim,formatDate_Ymd"),
    
    "Company ID" => array("key" => "googlesheet_company_id", "sanitizer" => "trim"),
    "Company Name" => array("key" => "_company_name", "sanitizer" => "trim", "source" => "company"),
    "Website" => array("key" => "_company_website", "sanitizer" => "trim", "source" => "company"),
    
    "Location ID" => array("key" => "googlesheet_location_id", "sanitizer" => "trim"),
    "Facility Name" => array("key" => "_facility", "sanitizer" => "trim", "source" => "location"),
    "Address" => array("key" => "_address", "sanitizer" => "trim", "source" => "location"),
    "City" => array("key" => "_city", "sanitizer" => "trim", "source" => "location"),
    "State" => array("key" => "_state", "sanitizer" => "trim", "source" => "location"),
    "Zip" => array("key" => "_zip_code", "sanitizer" => "trim", "source" => "location"),
    
    "Hourly Guarantee" => array("key" => "_hours", "sanitizer" => "trim,floatval"),
    "Contract Length" => array("key" => "_job_contract_length", "sanitizer" => "trim,floatval"),    
    "Hourly Max" => array("key" => "_hourly_max", "sanitizer" => "currency_clean,floatval"),
    "Weekly Gross" => array("key" => "_job_weekly_gross", "sanitizer" => "currency_clean,floatval"),
    "Hourly" => array("key" => "_hourly", "sanitizer" => "currency_clean,floatval"),    
    
    "Specialty" => array("key" => "_job_category", "sanitizer" => "splitcsv,specialty_map",   "type" => "term", "tax" => "job_listing_category", "createNewTerm" => true),
    "Shift" => array("key" => "_job_tags", "sanitizer" => "splitcsv,shift_map",         "type" => "term", "tax" => "job_listing_tag"),     
    
    "Travel Stipend" => array("key" => "_job_type_travel", "sanitizer" => "currency_clean,floatval,amenities_travel_map",    "type" => "term", "tax" => "job_listing_type", "term" => "Travel", 'meta_value_sanitize' => 'currency_clean,floatval'),
    "M&I" => array("key" => "_job_type_meals-and-incidentals", "sanitizer" => "currency_clean,floatval,amenities_mm_map", "type" => "term", "tax" => "job_listing_type", "term" => "Meals &amp; Incidentals", 'meta_value_sanitize' => 'currency_clean,floatval'),
    "Housing Stipend" => array("key" => "_job_type_housing-stipend", "sanitizer" => "currency_clean,floatval,amenities_housing_map",   "type" => "term", "tax" => "job_listing_type", "term" => "Housing Stipend", 'meta_value_sanitize' => 'currency_clean,floatval'),
    
    "job_region" => array("key" => "_job_region", "sanitizer" => "trim", "source" => "location", "type" => "action", "tax" => "job_listing_region", "actionTerm" => true, "action" => "detectRegions", "fields" => "State"),
    "Compact State?" => array("key" => "_compact_state", "sanitizer" => "trim", "type" => "action", "action" => "detectCompactState", "fields" => "State" ),
    "Job Location" => array("key" => "_job_location", "type" => "action",  "sanitizer" => "trim", "source" => "location", "fields"=> "City,State,Zip", "action" => "joinFields", "param" => ", \n"),

    "Logo URL" => array("key" => "company_logo", "sanitizer" => "trim", "source" => "company"),        
    "Image(s)" => array("key" => "location_image_path", "sanitizer" => "randomCSV", "source" => "location"),
    "location_image_url" => array("key" => "location_image_url", "source" => "location","type" => "action", "action" => "setLocationImageUrl"),
    "Job Image" => array("key" => "_company_logo", "type" => "action", "source" => "location", "action" => "setJobImage"),
    "Orientation Date ASAP" => array("key" => "_orientation_date_asap", "type" => "action", "action" => "setASAPDeadline"),
    
//    "Image(s)" => array("key" => "location_image", "type" => "action", "sanitizer" => "randomCSV", "source" => "location", "action" => "setFeaturedImage"),
//    "Logo URL"      => array("key" => "_company_logo", "sanitizer" => "trim,appendUploadUrl", "source" => "company", "action" => "setCompanyLogo"),        
 
    ////"_apply_link" => array("key" => "_apply_link", "sanitizer" => "trim"),
    ////"ad_created" => array("key" => "ad_created", "sanitizer" => "trim"),
    
    //"Notes" => array("key" => "", "sanitizer" => "trim"),
    //"Bonus" => array("key" => "", "sanitizer" => "trim"),
    //"job_updated" => array("key" => "", "sanitizer" => "trim"),
    //"company_website" => array("key" => "", "sanitizer" => "trim"),
    //"facebook_page" => array("key" => "", "sanitizer" => "trim"),
    //"twitter_page" => array("key" => "", "sanitizer" => "trim"),
    //"travel_image" => array("key" => "", "sanitizer" => "trim"),
    //"job_type" => array("key" => "", "sanitizer" => "trim"),        
    
);



/** That's all, stop editing from here **/

if ( !defined('WP_LOAD_PATH') ) {

	/** classic root path if wp-content and plugins is below wp-config.php */
	 $classic_root = dirname(dirname(__FILE__)) . '/' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
global $wpdb;


$regionTerms = getTaxonomyTerms('job_listing_region');
$stateRegionMap = orderRegionTermsByState($regionTerms);
$compactStatesTermId = getCompactStateParentId($regionTerms);

$shiftTermsRaw = getTaxonomyTerms('job_listing_tag');
$shiftTermsByName = listTermsBy($shiftTermsRaw);
$amenitiesTermsRaw = getTaxonomyTerms('job_listing_type');
$amenitiesTermsByName = listTermsBy($amenitiesTermsRaw);

$specialtyTermsRaw = getTaxonomyTerms('job_listing_category');
$specialtyTermsByName = listTermsBy($specialtyTermsRaw);
            
$getadjobtable = $wpdb->prefix.'job_ad';

//header('Content-type: application/json');

if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) {
    // staging
    $openOrdersFeedUrl = 'https://docs.google.com/spreadsheets/d/1gO1rfe5otYTCJM0uqlPv3-wFZBDYxbKTQwKXAtRydo4/pub?gid=283842081&single=true&output=csv'; // "Open Orders"
    //$openOrdersFeedUrl = 'https://docs.google.com/spreadsheets/d/1gO1rfe5otYTCJM0uqlPv3-wFZBDYxbKTQwKXAtRydo4/pub?gid=1316702949&single=true&output=csv';// "Open Orders Test"
    $companyFeedUrl = 'https://docs.google.com/spreadsheets/d/1gO1rfe5otYTCJM0uqlPv3-wFZBDYxbKTQwKXAtRydo4/pub?gid=1497752793&single=true&output=csv';
    $locationsFeedUrl = 'https://docs.google.com/spreadsheets/d/1gO1rfe5otYTCJM0uqlPv3-wFZBDYxbKTQwKXAtRydo4/pub?gid=2140883496&single=true&output=csv';   
//    $openOrdersFeedUrl = 'https://docs.google.com/spreadsheets/d/1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM/pub?gid=410737819&single=true&output=csv'; 
//    $companyFeedUrl = 'https://docs.google.com/spreadsheets/d/1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM/pub?gid=1119417421&single=true&output=csv';
//    $locationsFeedUrl = 'https://docs.google.com/spreadsheets/d/1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM/pub?gid=1688839843&single=true&output=csv';
}
else {
    //LIVE
    $openOrdersFeedUrl = 'https://docs.google.com/spreadsheets/d/1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM/pub?gid=410737819&single=true&output=csv'; 
    $companyFeedUrl = 'https://docs.google.com/spreadsheets/d/1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM/pub?gid=1119417421&single=true&output=csv';
    $locationsFeedUrl = 'https://docs.google.com/spreadsheets/d/1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM/pub?gid=1688839843&single=true&output=csv';
    
}


$openOrdersData = csvToArray($openOrdersFeedUrl, ',');
if ($openOrdersData !== false) {
	logit( "\r\nConnected to Google Sheet\r\n");
	error_log("[Google2WP Jobs] Connected to Google Sheet", 0);
	$companyData = csvToArray($companyFeedUrl, ',', 'Company ID');
	if($companyData === false) {
		error_log("[Google2WP Jobs] Failed to connect Company to Google Sheet...! Quiting process", 0);
		logit( "\r\nFailed to connect to Company Google Sheet...! Quiting process\r\n");
	} 

	$locationsData = csvToArray($locationsFeedUrl, ',', 'Location');
	if($locationsData === false) {
		error_log("[Google2WP Jobs] Failed to connect to Location Google Sheet...! Quiting process", 0);
		logit( "\r\nFailed to connect to Location Google Sheet...! Quiting process\r\n");
	}

	if($companyData !== false || $locationsData !== false)
	{
		processOpenOrders($metaMap, $openOrdersData, $companyData, $locationsData);
		check_new_locations($locationsData);
	}
}
else {
	error_log("[Google2WP Jobs] Failed to connect to Google Sheet...! Quiting process", 0);
	logit( "\r\nFailed to connect to Google Sheet...! Quiting process\r\n");
}

$endTime = date("Y-m-d H:i:s");
$endTimeStamp = strtotime($endTime);
$timegap = $endTimeStamp - $startTimeStamp;
logit( "\r\n****************** End Execution ****************************" . $endTime . "\r\n");
logit( "\r\nTotal time taken: " . ($timegap) . " seconds.\r\n");
error_log("[Google2WP Jobs] Ending Update of WP Jobs from Google Docs", 0);




die();

foreach($openOrdersData as $openOrdersRow){
    
    

	$companyName = $dataArray['Client']; // company name
	$Facility = $dataArray['Facility']; //location
	$City = $dataArray['City']; //geolocation_city cusatom filed c
	$State = $dataArray['State']; //geolocation_state_short filed D
	$Specialty = $dataArray['Specialty']; //custom filed E
	$Orientation = $dataArray['Orientation']; // custom filed
	$Shift = $dataArray['Shift']; //// custom filed
	$Guarantee = $dataArray['Guarantee']; // guramtee I custom field
	$ContractLength = $dataArray['Contract Length']; // comumn j contract length custom field
	$PPC = $dataArray['PPC'];// custom filed
	$HourlyMax = $dataArray['Hourly Max']; //rate per hour_max
	$HourlyMax = str_replace("$","",$HourlyMax);
	$HourlyMax = trim($HourlyMax);
	$WeeklyGross = $dataArray['Weekly Gross']; // custom filed
	$WeeklyGross = str_replace("$","",$WeeklyGross);
	$WeeklyGross = str_replace(",","",$WeeklyGross);
	$WeeklyGross = trim($WeeklyGross);
	if(empty($WeeklyGross)){
		$WeeklyGross = 0;
	}
	$TravelStipend = $dataArray['Travel Stipend']; // custom filed
	$TravelStipend = str_replace("$","",$TravelStipend);
	$TravelStipend = trim($TravelStipend);
	$Notes = $dataArray['Notes'];
	$Bonus = $dataArray['Bonus']; // bonus p custom field
	$CompactState = $dataArray['Compact State?']; // Column R custom field
	$HousingStipend = $dataArray['Housing Stipend']; // housing sdtpend column ???U 
	$HousingStipend = str_replace("$","",$HousingStipend);
	$HousingStipend = trim($HousingStipend);
	$jobId = $dataArray['id'];
	$adbuilder = $dataArray['Ad Builder'];
	
	
	//$job_title = $companyName. ' job';
	
	$job_title = $dataArray['Job_title'];
	if(empty($job_title)){
		$job_title = $companyName. ' job';
	}
	$job_description = $dataArray['job_description'];
	$job_region = $dataArray['job_region'];
	$job_updated = $dataArray['job_updated'];
	$zipcode = $dataArray['zipcode'];
	$company_website = $dataArray['company_website'];
	$facebook_page = $dataArray['facebook_page'];
	$twitter_page = $dataArray['twitter_page'];
	$featured_image = $dataArray['featured_image'];
	$travel_image = $dataArray['travel_image'];
	$job_type = $dataArray['job_type'];
	$job_typesss= explode(',',$dataArray['job_type'] );	
if(!empty($Facility) && !empty($companyName)) {
	$getjob = new WP_Query( array( 'post_type' => 'job_listing', 'meta_key' => 'jobId', 'meta_value' => $jobId) );
	$getpost = $getjob->posts;
	//print_r($getpost);
	echo '**********************';
	echo $postid = $getpost->ID;
	
    if(!empty($postid )) {
		echo 'if';
		echo $adbuilder;
		if($adbuilder == 'Yes'){
		   $getjobrow = $wpdb->get_results("select * from $getadjobtable where job_id = $postid");
		   if(empty($getjobrow)){
			 $wpdb->query("INSERT INTO $getadjobtable(job_id, status) VALUES ($postid,'1')");   
		   }
		   else{
			 //$wpdb->query("update $getadjobtable set status = 1 where job_id = '$postid'");	  
		   }
		}
		if($adbuilder == 'No'){
	         $wpdb->query("update $getadjobtable set status = 0 where job_id = '$postid'");	  
		}
		
					
					if($job_updated == 'yes') {
					   $post = array(
									   "ID" => $postid,
									   "post_type" => 'job_listing',
									   "post_title" => $job_title,
									   "post_content" => $job_description,
									   "post_status" =>"publish",
									   "post_author" => 0,				
								   );					
					   $job_id = wp_update_post( $post );	
					  
					   $term = term_exists($Specialty, 'job_listing_category');
			
			   if ($term !== 0 && $term !== null) {
				  $termid = $term['term_id']; 	  
				  wp_set_object_terms( $job_id, $Specialty, 'job_listing_category');       
				}
			 else{
				 $speciality_slug = strtolower($Specialty);
				 $speciality_slug =str_replace(' ', '-', $speciality_slug);
				$termss = wp_insert_term(
				  $Specialty, 
				  'job_listing_category', 
				  array(
					'description'=> '',
					'slug' => $speciality_slug,
					'parent'=> 0
				   )
				 ); 
				 
				 wp_set_object_terms( $job_id, $speciality_slug, 'job_listing_category', true);     
			 }
			 
			 if(count($job_types) > 1) {
			   foreach($job_types as $jtype){
					 $jtype = trim($jtype);
					$term = get_term_by('slug', $jtype, 'job_listing_type');	
					 if ($term !== 0 && $term !== null) {
						 $termid = $term->term_id; 		   
							wp_set_object_terms( $job_id, $termid, 'job_listing_type', true);       
					}
				}
			 }
			 else{
			
				 $job_type = trim($dataArray['job_type']);
				 //$term = term_exists($job_type, 'job_listing_type');
				 $term = get_term_by('slug', $job_type, 'job_listing_type');	
					 if ($term !== 0 && $term !== null) {
						$termid = $term->term_id; 		          
						wp_set_object_terms( $job_id, $termid, 'job_listing_type');          
					} 
					
				 
			 }
			  
				update_post_meta($job_id, '_company_name', $companyName);
			  
				update_post_meta($job_id, '_facility', $Facility);                
				//update_post_meta($job_id, '_job_location', $Facility);
			   
				update_post_meta($job_id, '_rate_max', $HourlyMax);
				//update_post_meta($job_id, 'Orientation', $Orientation);
				update_post_meta($job_id, '_job_deadline', $Orientation);
				update_post_meta($job_id, '_city', $City);
				//update_post_meta($job_id, 'geolocation_city', $City);
				update_post_meta($job_id, '_state', $State);
				//update_post_meta($job_id, 'geolocation_state_long', $State);
				
				update_post_meta($job_id, 'Shift', $Shift);
				update_post_meta($job_id, 'Guarantee', $Guarantee);
				update_post_meta($job_id, 'ContractLength', $ContractLength);
				
				 update_post_meta($job_id, 'PPC', $PPC);
				update_post_meta($job_id, 'WeeklyGross', $WeeklyGross);
				update_post_meta($job_id, 'TravelStipend', $TravelStipend);
				
				update_post_meta($job_id, 'Notes', $Notes);
				update_post_meta($job_id, 'Bonus', $Bonus);
				update_post_meta($job_id, 'CompactState', $CompactState);
				
				update_post_meta($job_id, 'HousingStipend', $HousingStipend);
				update_post_meta($job_id, 'jobId', $jobId);
				
				update_post_meta($job_id, 'geolocation_country_short', 'US');
				update_post_meta($job_id, 'geolocation_country_long', 'United States');
				//update_post_meta($job_id, 'Specialty', $Specialty);
				update_post_meta($job_id, '_job_category', $Specialty);
				
				
				update_post_meta($job_id, '_company_website', $company_website);
				update_post_meta($job_id, '_company_twitter', $twitter_page);
				update_post_meta($job_id, 'facebook_link', $facebook_page);
				
				update_post_meta($job_id, 'job_region', $job_region);
				update_post_meta($job_id, 'job_zipcode', $zipcode);
				update_post_meta($job_id, 'travel_image', $travel_image);
				
				
					//print_r(get_post_meta($job_id));
					
				   $attachment_id = processImage($job_id,$featured_image);
						 if(!empty($attachment_id)){
									 set_post_thumbnail( $job_id, $attachment_id ); 
						  }
					   
					   
					}
				}
	else{
		echo 'else';
			$post = array(
				   "post_type" => 'job_listing',
				   "post_title" => $job_title,
				   "post_content" => $job_description,
				   "post_status" =>"publish",
				   "post_author" => 0,				
			   );					
			$job_id = wp_insert_post( $post );	
			echo $adbuilder; 
			
			if($adbuilder == 'Yes'){
				 $wpdb->query("INSERT INTO $getadjobtable(job_id, status) VALUES ($job_id,'1')");  
			}
			
			$term = term_exists($Specialty, 'job_listing_category');
			
			if ($term !== 0 && $term !== null) {
			$termid = $term['term_id']; 	  
			wp_set_object_terms( $job_id, $Specialty, 'job_listing_category');       
			}
			else{
			$speciality_slug = strtolower($Specialty);
			$speciality_slug =str_replace(' ', '-', $speciality_slug);
			$termss = wp_insert_term(
			$Specialty, 
			'job_listing_category', 
			array(
			'description'=> '',
			'slug' => $speciality_slug,
			'parent'=> 0
			)
			); 
			
			wp_set_object_terms( $job_id, $speciality_slug, 'job_listing_category', true);     
			}
			
			if(count($job_types) > 1) {
			foreach($job_types as $jtype){
			$jtype = trim($jtype);
			$term = get_term_by('slug', $jtype, 'job_listing_type');	
			if ($term !== 0 && $term !== null) {
			$termid = $term->term_id; 		   
			wp_set_object_terms( $job_id, $termid, 'job_listing_type', true);       
			}
			}
			}
			else{
			
			$job_type = trim($dataArray['job_type']);
			//$term = term_exists($job_type, 'job_listing_type');
			$term = get_term_by('slug', $job_type, 'job_listing_type');	
			if ($term !== 0 && $term !== null) {
			$termid = $term->term_id; 		          
			wp_set_object_terms( $job_id, $termid, 'job_listing_type');          
			} 
			
			
			}
			
			update_post_meta($job_id, '_company_name', $companyName);
			
			update_post_meta($job_id, '_job_location', $Facility);
			
			update_post_meta($job_id, '_rate_max', $HourlyMax);
			update_post_meta($job_id, 'Orientation', $Orientation);
			update_post_meta($job_id, 'geolocation_city', $City);
			update_post_meta($job_id, 'geolocation_state_long', $State);
			
			update_post_meta($job_id, 'Shift', $Shift);
			update_post_meta($job_id, 'Guarantee', $Guarantee);
			update_post_meta($job_id, 'ContractLength', $ContractLength);
			
			update_post_meta($job_id, 'PPC', $PPC);
			update_post_meta($job_id, 'WeeklyGross', $WeeklyGross);
			update_post_meta($job_id, 'TravelStipend', $TravelStipend);
			
			update_post_meta($job_id, 'Notes', $Notes);
			update_post_meta($job_id, 'Bonus', $Bonus);
			update_post_meta($job_id, 'CompactState', $CompactState);
			
			update_post_meta($job_id, 'HousingStipend', $HousingStipend);
			update_post_meta($job_id, 'jobId', $jobId);
			
			update_post_meta($job_id, 'geolocation_country_short', 'US');
			update_post_meta($job_id, 'geolocation_country_long', 'United States');
			update_post_meta($job_id, 'Specialty', $Specialty);
			
			
			update_post_meta($job_id, '_company_website', $company_website);
			update_post_meta($job_id, '_company_twitter', $twitter_page);
			update_post_meta($job_id, 'facebook_link', $facebook_page);
			
			update_post_meta($job_id, 'job_region', $job_region);
			update_post_meta($job_id, 'job_zipcode', $zipcode);
			update_post_meta($job_id, 'travel_image', $travel_image);
			
			
			//print_r(get_post_meta($job_id));
			
			$attachment_id = processImage($job_id,$featured_image);
			if(!empty($attachment_id)){
					 set_post_thumbnail( $job_id, $attachment_id ); 
			}
			
		}	
       } 
	
}

logit(json_encode($newArray));
?>
<?php

function check_new_locations($locationsData) {
	$current_count = count($locationsData);
	$loc_count = get_option('__location_last_count', 0);

	logit( "\r\n Check new Location \r\n");
	logit( "\r\n Last Location count:  $loc_count \r\n");
	logit( "\r\n Current Location count: $current_count \r\n");

	if($current_count > $loc_count) {
		$new_listings = array_slice($locationsData, $loc_count);
		//var_dump(count($new_listings));
		logit( "\r\n Sending New Location: \r\n");
		logit($new_listings);
		send_mail_for_new_listings($new_listings);

		update_option('__location_last_count', $current_count);
	}
}


function update_post_metas($ID, $metas) {
    foreach($metas as $key=>$value) {
        update_post_meta($ID, $key, $value);
    }
}
function update_post_terms($ID, $taxTerms) {
    foreach($taxTerms as $taxname => $terms) {
        try {
            wp_set_object_terms($ID, $terms, $taxname);
        } catch (Exception $ex) {
        }
    }
}

function get_my_option($option, $default = "") {
    global $wpdb;
    $value = $default;
    $suppress = $wpdb->suppress_errors();
    $options = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name = '$option' ORDER BY option_id DESC" );
    $wpdb->suppress_errors($suppress);
    
    if (!empty($options) && isset($options[0])) {
        $searchResult = $options[0];
        $value = $searchResult->option_value;
        $value = maybe_unserialize($value);        
    }
    
    return $value;    
}
function getLastDataEntryTimeStamp() {
    return get_my_option('last_job_updated_googlesheet', '');
}
function getLastUpdatedTimeStamp() {
    return get_my_option('last_job_updated_wp', '');
}
function getRecordedGSIDs() {
	global $wpdb;
    $res = $wpdb->get_results(
    	$wpdb->prepare("SELECT meta_value, post_id
		FROM $wpdb->postmeta
		WHERE meta_key = '%s'", 'googlesheet_openorders_id'), ARRAY_A);
    $results = [];
    foreach ($res as $value) {
    	$results[$value['meta_value']] = $value['post_id'];
    }
    return $results;
    //return get_my_option('job_gs_ids', array());
}
function setLastDataEntryTimeStamp($val) {
    update_option('last_job_updated_googlesheet', $val);
}
function setLastUpdatedTimeStamp($val) {
    update_option('last_job_updated_wp', $val);
}
function setRecordedGSIDs($val) {
    update_option('job_gs_ids', $val);
}
function trashPosts($IDs) {
    foreach($IDs as $ID) {
        logit("Trash Post: '$ID'\r\n");
        if (!empty($ID)) {
            wp_trash_post( $ID  ); 
        }        
    }    
}

function processOpenOrders($map, $orders, $companies = array(), $locations = array()) {
    $last_dataentry = intval(getLastDataEntryTimeStamp());
    $last_updated = intval(getLastUpdatedTimeStamp());
    $nowTimestamp = intval(date('U'));
    $lastest_dataentry = $last_dataentry;
    
    
    $recordedGSIDs = getRecordedGSIDs();
    logit("JOB GS IDS:");
    logit(var_export($recordedGSIDs, true));
    logit("\r\n############################\r\n");


    /*$newArray = array();
	foreach ($orders as $order) {
	    $newArray[] = @$order['ID'];;
	}

	print_r(array_intersect(array_keys($recordedGSIDs), $newArray));
	die;*/




    $newGSIDs = array();
    $duplicateGSIDs = array();
    $locationNotFoundGSIDs = array();
    // $locationNotIDs = array();
    
    $orderCount = 0;
    $rowCount = 1;
    
    //begin function random doctor
    function random_doctor($num_template) {
        $rand = rand(1,8);

        if ($num_template == 1) {
            $name_doctor = $_SERVER['DOCUMENT_ROOT']."/webServices/facebook_images/icons/doctor1_".$rand.".png";
        } else if ($num_template == 2) {
            $name_doctor = $_SERVER['DOCUMENT_ROOT']."/webServices/facebook_images/icons/doctor2_".$rand.".png";
        } else if ($num_template == 3) {
            $name_doctor = $_SERVER['DOCUMENT_ROOT']."/webServices/facebook_images/icons/doctor3_".$rand.".png";
        } else {
            $name_doctor = 'none';
        }

        return $name_doctor;
    };
    //end function random doctor

    //begin function for to switch templates
    function number_template() {
        $num = intval(file_get_contents($_SERVER['DOCUMENT_ROOT']."/webServices/number_template.txt"));
        if ($num < 4) {
            $num++;
        } else {
            $num = 1;
        }
        $myfile = fopen($_SERVER['DOCUMENT_ROOT']."/webServices/number_template.txt", "w");

        $txt = strval($num);
        fwrite($myfile, $txt);
        fclose($myfile);

        return $num;
    };
    //end function for to switch templates

    
    
    foreach($orders as $order) {  
        
        $rowCount++;
        $GSID = @$order['ID'];
        $existingPostId = isset($recordedGSIDs[$GSID]) ? $recordedGSIDs[$GSID] : '';
        if (empty($GSID)) {
            continue;
        }
        
        if (isset($recordedGSIDs[$GSID])) {
            unset($recordedGSIDs[$GSID]);
        }
        if (isset($newGSIDs[$GSID])) {
            logit("!!!! [ ERROR ] Duplicate Google ID [$GSID] found in Spreadsheet ROW-$rowCount. Not posting!\r\n");
            if (!isset($duplicateGSIDs[$GSID])) {
                $duplicateGSIDs[$GSID] = array();
            }
            $duplicateGSIDs[$GSID][] = $rowCount;            
            continue;
        }

        $gsEntryDatetime = @$order['Timestamp'];
        logit("[$GSID] GS timestamp [$gsEntryDatetime] \r\n");
        $gsSheetDate = $gsEntryDatetime;
        if (empty($gsEntryDatetime)) {
            $gsSheetDate = date("Y-m-d H:i:s");
        }        
        $gsSheetUpdateTimestamp = strtotime($gsSheetDate);
        logit((date("Y-m-d H:i:s", $last_dataentry) . ' >= ' . date("Y-m-d H:i:s", $gsSheetUpdateTimestamp) ."\r\n"));
		
		$isPostNotUpdated = !empty($gsEntryDatetime) && $last_dataentry >= $gsSheetUpdateTimestamp;
        if ($isPostNotUpdated) {
            if (!empty($existingPostId)) {
				logit("Skipping as no update found ... $GSID - \r\n");
                $newGSIDs[$GSID] = $existingPostId;
				continue;
            }
            else {
                logit("Google POST [$GSID] not found in WP Data. Will be added as new JOB.\r\n");
            }           
        }
        
        $lastest_dataentry = max($lastest_dataentry, $gsSheetUpdateTimestamp);
               
        $post_info = processOpenOrderItem($order, $map, $companies, $locations);
		
		if (isset($post_info['error'])) {
			logit("ERROR: Skipping record from sync for Order: $GSID. Error Description: ". $post_info['error']." ...  - \r\n");
			if (isset($post_info['error_code']) && $post_info['error_code'] == 'UNDEFINED_LOCATION') {
				if (get_option('locationNotFoundGSIDs_job_alert_sent') != strtotime(date('y-m-d'))) {
					$locationNotFoundGSIDs[$post_info['location_id']] = $GSID;
					// $locationNotIDs[] = $post_info['location_id'];
				} else {
					logit("NOTICE: Skipping email alert for $GSID because it is already sent today ".date('y-m-d', get_option('locationNotFoundGSIDs_job_alert_sent')).". Error Description: ". $post_info['error']." ...  - \r\n");
				}
				// }
			}
			continue;
		}

        $orderCount++;
        logit("[$orderCount]----------------------------------------</br>\r\n");
        
        $options = $post_info['options'];
        $publish_ad = $options['publish_ad'];
        $force_publish_ad = $options['force_publish_ad'];

        $ID = $existingPostId;//getPostByGSID($GSID);
        $neverPosted = empty($ID); 

        logit("Existing PostID for Google Entry [$GSID] is [$ID] \r\n");
        
        if (!empty($ID)) {
            $post = get_post($ID);
            $eID = $post->ID;
            if (empty($eID)) {
                logit("Post $ID not found! \r\n");
                $ID = $eID;
            }
        } 

        $newID = updateJob($post_info, $ID);
        
        $newGSIDs[$GSID] = $newID;

        if ($force_publish_ad || $publish_ad) {
            
            build_ad($post_info, $newID, $force_publish_ad);
        }

        logit("----------------------$GSID = $newID ---------------------------</br>\r\n");
        if ($orderCount >= 75) {
            break;
        }        
    }
    
    logit("TRASH IDS:");
    logit(var_export($recordedGSIDs, true));
    trashPosts($recordedGSIDs);
    logit("\r\n############################\r\n");
    
    setLastDataEntryTimeStamp($lastest_dataentry);
    setLastUpdatedTimeStamp($nowTimestamp);
    logit("ALl recorded IDS: ");
    logit(var_export($newGSIDs, true));
    logit("\r\n");
    setRecordedGSIDs($newGSIDs);
    
    if (!empty($duplicateGSIDs)) {
        mailDuplicateGSIDWarning($duplicateGSIDs);
    }
    if (!empty($locationNotFoundGSIDs)) {
		sendUndefinedLocationAlertAdmin($locationNotFoundGSIDs);
    }
}

function updateJob($data, $ID) {
    extract($data);//meta, tax, actions, post_param, options, , 
    $status = $options['publish_job'] == 1 ? "publish" : "draft";
    $post_data = array(
        "post_status" => $status, //"publish", //draft
    );					
    $post_data = array_merge($post_data, $post_param);
    
    if (!empty($ID)) {
        $post = get_post($ID);
        $eID = $post->ID;
        if (empty($eID)) {
            logit("Post $ID not found! \r\n");    
            $ID = $eID;
        }
    }
    
    if (empty($ID)) {
        logit("Creating new post --- ");    
        $nID = wp_insert_post( $post_data );
        logit("Created new post $nID\r\n");    
    }
    else {
        logit("Updating Post ID: [$ID] -- ");    
        $post_data['ID'] = $ID;
        $nID = wp_update_post( $post_data );
        logit("Updated Post ID: [$nID]\r\n");    
    }    
    
    update_post_metas($nID, $meta);
    update_post_terms($nID, $tax);
    
    $data['post_id'] = $nID;    
    runActions($actions, $data);
    
    return $nID;
    
}

function runActions($actions, $data) {       
    foreach($actions as $actionKey => $actionItem) {
        $action = $actionItem['action'];
        if (function_exists($action)) {
            $actionItem['post_id'] = $data['post_id'];
            $value = $action($actionItem);
       }     
    }    
}


function build_ad($data, $ID, $isForce = false) {
    $isAdBuilt = get_post_meta($ID, 'ad_builder', true);
    $isLinkedinBuilt = get_post_meta($ID, 'ad_builder_linkedin', true);


    $list = false;
    $global_limit = get_option('_posting_limit_');
    $today_count = get_option('__ad_posting_count');

    if($global_limit > 0){

		//$date_posting = get_option('__ad_posting_date', date('Y-m-d', strtotime("1 day ago")));
		//$date1 = date_create($date_posting); // format of yyyy-mm-dd
	    //$date2 = date_create(date('Y-m-d')); // format of yyyy-mm-dd
	    //$dateDiff = date_diff($date2, $date1);
        
        $date_posting = get_option('__ad_posting_date');
        $date_posting = new DateTime($date_posting);
        $date_posting = $date_posting->Format("m / j / Y");
        $date_now = date("m / j / Y"); 
        if($date_posting == $date_now) {
            if($today_count < $global_limit) {
               $list = true; 
            }
        } else {
          update_option('__ad_posting_date', date('Y-m-d')); 
          update_option('__ad_posting_count',0);
          $date_posting = get_option('__ad_posting_date');
          $date_posting = new DateTime($date_posting);
          $date_posting = $date_posting->Format("m / j / Y");  
          $list = true;
        }
    }

    if( $list ) {
	    if ($isForce || empty($isAdBuilt)) {
            $GLOBALS['num_template'] = number_template();
            $GLOBALS['doctor'] = random_doctor($GLOBALS['num_template']);  
	        require_once 'generateimage.php';
	        create_ads($ID);
	        update_post_meta($ID, 'ad_builder', 1);
	    }
	    elseif (empty($isLinkedinBuilt)) {
	        require_once 'generateimage.php';
	        create_ads($ID, true);
	    }
    	update_option('__ad_posting_count',$today_count+1);
	    //add_option('__ad_posting_date', date('Y-m-d'));
    }
}


function processOpenOrderItem($order, $mapList, $companies = array(), $locations = array()) {    
    $data = array(
        "options" => array(),
        "post_param" => array("post_type" => 'job_listing', "post_author" => 0,),
        "tax" => array(),
        "meta" => array(),
        "actions" => array(),
    );
    $mapValues = array();
    
    $company = array();
    $location = array();
    
    $company_id = @$order['Company ID'];
    $location_id = @$order['Location ID'];
    
    if (isset($companies[$company_id])) {
        $company = $companies[$company_id];
    }
    if (empty(trim($location_id))) {
    	$location_id = 'BLANK';
		$errorData = array_merge($order, array('error' => "Location is not found for Location ID: $location_id", 'location_id' => $location_id, "error_code" => "UNDEFINED_LOCATION"));
		return $errorData;
    }
    if (isset($locations[$location_id])) {
        $location = $locations[$location_id];
    }    
	else {	
		$errorData = array_merge($order, array('error' => "Location is not found for Location ID: $location_id", 'location_id' => $location_id, "error_code" => "UNDEFINED_LOCATION"));
		return $errorData;
	}
 
    //$data['rawCompany'] = $company;
    //$data['rawLocation'] = $location;
    //$data['rawOrder'] = $order;
    
    foreach($mapList as $mapKey => $map) {

        $sourceSheet = @$map['source'];
        if (empty($sourceSheet)) {
            $sourceSheet = "order";
        }
        $source = $$sourceSheet;  // $$ is a must here to work as a pointer
        $value = @$source[$mapKey]; 
        
        $type = @$map['type'];
        if (empty($type)) {
            $type = "meta";
        }
        
        $key = @$map['key'];
        $tax = @$map['tax'];
        $post_param = @$map['post_param'];
        $option = @$map['option'];
        $action = @$map['action'];
            
        $sanitizer = @$map['sanitizer'];
        $sanitizers = explode(',', $sanitizer); 
        $value = applySanitizers($value, $sanitizers);
        
        $mapValues[$mapKey] = $value;
        
        if (!empty($option)) {                        
            $data['options'][$option] = $value;
        }
        
        if (!empty($post_param)) {                        
            $data['post_param'][$post_param] = $value;
        }
        
        if (!empty($key)) {  
	        $add_meta = @$map['meta_value_sanitize'];
	        if($add_meta){
	        	$data['meta'][$key] = applySanitizers(@$source[$mapKey], explode(',', $add_meta));
	        }else {
	        	$data['meta'][$key] = $value;
	        }                   
        }         
        
        if (!empty($tax)) {     
            $actionTerm = @$map['actionTerm'];

            if (!$actionTerm) {
                $booleanTerm = @$map['booleanTerm'];
                $term = @$map['term'];
                if (!isset($data['tax'][$tax])) {
                    $data['tax'][$tax] = array();
                }

                if (!empty($value)) {
                    if (is_array($value)) {
                        $data['tax'][$tax] = array_merge($data['tax'][$tax], $value);
                    }
                    else {
                        $data['tax'][$tax][] = $value;
                    }
                }                            
            }
        }        
        if (!empty($action)) {
            $data['actions'][] = array(
                "action" => $action,
                "map" => $map,
                "value" => $value,
                "values" => $mapValues,
                "meta" => $key,
                "post_param" => $post_param,
                "option" => $option,
                "tax" => $tax,
                "field" => $mapKey,
            );
        }           

    }
       
    foreach($data['actions'] as $actionKey => $actionItem) {
        $action = $actionItem['action'];
        if (function_exists($action)) {
            $map = $actionItem['map'];
            $value = $action($actionItem, false);
            $sanitizer = @$map['postactionsanitizer'];
            $sanitizers = explode(',', $sanitizer); 
            $value = applySanitizers($value, $sanitizers);            
            
            $data['actions'][$actionKey]['value'] = $value;
            
            $tax = $actionItem['tax'];
            $booleanTerm = @$map['booleanTerm'];
            $term = @$map['term'];
            
            if (!empty($tax)) {     
                if (!isset($data['tax'][$tax] )) {
                    $data['tax'][$tax] = array();
                }

                if (!empty($value)) {
                    if (is_array($value)) {
                        $data['tax'][$tax] = array_merge($data['tax'][$tax], $value);
                    }
                    else {
                        $data['tax'][$tax][] = $value;
                    }
                }              
            } 
            
            $key = $actionItem['meta'];
            if (!empty($key)) {                        
                $data['meta'][$key] = $value;
            }            
            $post_param = $actionItem['post_param'];            
            if (!empty($post_param)) {                        
                $data['post_param'][$post_param] = $value;
            }              
            $option = $actionItem['option'];            
            if (!empty($option)) {                        
                $data['options'][$option] = $value;
            }              
            $metaOverwrite = $actionItem['metaoverwrites'];
            if (!empty($metaOverwrite)) {
                foreach($metaOverwrite as $metakey => $metavalue) {
                    $data['meta'][$metakey] = $metavalue;
                }
                
            }             
            
        }
    }   
    return $data;
    
}


/* sanitizers */
function currency_clean($val = "") {
	$val = str_replace("$","",trim($val));
	$val = str_replace(",","",$val);
	$val = trim($val);
    return $val;
}
function splitcsv($value) {    
    $values =  preg_split('/\s{0,},\s{0,}/', trim($value));
    return $values;
}
function randomCSV($value) {    
    $values =  splitcsv($value);    
    $pos = mt_rand(0, count($values) -1);
    $finalValue = $values[$pos];
    return $finalValue;
}
function setbool($value) {
    $value = strtolower(trim($value));
    $ret = 1;
    if ($value == 'no'  || 
        $value == 'n'   ||
        $value === "0"  ||
        is_nan(floatval($value))  ||
        is_null($value) ||
        empty($value) ||
        !isset($value) ||
        $value === FALSE        
    ) {
        $ret = 0;
    }
    
    return $ret;
}

function formatDate_Ymd($val) {
	
	if ($val !== '') {
		if (strtolower($val) != 'asap') {
			$date = strtotime($val);
			if ($date !== FALSE) {
				$val = date("Y-m-d", $date);
			}
		}
	}	
	return $val;	
}

function shift_map($vals) {
    global $shiftTermsByName;
    $map = array(
        "d-10" => "Days (10 Hours)",
        "d-12" => "Days (12 Hours)",
        "d" => "Days (8 Hours)", 
        "d-8" => "Days (8 Hours)", 
        "n-10" => "Night (10 Hours)",
        "n-12" => "Night (12 Hours)",
        "n" => "Night (8 Hours)", 
        "n-8" => "Night (8 Hours)", 
        "s-10" => "Swing (10 Hours)",
        "s-12" => "Swing (12 Hours)",
        "s" => "Swing (8 Hours)",
        "s-8" => "Swing (8 Hours)",
        "r-10" => "Rotating (10 Hours)",
        "r-12" => "Rotating (12 Hours)",
        "r" => "Rotating (8 Hours)",
        "r-8" => "Rotating (8 Hours)",
    );
    
    $ids = array();
    foreach($vals as $val) {
        if (isset($map[strtolower($val)])) {
            $namelcase = strtolower($map[strtolower($val)]);
            $id = @$shiftTermsByName[$namelcase];
            if (!empty($id)) {
                $ids[] = $id;
            }
        }        
    }
    return $ids;
}

function amenities_travel_map($val) {
    $termName = "Travel";
    return amenities_map($val, $termName);
}
function amenities_mm_map($val) {
    $termName = "Meals &amp; Incidentals";
    return amenities_map($val, $termName);
}
function amenities_housing_map($val) {
    $termName = "Housing Stipend";
    return amenities_map($val, $termName);
}
function amenities_map($val, $termName) {
    global $amenitiesTermsByName;
    $id = "";
    if ($val != 0) {
        $namelcase = strtolower($termName);
        $id = @$amenitiesTermsByName[$namelcase];
    }
    return $id;    
}

function specialty_map($vals) {
    global $specialtyTermsByName;
    $taxonomy = "job_listing_category";
    $ids = array();
    foreach($vals as $val) {
        if (!empty($val)) {            
            $val = htmlentities(trim($val));
            $lcaseval = strtolower($val);
            $id = $specialtyTermsByName[$lcaseval];
            if (empty($id)) {
                try {
                    $term  = wp_insert_term($val, $taxonomy);
                    $id = $term['term_id'];                
                    $specialtyTermsByName[$lcaseval] = $id;                    
                } catch (Exception $ex) {

                }
            }
            if (!empty($id)) {
                $ids[] = $id;
            }            
        }        
    }
    return $ids;
}

function appendUploadUrl($val) {
    
    if (!empty($val) && stripos($val, "://") === FALSE) {
        $upload_dir = wp_upload_dir(null, false);
        $val = $upload_dir['baseurl'].'/'.$val;
    }
    return $val;
}
function appendUploadPath($val) {
    if (!empty($val) && stripos($val, '/') !== 0 && stripos($val, "://") === FALSE) {
        $upload_dir = wp_upload_dir(null, false);
        $val = $upload_dir['basedir'].'/'.$val;
    }
    return $val;
}

/* actions */
function joinFields($data, $afterPostCreation = true) {
    $finalValue = "";
    if ($afterPostCreation === FALSE) {
        extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $fields = @$map['fields'];
        $delim = @$map['param'];
        $fieldKeys = explode(',', $fields);
        $valueArray = array();
        foreach($fieldKeys as $key) {
            $value = trim(@$values[$key]);
            if (!empty($value)) {
                $valueArray[] = $value;
            }            
        }
        $finalValue = implode($delim, $valueArray);
        return $finalValue;       
    }
    else {
        return $data['value'];
    }    
}


function setASAPDeadline(&$data, $afterPostCreation = true) {
    $value = 0;
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        $deadline = $values['Orientation'];
        if (strtolower(trim($deadline)) == 'asap') {
            $value = 1;            
            $newOrienation = date("Y-m-d", strtotime("+1 day"));
            $data['values']['Orientation'] = $newOrienation;
            if (!isset($data['metaoverwrites'])) {
                $data['metaoverwrites'] = array();
            }
            $data['metaoverwrites']['_application_deadline'] = $newOrienation;
			$data['metaoverwrites']['job_deadline'] = $newOrienation;
			$data['metaoverwrites']['job_asap'] = "ASAP";
        }
        
    }
    return $value;
}
function setLocationImageUrl($data, $afterPostCreation = true) {
    $value = @$data['value'];

    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        $location_image_path = $values['Image(s)'];
        $value = $location_image_path;

        $recordedGSIDs = getRecordedGSIDs();
        $update_post = $recordedGSIDs[$values['ID']];
        if($update_post && empty($value))
        {
        	$existing_loc_img = get_post_meta($update_post, 'location_image_url', true);
        	$value = str_ireplace(wp_upload_dir()['baseurl'].'/', '', $existing_loc_img);
        }
        

        if (empty($value)) {
        	$current_count = 0;
			$pic_count = get_option('__default_location_pic', $current_count);
			$pic_url= '2016/06/default-location-pic.jpg';
			switch($pic_count)
			{
				case 1:
					if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
						$pic_url= '2017/06/nurse1.jpg';
					else 
						$pic_url= '2017/05/nurse1.jpg';
					$current_count = 2;
					break; 
				case 2:
					if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
						$pic_url= '2017/06/nurse2.jpg';
					else 
						$pic_url= '2017/05/nurse2.jpg';
					$current_count = 3;
					break; 
				case 3:
					if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
						$pic_url= '2017/06/nurse3.jpg';
					else 
						$pic_url= '2017/05/nurse3.jpg';
					$current_count = 4;
					break; 
				case 4:
					if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
						$pic_url= '2017/06/nurse4jpg';
					else 
						$pic_url= '2017/05/nurse4.jpg';
					$current_count = 0;
					break; 
				default:
					if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
						$pic_url= '2016/06/default-location-pic.jpg';
					else 
						$pic_url= '2016/05/default-location-pic.jpg';
					$current_count = 1;

			}

			if(!file_exists(wp_upload_dir()['basedir'].'/'. $pic_url))
			{
				if (function_exists('is_wpe_snapshot') && is_wpe_snapshot()) 
					$pic_url= '2016/06/default-location-pic.jpg';
				else 
					$pic_url= '2016/05/default-location-pic.jpg';
			}

			$value = $pic_url;
			$value = appendUploadUrl($value);
			update_option('__default_location_pic', $current_count);
        } else {
        	$value = appendUploadUrl($value);
        }
    }
    return $value;
}
function setJobImage($data, $afterPostCreation = true) {
    $value = @$data['value'];
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        $location_image_url = appendUploadUrl($values['Image(s)']);
        $logoUrl = $values['Logo URL'];
        $value = $location_image_url;        
        if (empty($value)) {
            $value = $logoUrl;
        }        
    }
    return $value;
}
function setCompanyLogo($data, $afterPostCreation = true) {
    $value = @$data['value'];
    if ($afterPostCreation === FALSE) {
        $values = $data['values'];
        if (empty($value)) {
            if(!empty($values['Image(s)'])) {
                $value = appendUploadUrl($values['Image(s)']);
            }
        }        
    }
    return $value;
}

function detectRegions($data, $afterPostCreation = true) {
    global $stateRegionMap;
    $ret = null;
    if ($afterPostCreation === FALSE) {    
        extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $fields = @$map['fields'];
        $state = @$values[$fields];
        
        if (!empty($state)) {
            $state = strtolower(trim($state));
            $ret = $stateRegionMap[$state];
        }
        
        return $ret;
    }
    else {
        return $data['value'];
    }    
}

function detectCompactState($data, $afterPostCreation = true) {    
    global $compactStatesTermId;
    $ret = 0;
    if ($afterPostCreation === FALSE) {    
        extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $termIds = detectRegions($data, false);
        if (!empty($termIds) && !empty($compactStatesTermId)) {
            if (in_array($compactStatesTermId, $termIds)) {
                $ret = 1;
            }            
        }
        return $ret;
    }
    else {
        return $data['value'];
    }    
}


/* Helper functions */
function applySanitizers($val, $sanitizers = array()) {
    foreach ($sanitizers as $sanitizer) {
        if (function_exists($sanitizer)) {
            $val = $sanitizer($val);
        }
    }
    return $val;
}
function getCompactStateParentId($raw) {
    $ret = null;
    foreach($raw as $term) {
        $slug = $term->slug;
        $id = $term->term_id;
        if ($slug == 'multi-state-license') {
            $ret = $id;
            break;
        }
    }    
    return $ret;    
}
/**
 * 
 * @param Array $raw Array of WP_Term object
 * @param Boolean $useStateName true/false (default false) - to set state names (lowercase) as the key of the resultant array
 * @return Array Associative array with lowercase state abbreviation (stored in description field of the term) as the key 
 *                      or State Names in lowercase if $useStateName is set to true.
 *                      Term id is set as value of the array element.
 *      
 */
function orderRegionTermsByState($raw, $useStateName = false) {
    $ret = array();
    $parents = array();
    foreach($raw as $term) {
        $id = $term->term_id;
        $name = $term->name;
        $description = strtolower($term->description);
        $lcasename = strtolower($name);
        $slug = $term->slug;
        $parentId = $term->parent;
        if ($parentId == 0) {
            $parents[$parentId] = array("name" => $name, "id" => $id, "slug" => $slug, "parent" => $parentId);
        }
        else {
            if (!isset($ret[$description])) {
                $ret[$description] = array();                
            }
            $ret[$description][] = $id;
            if (!in_array($parentId, $ret[$description])) {
                $ret[$description][] = $parentId;
            }
        }        
    }    
    return $ret;
    
}
function getTaxonomyTerms($tax, $fields = "") {
    $args = array(
        'hide_empty' => false,
        'hierarchical' => true,
        'orderby' => 'term_group',
    );
    
    if (!empty($fields)) {
        $args['fields'] = $fields;
    }
    
    $terms = get_terms($tax, $args);
    return $terms;
}

/**
 * 
 * @param type $terms   => array of WP_Term objects
 * @param type $byWhat  => string. value can be 'name', 'id', 'slug',  'name-lowercase' (default)
 * @param type $valueAs => string. Value can be:
 *                                   'all' (resulting in an associative array),
 *                                   'id' (resulting in an associative array),
 *                                   'name' (resulting in an associative array),
 *                                   'name-lowercase',
 *                                   'slug',
 *                                   'description',
 *                                   'parent' (for parent id),
 * 
 * @return type
 */
function listTermsBy($terms, $byWhat = 'name-lowercase', $valueAs = 'id') {
    $ret = array();   
    foreach($terms as $term) {
        $values = array();
        $values['id'] = $term->term_id;
        $name = $values['name'] = $term->name;
        $values['name-lowercase'] = strtolower($name);
        $values['slug'] = $term->slug;
        $values['parent'] = $term->parent;
        $values['description'] = $term->description;
        
        $byWhat = strtolower($byWhat);
        if (!isset($values[$byWhat])) {
            $byWhat = 'name-lowercase';
        }
        $valueAs = strtolower($valueAs);
        if ($valueAs != 'all' && !isset($values[$valueAs])) {
            $valueAs = 'id';
        }
        if ($valueAs == 'all') {
            $ret[$values[$byWhat]] = $values;
        }
        else {
            $ret[$values[$byWhat]] = $values[$valueAs];
        }        
    }    
    return $ret;    
}
    
// Function to convert CSV into associative array
function csvToArrayFilter($file, $delimiter = ',', $matchField = "", $matchValue = "") { 
    $keys = array();
    $data = array();
    $matchIndex = FALSE;
    if (($handle = fopen($file, 'r')) !== FALSE) { 
        $i = 0;
        while (($lineArray = fgetcsv($handle, 0, $delimiter, '"')) !== FALSE) { 
            if ($i == 0) {
                $keys = $lineArray;
                if (!empty($matchField)) {
                    $matchIndex = array_search($matchField, $keys);
                }
                
            }
            else {
                if (!empty($matchField)) {
                    if ($matchIndex !== FALSE) {
                        if ($lineArray[$matchIndex] === $matchValue) {
                            $data[] = array_combine($keys, $lineArray);
                        }
                    }                    
                }
                else {
                    $data[] = array_combine($keys, $lineArray);
                }
                
            }
            $i++; 
        }    
        fclose($handle);     
    } 

  return $data; 
} 
function csvToArray($file, $delimiter = ',', $keyField = "") { 
    $keys = array();
    $data = array();
    $matchIndex = FALSE;
	$handle = fopen($file, 'r');
    if ($handle !== FALSE) { 
        $i = 0;
        while (($lineArray = fgetcsv($handle, 0, $delimiter, '"')) !== FALSE) { 
            if ($i == 0) {
                $keys = $lineArray;
                if (!empty($keyField)) {
                    $matchIndex = array_search($keyField, $keys);
                }			
            }
            else {
                if ($matchIndex !== FALSE) {
                    $data["".$lineArray[$matchIndex]] = array_combine($keys, $lineArray);
                }                    
                else {
                    $data[] = array_combine($keys, $lineArray);
                }
                
            }
            $i++; 
        }    
        fclose($handle);     
    } 
	else {
		return false;
	}

  return $data; 
} 

function getPostByGSID($GSID, $returnPostID = true) {
    $ID = null;
    $job = new WP_Query( array( 'post_type' => 'job_listing', 
                        'meta_key' => 'googlesheet_openorders_id', 
                        'meta_value' => $GSID) 
        );
    $posts = $job->posts;
    $post = null;
    if (!empty($posts)) {
        $post = $posts[0];
        $ID = $post->ID;        
    }
    logit("Detected POST ID: $ID (num posts: {count($posts)})... = " .count($posts));
    if ($returnPostID) {
        return $ID;
    }
    return $post;    
}

function setFeaturedImage($data, $afterPostCreation = true) {
    if ($afterPostCreation) {    
        //extract($data); //action, map, value, values, meta, post_param, option, tax, field
        $value = @$data['value'];
        $ID = @$data['post_id'];
        
        if (!empty($ID) && !empty($value)) {
            setPostLocationImage($ID, $value);
            setPostFeaturedImage($ID, $value);
        }
        return $value;
    }
    else {
        return $data['value'];
    }
    
}

function setPostLocationImage($post_id, $image) {
    $upload_dir = wp_upload_dir();
    $file = $upload_dir['basedir'] . '/' . $image;
    $url = $upload_dir['baseurl'] . '/' . $image;
    update_post_meta($post_id, 'location_image_path', $file);
    update_post_meta($post_id, 'location_image_url', $url);
}

function setPostFeaturedImage($post_id, $image) {

    $prevAttachmentId = get_post_meta($post_id, 'featured_image_id', true);
    $prevAttachmentName = get_post_meta($post_id, 'featured_image_name', true);
    
    if ($image == $prevAttachmentName) {
        return $prevAttachmentId;
    }
    
    wp_delete_attachment($prevAttachmentId);
    if ($prevAttachmentId && $image == "") {
        update_post_meta($post_id, 'featured_image_id', null);
        update_post_meta($post_id, 'featured_image_name', null);
        return null;
    }
    
    $upload_dir = wp_upload_dir();

    $filename = basename($image);
    if ($filename == "no-image-white-original.png") { 
      return null;//ignore white
    }
//    if(wp_mkdir_p($upload_dir['path']))
//        $file = $upload_dir['path'] . '/' . $filename;
//    else
    $file = $upload_dir['basedir'] . '/' . $image;

    $image_data = file_get_contents($file);
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit',
        'guid' => $image
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    update_post_meta($post_id, 'featured_image_id', $attach_id);
    update_post_meta($post_id, 'featured_image_name', $file);
    
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );    
	
	wp_update_attachment_metadata( $attach_id, $attach_data );
	
	
    return $attach_id; 
  }  

function logit($obj, $isEcho = true, $file = "") {
    if (empty($file)) {
        $file = "job.update.".date("Ymd").".log";
    }
    $path = $file;
    $date = "[" . date("Y-m-d H:i:s"). "] ";
    if (is_array($obj) || is_object($obj)) {
        $log = json_encode($obj);
    }
    else {
        $log = $obj;
    }
    file_put_contents($path, $date.$log, FILE_APPEND);
    if ($isEcho) {
        echo $log;
    }    
} 

function mailDuplicateGSIDWarning($GSIDs) {
    $isStaging  = function_exists('is_wpe_snapshot') && is_wpe_snapshot();
    $site = get_my_option("blogname");
    if ($isStaging) {
        $to   = "olga@element5digital.com,ahsanr@addiedigital.com";//get_my_option("admin_email");
    }
    else {
        $to   = "recruiting@blueforcestaffing.com,scassady@huffmaster.com";//get_my_option("admin_email");
    }
    
    $from = "social@blueforcestaffing.com";//get_my_option("admin_email");
    $subject = ($isStaging? "[Staging] ":""). "Problem with the Open Orders Spreadsheet";
    $headers = array();
    $headers[] = "From: ". $from;
    if (!$isStaging) {
        $headers[] = 'Cc: support@element5digital.com';
    }
    
    $count = count($GSIDs);
    if ($count <= 0) {
        return;
    }
    if ($count > 1) {
        $pref1Tmpl = "some";
        $sufx1Tmpl = "s";
        $numTempl = "s are";
        $pref2Tmpl = "each";
                
    }
    else {
        $pref1Tmpl = "a";
        $sufx1Tmpl = "";
        $numTempl = " is";
        $pref2Tmpl = "the";        
    }
    
    
    $IdList = "";
    foreach($GSIDs as $ID => $rows) {
        $rowsCount = count($rows);
        if ($rowsCount > 0) {
            $IdList .= "\r\n\t{$ID} in row" . ($rowsCount > 1 ? 's ' : ' '). implode(", ", $rows);
        }        
    }
    
    
    $message =  "Attention BlueForceStaffing.com team,

Someone has entered $pref1Tmpl duplicate unique identifier number{$sufx1Tmpl} in column A of your Open Orders Google Sheet. This will cause problems with the job listing. Please correct the problem by assigning a unique ID number to {$pref2Tmpl} job listing.

The duplicated ID number{$numTempl}: 
$IdList


Thank you for your attention in this matter.

Sincerely,

The BlueForceStaffing.com WordPress Script";    
    wp_mail($to, $subject, $message, $headers);     
}


function send_mail_for_new_listings($new_listings){
    $site = get_my_option("blogname");
    $to   = get_option("_location_alert_emails_", get_my_option("admin_email"));
    $from = get_my_option("admin_email");
    // $to   = "recruiting@blueforcestaffing.com,scassady@huffmaster.com";//get_my_option("admin_email");
    // $from = "social@blueforcestaffing.com";//get_my_option("admin_email");
    $subject = "New Listings added in Location Sheet";  
    $headers = array();
    $headers[] = "From: ". $from;
    $headers[] = 'Bcc: montypaliwal.biz@gmail.com';
    
    $count = count($new_listings);
    if ($count <= 0) {
        return;
    }
    if ($count > 1) {
        $pref1Tmpl = "some";
        $sufx1Tmpl = "s";
        $numTempl = "s are";
        $pref2Tmpl = "each";
                
    }
    else {
        $pref1Tmpl = "a";
        $sufx1Tmpl = "";
        $numTempl = " is";
        $pref2Tmpl = "the";        
    }
    
    
    $IdList = "";
    foreach($new_listings as $ID => $rows) {
        $rowsCount = count($rows);
        if ($rowsCount > 0) {
            $IdList .= "\r\n\t" . ($rowsCount > 1 ? 's ' : ' '). implode(", ", $rows);
        }        
    }
    
    
    $message =  "Attention BlueForceStaffing.com team,

Someone has entered $pref1Tmpl new Location{$sufx1Tmpl} in your Location Google Sheet.

The new locations ID number{$numTempl}: 
$IdList


Thank you for your attention in this matter.

Sincerely,

The BlueForceStaffing.com WordPress Script";    
    wp_mail($to, $subject, $message, $headers);     
}

function sendUndefinedLocationAlertAdmin($locationNotFoundGSIDs) {
	$locationsUniqueIds = '';
	foreach ($locationNotFoundGSIDs as $locationID => $uniqueID) {
		$locationsUniqueIds .= 'Location is not found for Location ID: '.$locationID.' and the unique ID number is '.$uniqueID.'.<br>';
	}
	
	update_option('locationNotFoundGSIDs_job_alert_sent', strtotime(date('y-m-d')));
	// echo get_option($locationNotFoundGSIDs.'_job_alert_sent');
    
    $isStaging  = function_exists('is_wpe_snapshot') && is_wpe_snapshot();
    $site = get_my_option("blogname");
    if ($isStaging) {
        $to   = "gabe@element5digital.com,ahsanr@addiedigital.com";//get_my_option("admin_email");
    }
    else {
        $to   = "recruiting@blueforcestaffing.com,scassady@huffmaster.com";//get_my_option("admin_email");
    }
    
    $from = "social@blueforcestaffing.com";//get_my_option("admin_email");
    $to   = 'gabe@element5digital.com,ahsanr@addiedigital.com';
    $from = get_my_option("admin_email");

    $to = 'olga@element5digital.com,ahsanr@addiedigital.com';
    // $from = 'olga@element5digital.com';
    $subject = ($isStaging? "[Staging] ":""). "Problem with the Open Orders Spreadsheet";
    $headers = array();
    $headers[] = "From: ". $from;
    $headers[] = "Content-Type: text/html; charset=UTF-8";
    if (!$isStaging) {
        // $headers[] = 'Cc: support@element5digital.com';
    }
    
    $message =  "Attention BlueForceStaffing.com team,
<br>
<br>
$locationsUniqueIds
<br>
Thank you for your attention in this matter.
<br>
<br>
Sincerely,
<br>
<br>
The BlueForceStaffing.com WordPress Script";    
    wp_mail($to, $subject, $message, $headers);     
}