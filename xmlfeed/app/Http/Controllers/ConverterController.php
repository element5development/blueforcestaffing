<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sheets;
use Google;
use Spatie\ArrayToXml\ArrayToXml;

class ConverterController extends Controller
{

    private function spread_sheet_to_xml($spreadSheetId, $sheet) {

        Sheets::setService(Google::make('sheets'));
        Sheets::spreadsheet($spreadSheetId);

        $values = Sheets::sheet($sheet)->all();
        return $values;
    
    }

    public function sstoxml() {
        $rows = $this->spread_sheet_to_xml("1n6edIHljMtHoiAda8cWqTlRQ19PLmRXbisJuAdGQShM", "Open Orders");
        $content = [];

        $headers = [];
        foreach($rows[0] as $k => $header){
            $headers[strtolower($header)] = $k;
        }

        foreach($rows as $i => $row) {
            if($i == 0) {
                continue;
            }
            if(empty($row) || count($row) < 29) {
                break;
            }

            $adTitle = (string)$row[$headers["job title"]] . ' - ' . $row[$headers["city"]] . ', ' . $row[$headers["state"]] . ' - Earn $' . number_format((float)str_replace(',', '', str_replace('$', '', $row[$headers["weekly gross"]])) * (int)str_replace('$', '', $row[$headers["contract length"]])) . ' For ' . $row[$headers["contract length"]] . ' weeks of work';
            $earn = '$' . number_format((float)str_replace(',', '', str_replace('$', '', $row[$headers["weekly gross"]])) * (int)str_replace('$', '', $row[$headers["contract length"]]));
            $shift = str_replace('R', 'Rotating', str_replace('S', 'Swing', str_replace('D', 'Day', str_replace('N', 'Night', $row[$headers["shift"]]))));
            $totalCompensation = '$' . number_format((float)str_replace(',', '', str_replace('$', '', $row[$headers["hourly"]])) + (float)str_replace(',', '', str_replace('$', '', $row[$headers["m&i"]])) + (float)str_replace(',', '', str_replace('$', '', $row[$headers["housing stipend"]])));
            $jobFullListing = $row[$headers["specialty"]] . " - Earn " . $earn . " For " . $row[$headers["weekly gross"]] . " Weeks of work\n"
                . "ID#" . $row[$headers["id"]] . "\n"
                . "Speciality: " . $row[$headers["specialty"]] . "\n"
                . "Location: " . $row[$headers["city"]] . ", " . $row[$headers["state"]] . "\n"
                . "\n"
                . ". Earn " . $row[$headers["weekly gross"]] . " / Week for each full week worked\n"
                . "(weekly gross total has all stipends factored in already - M&I and Housing)\n"
                . "\n"
                . "Pay Breakdown:\n"
                . ". " . $row[$headers["hourly"]] . " / Hr.\n"
                . ". " . $row[$headers["m&i"]] . " weekly tax free M&I Stipend\n"
                . ". " . $row[$headers["housing stipend"]] . " weekly tax free Housing Stipend\n"
                . "\n"
                . "Shifts: " . $shift . " hour shift - " . $row[$headers["hourly guarantee"]] . " Hour Week.\n"
                . "Duration: " . $row[$headers["contract length"]] . " Weeks\n"
                . "Start Date: " . $row[$headers["orientation"]] . " (future orientation dates available as well)\n"
                . "\n"
                . "Description: " . $row[$headers["job description"]] . "\n"
                . "\n"
                . "Requirements: " . $row[$headers["requirements"]] . "\n"
                . "\n"
                . $row[$headers["job ad closing"]];

            $content["Row-" . $i] = [
                "Ad Title" => $this->stripInvalidXml($adTitle),
                "Earn" => $this->stripInvalidXml($earn),
                "Job ID" => $this->stripInvalidXml($row[$headers["id"]]),
                "Job Title" => $this->stripInvalidXml($row[$headers["job title"]]),
                "Speciality" => $this->stripInvalidXml($row[$headers["specialty"]]),
                "City" => $this->stripInvalidXml($row[$headers["city"]]),
                "State" => $this->stripInvalidXml($row[$headers["state"]]),
                "Orientation" => $this->stripInvalidXml($row[$headers["orientation"]]),
                "Shift" => $this->stripInvalidXml($shift),
                "Guarantee Hours" => $this->stripInvalidXml($row[$headers["hourly guarantee"]]),
                "Contract Length" => $this->stripInvalidXml($row[$headers["contract length"]]),
                "Hourly Max" => $this->stripInvalidXml($row[$headers["hourly max"]]),
                "Weekly Gross" => $this->stripInvalidXml($row[$headers["weekly gross"]]),
                "Travel Stipend" => $this->stripInvalidXml($row[$headers["travel stipend"]]),
                "Hourly" => $this->stripInvalidXml($row[$headers["hourly"]]),
                "M and I" => $this->stripInvalidXml($row[$headers["m&i"]]),
                "Housing" => $this->stripInvalidXml($row[$headers["housing stipend"]]),
                "Total Compensation" => $this->stripInvalidXml($totalCompensation),
                "Compact State" => $this->stripInvalidXml($row[$headers["compact state?"]]),
                "Job Description" => $this->stripInvalidXml($row[$headers["job description"]]),
                "Requirements" => $this->stripInvalidXml($row[$headers["requirements"]]),
                "Job Closing" => $this->stripInvalidXml($row[$headers["job ad closing"]]),
                "Job Full Listing" => $this->stripInvalidXml($jobFullListing)
            ];

        }

        $result = ArrayToXml::convert($content);
        $result = preg_replace("/Row-[\d]*/", "Row", $result);

        return response($result, 200)
            ->header('Content-Type', 'text/xml');
    }

    /**
     * Removes invalid XML
     *
     * @access public
     * @param string $value
     * @return string
     */
    public function stripInvalidXml($value)
    {
        return preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]|[\x00-\x7F][\x80-\xBF]+|'
        . '([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})|'
        . '[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S', '', $value);
    }

}
