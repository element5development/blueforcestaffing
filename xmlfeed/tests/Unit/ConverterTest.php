<?php
namespace Tests\Unit;

use Tests\TestCase;

class ConverterTest extends TestCase
{
    /**
     * test it respond with xml.
     *
     * @return void
     */
    public function testReturnXml()
    {

        $this->get('/sstoxml')
                -> assertSee('<root>');
    }
}
